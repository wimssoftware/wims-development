-- Master Data Tables --
INSERT INTO `wims`.`unit_type` (`UTP_Id`, `UTP_Description`) VALUES ('1', 'Pallet');
INSERT INTO `wims`.`unit_type` (`UTP_Id`, `UTP_Description`) VALUES ('2', 'Cartoon');
INSERT INTO `wims`.`stock_keeping_unit` (`SKU_Id`, `SKU_Code`, `SKU_Description`, `SKU_LiftingNumber`, `SKU_LifeTimeYears`, `SKU_LifeTimeMonths`, `SKU_LifeTimeDays`) VALUES ('2', '2', 'gdafdas', '3', '2', '24', '730');
INSERT INTO `wims`.`stock_keeping_unit` (`SKU_Id`, `SKU_Code`, `SKU_Description`, `SKU_LiftingNumber`, `SKU_LifeTimeYears`, `SKU_LifeTimeMonths`, `SKU_LifeTimeDays`) VALUES ('3', '3', 'sadfufas', '2', '1', '12', '365');
INSERT INTO `wims`.`contact` (`CNT_Id`, `CNT_Fname`, `CNT_Lname`, `CNT_MobileNumber`, `CNT_Address`) VALUES ('1', 'Hesham', 'Usama', '010000000000', '421, street, city');
INSERT INTO `wims`.`contact` (`CNT_Id`, `CNT_Fname`, `CNT_Lname`, `CNT_MobileNumber`, `CNT_Address`) VALUES ('2', 'Hesham2', 'Usama2', '010000000002', '422, street, city');
INSERT INTO `wims`.`contact` (`CNT_Id`, `CNT_Fname`, `CNT_Lname`, `CNT_MobileNumber`, `CNT_Address`) VALUES ('3', 'Hesham3', 'Usama3', '010000000003', '423, street, city');
INSERT INTO `wims`.`contact` (`CNT_Id`, `CNT_Fname`, `CNT_Lname`, `CNT_MobileNumber`, `CNT_Address`) VALUES ('4', 'Hesham4', 'Usama4', '010000000004', '424, street, city');
INSERT INTO `wims`.`contact` (`CNT_Id`, `CNT_Fname`, `CNT_Lname`, `CNT_MobileNumber`, `CNT_Address`) VALUES ('5', 'Hesham5', 'Usama5', '010000000005', '425, street, city');
INSERT INTO `wims`.`owner` (`OWN_Id`, `OWN_CNT_Id`) VALUES ('1', '1');
INSERT INTO `wims`.`owner` (`OWN_Id`, `OWN_CNT_Id`) VALUES ('2', '2');
INSERT INTO `wims`.`supplier` (`SUP_Id`, `SUP_CNT_Id`) VALUES ('1', '3');
INSERT INTO `wims`.`supplier` (`SUP_Id`, `SUP_CNT_Id`) VALUES ('2', '4');
INSERT INTO `wims`.`supplier` (`SUP_Id`, `SUP_CNT_Id`) VALUES ('3', '5');