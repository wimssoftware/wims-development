-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2015 at 04:26 PM
-- Server version: 5.5.40
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wims`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `CNT_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CNT_Fname` varchar(32) DEFAULT NULL,
  `CNT_Lname` varchar(32) DEFAULT NULL,
  `CNT_MobileNumber` varchar(15) DEFAULT NULL,
  `CNT_Address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CNT_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`CNT_Id`, `CNT_Fname`, `CNT_Lname`, `CNT_MobileNumber`, `CNT_Address`) VALUES
(1, 'Hesham', 'Usama', '010000000000', '421, street, city'),
(2, 'Hesham2', 'Usama2', '010000000002', '422, street, city'),
(3, 'Hesham3', 'Usama3', '010000000003', '423, street, city'),
(4, 'Hesham4', 'Usama4', '010000000004', '424, street, city'),
(5, 'Hesham5', 'Usama5', '010000000005', '425, street, city'),
(6, 'Tariq', 'Shatat', '01126054008', '7 Hussein Shatat ST, '),
(7, 'Tariq', 'Shatat', '01126054008', '7 Hussein Shatat ST, ');

-- --------------------------------------------------------

--
-- Table structure for table `current_bin_item`
--

CREATE TABLE IF NOT EXISTS `current_bin_item` (
  `CBI_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CBI_STE_Id` int(11) DEFAULT NULL,
  `CBI_TOI_Serial` varchar(256) DEFAULT NULL,
  `CBI_GBN_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`CBI_Id`),
  KEY `CBI_STE_Id` (`CBI_STE_Id`),
  KEY `CBI_GBN_Id` (`CBI_GBN_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `current_bin_item`
--

INSERT INTO `current_bin_item` (`CBI_Id`, `CBI_STE_Id`, `CBI_TOI_Serial`, `CBI_GBN_Id`) VALUES
(1, 1, 'GHI678', 1),
(2, 1, 'DEF345', 1),
(3, 1, 'JKL901', 3),
(4, 1, 'ABC012', 3),
(5, 1, 'MNO234', 3);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `CMR_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CMR_CNT_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`CMR_Id`),
  KEY `CMR_CNT_Id` (`CMR_CNT_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`CMR_Id`, `CMR_CNT_Id`) VALUES
(1, 6),
(2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `group_bin`
--

CREATE TABLE IF NOT EXISTS `group_bin` (
  `GBN_Id` int(11) NOT NULL AUTO_INCREMENT,
  `GBN_CBG_Id` int(11) DEFAULT NULL,
  `GBN_Number` int(11) DEFAULT NULL,
  PRIMARY KEY (`GBN_Id`),
  KEY `GBN_CBG_Id` (`GBN_CBG_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `group_bin`
--

INSERT INTO `group_bin` (`GBN_Id`, `GBN_CBG_Id`, `GBN_Number`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `issue_item_bin`
--

CREATE TABLE IF NOT EXISTS `issue_item_bin` (
  `SIB_Id` int(11) NOT NULL AUTO_INCREMENT,
  `SIB_TES_Id` int(11) DEFAULT NULL,
  `SIB_TOI_Id` int(11) DEFAULT NULL,
  `SIB_GBN_Id` int(11) DEFAULT NULL,
  `SIB_ExecuteTime` date DEFAULT NULL,
  PRIMARY KEY (`SIB_Id`),
  KEY `SIB_TES_Id` (`SIB_TES_Id`),
  KEY `SIB_TOI_Id` (`SIB_TOI_Id`),
  KEY `SIB_GBN_Id` (`SIB_GBN_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_contained_in`
--

CREATE TABLE IF NOT EXISTS `item_contained_in` (
  `ICI_Id` int(11) NOT NULL AUTO_INCREMENT,
  `ICI_Container_TOI_Id` int(11) DEFAULT NULL,
  `ICI_Contained_TOI_Id` int(11) DEFAULT NULL,
  `ICI_StartDate` date DEFAULT NULL,
  `ICI_EndDate` date DEFAULT NULL,
  PRIMARY KEY (`ICI_Id`),
  KEY `ICI_Container_TOI_Id` (`ICI_Container_TOI_Id`),
  KEY `ICI_Contained_TOI_Id` (`ICI_Contained_TOI_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_quality_status`
--

CREATE TABLE IF NOT EXISTS `item_quality_status` (
  `IQS_Id` int(11) NOT NULL AUTO_INCREMENT,
  `IQS_SAD_Id` int(11) DEFAULT NULL,
  `IQS_Date` date DEFAULT NULL,
  `IQS_QST_Id` int(11) DEFAULT NULL,
  `IQS_TOI_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`IQS_Id`),
  KEY `IQS_SAD_Id` (`IQS_SAD_Id`),
  KEY `IQS_QST_Id` (`IQS_QST_Id`),
  KEY `IQS_TOI_Id` (`IQS_TOI_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE IF NOT EXISTS `owner` (
  `OWN_Id` int(11) NOT NULL AUTO_INCREMENT,
  `OWN_CNT_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`OWN_Id`),
  KEY `OWN_CNT_Id` (`OWN_CNT_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`OWN_Id`, `OWN_CNT_Id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `quality_status_type`
--

CREATE TABLE IF NOT EXISTS `quality_status_type` (
  `QST_Id` int(11) NOT NULL AUTO_INCREMENT,
  `QST_Description` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`QST_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `receive_item_bin`
--

CREATE TABLE IF NOT EXISTS `receive_item_bin` (
  `RIB_Id` int(11) NOT NULL AUTO_INCREMENT,
  `RIB_TER_Id` int(11) DEFAULT NULL,
  `RIB_TOI_Id` int(11) DEFAULT NULL,
  `RIB_GBN_Id` int(11) DEFAULT NULL,
  `RIB_ExecuteTime` date DEFAULT NULL,
  PRIMARY KEY (`RIB_Id`),
  KEY `RIB_TER_Id` (`RIB_TER_Id`),
  KEY `RIB_TOI_Id` (`RIB_TOI_Id`),
  KEY `RIB_GBN_Id` (`RIB_GBN_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `receive_item_bin`
--

INSERT INTO `receive_item_bin` (`RIB_Id`, `RIB_TER_Id`, `RIB_TOI_Id`, `RIB_GBN_Id`, `RIB_ExecuteTime`) VALUES
(1, 1, 1, 1, '2015-01-04'),
(2, 1, 2, 1, '2015-01-04'),
(3, 1, 3, 1, '2015-01-04'),
(4, 1, 4, 1, '2015-01-04'),
(5, 1, 5, 1, '2015-01-04');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `STE_Id` int(11) NOT NULL AUTO_INCREMENT,
  `STE_Name` varchar(32) DEFAULT NULL,
  `STE_Address` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`STE_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`STE_Id`, `STE_Name`, `STE_Address`) VALUES
(1, 'roboVicsSite', 'Site Address'),
(2, 'Second_SiTe', 'My compane');

-- --------------------------------------------------------

--
-- Table structure for table `site_admin`
--

CREATE TABLE IF NOT EXISTS `site_admin` (
  `SAD_Id` int(11) NOT NULL AUTO_INCREMENT,
  `SAD_STF_Id` int(11) DEFAULT NULL,
  `SAD_STE_Id` int(11) DEFAULT NULL,
  `SAD_StartDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SAD_EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`SAD_Id`),
  KEY `SAD_STF_Id` (`SAD_STF_Id`),
  KEY `SAD_STE_Id` (`SAD_STE_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_admin`
--

INSERT INTO `site_admin` (`SAD_Id`, `SAD_STF_Id`, `SAD_STE_Id`, `SAD_StartDate`, `SAD_EndDate`) VALUES
(1, 1, 1, '2015-01-04 16:37:48', '2015-01-04 16:37:48');

-- --------------------------------------------------------

--
-- Table structure for table `site_configuration`
--

CREATE TABLE IF NOT EXISTS `site_configuration` (
  `CFG_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CFG_STE_Id` int(11) DEFAULT NULL,
  `CFG_DateTime` date DEFAULT NULL,
  PRIMARY KEY (`CFG_Id`),
  KEY `CFG_STE_Id` (`CFG_STE_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_configuration`
--

INSERT INTO `site_configuration` (`CFG_Id`, `CFG_STE_Id`, `CFG_DateTime`) VALUES
(1, 1, '2014-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `site_configuration_bin_group`
--

CREATE TABLE IF NOT EXISTS `site_configuration_bin_group` (
  `CBG_Id` int(11) NOT NULL AUTO_INCREMENT,
  `CBG_Symbol` varchar(32) DEFAULT NULL,
  `CBG_CFG_Id` int(11) NOT NULL,
  PRIMARY KEY (`CBG_Id`),
  KEY `CBG_CFG_Id` (`CBG_CFG_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `site_configuration_bin_group`
--

INSERT INTO `site_configuration_bin_group` (`CBG_Id`, `CBG_Symbol`, `CBG_CFG_Id`) VALUES
(1, 'A', 1),
(2, 'B', 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_labor`
--

CREATE TABLE IF NOT EXISTS `site_labor` (
  `SLB_Id` int(11) NOT NULL AUTO_INCREMENT,
  `SLB_STF_Id` int(11) DEFAULT NULL,
  `SLB_STE_Id` int(11) DEFAULT NULL,
  `SLB_StartDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SLB_EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`SLB_Id`),
  KEY `SLB_STF_Id` (`SLB_STF_Id`),
  KEY `SLB_STE_Id` (`SLB_STE_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_labor`
--

INSERT INTO `site_labor` (`SLB_Id`, `SLB_STF_Id`, `SLB_STE_Id`, `SLB_StartDate`, `SLB_EndDate`) VALUES
(1, 2, 1, '2015-01-04 16:37:48', '2015-01-04 16:37:49');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `STF_Id` int(11) NOT NULL AUTO_INCREMENT,
  `STF_Fname` varchar(32) DEFAULT NULL,
  `STF_Lname` varchar(32) DEFAULT NULL,
  `STF_MobileNumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`STF_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`STF_Id`, `STF_Fname`, `STF_Lname`, `STF_MobileNumber`) VALUES
(1, 'Admin First Name', 'Admin Last Names', '01812345679'),
(2, 'Labor First Name', 'Labor Last Name', '01912345678');

-- --------------------------------------------------------

--
-- Table structure for table `stock_keeping_unit`
--

CREATE TABLE IF NOT EXISTS `stock_keeping_unit` (
  `SKU_Id` int(11) NOT NULL AUTO_INCREMENT,
  `SKU_Code` varchar(32) DEFAULT NULL,
  `SKU_Description` varchar(256) DEFAULT NULL,
  `SKU_LiftingNumber` int(11) DEFAULT NULL,
  `SKU_LifeTimeYears` int(4) DEFAULT NULL,
  `SKU_LifeTimeMonths` int(2) DEFAULT NULL,
  `SKU_LifeTimeDays` int(2) DEFAULT NULL,
  PRIMARY KEY (`SKU_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `stock_keeping_unit`
--

INSERT INTO `stock_keeping_unit` (`SKU_Id`, `SKU_Code`, `SKU_Description`, `SKU_LiftingNumber`, `SKU_LifeTimeYears`, `SKU_LifeTimeMonths`, `SKU_LifeTimeDays`) VALUES
(1, 'AAA111', 'Detailed Specs of Product', 3, 0, 2, 0),
(2, '2', 'gdafdas', 3, 2, 24, 730),
(3, '3', 'sadfufas', 2, 1, 12, 365);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `SUP_Id` int(11) NOT NULL AUTO_INCREMENT,
  `SUP_CNT_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`SUP_Id`),
  KEY `SUP_CNT_Id` (`SUP_CNT_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`SUP_Id`, `SUP_CNT_Id`) VALUES
(1, 3),
(2, 4),
(3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_execute_issue`
--

CREATE TABLE IF NOT EXISTS `transaction_execute_issue` (
  `TES_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TES_SLB_Id` int(11) DEFAULT NULL,
  `TES_TOS_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`TES_Id`),
  KEY `TES_SLB_Id` (`TES_SLB_Id`),
  KEY `TES_TOS_Id` (`TES_TOS_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_execute_receive`
--

CREATE TABLE IF NOT EXISTS `transaction_execute_receive` (
  `TER_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TER_SLB_Id` int(11) DEFAULT NULL,
  `TER_TOR_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`TER_Id`),
  KEY `TER_SLB_Id` (`TER_SLB_Id`),
  KEY `TER_TOR_Id` (`TER_TOR_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `transaction_execute_receive`
--

INSERT INTO `transaction_execute_receive` (`TER_Id`, `TER_SLB_Id`, `TER_TOR_Id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_execute_transfer`
--

CREATE TABLE IF NOT EXISTS `transaction_execute_transfer` (
  `TET_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TET_SLB_Id` int(11) DEFAULT NULL,
  `TET_TOT_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`TET_Id`),
  KEY `TET_SLB_Id` (`TET_SLB_Id`),
  KEY `TET_TOT_Id` (`TET_TOT_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_order`
--

CREATE TABLE IF NOT EXISTS `transaction_order` (
  `TOD_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TOD_OrderDate` date DEFAULT NULL,
  `TOD_TransactionTime` date DEFAULT NULL,
  `TOD_SAD_Id` int(11) DEFAULT NULL,
  `TOD_TTP_Id` int(11) DEFAULT NULL,
  `TOD_ItemsQuantity` int(11) DEFAULT NULL,
  `TOD_IsExecuted` int(1) DEFAULT NULL,
  PRIMARY KEY (`TOD_Id`),
  KEY `TOD_SAD_Id` (`TOD_SAD_Id`),
  KEY `TOD_TTP_Id` (`TOD_TTP_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `transaction_order`
--

INSERT INTO `transaction_order` (`TOD_Id`, `TOD_OrderDate`, `TOD_TransactionTime`, `TOD_SAD_Id`, `TOD_TTP_Id`, `TOD_ItemsQuantity`, `TOD_IsExecuted`) VALUES
(1, '2015-01-04', '2015-01-04', 1, 2, 5, 1),
(2, '2015-01-04', '2015-01-06', 1, 2, 5, NULL),
(3, '2015-01-04', '2015-01-02', 1, 2, 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_order_issue`
--

CREATE TABLE IF NOT EXISTS `transaction_order_issue` (
  `TOS_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TOS_TOD_Id` int(11) DEFAULT NULL,
  `TOS_OWN_Id` int(11) DEFAULT NULL,
  `TOS_CMR_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`TOS_Id`),
  KEY `TOS_TOD_Id` (`TOS_TOD_Id`),
  KEY `TOS_OWN_Id` (`TOS_OWN_Id`),
  KEY `TOS_CMR_Id` (`TOS_CMR_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_order_item`
--

CREATE TABLE IF NOT EXISTS `transaction_order_item` (
  `TOI_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TOI_TOD_Id` int(11) DEFAULT NULL,
  `TOI_SKU_Id` int(11) DEFAULT NULL,
  `TOI_UTP_Id` int(11) DEFAULT NULL,
  `TOI_Serial` varchar(256) DEFAULT NULL,
  `TOI_IsContained` int(1) DEFAULT NULL,
  `TOI_IsContaining` int(1) DEFAULT NULL,
  `TOI_ProductionDate` date DEFAULT NULL,
  `TOI_ExpirationDate` date DEFAULT NULL,
  PRIMARY KEY (`TOI_Id`),
  KEY `TOI_TOD_Id` (`TOI_TOD_Id`),
  KEY `TOI_SKU_Id` (`TOI_SKU_Id`),
  KEY `TOI_UTP_Id` (`TOI_UTP_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `transaction_order_item`
--

INSERT INTO `transaction_order_item` (`TOI_Id`, `TOI_TOD_Id`, `TOI_SKU_Id`, `TOI_UTP_Id`, `TOI_Serial`, `TOI_IsContained`, `TOI_IsContaining`, `TOI_ProductionDate`, `TOI_ExpirationDate`) VALUES
(1, 1, 1, NULL, 'ABC012', NULL, NULL, '2014-12-05', '2015-02-03'),
(2, 1, 1, NULL, 'DEF345', NULL, NULL, '2014-12-05', '2015-02-03'),
(3, 1, 1, NULL, 'GHI678', NULL, NULL, '2014-12-05', '2015-02-03'),
(4, 1, 1, NULL, 'JKL901', NULL, NULL, '2014-12-05', '2015-02-03'),
(5, 1, 2, NULL, 'MNO234', NULL, NULL, '2014-12-05', '2015-02-03');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_order_receive`
--

CREATE TABLE IF NOT EXISTS `transaction_order_receive` (
  `TOR_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TOR_TOD_Id` int(11) DEFAULT NULL,
  `TOR_OWN_Id` int(11) DEFAULT NULL,
  `TOR_SUP_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`TOR_Id`),
  KEY `TOR_TOD_Id` (`TOR_TOD_Id`),
  KEY `TOR_OWN_Id` (`TOR_OWN_Id`),
  KEY `TOR_SUP_Id` (`TOR_SUP_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `transaction_order_receive`
--

INSERT INTO `transaction_order_receive` (`TOR_Id`, `TOR_TOD_Id`, `TOR_OWN_Id`, `TOR_SUP_Id`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_order_transfer`
--

CREATE TABLE IF NOT EXISTS `transaction_order_transfer` (
  `TOT_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TOT_TOD_Id` int(11) DEFAULT NULL,
  `TOT_From_GBN_Id` int(11) DEFAULT NULL,
  `TOT_To_GBN_Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`TOT_Id`),
  KEY `TOT_TOD_Id` (`TOT_TOD_Id`),
  KEY `TOT_From_GBN_Id` (`TOT_From_GBN_Id`),
  KEY `TOT_To_GBN_Id` (`TOT_To_GBN_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE IF NOT EXISTS `transaction_type` (
  `TTP_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TTP_Description` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TTP_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`TTP_Id`, `TTP_Description`) VALUES
(1, 'Issue'),
(2, 'Receive'),
(3, 'Transfer');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_item_bin`
--

CREATE TABLE IF NOT EXISTS `transfer_item_bin` (
  `TIB_Id` int(11) NOT NULL AUTO_INCREMENT,
  `TIB_TET_Id` int(11) DEFAULT NULL,
  `TIB_TOI_Id` int(11) DEFAULT NULL,
  `TIB_From_GBN_Id` int(11) DEFAULT NULL,
  `TIB_To_GBN_Id` int(11) DEFAULT NULL,
  `TIB_ExecuteTime` date DEFAULT NULL,
  PRIMARY KEY (`TIB_Id`),
  KEY `TIB_TET_Id` (`TIB_TET_Id`),
  KEY `TIB_TOI_Id` (`TIB_TOI_Id`),
  KEY `TIB_From_GBN_Id` (`TIB_From_GBN_Id`),
  KEY `TIB_To_GBN_Id` (`TIB_To_GBN_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `unit_type`
--

CREATE TABLE IF NOT EXISTS `unit_type` (
  `UTP_Id` int(11) NOT NULL AUTO_INCREMENT,
  `UTP_Description` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`UTP_Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `unit_type`
--

INSERT INTO `unit_type` (`UTP_Id`, `UTP_Description`) VALUES
(1, 'Pallet'),
(2, 'Cartoon');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `current_bin_item`
--
ALTER TABLE `current_bin_item`
  ADD CONSTRAINT `current_bin_item_ibfk_1` FOREIGN KEY (`CBI_STE_Id`) REFERENCES `site` (`STE_Id`),
  ADD CONSTRAINT `current_bin_item_ibfk_2` FOREIGN KEY (`CBI_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`);

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`CMR_CNT_Id`) REFERENCES `contact` (`CNT_Id`);

--
-- Constraints for table `group_bin`
--
ALTER TABLE `group_bin`
  ADD CONSTRAINT `group_bin_ibfk_1` FOREIGN KEY (`GBN_CBG_Id`) REFERENCES `site_configuration_bin_group` (`CBG_Id`);

--
-- Constraints for table `issue_item_bin`
--
ALTER TABLE `issue_item_bin`
  ADD CONSTRAINT `issue_item_bin_ibfk_1` FOREIGN KEY (`SIB_TES_Id`) REFERENCES `transaction_execute_issue` (`TES_Id`),
  ADD CONSTRAINT `issue_item_bin_ibfk_2` FOREIGN KEY (`SIB_TOI_Id`) REFERENCES `transaction_order_item` (`TOI_Id`),
  ADD CONSTRAINT `issue_item_bin_ibfk_3` FOREIGN KEY (`SIB_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`);

--
-- Constraints for table `item_contained_in`
--
ALTER TABLE `item_contained_in`
  ADD CONSTRAINT `item_contained_in_ibfk_1` FOREIGN KEY (`ICI_Container_TOI_Id`) REFERENCES `transaction_order_item` (`TOI_Id`),
  ADD CONSTRAINT `item_contained_in_ibfk_2` FOREIGN KEY (`ICI_Contained_TOI_Id`) REFERENCES `transaction_order_item` (`TOI_Id`);

--
-- Constraints for table `item_quality_status`
--
ALTER TABLE `item_quality_status`
  ADD CONSTRAINT `item_quality_status_ibfk_1` FOREIGN KEY (`IQS_SAD_Id`) REFERENCES `site_admin` (`SAD_Id`),
  ADD CONSTRAINT `item_quality_status_ibfk_2` FOREIGN KEY (`IQS_QST_Id`) REFERENCES `quality_status_type` (`QST_Id`),
  ADD CONSTRAINT `item_quality_status_ibfk_3` FOREIGN KEY (`IQS_TOI_Id`) REFERENCES `transaction_order_item` (`TOI_Id`);

--
-- Constraints for table `owner`
--
ALTER TABLE `owner`
  ADD CONSTRAINT `owner_ibfk_1` FOREIGN KEY (`OWN_CNT_Id`) REFERENCES `contact` (`CNT_Id`);

--
-- Constraints for table `receive_item_bin`
--
ALTER TABLE `receive_item_bin`
  ADD CONSTRAINT `receive_item_bin_ibfk_1` FOREIGN KEY (`RIB_TER_Id`) REFERENCES `transaction_execute_receive` (`TER_Id`),
  ADD CONSTRAINT `receive_item_bin_ibfk_2` FOREIGN KEY (`RIB_TOI_Id`) REFERENCES `transaction_order_item` (`TOI_Id`),
  ADD CONSTRAINT `receive_item_bin_ibfk_3` FOREIGN KEY (`RIB_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`);

--
-- Constraints for table `site_admin`
--
ALTER TABLE `site_admin`
  ADD CONSTRAINT `site_admin_ibfk_1` FOREIGN KEY (`SAD_STF_Id`) REFERENCES `staff` (`STF_Id`),
  ADD CONSTRAINT `site_admin_ibfk_2` FOREIGN KEY (`SAD_STE_Id`) REFERENCES `site` (`STE_Id`);

--
-- Constraints for table `site_configuration`
--
ALTER TABLE `site_configuration`
  ADD CONSTRAINT `site_configuration_ibfk_1` FOREIGN KEY (`CFG_STE_Id`) REFERENCES `site` (`STE_Id`);

--
-- Constraints for table `site_configuration_bin_group`
--
ALTER TABLE `site_configuration_bin_group`
  ADD CONSTRAINT `site_configuration_bin_group_ibfk_1` FOREIGN KEY (`CBG_CFG_Id`) REFERENCES `site_configuration` (`CFG_Id`);

--
-- Constraints for table `site_labor`
--
ALTER TABLE `site_labor`
  ADD CONSTRAINT `site_labor_ibfk_1` FOREIGN KEY (`SLB_STF_Id`) REFERENCES `staff` (`STF_Id`),
  ADD CONSTRAINT `site_labor_ibfk_2` FOREIGN KEY (`SLB_STE_Id`) REFERENCES `site` (`STE_Id`);

--
-- Constraints for table `supplier`
--
ALTER TABLE `supplier`
  ADD CONSTRAINT `supplier_ibfk_1` FOREIGN KEY (`SUP_CNT_Id`) REFERENCES `contact` (`CNT_Id`);

--
-- Constraints for table `transaction_execute_issue`
--
ALTER TABLE `transaction_execute_issue`
  ADD CONSTRAINT `transaction_execute_issue_ibfk_1` FOREIGN KEY (`TES_SLB_Id`) REFERENCES `site_labor` (`SLB_Id`),
  ADD CONSTRAINT `transaction_execute_issue_ibfk_2` FOREIGN KEY (`TES_TOS_Id`) REFERENCES `transaction_order_issue` (`TOS_Id`);

--
-- Constraints for table `transaction_execute_receive`
--
ALTER TABLE `transaction_execute_receive`
  ADD CONSTRAINT `transaction_execute_receive_ibfk_1` FOREIGN KEY (`TER_SLB_Id`) REFERENCES `site_labor` (`SLB_Id`),
  ADD CONSTRAINT `transaction_execute_receive_ibfk_2` FOREIGN KEY (`TER_TOR_Id`) REFERENCES `transaction_order_receive` (`TOR_Id`);

--
-- Constraints for table `transaction_execute_transfer`
--
ALTER TABLE `transaction_execute_transfer`
  ADD CONSTRAINT `transaction_execute_transfer_ibfk_1` FOREIGN KEY (`TET_SLB_Id`) REFERENCES `site_labor` (`SLB_Id`),
  ADD CONSTRAINT `transaction_execute_transfer_ibfk_2` FOREIGN KEY (`TET_TOT_Id`) REFERENCES `transaction_order_transfer` (`TOT_Id`);

--
-- Constraints for table `transaction_order`
--
ALTER TABLE `transaction_order`
  ADD CONSTRAINT `transaction_order_ibfk_1` FOREIGN KEY (`TOD_SAD_Id`) REFERENCES `site_admin` (`SAD_Id`),
  ADD CONSTRAINT `transaction_order_ibfk_2` FOREIGN KEY (`TOD_TTP_Id`) REFERENCES `transaction_type` (`TTP_Id`);

--
-- Constraints for table `transaction_order_issue`
--
ALTER TABLE `transaction_order_issue`
  ADD CONSTRAINT `transaction_order_issue_ibfk_1` FOREIGN KEY (`TOS_TOD_Id`) REFERENCES `transaction_order` (`TOD_Id`),
  ADD CONSTRAINT `transaction_order_issue_ibfk_2` FOREIGN KEY (`TOS_OWN_Id`) REFERENCES `owner` (`OWN_Id`),
  ADD CONSTRAINT `transaction_order_issue_ibfk_3` FOREIGN KEY (`TOS_CMR_Id`) REFERENCES `customer` (`CMR_Id`);

--
-- Constraints for table `transaction_order_item`
--
ALTER TABLE `transaction_order_item`
  ADD CONSTRAINT `transaction_order_item_ibfk_1` FOREIGN KEY (`TOI_TOD_Id`) REFERENCES `transaction_order` (`TOD_Id`),
  ADD CONSTRAINT `transaction_order_item_ibfk_2` FOREIGN KEY (`TOI_SKU_Id`) REFERENCES `stock_keeping_unit` (`SKU_Id`),
  ADD CONSTRAINT `transaction_order_item_ibfk_3` FOREIGN KEY (`TOI_UTP_Id`) REFERENCES `unit_type` (`UTP_Id`);

--
-- Constraints for table `transaction_order_receive`
--
ALTER TABLE `transaction_order_receive`
  ADD CONSTRAINT `transaction_order_receive_ibfk_1` FOREIGN KEY (`TOR_TOD_Id`) REFERENCES `transaction_order` (`TOD_Id`),
  ADD CONSTRAINT `transaction_order_receive_ibfk_2` FOREIGN KEY (`TOR_OWN_Id`) REFERENCES `owner` (`OWN_Id`),
  ADD CONSTRAINT `transaction_order_receive_ibfk_3` FOREIGN KEY (`TOR_SUP_Id`) REFERENCES `supplier` (`SUP_Id`);

--
-- Constraints for table `transaction_order_transfer`
--
ALTER TABLE `transaction_order_transfer`
  ADD CONSTRAINT `transaction_order_transfer_ibfk_1` FOREIGN KEY (`TOT_TOD_Id`) REFERENCES `transaction_order` (`TOD_Id`),
  ADD CONSTRAINT `transaction_order_transfer_ibfk_2` FOREIGN KEY (`TOT_From_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`),
  ADD CONSTRAINT `transaction_order_transfer_ibfk_3` FOREIGN KEY (`TOT_To_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`);

--
-- Constraints for table `transfer_item_bin`
--
ALTER TABLE `transfer_item_bin`
  ADD CONSTRAINT `transfer_item_bin_ibfk_1` FOREIGN KEY (`TIB_TET_Id`) REFERENCES `transaction_execute_transfer` (`TET_Id`),
  ADD CONSTRAINT `transfer_item_bin_ibfk_2` FOREIGN KEY (`TIB_TOI_Id`) REFERENCES `transaction_order_item` (`TOI_Id`),
  ADD CONSTRAINT `transfer_item_bin_ibfk_3` FOREIGN KEY (`TIB_From_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`),
  ADD CONSTRAINT `transfer_item_bin_ibfk_4` FOREIGN KEY (`TIB_To_GBN_Id`) REFERENCES `group_bin` (`GBN_Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
