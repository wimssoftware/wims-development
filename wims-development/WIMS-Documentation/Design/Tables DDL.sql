-- Master Data Tables --
create table Site (
	STE_Id 			INTEGER AUTO_INCREMENT,
	STE_Name      	VARCHAR(32),
	STE_Address     VARCHAR(100),
	PRIMARY KEY (STE_Id)
);

create table Site_Configuration_Bin_Group (
	CBG_Id      INTEGER AUTO_INCREMENT,
	CBG_Symbol  VARCHAR(32),
	PRIMARY KEY (CBG_Id)
);

create table Site_Configuration (
	CFG_Id      	INTEGER AUTO_INCREMENT,
	CFG_STE_Id   	INTEGER,
	CFG_DateTime    Date,
	PRIMARY KEY (CFG_Id),
	FOREIGN KEY (CFG_STE_Id) REFERENCES Site(STE_Id)
);

create table Group_Bin (
	GBN_Id      	INTEGER AUTO_INCREMENT,
	GBN_CBG_Id      INTEGER,
	GBN_Number     	INTEGER,
	PRIMARY KEY (GBN_Id),
	FOREIGN KEY (GBN_CBG_Id) REFERENCES Site_Configuration_Bin_Group(CBG_Id)
);

-- Staff Tables --
create table Staff (
	STF_Id      		INTEGER AUTO_INCREMENT,
	STF_Fname      		VARCHAR(32),
	STF_Lname      		VARCHAR(32),
	STF_MobileNumber   	VARCHAR(15),
	PRIMARY KEY (STF_Id)
);

create table Site_Labor (
	SLB_Id      	INTEGER AUTO_INCREMENT,
	SLB_STF_Id      INTEGER,
	SLB_STE_Id      INTEGER,
	SLB_StartDate   Date,
	SLB_EndDate     Date,
	PRIMARY KEY (SLB_Id),
	FOREIGN KEY (SLB_STF_Id) REFERENCES Staff(STF_Id),
	FOREIGN KEY (SLB_STE_Id) REFERENCES Site(STE_Id)
);

create table Site_Admin (
	SAD_Id      	INTEGER AUTO_INCREMENT,
	SAD_STF_Id      INTEGER,
	SAD_STE_Id      INTEGER,
	SAD_StartDate   Date,
	SAD_EndDate     Date,
	PRIMARY KEY (SAD_Id),
	FOREIGN KEY (SAD_STF_Id) REFERENCES Staff(STF_Id),
	FOREIGN KEY (SAD_STE_Id) REFERENCES Site(STE_Id)
);

-- Contact Tables --
create table Contact (
	CNT_Id      		INTEGER AUTO_INCREMENT,
	CNT_Fname      		VARCHAR(32),
	CNT_Lname      		VARCHAR(32),
	CNT_MobileNumber   	VARCHAR(15),
	CNT_Address      	VARCHAR(100),
	PRIMARY KEY (CNT_Id)
);

create table Supplier (
	SUP_Id      INTEGER AUTO_INCREMENT,
	SUP_CNT_Id  INTEGER,
	PRIMARY KEY (SUP_Id),
	FOREIGN KEY (SUP_CNT_Id) REFERENCES Contact(CNT_Id)
);

create table Owner (
	OWN_Id      INTEGER AUTO_INCREMENT,
	OWN_CNT_Id  INTEGER,
	PRIMARY KEY (OWN_Id),
	FOREIGN KEY (OWN_CNT_Id) REFERENCES Contact(CNT_Id)
);

create table Customer (
	CMR_Id      INTEGER AUTO_INCREMENT,
	CMR_CNT_Id  INTEGER,
	PRIMARY KEY (CMR_Id),
	FOREIGN KEY (CMR_CNT_Id) REFERENCES Contact(CNT_Id)
);

-- SKU Table --
create table Stock_Keeping_Unit (
	SKU_Id      			INTEGER AUTO_INCREMENT,
	SKU_Code      			VARCHAR(32),
	SKU_Description      	VARCHAR(256),
	SKU_LiftingNumber      	INTEGER,
	SKU_LifeTimeYears      	INTEGER(4),
	SKU_LifeTimeMonths      INTEGER(2),
	SKU_LifeTimeDays      	INTEGER(2),
	PRIMARY KEY (SKU_Id)
);

-- Transaction Order Tables --
create table Transaction_Type (
	TTP_Id      		INTEGER AUTO_INCREMENT,
	TTP_Description		VARCHAR(32),
	PRIMARY KEY (TTP_Id)
);

create table Transaction_Order (
	TOD_Id      			INTEGER AUTO_INCREMENT,
	TOD_OrderDate      		Date,
	TOD_TransactionTime     Date,
	TOD_SAD_Id      		INTEGER,
	TOD_TTP_Id      		INTEGER,
	TOD_ItemsQuantity      	INTEGER,
	PRIMARY KEY (TOD_Id),
	FOREIGN KEY (TOD_SAD_Id) REFERENCES Site_Admin(SAD_Id),
	FOREIGN KEY (TOD_TTP_Id) REFERENCES Transaction_Type(TTP_Id)
);

-- Transaction Order Item Tables --
create table Unit_Type (
	UTP_Id      		INTEGER AUTO_INCREMENT,
	UTP_Description     VARCHAR(32),
	PRIMARY KEY (UTP_Id)
);

create table Transaction_Order_Item (
	TOI_Id      			INTEGER AUTO_INCREMENT,
	TOI_TOD_Id      		INTEGER,
	TOI_SKU_Id      		INTEGER,
	TOI_UTP_Id      		INTEGER,
	TOI_Serial      		VARCHAR(256),
	TOI_IsContained      	INTEGER(1),
	TOI_IsContaining      	INTEGER(1),
	TOI_ProductionDate      Date,
	TOI_ExpirationDate      Date,
	PRIMARY KEY (TOI_Id),
	FOREIGN KEY (TOI_TOD_Id) REFERENCES Transaction_Order(TOD_Id),
	FOREIGN KEY (TOI_SKU_Id) REFERENCES Stock_Keeping_Unit(SKU_Id),
	FOREIGN KEY (TOI_UTP_Id) REFERENCES Unit_Type(UTP_Id)
);

create table Item_Contained_In (
	ICI_Id      			INTEGER AUTO_INCREMENT,
	ICI_Container_TOI_Id    INTEGER,
	ICI_Contained_TOI_Id    INTEGER,
	ICI_StartDate      		Date,
	ICI_EndDate      		Date,
	PRIMARY KEY (ICI_Id),
	FOREIGN KEY (ICI_Container_TOI_Id) REFERENCES Transaction_Order_Item(TOI_Id),
	FOREIGN KEY (ICI_Contained_TOI_Id) REFERENCES Transaction_Order_Item(TOI_Id)
);

create table Quality_Status_Type (
	QST_Id      		INTEGER AUTO_INCREMENT,
	QST_Description     VARCHAR(30),
	PRIMARY KEY (QST_Id)
);

create table Item_Quality_Status (
	IQS_Id      	INTEGER AUTO_INCREMENT,
	IQS_SAD_Id      INTEGER,
	IQS_Date      	Date,
	IQS_QST_Id      INTEGER,
	IQS_TOI_Id      INTEGER,
	PRIMARY KEY (IQS_Id),
	FOREIGN KEY (IQS_SAD_Id) REFERENCES Site_Admin(SAD_Id),
	FOREIGN KEY (IQS_QST_Id) REFERENCES Quality_Status_Type(QST_Id),
	FOREIGN KEY (IQS_TOI_Id) REFERENCES Transaction_Order_Item(TOI_Id)
);

-- Transaction Order Details Tables --
create table Transaction_Order_Issue (
	TOS_Id      	INTEGER AUTO_INCREMENT,
	TOS_TOD_Id      INTEGER,
	TOS_OWN_Id      INTEGER,
	TOS_CMR_Id      INTEGER,
	PRIMARY KEY (TOS_Id),
	FOREIGN KEY (TOS_TOD_Id) REFERENCES Transaction_Order(TOD_Id),
	FOREIGN KEY (TOS_OWN_Id) REFERENCES Owner(OWN_Id),
	FOREIGN KEY (TOS_CMR_Id) REFERENCES Customer(CMR_Id)
);

create table Transaction_Order_Receive (
	TOR_Id      	INTEGER AUTO_INCREMENT,
	TOR_TOD_Id      INTEGER,
	TOR_OWN_Id      INTEGER,
	TOR_SUP_Id      INTEGER,
	PRIMARY KEY (TOR_Id),
	FOREIGN KEY (TOR_TOD_Id) REFERENCES Transaction_Order(TOD_Id),
	FOREIGN KEY (TOR_OWN_Id) REFERENCES Owner(OWN_Id),
	FOREIGN KEY (TOR_SUP_Id) REFERENCES Supplier(SUP_Id)
);

create table Transaction_Order_Transfer (
	TOT_Id      		INTEGER AUTO_INCREMENT,
	TOT_TOD_Id      	INTEGER,
	TOT_From_GBN_Id     INTEGER,
	TOT_To_GBN_Id      	INTEGER,
	PRIMARY KEY (TOT_Id),
	FOREIGN KEY (TOT_TOD_Id) REFERENCES Transaction_Order(TOD_Id),
	FOREIGN KEY (TOT_From_GBN_Id) REFERENCES Group_Bin(GBN_Id),
	FOREIGN KEY (TOT_To_GBN_Id) REFERENCES Group_Bin(GBN_Id)
);

-- Transaction Execution Tables --
create table Transaction_Execute_Issue (
	TES_Id      	INTEGER AUTO_INCREMENT,
	TES_SLB_Id      INTEGER,
	TES_TOS_Id      INTEGER,
	PRIMARY KEY (TES_Id),
	FOREIGN KEY (TES_SLB_Id) REFERENCES Site_Labor(SLB_Id),
	FOREIGN KEY (TES_TOS_Id) REFERENCES Transaction_Order_Issue(TOS_Id)
);

create table Transaction_Execute_Receive (
	TER_Id      	INTEGER AUTO_INCREMENT,
	TER_SLB_Id      INTEGER,
	TER_TOR_Id      INTEGER,
	PRIMARY KEY (TER_Id),
	FOREIGN KEY (TER_SLB_Id) REFERENCES Site_Labor(SLB_Id),
	FOREIGN KEY (TER_TOR_Id) REFERENCES Transaction_Order_Receive(TOR_Id)
);

create table Transaction_Execute_Transfer (
	TET_Id      	INTEGER AUTO_INCREMENT,
	TET_SLB_Id      INTEGER,
	TET_TOT_Id      INTEGER,
	PRIMARY KEY (TET_Id),
	FOREIGN KEY (TET_SLB_Id) REFERENCES Site_Labor(SLB_Id),
	FOREIGN KEY (TET_TOT_Id) REFERENCES Transaction_Order_Transfer(TOT_Id)
);

-- Item Bin Tables --
create table Issue_Item_Bin (
	SIB_Id      		INTEGER AUTO_INCREMENT,
	SIB_TES_Id      	INTEGER,
	SIB_TOI_Id      	INTEGER,
	SIB_GBN_Id      	INTEGER,
	SIB_ExecuteTime     Date,
	PRIMARY KEY (SIB_Id),
	FOREIGN KEY (SIB_TES_Id) REFERENCES Transaction_Execute_Issue(TES_Id),
	FOREIGN KEY (SIB_TOI_Id) REFERENCES Transaction_Order_Item(TOI_Id),
	FOREIGN KEY (SIB_GBN_Id) REFERENCES Group_Bin(GBN_Id)
);

create table Receive_Item_Bin (
	RIB_Id      		INTEGER AUTO_INCREMENT,
	RIB_TER_Id      	INTEGER,
	RIB_TOI_Id      	INTEGER,
	RIB_GBN_Id      	INTEGER,
	RIB_ExecuteTime     Date,
	PRIMARY KEY (RIB_Id),
	FOREIGN KEY (RIB_TER_Id) REFERENCES Transaction_Execute_Receive(TER_Id),
	FOREIGN KEY (RIB_TOI_Id) REFERENCES Transaction_Order_Item(TOI_Id),
	FOREIGN KEY (RIB_GBN_Id) REFERENCES Group_Bin(GBN_Id)
);

create table Transfer_Item_Bin (
	TIB_Id      		INTEGER AUTO_INCREMENT,
	TIB_TET_Id      	INTEGER,
	TIB_TOI_Id      	INTEGER,
	TIB_From_GBN_Id     INTEGER,
	TIB_To_GBN_Id      	INTEGER,
	TIB_ExecuteTime    	Date,
	PRIMARY KEY (TIB_Id),
	FOREIGN KEY (TIB_TET_Id) REFERENCES Transaction_Execute_Transfer(TET_Id),
	FOREIGN KEY (TIB_TOI_Id) REFERENCES Transaction_Order_Item(TOI_Id),
	FOREIGN KEY (TIB_From_GBN_Id) REFERENCES Group_Bin(GBN_Id),
	FOREIGN KEY (TIB_To_GBN_Id) REFERENCES Group_Bin(GBN_Id)
);

alter table Transaction_Order add TOD_IsExecuted INTEGER(1);

insert into Site (STE_Id, STE_Name, STE_Address) values (1, 'Site', 'Site Address');

insert into Transaction_Type (TTP_Id, TTP_Description) values (1, 'Issue');
insert into Transaction_Type (TTP_Id, TTP_Description) values (2, 'Receive');
insert into Transaction_Type (TTP_Id, TTP_Description) values (3, 'Transfer');

insert into Staff (STF_Id, STF_Fname, STF_Lname, STF_MobileNumber) values (1, 'Admin First Name', 'Admin Last Name', '01812345678');
insert into Staff (STF_Id, STF_Fname, STF_Lname, STF_MobileNumber) values (2, 'Labor First Name', 'Labor Last Name', '01912345678');
insert into Site_Admin (SAD_Id, SAD_STF_Id, SAD_STE_Id) values (1, 1, 1);
insert into Site_Labor (SLB_Id, SLB_STF_Id, SLB_STE_Id) values (1, 2, 1);

insert into Transaction_Order (TOD_Id, TOD_OrderDate, TOD_TransactionTime, TOD_SAD_Id, TOD_TTP_Id, TOD_ItemsQuantity) values (1, current_date(), current_date(), 1, 2, 5);
insert into Transaction_Order (TOD_Id, TOD_OrderDate, TOD_TransactionTime, TOD_SAD_Id, TOD_TTP_Id, TOD_ItemsQuantity) values (2, current_date(), DATE_ADD(current_date(), INTERVAL 2 DAY), 1, 2, 5);
insert into Transaction_Order (TOD_Id, TOD_OrderDate, TOD_TransactionTime, TOD_SAD_Id, TOD_TTP_Id, TOD_ItemsQuantity) values (3, current_date(), DATE_SUB(current_date(), INTERVAL 2 DAY), 1, 2, 5);

insert into Stock_Keeping_Unit (SKU_Id, SKU_Code, SKU_Description, SKU_LiftingNumber, SKU_LifeTimeYears, SKU_LifeTimeMonths, SKU_LifeTimeDays) values (1, 'AAA111', 'Detailed Specs of Product', 3, 0, 2, 0);

insert into Transaction_Order_Item (TOI_Id, TOI_TOD_Id, TOI_SKU_Id, TOI_Serial, TOI_ProductionDate, TOI_ExpirationDate) values (1, 1, 1, 'ABC012', DATE_SUB(current_date(), INTERVAL 30 DAY), DATE_ADD(current_date(), INTERVAL 30 DAY));
insert into Transaction_Order_Item (TOI_Id, TOI_TOD_Id, TOI_SKU_Id, TOI_Serial, TOI_ProductionDate, TOI_ExpirationDate) values (2, 1, 1, 'DEF345', DATE_SUB(current_date(), INTERVAL 30 DAY), DATE_ADD(current_date(), INTERVAL 30 DAY));
insert into Transaction_Order_Item (TOI_Id, TOI_TOD_Id, TOI_SKU_Id, TOI_Serial, TOI_ProductionDate, TOI_ExpirationDate) values (3, 1, 1, 'GHI678', DATE_SUB(current_date(), INTERVAL 30 DAY), DATE_ADD(current_date(), INTERVAL 30 DAY));
insert into Transaction_Order_Item (TOI_Id, TOI_TOD_Id, TOI_SKU_Id, TOI_Serial, TOI_ProductionDate, TOI_ExpirationDate) values (4, 1, 1, 'JKL901', DATE_SUB(current_date(), INTERVAL 30 DAY), DATE_ADD(current_date(), INTERVAL 30 DAY));
insert into Transaction_Order_Item (TOI_Id, TOI_TOD_Id, TOI_SKU_Id, TOI_Serial, TOI_ProductionDate, TOI_ExpirationDate) values (5, 1, 1, 'MNO234', DATE_SUB(current_date(), INTERVAL 30 DAY), DATE_ADD(current_date(), INTERVAL 30 DAY));

insert into Site_Configuration_Bin_Group (CBG_Id, CBG_Symbol) values (1, 'A');
insert into Site_Configuration_Bin_Group (CBG_Id, CBG_Symbol) values (2, 'B');

insert into Group_Bin (GBN_Id, GBN_CBG_Id, GBN_Number) values (1, 1, 1);
insert into Group_Bin (GBN_Id, GBN_CBG_Id, GBN_Number) values (2, 1, 2);
insert into Group_Bin (GBN_Id, GBN_CBG_Id, GBN_Number) values (3, 2, 1);
insert into Group_Bin (GBN_Id, GBN_CBG_Id, GBN_Number) values (4, 2, 2);

insert into Transaction_Order_Receive (TOR_Id, TOR_TOD_Id) values (1, 1);

alter table Site_Admin modify SAD_StartDate TIMESTAMP;
alter table Site_Admin modify SAD_EndDate TIMESTAMP;
alter table Site_Labor modify SLB_StartDate TIMESTAMP;
alter table Site_Labor modify SLB_EndDate TIMESTAMP;

create table Current_Bin_Item (
	CBI_Id 				INTEGER AUTO_INCREMENT,
	CBI_STE_Id 			INTEGER,
	CBI_TOI_Serial 		VARCHAR(256),
	CBI_GBN_Id  		INTEGER,
	PRIMARY KEY (CBI_Id),
	FOREIGN KEY (CBI_STE_Id) REFERENCES Site(STE_Id),
	FOREIGN KEY (CBI_GBN_Id) REFERENCES Group_Bin(GBN_Id)
);

insert into Current_Bin_Item (CBI_Id, CBI_STE_Id, CBI_TOI_Serial, CBI_GBN_Id) values (1, 1, 'GHI678', 1);
insert into Current_Bin_Item (CBI_Id, CBI_STE_Id, CBI_TOI_Serial, CBI_GBN_Id) values (2, 1, 'DEF345', 1);
insert into Current_Bin_Item (CBI_Id, CBI_STE_Id, CBI_TOI_Serial, CBI_GBN_Id) values (3, 1, 'JKL901', 3);
insert into Current_Bin_Item (CBI_Id, CBI_STE_Id, CBI_TOI_Serial, CBI_GBN_Id) values (4, 1, 'ABC012', 3);
insert into Current_Bin_Item (CBI_Id, CBI_STE_Id, CBI_TOI_Serial, CBI_GBN_Id) values (5, 1, 'MNO234', 3);