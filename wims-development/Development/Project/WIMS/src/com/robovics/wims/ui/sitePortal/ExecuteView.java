package com.robovics.wims.ui.sitePortal;

import java.util.Date;


import com.vaadin.data.fieldgroup.PropertyId;




public class ExecuteView {

	@PropertyId(value = "slotId") 
	private Integer slotId;
	 
	@PropertyId(value = "noOfCartons") 
	private Integer noOfCartons;
	
	@PropertyId ("binSymbol") 
	private String binSymbol;
	
	@PropertyId(value = "slotSymbol") 
	private String slotSymbol;
	
	@PropertyId(value = "slotSerial") 
	private String slotSerial;
	
	@PropertyId(value = "toiId") 
	private Integer toiId;
	
	@PropertyId(value = "skuId") 
	private Integer skuId; 
	
	@PropertyId(value = "skuDesc") 
	private String skuDesc;
	
	@PropertyId(value = "palletCarton") 
	private String palletCarton;
	
	@PropertyId(value = "itemSerial") 
	private String itemSerial;
	
	@PropertyId(value = "prodDate") 
	private Date prodDate;
	
	@PropertyId(value = "expireDate") 
	private Date expireDate;
	
	@PropertyId(value = "qualityStatus") 
	private Integer qualityStatus;
	
	public Integer getQualityStatus() {
		return qualityStatus;
	}
	public void setQualityStatus(Integer qualityStatus) {
		this.qualityStatus = qualityStatus;
	}
	public Integer getSlotId() {
		return slotId;
	}
	public void setSlotId(Integer slotId) {
		this.slotId = slotId;
	}
	public String getBinSymbol() {
		return binSymbol;
	}
	public void setBinSymbol(String binSymbol) {
		this.binSymbol = binSymbol;
	}
	public String getSlotSymbol() {
		return slotSymbol;
	}
	public void setSlotSymbol(String slotSymbol) {
		this.slotSymbol = slotSymbol;
	}
	public String getSlotSerial() {
		return slotSerial;
	}
	public void setSlotSerial(String slotSerial) {
		this.slotSerial = slotSerial;
	}
	public Integer getToiId() {
		return toiId;
	}
	public void setToiId(Integer toiId) {
		this.toiId = toiId;
	}
	public Integer getSkuId() {
		return skuId;
	}
	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}
	public String getSkuDesc() {
		return skuDesc;
	}
	public void setSkuDesc(String skuDesc) {
		this.skuDesc = skuDesc;
	}
	public String getPalletCarton() {
		return palletCarton;
	}
	public void setPalletCarton(String palletCarton) {
		this.palletCarton = palletCarton;
	}
	public String getItemSerial() {
		return itemSerial;
	}
	public void setItemSerial(String itemSerial) {
		this.itemSerial = itemSerial;
	}
	public Date getProdDate() {
		return prodDate;
	}
	public void setProdDate(Date prodDate) {
		this.prodDate = prodDate;
	}
	public Date getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	} 
	public Integer getNoOfCartons() {
		return noOfCartons;
	}
	public void setNoOfCartons(Integer noOfCartons) {
		this.noOfCartons = noOfCartons;
	}
	
	
	}