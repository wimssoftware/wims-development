package com.robovics.wims.model.orm.entities.transaction.orderItem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		
		@NamedQuery(name = "GetNoOfCartonsIn", query = " select count(cip.id) "+
				" from CartonInPalet cip "+
				" where cip.paletId = :P_PaletId"
			 		),
		
		@NamedQuery(name = "GetCartonInPalletId", query = " select cip.id "+
				" from CartonInPalet cip "+
				" where cip.cartonId = :P_cartonId"
					) 
		})


@Table(name = "carton_palet")
@Entity
public class CartonInPalet extends BaseEntity {

	private static final long serialVersionUID = -8681441882522865576L;
	private Integer id;
	private Integer cartonId;
	private Integer paletId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CAR_PAL_id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CTN_ID")
	public Integer getCartonId() {
		return cartonId;
	}

	public void setCartonId(
			Integer carton_Id) {
		this.cartonId = carton_Id;
	}

	@Column(name = "PLT_ID")
	public Integer getPaletId() {
		return paletId;
	}

	public void setPaletId(
			Integer palet_Id) {
		this.paletId = palet_Id;
	}
}