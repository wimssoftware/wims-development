package com.robovics.wims.model.orm.entities.masterData;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.BaseEntity;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.service.contact.ContactService.ContactView;

@Table(name = "Site")
@Entity
public class Site extends BaseEntity {

	private static final long serialVersionUID = 7191629462969191322L;
	private Integer id;
	private String name;
	private String address;
	private String active;
	
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "STE_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "STE_Name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "STE_Address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	@Column(name = "STE_Active")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
	public void setActive(int active) {
		if (active == 1) {
			this.active = "Active";
		} else if (active == 0) {
			this.active = "In Active";
		}
	}

	
}