package com.robovics.wims.ui.staff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.model.service.staff.StaffService.StaffView;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Window;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class StaffPanel extends UI {
	StaffService staffService = null;
	List<StaffView> staffList = null;
	int currentIndex = 0;
	int allStaffsize = 0;
	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
 	Action delete;
 	

	@Override
	protected void init(VaadinRequest request) {

	}

	@SuppressWarnings("deprecation")
	public Layout buildStaff() {
		Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
		 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
		 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
		 delete = new Action(this.getMessage(GasDiaryMessages.Delete));
		 final Button newBeanBtn = new Button(this.getMessage(GasDiaryMessages.NewStaff));
		 final Button saveBtn = new Button(this.getMessage(GasDiaryMessages.Save));
		 Button cancelBtn = new Button(this.getMessage(GasDiaryMessages.CancelKey));
		final MasterDataService masterDataService;
		final String SITE_ADMIN_TYPE = "Site Admin";
		final String SITE_LABOR_TYPE = "Site Labor";
		staffService = (StaffService) ServiceFactory.getInstance().getService(
				ServicesEnum.STAFF_SERVICE);
		masterDataService = (MasterDataService) ServiceFactory.getInstance()
				.getService(ServicesEnum.MASTER_DATA_SERVICE);
		final List<Site> sites = masterDataService.getAllSites();
		final ArrayList<String> sitenames = new ArrayList<>();
		for(Site s:sites){
			sitenames.add(s.getName());
		}
		
		List<StaffView> staffs = null;
		staffList = staffService.getAllStaff();
		allStaffsize = staffList.size();

		System.err.println(allStaffsize);
		staffs = getListOfStaff();
		VerticalLayout vlayout = new VerticalLayout();
		vlayout.setMargin(true);
		final BeanItemContainer<StaffView> beans = new BeanItemContainer<StaffView>(
				StaffView.class);
		beans.addAll(staffs);
		// A layout for the table and form
		VerticalLayout layout = new VerticalLayout();
		// layout.setMargin(true);
		// Bind a table to it
		final Table table = new Table("", beans);
		table.setPageLength(staffs.size());
		table.setBuffered(false);
		table.setVisibleColumns(new Object[] { "firstName", "lastName",
				"mobileNumber", "siteName", "staffType", "startDate", "endDate" });
		table.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.firstName), this.getMessage(GasDiaryMessages.lastName),
				this.getMessage(GasDiaryMessages.mobileNumber),  this.getMessage(GasDiaryMessages.SiteName), this.getMessage(GasDiaryMessages.Type), 
				 this.getMessage(GasDiaryMessages.StartDate), this.getMessage(GasDiaryMessages.EndDate)});
		layout.addComponent(table);
		
		final Window StaffWindow = new Window();
		StaffWindow.center();
		StaffWindow.setHeight("500px");
		StaffWindow.setWidth("400px");
		StaffWindow.setResizable(false);
		StaffWindow.setDraggable(false);
		StaffWindow.setCaption(this.getMessage(GasDiaryMessages.StaffWindowCaption));


		table.addActionHandler(new Handler() {
			

			@Override
			public void handleAction(Action action, Object sender,
					final Object target) {
				if (action == delete) {
					int tableLength = table.getPageLength();
					try {
						StaffView staff = (StaffView) target;
						staffService.deleteStaff(staff);
						table.removeItem(target);
						table.setPageLength(tableLength - 1);
						Notification.show("Info",
								"Record deleted successfully",
								Notification.Type.TRAY_NOTIFICATION);
						staffList.remove(staff);
						allStaffsize--;
					} catch (BusinessException e) {
						table.setPageLength(tableLength);
						Notification.show("Error", e.getMessage(),
								Notification.Type.TRAY_NOTIFICATION);
					}
				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				ArrayList<Action> actions = new ArrayList<Action>();
				// Enable deleting only if clicked on an item
				if (target != null)
					actions.add(delete);
				return (Action[]) (actions.toArray(new Action[actions.size()]));
			}
		});

		// Create a form for editing a selected or new item.
		// It is invisible until actually used.
		final FormLayout formLayout = new FormLayout();

		PropertysetItem itemsProperty = new PropertysetItem();
		itemsProperty.addItemProperty("firstName", new ObjectProperty<String>(
				""));
		itemsProperty.addItemProperty("lastName",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("mobileNumber",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("startDate", new ObjectProperty<Date>(
				new Date()));
		itemsProperty.addItemProperty("endDate", new ObjectProperty<Date>(
				new Date()));
		itemsProperty.addItemProperty("userName",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("password",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("email", new ObjectProperty<String>(""));

		final FieldGroup form = new FieldGroup(itemsProperty);
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.firstName), "firstName"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.lastName), "lastName"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.mobileNumber),
				"mobileNumber"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.StartDate), "startDate"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.EndDate), "endDate"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.Username), "userName"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.Password), "password"));
		formLayout.addComponent(form.buildAndBind( this.getMessage(GasDiaryMessages.Email), "email"));

		formLayout.setVisible(false);
		form.setBuffered(true);
		//layout.addComponent(formLayout);
		formLayout.setSpacing(true);
		formLayout.setMargin(true);
        StaffWindow.setContent(formLayout);
       
		
        table.addListener(new ItemClickListener() {

	         @Override
	         public void itemClick(ItemClickEvent event) {
	          if (event.getButton()!=ItemClickEvent.BUTTON_RIGHT)
	          {
	        	  StaffWindow.setCaption("Update Staff");
	          getCurrent().addWindow(StaffWindow);
	          }
	         }
	    });

		HorizontalLayout hNextpervios = new HorizontalLayout();
		hNextpervios.setSizeUndefined();
		Button perviousb = new Button("<<");
		hNextpervios.addComponent(perviousb);
		Button nextb = new Button(">>");
		hNextpervios.addComponent(nextb);
		nextb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<StaffView> staffs = getListOfStaff();
				beans.removeAllItems();
				beans.addAll(staffs);
				table.setData(staffs);
				table.setPageLength(staffs.size());
				table.commit();
			}
		});
		perviousb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<StaffView> staffs = getPerviousListOfStaff();
				beans.removeAllItems();
				beans.addAll(staffs);
				table.setData(staffs);
				table.setPageLength(staffs.size());
				table.commit();
			}
		});
		layout.addComponent(hNextpervios);

		// When the user selects an item, show it in the form
		table.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				// Close the form if the item is deselected
				if (event.getProperty().getValue() == null) {
					//formLayout.setVisible(false);
					StaffWindow.close();
					return;
				}
				// Bind the form to the selected item
				form.setItemDataSource(table.getItem(table.getValue()));
				formLayout.setVisible(true);
				// The form was opened for editing an existing item
				table.setData(null);
			}
		});
		table.setSelectable(true);
		table.setImmediate(true);

		HorizontalLayout newStaffLayout = new HorizontalLayout();

		
		final ComboBox newStaffTypeCombo = new ComboBox(this.getMessage(GasDiaryMessages.privilege), Arrays.asList(
				SITE_ADMIN_TYPE, SITE_LABOR_TYPE));      
		newStaffTypeCombo.setNullSelectionAllowed(false);
		newStaffTypeCombo.select(SITE_ADMIN_TYPE);
		newStaffTypeCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (newStaffTypeCombo.getValue() == null
						|| ((String) newStaffTypeCombo.getValue()).isEmpty())
					newBeanBtn.setEnabled(false);
				else
					newBeanBtn.setEnabled(true);
			}
		});
		final ComboBox newStaffSiteCombo = new ComboBox(this.getMessage(GasDiaryMessages.SiteName), sitenames);
		//final ComboBox newStaffSiteCombo = new ComboBox("Sites");
		newStaffSiteCombo.setNullSelectionAllowed(false);
		newStaffSiteCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (newStaffSiteCombo.getValue() == null
						|| ((String) newStaffSiteCombo.getValue()).isEmpty())
					newBeanBtn.setEnabled(false);
				else
					newBeanBtn.setEnabled(true);
			}
		});
		// Creates a new bean for editing in the form before adding it to the
		// table. Adding is handled after committing the form.
		newBeanBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				// Create a new item, this will create a new bean
				
				StaffView staff = staffService.new StaffView();
				staff.setFirstName("");
				staff.setLastName("");
				staff.setMobileNumber("01");
				staff.setUserName("");
				staff.setPassword("");
				staff.setEmail("");
				Site site = null;
				try{
				 site = sites.get(sitenames.indexOf((String)newStaffSiteCombo.getValue()));
				}catch(ArrayIndexOutOfBoundsException e){
					Notification.show("Please Choose site for the new staff ",Type.ERROR_MESSAGE);
				}
				staff.setSiteId(site.getId());
				staff.setSiteName(site.getName());
				String staffType = (String) newStaffTypeCombo.getValue();
				staff.setStaffType(staffType);
				if (staffType.equals(SITE_ADMIN_TYPE)) {
					staff.setSiteAdmin(true);
					staff.setPriviladge(2);
				} else if (staffType.equals(SITE_LABOR_TYPE)) {
					staff.setSiteLabor(true);
					staff.setPriviladge(3);
				}
				Date currentDate = new Date();
				staff.setStartDate(currentDate);

				beans.addBean(staff);
				table.setPageLength(table.getPageLength() + 1);

				form.setItemDataSource(table.getItem(staff));
				// Make the form a bit nicer
				// The form was opened for editing a new item
				table.setData(staff);
				table.select(staff);
				table.setEnabled(false);
				newBeanBtn.setEnabled(false);
				//formLayout.setVisible(true);
				getCurrent().addWindow(StaffWindow);
			}
		});
		// When OK button is clicked, commit the form to the bean
		
		saveBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				StaffView staffView = staffService.new StaffView();
				staffView.setFirstName((String) form.getField("firstName")
						.getValue());
				staffView.setLastName((String) form.getField("lastName")
						.getValue());
				staffView.setMobileNumber((String) form
						.getField("mobileNumber").getValue());
				staffView.setStartDate((Date) form.getField("startDate")
						.getValue());
				staffView
						.setEndDate((Date) form.getField("endDate").getValue());
				List<String> errorMessages = staffService
						.validateStaff(staffView);
				if (errorMessages.size() > 0) {
					Collections.reverse(errorMessages);
					for (String errorMessage : errorMessages)
						Notification.show("Error", errorMessage,
								Notification.Type.TRAY_NOTIFICATION);
					return;
				}
				try {
					form.commit();
					Page.getCurrent().getJavaScript().execute("window.location.reload();");
				} catch (CommitException e1) {
					Notification
							.show("Error",
									"Error saving Staff, please return to technical support",
									Notification.Type.TRAY_NOTIFICATION);
				}
				formLayout.setVisible(false); // and close it
				StaffWindow.close();
				// New items have to be added to the container
				table.commit();
				table.setEnabled(true);
				StaffView staff = (StaffView) table.getValue();
				try {
					if (staff.getId() == null) {
						staffService.addStaff(staff);
						Notification.show("Info",
								"Record inserted successfully",
								Notification.Type.TRAY_NOTIFICATION);
					} else {
						staffService.updateStaff(staff);
						Notification.show("Info",
								"Record updated successfully",
								Notification.Type.TRAY_NOTIFICATION);
					}
				} catch (BusinessException e) {
					Notification.show("Error", e.getMessage(),
							Notification.Type.TRAY_NOTIFICATION);
				}
				newBeanBtn.setEnabled(true);
			}
		});
		HorizontalLayout formFooter = new HorizontalLayout();
		formFooter.addComponent(saveBtn);

		
		cancelBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.discard(); // Not really necessary
				formLayout.setVisible(false); // and close it
				StaffWindow.close();
				StaffView staff = (StaffView) table.getValue();
				if (staff.getId() == null) {
					beans.removeItem(staff);
					table.setPageLength(table.getPageLength() - 1);
				} else
					table.unselect(staff);

				table.discard(); // Discards possible addItem()
				table.setEnabled(true);
				newBeanBtn.setEnabled(true);
			}
		});

		formFooter.addComponent(cancelBtn);
		formLayout.addComponent(formFooter);
        VerticalLayout vertical = new VerticalLayout();
		//newStaffLayout.addComponent(newBeanBtn);
        vertical.addComponent(newBeanBtn);
		newStaffLayout.addComponent(newStaffTypeCombo);
		newStaffLayout.addComponent(newStaffSiteCombo);
		vertical.addComponent(newStaffLayout);

		HorizontalLayout filterLayout = new HorizontalLayout();
		TextField firstNameFilter = new TextField(this.getMessage(GasDiaryMessages.firstName));
		TextField lastNameFilter = new TextField(this.getMessage(GasDiaryMessages.lastName));
		filterLayout.addComponent(firstNameFilter);
		filterLayout.addComponent(lastNameFilter);

		firstNameFilter.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("firstName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});

		lastNameFilter.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("lastName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});
		firstNameFilter.setImmediate(true);
		lastNameFilter.setImmediate(true);

		vlayout.addComponent(vertical);
		vlayout.addComponent(filterLayout);

		HorizontalLayout Hlayout = new HorizontalLayout();
		Hlayout.setSizeUndefined();
		Hlayout.addComponent(vlayout);
		Hlayout.addComponent(layout);

		return Hlayout;
	}

	public List<StaffView> getListOfStaff() {
		ArrayList<StaffView> lStaff = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i < allStaffsize; i++) {
			try {
				lStaff.add(staffList.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lStaff;
	}

	public List<StaffView> getPerviousListOfStaff() {
		ArrayList<StaffView> lStaff = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i >= 0; i--) {
			try {
				lStaff.add(staffList.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lStaff;
	}
	
	 @Override
	    public void setLocale(Locale locale) {
	        super.setLocale(locale);
	        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	    }
	    
	    /** Returns the bundle for the current locale. */
	    public ResourceBundle getBundle() {
	        return i18nBundle;
	    }
	 
	    /**
	     * Returns a localized message from the resource bundle
	     * with the current application locale.
	     **/ 
	    public String getMessage(String key) {
	        return i18nBundle.getString(key);
	    }
}
