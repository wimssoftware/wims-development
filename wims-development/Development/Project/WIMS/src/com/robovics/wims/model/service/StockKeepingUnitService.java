package com.robovics.wims.model.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;

@Service(value = "stockKeepingUnitService")
public class StockKeepingUnitService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	public void addSKU(StockKeepingUnit sku) throws BusinessException {
		try {
			dao.addEntity(sku);
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void updateSKU(StockKeepingUnit sku) throws BusinessException {
		try {
			dao.updateEntity(sku);
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void deleteSKU(StockKeepingUnit sku) throws BusinessException {
		try {
			dao.deleteEntity(sku);
		} catch (DatabaseException e) {
			e.printStackTrace();
			if (e.getCause() instanceof ConstraintViolationException)
				throw new BusinessException(
						"this staff member is invovlved in some transactions and can not be deleted");
			throw new BusinessException("Internal Database Error");
		}
	}

	public List<StockKeepingUnit> getAllSKU() {
		return dao.getAllEntities(StockKeepingUnit.class);
	}

	public StockKeepingUnit getSKUById(Integer id) {
		return dao.getEntityById(id, StockKeepingUnit.class, "id");
	}

	@SuppressWarnings("null")
	public List<Integer> YearAndMonth(int noOfDays){
		List<Integer> date_ = new ArrayList<Integer>()
				;
		
		int  year = noOfDays/365;
	        noOfDays=noOfDays%365;
System.out.println("year=" + year);
	      int   month = noOfDays/30;
	        noOfDays=noOfDays%30;
	        System.out.println("month=" + month);
	     
	        System.out.println("days=" + noOfDays);
	    date_.add(year);
	    date_.add(month);
	    date_.add(noOfDays);
	    return date_ ;
	}
	
	public List<String> validateSKU(StockKeepingUnit sku) {
		List<String> errorMessages = new ArrayList<String>();
		if (sku.getCode() == null || sku.getCode().isEmpty())
			errorMessages.add("SKU code can't be empty");
		if (sku.getLiftingNumber() < 0)
			errorMessages
					.add("Capacity must be greater than or equal to zero");
		if(sku.getLifeTimeDays() <= 0  )
		{
			errorMessages.add("Life time day should be grater than zero");
		}
	/*	if (sku.getLifeTimeYears() == 0 && sku.getLifeTimeMonths() == 0
				&& sku.getLifeTimeDays() == 0)
			errorMessages.add("Please write down the SKU life time");
		if(sku.getLifeTimeMonths() >= 12  )
		{
			errorMessages.add("Life time month should be less than 12");
		}
		if(sku.getLifeTimeDays() >= 30  )
		{
			errorMessages.add("Life time day should be less than 30");
		}*/
		
		return errorMessages;
	}
}