package com.robovics.wims.ui.contact;

import java.lang.reflect.Field;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.contact.ContactService;
import com.vaadin.annotations.JavaScript;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("wims")
public class NewContactUI extends UI {

	@WebServlet(value = "/Contact/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = NewContactUI.class)
	public static class Servlet extends VaadinServlet {
	}

	private VerticalLayout layout = new VerticalLayout();
	private FormLayout formLayout = new FormLayout();
	private FieldGroup formFields = new FieldGroup();
	private Button submitButton = new Button("Submit");

	private static final String[] fieldNames = new String[] { "firstName",
			"lastName", "mobileNumber", "address" };

	@Override
	protected void init(VaadinRequest request) {
		initLayout();
		initForm();

		final ContactService contactService = (ContactService) ServiceFactory
				.getInstance().getService(ServicesEnum.CONTACT_SERVICE);

		submitButton.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				
				Contact contact = new Contact();

				for (String fieldName : fieldNames) {
					TextField textField = (TextField) formFields
							.getField(fieldName);

					try {
						Field contactObjectAttribute = Contact.class
								.getDeclaredField(fieldName);
						contactObjectAttribute.setAccessible(true);
						contactObjectAttribute.set(contact,
								textField.getValue());
					} catch (NoSuchFieldException e) { // for
														// getDeclaredField(fieldName)
						e.printStackTrace();
					} catch (SecurityException e) { // for
													// getDeclaredField(fieldName)
						e.printStackTrace();
					} catch (IllegalArgumentException e) { // for f.set
						e.printStackTrace();
					} catch (IllegalAccessException e) { // for f.set
						e.printStackTrace();
					}
				}
				boolean[] types = new boolean[3];
				types[0] = (Boolean) formFields.getField("isCustomer")
						.getValue();
				types[1] = (Boolean) formFields.getField("isOwner").getValue();
				types[2] = (Boolean) formFields.getField("isSupplier")
						.getValue();

				try {
					contactService.addContact(contact, types);
				
				} catch (BusinessException e) {
					Notification.show(e.getMessage(), Type.ERROR_MESSAGE);
				}
			}
		});

	}

	private void initLayout() {

		layout.setMargin(true);
		formLayout.setMargin(true);
		// editorLayout.setMargin(false);
		layout.addComponent(formLayout);
		layout.addComponent(submitButton);
		setContent(layout);

	}

	private void initForm() {
		for (String fieldName : fieldNames) {
			TextField field = new TextField(fieldName);
			formLayout.addComponent(field);
			// field.setWidth("100%");
			formFields.bind(field, fieldName);
		}
		CheckBox checkbox1 = new CheckBox("Customer");
		CheckBox checkbox2 = new CheckBox("Owner");
		CheckBox checkbox3 = new CheckBox("Supplier");
		formLayout.addComponent(checkbox1);
		formLayout.addComponent(checkbox2);
		formLayout.addComponent(checkbox3);
		formFields.bind(checkbox1, "isCustomer");
		formFields.bind(checkbox2, "isOwner");
		formFields.bind(checkbox3, "isSupplier");
		formFields.setBuffered(false);
	}
}