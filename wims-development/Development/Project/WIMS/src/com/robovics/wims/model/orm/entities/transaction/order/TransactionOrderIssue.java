package com.robovics.wims.model.orm.entities.transaction.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		@NamedQuery(name = "getIssueOrdersElements", query = "select a.id from TransactionOrderIssue a  where a.transactionOrderId =:P_ID")
		}) 

@Table(name = "Transaction_Order_Issue")
@Entity
public class TransactionOrderIssue extends BaseEntity {

	private static final long serialVersionUID = -943117059230171314L;
	private Integer id;
	private Integer transactionOrderId;
	private Integer ownerId;
	private Integer customerId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TOS_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TOS_TOD_Id")
	public Integer getTransactionOrderId() {
		return transactionOrderId;
	}

	public void setTransactionOrderId(Integer transactionOrderId) {
		this.transactionOrderId = transactionOrderId;
	}

	@Column(name = "TOS_OWN_Id")
	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	@Column(name = "TOS_CMR_Id")
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
}