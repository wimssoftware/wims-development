package com.robovics.wims.ui;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.user_session.user_session;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Title("WHMS")
@Theme("wims")
public class wimsIndex extends UI {
	
	Navigator navigator;
	/* User interface components are stored in session. */
  	
 	private Button SiteAdmin = new Button("Site Admin");
 	private Button WareHouseAdmin = new Button("WareHouse Admin");
 	private TextField quatityText = new TextField("Site Id");//to be replace be dropdown combo box with site names
 	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
 	
 	
 	
 	 Button SiteOrders;
 	@WebServlet(value = { "/whms/*" }, asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = wimsIndex.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		
		 if (((Locale)VaadinSession.getCurrent().getAttribute("Locale"))==null){
			 	i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
			 	i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
			 	VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
		 }else {
			Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
			 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
			 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
		 }
		 
		 SiteOrders = new Button(this.getMessage(GasDiaryMessages.Login));
		 VerticalLayout MainLayout = new VerticalLayout();
 		
 		MainLayout.addComponent(WareHouseAdmin);
 		MainLayout.addComponent(SiteAdmin);
 		MainLayout.addComponent(quatityText);
 		MainLayout.addComponent(SiteOrders);
 		
 		SiteOrders.addClickListener(new ClickListener() {
 			public void buttonClick(ClickEvent event) {
 				getUI().getPage().setLocation("siteOrders? siteId="+Integer.parseInt(quatityText.getValue()));
 			}
 		});
 
 		SiteAdmin.addClickListener(new ClickListener() {
 			public void buttonClick(ClickEvent event) {
 				setLocale(new Locale("en")); 
 			   // rebuild it with the new locale.
 				 if (((Locale)VaadinSession.getCurrent().getAttribute("Locale")).getLanguage().equals("fi")){
 					VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
 					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
 				}
 				else{
 					VaadinSession.getCurrent().setAttribute("Locale", new Locale("fi"));
 				i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),new Locale("fi") );
 				}
 						
 			} 
 		});
 		
 		WareHouseAdmin.addClickListener(new ClickListener() {
 			public void buttonClick(ClickEvent event) {
 				getUI().getPage().setLocation("WPHI");
 			}
 		});
 		
 		MainLayout.setSizeFull();
 		MainLayout.setVisible(true);
 		quatityText.setVisible(true);
 		quatityText.setRequired(true);
		quatityText.addValidator(new com.vaadin.data.validator.IntegerValidator("Must be number"));
 		MainLayout.setMargin(true);
 		MainLayout.setSpacing(true);
 		this.setContent(MainLayout);
	}
	 @Override
	    public void setLocale(Locale locale) {
	        super.setLocale(locale);
	        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	    }
	    
	    /** Returns the bundle for the current locale. */
	    public ResourceBundle getBundle() {
	        return i18nBundle;
	    }
	 
	    /**
	     * Returns a localized message from the resource bundle
	     * with the current application locale.
	     **/ 
	    public String getMessage(String key) {
	        return i18nBundle.getString(key);
	    }

}