package com.robovics.wims.model.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.QualityStatus;

@Service(value = "qualityStatusService")
public class QualityStatusServices {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	
	public List<QualityStatus> getQualityStatusValues() {
		return dao.getAllEntities(QualityStatus.class);
	}

}