package com.robovics.wims.model.service.staff;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.enums.StaffTypeEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.staff.SiteAdmin;
import com.robovics.wims.model.orm.entities.staff.SiteLabor;
import com.robovics.wims.model.orm.entities.staff.Staff;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;

@Service(value = "staffService")
public class StaffService {

	public class StaffView {
		private Integer id;
		private String firstName;
		private String lastName;
		private String mobileNumber;
		private boolean siteAdmin;
		private boolean siteLabor;
		private String staffType;
		private Integer siteAdminOrLaborId;
		private Integer siteId;
		private String siteName;
		private Date startDate;
		private Date endDate;
		private String userName;
		private String password;
		private String email;
		private Integer priviladge;

		Staff getStaff() {
			Staff staff = new Staff();
			staff.setId(getId());
			staff.setFirstName(getFirstName());
			staff.setLastName(getLastName());
			staff.setMobileNumber(getMobileNumber());
			staff.setUname(getUserName());
			staff.setPassword(getPassword());
			staff.setPriviladge(getPriviladge());
			staff.setEmail(getEmail());
			return staff;
		}

		SiteAdmin getSiteAdmin() {
			if (isSiteAdmin()) {
				SiteAdmin siteAdmin = new SiteAdmin();
				siteAdmin.setId(getSiteAdminOrLaborId());
				siteAdmin.setStaffId(getId());
				siteAdmin.setSiteId(getSiteId());
				siteAdmin.setStartDate(getStartDate());
				siteAdmin.setEndDate(getEndDate());
				return siteAdmin;
			}
			return null;
		}

		SiteLabor getSiteLabor() {
			if (isSiteLabor()) {
				SiteLabor siteLabor = new SiteLabor();
				siteLabor.setId(getSiteAdminOrLaborId());
				siteLabor.setStaffId(getId());
				siteLabor.setSiteId(getSiteId());
				siteLabor.setStartDate(getStartDate());
				siteLabor.setEndDate(getEndDate());
				return siteLabor;
			}
			return null;
		}

		void setStaff(Staff staff) {
			setId(staff.getId());
			setFirstName(staff.getFirstName());
			setLastName(staff.getLastName());
			setMobileNumber(staff.getMobileNumber());
			setUserName(staff.getUname());
			setPassword(staff.getPassword());
			setPriviladge(staff.getPriviladge());
			setEmail(staff.getEmail());
		}

		void setSiteAdminObject(SiteAdmin siteAdmin) {
			setSiteAdmin(true);
			setStaffType("Site Admin");
			setSiteAdminOrLaborId(siteAdmin.getId());
			setSiteId(siteAdmin.getSiteId());
			setStartDate(siteAdmin.getStartDate());
			setEndDate(siteAdmin.getEndDate());
		}

		void setSiteLaborObject(SiteLabor siteLabor) {
			setSiteLabor(true);
			setStaffType("Site Labor");
			setSiteAdminOrLaborId(siteLabor.getId());
			setSiteId(siteLabor.getSiteId());
			setStartDate(siteLabor.getStartDate());
			setEndDate(siteLabor.getEndDate());
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getMobileNumber() {
			return mobileNumber;
		}

		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String username) {
			this.userName = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Integer getPriviladge() {
			return priviladge;
		}

		public void setPriviladge(Integer priviladge) {
			this.priviladge = priviladge;
		}
		
		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public boolean isSiteAdmin() {
			return siteAdmin;
		}

		public void setSiteAdmin(boolean siteAdmin) {
			this.siteAdmin = siteAdmin;
		}

		public boolean isSiteLabor() {
			return siteLabor;
		}

		public void setSiteLabor(boolean siteLabor) {
			this.siteLabor = siteLabor;
		}

		public String getStaffType() {
			return staffType;
		}

		public void setStaffType(String staffType) {
			this.staffType = staffType;
		}

		public Integer getSiteAdminOrLaborId() {
			return siteAdminOrLaborId;
		}

		public void setSiteAdminOrLaborId(Integer siteAdminOrLaborId) {
			this.siteAdminOrLaborId = siteAdminOrLaborId;
		}

		public Integer getSiteId() {
			return siteId;
		}

		public void setSiteId(Integer siteId) {
			this.siteId = siteId;
		}

		public String getSiteName() {
			return siteName;
		}

		public void setSiteName(String siteName) {
			this.siteName = siteName;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	}

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	@Autowired
	private MasterDataService masterDataService;

	public void addStaff(StaffView staff) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			Staff addedStaff = staff.getStaff();
			dao.addEntity(addedStaff, session);
			staff.setId(addedStaff.getId());
			if (staff.isSiteAdmin()) {
				SiteAdmin siteAdmin = staff.getSiteAdmin();
				dao.addEntity(siteAdmin, session);
				staff.setSiteAdminOrLaborId(siteAdmin.getId());
			} else if (staff.isSiteLabor()) {
				SiteLabor siteLabor = staff.getSiteLabor();
				dao.addEntity(siteLabor, session);
				staff.setSiteAdminOrLaborId(siteLabor.getId());
			}

			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}

	public List<Integer> authenticateStaff(String username, String password)
			throws BusinessException {
		ArrayList<Integer> Site_Id = new ArrayList<Integer>();
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("p_password", password);
			parameters.put("p_uname", username);
			List<Integer> ID = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.Login.queryName, parameters);
			if (ID != null && !ID.isEmpty()) {
				Staff s = getStaffById(ID.get(0));
				Integer Priviladge = s.getPriviladge();
				if (Priviladge == 2) {
					parameters = new HashMap<String, Object>();
					parameters.put("p_STF_Id", ID.get(0));
					Site_Id = (ArrayList<Integer>) dao.executeNamedQuery(Integer.class,
							QueryNamesEnum.GET_Site_Admin_Id.queryName,
							parameters);
					VaadinSession.getCurrent().setAttribute("IsLogin", "true");
					VaadinSession.getCurrent().setAttribute("user_id", ID.get(0)+"");
					VaadinSession.getCurrent().setAttribute("user_priviladge", Priviladge+"");
					VaadinSession.getCurrent().setAttribute("Site_Id", Site_Id.get(0)+"");
				}else if (Priviladge == 1) {
					Site_Id.add(-1);
					VaadinSession.getCurrent().setAttribute("IsLogin", "true");
					VaadinSession.getCurrent().setAttribute("user_id", ID.get(0)+"");
					VaadinSession.getCurrent().setAttribute("user_priviladge", Priviladge+"");
					VaadinSession.getCurrent().setAttribute("Site_Id", "-1");
				}
				VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
			}
			session.commitTransaction();
			return Site_Id;
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		}
	}

	
	public Integer authenticateLabor(String username, String password)
			throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_userName", username);
			parameters.put("P_password", password);
			
			List<Integer> ID = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.LOGIN_LABOR.queryName, parameters);
			
			if (ID != null && !ID.isEmpty()) {
				ID.get(0);
			}
			return -1;
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		}
	}

	
	public void updateStaff(StaffView staff) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			dao.updateEntity(staff.getStaff(), session);
			if (staff.isSiteAdmin())
				dao.updateEntity(staff.getSiteAdmin(), session);
			else if (staff.isSiteLabor())
				dao.updateEntity(staff.getSiteLabor(), session);

			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	public void updateStaff(Staff staff) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			dao.updateEntity(staff, session);
				session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}

	public void deleteStaff(StaffView staff) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			if (staff.isSiteAdmin())
				dao.deleteEntity(staff.getSiteAdmin(), session);
			else if (staff.isSiteLabor())
				dao.deleteEntity(staff.getSiteLabor(), session);
			dao.deleteEntity(staff.getStaff(), session);

			session.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			session.rollbackTransaction();
			if (e instanceof ConstraintViolationException)
				throw new BusinessException(
						"this staff member is invovlved in some transactions and can not be deleted");
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}

	public List<StaffView> getAllStaff() {
		List<StaffView> staffViews = new ArrayList<StaffView>();
		List<Staff> staffs = dao.getAllEntities(Staff.class);
		for (Staff staff : staffs) {
			List<SiteAdmin> siteAdmins = getSiteAdminByStaffId(staff.getId());
			List<SiteLabor> siteLabors = null;
			if (siteAdmins == null || siteAdmins.size() == 0)
				siteLabors = getSiteLaborByStaffId(staff.getId());
			if (siteAdmins != null && siteAdmins.size() > 0) {
				for (SiteAdmin s : siteAdmins) {
					StaffView staffView = new StaffView();
					staffView.setStaff(staff);
					staffView.setSiteAdminObject(s);
					staffView.setSiteName(masterDataService.getSiteById(
							staffView.getSiteId()).getName());
					staffViews.add(staffView);
				}
			} else if (siteLabors != null && siteLabors.size() > 0) {
				for (SiteLabor l : siteLabors) {
					StaffView staffView = new StaffView();
					staffView.setStaff(staff);
					staffView.setSiteLaborObject(l);
					staffView.setSiteName(masterDataService.getSiteById(
							staffView.getSiteId()).getName());
					staffViews.add(staffView);
				}
			}
		}
		return staffViews;
	}

	public List<StaffView> getAllStaffByStaffType(Integer staffType) {
		List<StaffView> staffViews = new ArrayList<StaffView>();
		List<Staff> staffs = dao.getAllEntities(Staff.class);
		for (Staff staff : staffs) {
			SiteAdmin siteAdmin = getSiteAdminByStaffId(staff.getId()).get(0); // Tariq--
																				// Covert
																				// from
																				// List
																				// to
																				// Item
																				// .
			SiteLabor siteLabor = null;

			if (siteAdmin == null && staffType.equals(StaffTypeEnum.LABOR.id))// Tariq--
																				// Covert
																				// from
																				// List
																				// to
																				// Item
																				// .
				siteLabor = getSiteLaborByStaffId(staff.getId()).get(0);

			StaffView staffView = new StaffView();
			staffView.setStaff(staff);
			if (siteAdmin != null
					&& staffType.equals(StaffTypeEnum.SITEADMIN.id)) {
				staffView.setSiteAdminObject(siteAdmin);
				staffView.setSiteName(masterDataService.getSiteById(
						staffView.getSiteId()).getName());
				staffViews.add(staffView);
			} else if (siteLabor != null
					&& staffType.equals(StaffTypeEnum.LABOR.id)) {
				staffView.setSiteLaborObject(siteLabor);
				staffView.setSiteName(masterDataService.getSiteById(
						staffView.getSiteId()).getName());
				staffViews.add(staffView);
			}

		}
		return staffViews;
	}

	public Staff getStaffById(Integer id) {
		return dao.getEntityById(id, Staff.class, "id");
	}

	public SiteAdmin getSiteAdminById(Integer id) {
		return dao.getEntityById(id, SiteAdmin.class, "id");
	}

	@SuppressWarnings("unchecked")
	public List<SiteAdmin> getSiteAdminByStaffId(Integer staffId, Date... date) {
		
		CustomSession session = dao.openSession();
		Criteria criteria = session.getSession()
				.createCriteria(SiteAdmin.class);
		List<SiteAdmin> siteAdmins = (List<SiteAdmin>) criteria.add(
				Restrictions.eq("staffId", staffId)).list();
		
		List<SiteAdmin> returnSiteAdmins = new ArrayList<SiteAdmin>();

		if (date != null && date.length > 0) {
			System.out.println("Entered one");
			for (SiteAdmin siteAdmin : siteAdmins)
				if (siteAdmin.getStartDate() == null
						&& date[0].compareTo(siteAdmin.getEndDate()) <= 0
						|| siteAdmin.getEndDate() == null
						&& date[0].compareTo(siteAdmin.getStartDate()) >= 0
						|| (date[0].compareTo(siteAdmin.getStartDate()) >= 0 && date[0]
								.compareTo(siteAdmin.getEndDate()) <= 0)
						|| (siteAdmin.getStartDate() == null && siteAdmin
								.getEndDate() == null))
					returnSiteAdmins.add(siteAdmin);
		} else
			returnSiteAdmins = siteAdmins;
		System.out.println("dddd"+returnSiteAdmins.size()+"77777");
		session.close();
		return returnSiteAdmins;
	}

	public SiteLabor getSiteLaborById(Integer id) {
		return dao.getEntityById(id, SiteLabor.class, "id");
	}

	@Deprecated
	private static final int CURRENT_SITE_LABOR_ID = 1;

	@Deprecated
	public SiteLabor getCurrentSiteLabor() {
		return getSiteLaborById(CURRENT_SITE_LABOR_ID);
	}

	@SuppressWarnings("unchecked")
	public List<SiteLabor> getSiteLaborByStaffId(Integer staffId, Date... date) {
		CustomSession session = dao.openSession();
		List<SiteLabor> siteLabors = (List<SiteLabor>) session.getSession()
				.createCriteria(SiteLabor.class)
				.add(Restrictions.eq("staffId", staffId)).list();

		List<SiteLabor> returnSiteLabors = new ArrayList<SiteLabor>();

		if (date != null && date.length > 0) {
			for (SiteLabor siteLabor : siteLabors)
				if (siteLabor.getStartDate() == null
						&& date[0].compareTo(siteLabor.getEndDate()) <= 0
						|| siteLabor.getEndDate() == null
						&& date[0].compareTo(siteLabor.getStartDate()) >= 0
						|| (date[0].compareTo(siteLabor.getStartDate()) >= 0 && date[0]
								.compareTo(siteLabor.getEndDate()) <= 0)
						|| (siteLabor.getStartDate() == null && siteLabor
								.getEndDate() == null))
					returnSiteLabors.add(siteLabor);
		} else
			returnSiteLabors = siteLabors;
		session.close();
		return returnSiteLabors;
	}

	public List<String> validateStaff(StaffView staff) {
		List<String> errorMessages = new ArrayList<String>();
		if (staff.getFirstName() == null || staff.getFirstName().isEmpty())
			errorMessages.add("Staff first name can't be empty");
		if (staff.getLastName() == null || staff.getLastName().isEmpty())
			errorMessages.add("Staff last name can't be empty");
		if (staff.getMobileNumber() != null
				&& (!staff.getMobileNumber().startsWith("01") || staff
						.getMobileNumber().length() != 11))
			errorMessages.add("Invalid mobile number");
		if (staff.getStartDate() != null && staff.getEndDate() != null
				&& staff.getStartDate().compareTo(staff.getEndDate()) > 0)
			errorMessages.add("Start date must be bofore end date");
		return errorMessages;
	}
	
	public Integer getSiteId(Staff staff){
		if(staff.getPriviladge() == 2){
			List<SiteAdmin> admins = dao.getAllEntities(SiteAdmin.class);
			for(SiteAdmin admin: admins){
				if(admin.getStaffId() == staff.getId()){
					return admin.getSiteId();
				}
			}
		}
		return -1;
	}
}