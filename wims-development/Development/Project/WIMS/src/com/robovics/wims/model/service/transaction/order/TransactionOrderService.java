package com.robovics.wims.model.service.transaction.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.enums.TransactionTypesEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.staff.SiteAdmin;
import com.robovics.wims.model.orm.entities.staff.Staff;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.issue.IssueService;
import com.robovics.wims.model.service.receive.ReceiveService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.model.service.transaction.TransactionEntity;
import com.robovics.wims.model.service.transaction.orderItem.TransactionOrderItemService;

@Service(value = "transactionOrderService")
public class TransactionOrderService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	@Autowired
	private StaffService staffService;
	@Autowired
	private StockKeepingUnitService skuSer = new StockKeepingUnitService();
	@Autowired
	private StaffService staffSer = new StaffService();
	@Autowired
	private ContactService contactSer = new ContactService(); 
	
	public List<TransactionOrder> getUnExecutedOrdersByType( TransactionTypesEnum transType, Integer SiteId) 
			throws BusinessException {
		List<TransactionOrder> newList = new ArrayList<TransactionOrder>();
		
		List<SiteAdmin> Admins = dao.getAllEntities(SiteAdmin.class);
		for(SiteAdmin index : Admins){
			if(index.getSiteId() == SiteId){
				newList.addAll(this.getUnExecutedOrdersByType_(transType,index.getId()));
				System.out.println("Admin:"+index.getId());
				System.out.println("Orders no:"+this.getUnExecutedOrdersByType_(transType,index.getId()).size());
			}
		}
		System.out.println("Count:"+newList.size());
		return newList;
	}
	
	public List<TransactionOrder> getUnExecutedOrdersByType_(
			TransactionTypesEnum transType, Integer AdminId) throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_TRANSACTION_TYPE_ID", transType.id);
			parameters.put("P_SiteAdminId", AdminId);
			List <Integer> ordersIds =  dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_UN_EXECUTED_TRANSACTION_ORDERS_BY_TYPE.queryName,
					parameters);
			List<TransactionOrder> transactionOrders = new ArrayList<>();
			for (Integer id : ordersIds) {
				TransactionOrder transactionOrder = dao.getEntityById(id, TransactionOrder.class, "id");
				Staff staff = staffService.getStaffById((staffService
						.getSiteAdminById(transactionOrder.getSiteAdminId())
						.getStaffId()));
				System.out.println("staff id = "+staff.getId());
				transactionOrder.setSiteAdminName(staff.getFirstName() + " "
						+ staff.getLastName());
				transactionOrders.add(transactionOrder);
				
			}
			return transactionOrders;
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void updateTransactionOrder(TransactionOrder transactionOrder,
			CustomSession... useSession) throws BusinessException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		CustomSession session = isOpenedSession ? useSession[0] : dao
				.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();

			dao.updateEntity(transactionOrder, session);

			if (!isOpenedSession)
				session.commitTransaction();
			;
		} catch (Exception e) {
			if (!isOpenedSession)
				session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			if (!isOpenedSession)
				session.close();
		} 
	}  

	public TransactionOrderReceive getTransactionOrderReceiveByTransactionOrderId(
			Integer transactionOrderId) {
		CustomSession session = dao.openSession();
		TransactionOrderReceive transactionOrderReceive = (TransactionOrderReceive) session
				.getSession().createCriteria(TransactionOrderReceive.class)
				.add(Restrictions.eq("transactionOrderId", transactionOrderId))
				.list().get(0);
		session.close();
		return transactionOrderReceive;
	}
	@Autowired
	private TransactionOrderItemService transactionOrderItemService;
	@Autowired
	private ReceiveService receiveService;
	@Autowired
	private IssueService issueService;

	public  TransactionEntity getTransactionEntity(TransactionOrder tod){
		TransactionEntity trE = new TransactionEntity(); 
		try {
			System.out.println("Order id = "+tod.getId());
			List <TransactionOrderItem> items = transactionOrderItemService.getTransactionOrderItem(tod.getId());
			System.out.println("items size = "+items.size());
			trE.setItemIds(items);
			trE.setStockKeepingUnitId(items.get(0).getStockKeepingUnitId());
			  
			
			// Order elements   
			trE.setTodId(tod.getId()); 
			trE.setOrderDate(tod.getOrderDate());
			trE.setItemsQuantity(tod.getItemsQuantity());
			trE.setTransactionTime(tod.getTransactionTime());
			trE.setSiteAdminId(tod.getSiteAdminId());
			trE.setTransactionTypeId(tod.getTransactionTypeId());
			trE.setStockKeepingUnitDes(skuSer.getSKUById(trE.getStockKeepingUnitId()).getDescription() );
			trE.setSiteAdminName(tod.getSiteAdminName());
			trE.setUnitTypeId(items.get(0).getUnitTypeId());
			
			if (tod.getTransactionTypeId()==1){
				List<TransactionOrderIssue> issueOrders = issueService.getAllChidOrders(tod.getId());
				System.out.println(issueOrders.size());
				trE.setIorderids(issueOrders);
				trE.setOwnerId(issueOrders.get(0).getOwnerId());
				trE.setCustomerId(issueOrders.get(0).getCustomerId());
				trE.setCustomerName(contactSer.getContactOfCustomer(trE.getCustomerId()).getFirstName()+" "+contactSer.getContactOfCustomer(trE.getCustomerId()).getLastName());
				trE.setOwnerName(contactSer.getContactOfOwner(trE.getOwnerId()).getFirstName()+" "+contactSer.getContactOfOwner(trE.getOwnerId()).getLastName());
			}
			else if (tod.getTransactionTypeId()==2){
				List<TransactionOrderReceive> receiveOrders = receiveService.getAllChidOrders(tod.getId());
				
				System.out.println("Order "+tod.getId()+" have :"+receiveOrders.size()+"  Order");
				trE.setRorderids(receiveOrders);
				trE.setOwnerId(receiveOrders.get(0).getOwnerId());
				trE.setOwnerName(contactSer.getContactOfOwner(trE.getOwnerId()).getFirstName()+" "+contactSer.getContactOfOwner(trE.getOwnerId()).getLastName());
			}
			
					//Order Items elements 
			
			
			
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return trE;
		
	}
}