package com.robovics.wims.ui.contact;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.exceptions.OrderException;
import com.robovics.wims.model.enums.ErrorsEnum;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.contact.ContactService.ContactView;
import com.robovics.wims.model.service.staff.StaffService.StaffView;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ContactUI extends UI {
	ContactService contactService = null;
	List<ContactView> all_contacts = null;
	int currentIndex = 0;
	int allContactsize = 0;
	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
 	Action delete;
 	

	@SuppressWarnings("deprecation")
	public Layout buildForm() {
		Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
		 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
		 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
		 final Button b1 = new Button(this.getMessage(GasDiaryMessages.NewCont));
		 delete = new Action(this.getMessage(GasDiaryMessages.Delete));
		 final Button saveBtn = new Button(this.getMessage(GasDiaryMessages.Save));
		 Button cancelBtn = new Button(this.getMessage(GasDiaryMessages.CancelKey));
		 contactService = (ContactService) ServiceFactory.getInstance()
				.getService(ServicesEnum.CONTACT_SERVICE);
		
		List<ContactView> contacts = null;
		all_contacts = contactService.getAllContacts();
		allContactsize = all_contacts.size();

		System.err.println(allContactsize);
		contacts = getListOfContact();

		HorizontalLayout mainLayout = new HorizontalLayout();

		VerticalLayout toolLayout = new VerticalLayout();
		
		b1.setId("toolLayout_b1");

		HorizontalLayout searchLayout = new HorizontalLayout();
		TextField Fname = new TextField();
		Fname.setImmediate(true);
		Fname.setInputPrompt(this.getMessage(GasDiaryMessages.firstName));
		TextField Lname = new TextField();
		Lname.setImmediate(true);
		Lname.setInputPrompt(this.getMessage(GasDiaryMessages.lastName));
		searchLayout.addComponent(Fname);
		searchLayout.addComponent(Lname);
		searchLayout.setId("searchLayout");

		
		
		HorizontalLayout pLayout = new HorizontalLayout();
		pLayout.addComponent(b1);
		final ComboBox TypeCombo = new ComboBox(null, Arrays.asList("Customer",
				"Owner", "Supplier"));
		TypeCombo.select("Customer");
		TypeCombo.setId("toolLayout_b1");
		
		pLayout.addComponent(TypeCombo);
		toolLayout.addComponent(pLayout);
		toolLayout.addComponent(searchLayout);

		VerticalLayout tablelayout = new VerticalLayout();
		final BeanItemContainer<ContactView> beans = new BeanItemContainer<ContactView>(
				ContactView.class);
		beans.addAll(contacts);
		final Table table = new Table("", beans);
		table.setPageLength(contacts.size());
		table.setBuffered(false);
		table.setVisibleColumns(new Object[] { "firstName", "lastName",
				"mobileNumber", "address", "type" });
		table.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.firstName), this.getMessage(GasDiaryMessages.lastName),
				this.getMessage(GasDiaryMessages.mobileNumber), this.getMessage(GasDiaryMessages.Address), this.getMessage(GasDiaryMessages.Type)});
		tablelayout.addComponent(table);
		
		final Window ContactWindow = new Window();
		ContactWindow.center();
		ContactWindow.setHeight("300px");
		ContactWindow.setWidth("400px");
		ContactWindow.setResizable(false);
		ContactWindow.setDraggable(false);
		ContactWindow.setCaption(this.getMessage(GasDiaryMessages.ContactWindowCaption));
		
		
		
		table.addActionHandler(new Handler() {
			

			@Override
			public void handleAction(Action action, Object sender,
					final Object target) {
				if (action == delete) {
					
					int tableLength = table.getPageLength();
					try {
						ContactView contact = (ContactView)  target;
						contactService.deleteContact(contact);
						table.removeItem(target);
						table.setPageLength(tableLength - 1);
						Notification.show("Info",
								"Record deleted successfully",
								Notification.Type.TRAY_NOTIFICATION);
						all_contacts.remove(contact);
						allContactsize--;
					} catch (DatabaseException e) {
						table.setPageLength(tableLength);
						Notification.show("Error", e.getMessage(),
								Notification.Type.TRAY_NOTIFICATION);
					}
				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				ArrayList<Action> actions = new ArrayList<Action>();
				// Enable deleting only if clicked on an item
				if (target != null)
					actions.add(delete);
				return (Action[]) (actions.toArray(new Action[actions.size()]));
			}
		});

		final FormLayout formLayout = new FormLayout();
	    PropertysetItem itemsProperty = new PropertysetItem();
	    
		itemsProperty.addItemProperty("firstName", new ObjectProperty<String>(
				""));
		
		itemsProperty.addItemProperty("lastName",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("mobileNumber",
				new ObjectProperty<String>(""));
		itemsProperty
				.addItemProperty("address", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("type", new ObjectProperty<String>(""));

		final FieldGroup form = new FieldGroup(itemsProperty);
		Field<?> f;
		f = form.buildAndBind(this.getMessage(GasDiaryMessages.firstName), "firstName");
		f.setRequired(true);
		formLayout.addComponent(f);
		f = form.buildAndBind(this.getMessage(GasDiaryMessages.lastName), "lastName");
		f.setRequired(true);
		formLayout.addComponent(f);
		f = form.buildAndBind(this.getMessage(GasDiaryMessages.mobileNumber),"mobileNumber");
		f.setRequired(true);
		formLayout.addComponent(f);
		formLayout.addComponent(form.buildAndBind(this.getMessage(GasDiaryMessages.Address), "address"));
		
		
       
		formLayout.setVisible(false);
		form.setBuffered(true);
				formLayout.setSpacing(true);
				formLayout.setMargin(true);
			//	tablelayout.addComponent(formLayout);
				
				
				ContactWindow.setContent(formLayout);
			    
				
				 table.addListener(new ItemClickListener() {

			         @Override
			         public void itemClick(ItemClickEvent event) {
			          if (event.getButton()!=ItemClickEvent.BUTTON_RIGHT)
			          {
			        	  ContactWindow.setCaption("Update Contact");
			          getCurrent().addWindow(ContactWindow); 
			          }
			         }
			    });

		HorizontalLayout hNextpervios = new HorizontalLayout();
		hNextpervios.setSizeUndefined();
		Button perviousb = new Button("<<");
		hNextpervios.addComponent(perviousb);
		Button nextb = new Button(">>");
		hNextpervios.addComponent(nextb);
		nextb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<ContactView> contacts = getListOfContact();
				beans.removeAllItems();
				beans.addAll(contacts);
				table.setData(contacts);
				table.setPageLength(contacts.size());
				table.commit();
			}
		});
		perviousb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<ContactView> contacts = getPerviousListOfContact();
				beans.removeAllItems();
				beans.addAll(contacts);
				table.setData(contacts);
				table.setPageLength(contacts.size());
				table.commit();
			}
		});
		tablelayout.addComponent(hNextpervios);

		table.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				// Close the form if the item is deselected
				if (event.getProperty().getValue() == null) {
					//formLayout.setVisible(false);
					ContactWindow.close();
					return;
				}
				// Bind the form to the selected item
				form.setItemDataSource(table.getItem(table.getValue()));
				formLayout.setVisible(true);
				
				
				// The form was opened for editing an existing item
				table.setData(null);
			}
		});

		table.setSelectable(true);
		table.setImmediate(true);
		tablelayout.setId("tablelayout");

		b1.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				// Create a new item, this will create a new bean
				
				ContactView contact = contactService.new ContactView();
				contact.setFirstName("");
				contact.setLastName("");
				contact.setAddress("");
				contact.setMobileNumber("");
				contact.setType((String) TypeCombo.getValue());

				beans.addBean(contact);
				table.setPageLength(table.getPageLength() + 1);
				form.setItemDataSource(table.getItem(contact));
				// Make the form a bit nicer
				// form.setVisibleItemProperties(new Object[]{"code",
				// "description", "liftingNumber", "lifeTimeYears",
				// "lifeTimeMonths", "lifeTimeDays"});
				// The form was opened for editing a new item
				table.setData(contact);
				table.select(contact);
				table.setEnabled(false);
				b1.setEnabled(false);
                //formLayout.setVisible(true);
				
				getCurrent().addWindow(ContactWindow);
			}
		});

		
		saveBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				ContactView contact = contactService.new ContactView();
				contact.setFirstName((String) form.getField("firstName")
						.getValue());
				
				contact.setLastName((String) form.getField("lastName")
						.getValue());
				contact.setMobileNumber((String) form.getField("mobileNumber")
						.getValue());
				contact.setAddress((String) form.getField("address").getValue());
				contact.setType((String) TypeCombo.getValue());
				try {
					if (form.getField("firstName").getValue() == ""){
						throw (new OrderException(14));
					}
					else if (form.getField("lastName").getValue() == ""){
						throw (new OrderException(15));
					}else if(form.getField("mobileNumber").getValue() == "") {
						throw (new OrderException(20));
					}
						
				form.commit();
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
				}catch (OrderException e) {
					Notification.show(
							ErrorsEnum.values()[e.getType() - 1].valueAr,
							Type.ERROR_MESSAGE);
						return;
				} catch (CommitException e1) {
					Notification
							.show("Error",
									"Error saving Staff, please return to technical support",
									Notification.Type.TRAY_NOTIFICATION);
				}
				formLayout.setVisible(false); // and close it
				ContactWindow.close();
				// New items have to be added to the container
				table.commit();
				table.setEnabled(true);
				
				ContactView nContact = (ContactView) table.getValue();
				try {
					if (nContact.getId() == null) {
						Contact c = new Contact();
						
						c.setFirstName(nContact.getFirstName());
						c.setLastName(nContact.getLastName());
						c.setAddress(nContact.getAddress());
						c.setMobileNumber(nContact.getMobileNumber());
						contactService.addContact(c,
								(String) TypeCombo.getValue());
						Notification.show("Info",
								"Record inserted successfully",
								Notification.Type.TRAY_NOTIFICATION);
					} else {
						Contact c = new Contact();
						c.setId(nContact.getId());
						c.setFirstName(nContact.getFirstName());
						c.setLastName(nContact.getLastName());
						c.setAddress(nContact.getAddress());
						c.setMobileNumber(nContact.getMobileNumber());
						contactService.updateContact(c);
						Notification.show("Info",
								"Record updated successfully",
								Notification.Type.TRAY_NOTIFICATION);
					}
				} catch (BusinessException e) {
					Notification.show("Error", e.getMessage(),
							Notification.Type.TRAY_NOTIFICATION);
				}
				b1.setEnabled(true);
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
			}
		});
		HorizontalLayout formFooter = new HorizontalLayout();
		formFooter.addComponent(saveBtn);

		
		cancelBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.discard(); // Not really necessary
				formLayout.setVisible(false); // and close it
				ContactWindow.close();
				ContactView contact = (ContactView) table.getValue();
				if (contact.getId() == null) {
					beans.removeItem(contact);
					table.setPageLength(table.getPageLength() - 1);
				} else
					table.unselect(contact);

				table.discard(); // Discards possible addItem()
				table.setEnabled(true);
				b1.setEnabled(true);
			}
		});

		formFooter.addComponent(cancelBtn);
		formLayout.addComponent(formFooter);
		formLayout.setComponentAlignment(formFooter, Alignment.BOTTOM_CENTER);
		
		//ContactWindow.setContent(formLayout);

		Fname.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("firstName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});

		Lname.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("lastName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});
		Fname.setImmediate(true);
		Lname.setImmediate(true);

		mainLayout.setWidthUndefined();
		mainLayout.addComponent(toolLayout);
		mainLayout.addComponent(tablelayout);
		return mainLayout;
	}

	@Override
	protected void init(VaadinRequest request) {
	}

	public List<ContactView> getListOfContact() {
		ArrayList<ContactView> lStaff = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i < allContactsize; i++) {
			try {
				lStaff.add(all_contacts.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lStaff;
	}

	public List<ContactView> getPerviousListOfContact() {
		ArrayList<ContactView> lStaff = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i >= 0; i--) {
			try {
				lStaff.add(all_contacts.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lStaff;
	}
	
	 @Override
	    public void setLocale(Locale locale) {
	        super.setLocale(locale);
	        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	    }
	    
	    /** Returns the bundle for the current locale. */
	    public ResourceBundle getBundle() {
	        return i18nBundle;
	    }
	 
	    /**
	     * Returns a localized message from the resource bundle
	     * with the current application locale.
	     **/ 
	    public String getMessage(String key) {
	        return i18nBundle.getString(key);
	    }

}
