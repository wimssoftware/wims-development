package com.robovics.wims.model.service.masterData;

public class BINSCOUNT {
	
	public BINSCOUNT( String id_, Long count_) {
		// TODO Auto-generated constructor stub
		id = id_ ;
		count = count_;
	}
	public BINSCOUNT(int binId_, String id_, Long count_) {
		// TODO Auto-generated constructor stub
		id = id_ ;
		binID = binId_;
		count = count_;
	}
	public String  id ; 
	public int binID;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public Long count ;
}
