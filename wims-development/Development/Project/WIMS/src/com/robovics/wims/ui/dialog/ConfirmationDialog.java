package com.robovics.wims.ui.dialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ConfirmationDialog extends Window implements ClickListener {

	private static final long serialVersionUID = 7641644576559190559L;
	private Callback callback;
	private Button yes = new Button("Yes", this);
	private Button no = new Button("No", this);
	private VerticalLayout container = new VerticalLayout();

	public ConfirmationDialog(String caption, String question, Callback callback) {
		super(caption);

		setModal(true);

		this.callback = callback;

		if (question != null)
			container.addComponent(new Label(question));

		HorizontalLayout layout = new HorizontalLayout();
		layout.addComponent(yes);
		layout.addComponent(no);
		layout.setMargin(true);
		container.addComponent(layout);

		container.setMargin(true);

		setContent(container);
	}

	public void buttonClick(ClickEvent event) {
		if (getParent() != null)
			((UI) getParent()).removeWindow(this);
		callback.onDialogResult(event.getSource() == yes);
	}

	public interface Callback {

		public void onDialogResult(boolean resultIsYes);
	}
}