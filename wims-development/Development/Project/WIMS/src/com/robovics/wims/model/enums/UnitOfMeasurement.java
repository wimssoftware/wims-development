package com.robovics.wims.model.enums;

public enum UnitOfMeasurement {

	PALLETE(1),Carton(2) ;

	public final int type;

	private UnitOfMeasurement(int type_) {
		this.type = type_;
	}
}