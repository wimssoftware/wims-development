package com.robovics.wims.model.orm.entities.masterData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		/*@NamedQuery(name = "CurrentBinItem_getBySKUId", query = " select distinct c.groupBinId from  CurrentBinItem c ,"
				+ "BinSlotConfiguration bsc , BinSlot s where "
		+ " c.transactionOrderItemSerial in (select r.serial from TransactionOrderItem i , ReceiveItemBin r "
		+ "where i.stockKeepingUnitId = :P_SKU_ID and r.transactionOrderItemId = i.id ) "
		+ " and c.slotId = s.id and s.binSlotConfigurationId =bsc.id ") ,*/
		@NamedQuery(name = "CurrentBinItem_getbySlotId", query = " select distinct c.id from  CurrentBinItem c "
				+ "where c.slotId = :P_SOT_ID ") ,
	
		@NamedQuery(name = "topaletid", query = " select distinct c.id from  CurrentBinItem c "
						+ " , TransactionOrderItem t " 
						+ "where c.slotId =:P_SOT_ID and t.isContaining =:P_ONE"
						) ,
						
		@NamedQuery(name = "CurrentBinItem_getbyItemId", query = " select distinct c.id from  CurrentBinItem c "
						+ "where c.itemId = :P_itemId ") ,
						
		@NamedQuery(name = "SLot_getBySKUId", query = " select distinct r.slotId "
				+ " from ReceiveItemBin r , TransactionOrderItem t where "
				+ "t.id =r.transactionOrderItemId and t.stockKeepingUnitId = :P_SKU_ID and r.serial not in  "
				+ "(Select distinct i.serial from IssueItemBin i )   ") ,
		@NamedQuery(name = "CurrentBinItem_getAllItemBySiteNo", query = " select c.id from CurrentBinItem c where "
				+ " c.siteId= :P_SiteNo ") ,
		@NamedQuery(name = "CurrentBinItem_getAllSlots", query = " select distinct c.slotId from CurrentBinItem c where " 
				+ "c.siteId= :P_SiteNo ") ,
		@NamedQuery(name = "CurrentBin_getCount", query = " select  count(c.transactionOrderItemSerial)"+
				" from CurrentBinItem c where c.siteId= :P_SiteNo and c.slotId = :P_GBN_ID"
						) ,
		@NamedQuery(name = "GetOwnerIdofSlot", query = " select c.ownerId "+
				" from CurrentBinItem c "+
				" where c.slotId = :P_slot_id"
						) ,
		@NamedQuery(name = "GetSlotIdOfItem", query = " select c.slotId "+
				" from CurrentBinItem c "+
				" where c.itemId = :P_item_id"
										) ,

		@NamedQuery(name = "GetNoOfCartonsInSlotId", query = " select count(c.itemId) "+
				" from CurrentBinItem c "+
				" where c.slotId = :P_slot_id"
										) ,																		

		@NamedQuery(name = "GetAllCartonsInSlotId", query = " select distinct c.itemId "+
				" from CurrentBinItem c , TransactionOrderItem i "+
				" where c.slotId = :P_slot_id and c.transactionOrderItemSerial <> 'noSerial' "
										) ,																		

		@NamedQuery(name = "GetItemInSlotId", query = " select c.itemId "+
				" from CurrentBinItem c "+
				" where c.slotId = :P_slot_id"
										) ,								
		
		@NamedQuery(name = "CurrentBin_getSerials", query = " select distinct c.transactionOrderItemSerial"+
				" from CurrentBinItem c where  c.slotId = :P_GBN_ID"
						)  
							
		})

		



@Table(name = "Current_Bin_Item")
@Entity
public class CurrentBinItem extends BaseEntity {

	private static final long serialVersionUID = 8894738645619309623L;

	private Integer id;
	private Integer siteId;
	private String transactionOrderItemSerial;
	private Integer slotId;
	private Integer groupBinId;
	private Integer ownerId;
	private Integer itemId;	
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CBI_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	@Column(name = "CBI_TOI_Id")
	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	
	@Column(name = "CBI_STE_Id")
	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	@Column(name = "CBI_TOI_Serial")
	public String getTransactionOrderItemSerial() {
		return transactionOrderItemSerial;
	}

	public void setTransactionOrderItemSerial(String transactionOrderItemSerial) {
		this.transactionOrderItemSerial = transactionOrderItemSerial;
	}

	@Column(name = "CBI_SOT_Id")

	public Integer getSlotId() {
		return slotId;
	}

	public void setSlotId(Integer slotId) {
		this.slotId = slotId;
	}
	
	@Column(name = "CBI_OWN_Id")
	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	@Transient
	public Integer getGroupBinId() {
		return groupBinId;
	}

	public void setGroupBinId(Integer groupBinId) {
		this.groupBinId = groupBinId;
	}
}