package com.robovics.wims.model.orm.entities.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.web.bindery.autobean.shared.AutoBean.PropertyName;
import com.robovics.wims.model.orm.entities.BaseEntity;
import com.vaadin.data.fieldgroup.PropertyId;

@Table(name = "Contact")
@Entity
public class Contact extends BaseEntity {

	private static final long serialVersionUID = -7706837287217019036L;
	private Integer id;
	@PropertyId("firstName")
	private String firstName;
	private String lastName;
	private String mobileNumber;
	private String address;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CNT_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CNT_Fname")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "CNT_Lname")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "CNT_MobileNumber")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "CNT_Address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}