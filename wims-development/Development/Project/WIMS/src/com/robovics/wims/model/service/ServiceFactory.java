package com.robovics.wims.model.service;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.robovics.wims.model.service.issue.IssueService;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.receive.ReceiveService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.model.service.transaction.TransactionEntityService;
import com.robovics.wims.model.service.transaction.execute.TransactionExecuteIssueService;
import com.robovics.wims.model.service.transaction.execute.TransactionExecuteReceiveService;
import com.robovics.wims.model.service.transaction.itemBin.ReceiveItemBinService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderTransferService;
import com.robovics.wims.model.service.transaction.orderItem.TransactionOrderItemService;
import com.robovics.wims.model.service.masterData.BinSlotService;

public class ServiceFactory {

	public static enum ServicesEnum {

		MASTER_DATA_SERVICE("masterDataService"), STAFF_SERVICE("staffService"), SKU_SERVICE(
				"stockKeepingUnitService"), TRANSACTION_ORDER_SERVICE(
				"transactionOrderService"), TRANSACTION_ORDER_ITEM_SERVICE(
				"transactionOrderItemService"), TRANSACTION_EXECUTE_RECEIVE_SERVICE(
				"transactionExecuteReceiveService"), RECEIVE_ITEM_BIN_SERVICE(
				"receiveItemBinService"), CONTACT_SERVICE("contactService"), RECEIVE_SERVICE(
				"receiveService"), SITES_BINS_ITEMS("sitesBinsItemsService"),TRANSACTION_ORDER_ISSUE("issueService"),
				TRANSACTION_ENTITY_SERVICE("transactionEntityService"),BIN_SLOT_Service("slotService"),
				TRANSACTION_EXECUTE_ISSUE_SERVICE("transactionExecuteIssueService"),
				TRANSACTION_ORDER_TRANSFER_SERVICE("transactionOrderTransferService"), QUALITY_STATUS_SERVCE("qualityStatusService");

		private final String name;

		private ServicesEnum(String name) {
			this.name = name;
		}
	}

	private AnnotationConfigApplicationContext context;
	private static ServiceFactory instance;

	private ServiceFactory() {
		context = new AnnotationConfigApplicationContext();
		context.register(StockKeepingUnitService.class,
				MasterDataService.class, StaffService.class,
				TransactionOrderService.class,
				TransactionOrderItemService.class, ReceiveItemBinService.class,
				TransactionExecuteReceiveService.class, ContactService.class,
				ReceiveService.class, IssueService.class ,BinSlotService.class ,
				TransactionEntityService.class, TransactionExecuteIssueService.class , 
				TransactionOrderTransferService.class, QualityStatusServices.class );
		
		context.refresh();
	}

	public static ServiceFactory getInstance() {
		if (instance == null)
			instance = new ServiceFactory();
		return instance;
	}

	public Object getService(ServicesEnum service) {
		return context.getBean(service.name);
	}
}
