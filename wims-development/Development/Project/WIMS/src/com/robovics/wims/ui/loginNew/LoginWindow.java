package com.robovics.wims.ui.loginNew;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.staff.Staff;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.ui.staff.WareHouseUI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("wims")
public class LoginWindow extends UI {

	
	@WebServlet(value = "/Login/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = LoginWindow.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		getPage().setTitle("Login");
		setContent(buildForm());
	}

	private Layout buildForm() {
		VerticalLayout vlayout = new VerticalLayout();
		Panel panel = new Panel();
		panel.setSizeUndefined();
		vlayout.addComponent(panel);

		// Create custom layout from "layoutname.html" template.
		CustomLayout custom = new CustomLayout("login_temp");
		custom.addStyleName("customlayoutexample");

		// Use it as the layout of the Panel.
		panel.setContent(custom);

		// Create a few components and bind them to the location tags
		// in the custom layout.
		final TextField username = new TextField();
		custom.addComponent(username, "username");

		final TextField password = new TextField();
		custom.addComponent(password, "password");

		Button ok = new Button("Login");
		ok.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				StaffService ss = new StaffService();
				try {
					List<Integer> site_id = ss.authenticateStaff(
							username.getValue(), password.getValue());
					if (site_id!=null && !site_id.isEmpty()) {
							if (VaadinSession.getCurrent()
								.getAttribute("user_priviladge").equals("1")) {
//							getUI().getNavigator().navigateTo(WareHouseUI.NAME);
							 getUI().getPage().setLocation("WPHI");
						} else if (VaadinSession.getCurrent()
								.getAttribute("user_priviladge").equals("2")) {
							getUI().getPage().setLocation("AdminPage");
							// getUI().getNavigator().navigateTo("AdminPage");
						}} else {
						Notification not = new Notification(
								"Notification error",
								"login failed' please try again");
						not.show(Page.getCurrent());
					}
				} catch (BusinessException e) {
					Notification not = new Notification("Notification error", e
							.getMessage());
					not.show(Page.getCurrent());
					e.printStackTrace();
				}

			}
		});
		custom.addComponent(ok, "okbutton");

		return vlayout;
	}

}
