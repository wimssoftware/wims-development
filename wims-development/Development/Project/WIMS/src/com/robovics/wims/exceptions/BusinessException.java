package com.robovics.wims.exceptions;

public class BusinessException extends Exception {

	private static final long serialVersionUID = -4802887938013524376L;

	public BusinessException(String message) {
		super(message);
	}
	
}