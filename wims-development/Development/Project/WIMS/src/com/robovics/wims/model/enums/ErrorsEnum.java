package com.robovics.wims.model.enums;

public enum ErrorsEnum {
	/* Order Exception Errors **/
	QUANTITYERROR(1,"Quantity must be positive Number","الكمية لا بد ان تكون اكبر من الصفر"),
	TransactionTimeError(2,"You must Set Transaction Time","لا بد من تحديد وقت تنفيذ العملية"),
	OwnerError(3,"You must define owner","لا بد من تحديد المالك"),
	SKUError(4,"You must define the SKU","لا بد من تحديد كود الصنف"),
	UnitTupeError(5,"You must define the unit type","لا بد من تحديد الوحد بالتة او كرتونة"),
	TransactionTypeError(6,"You must Define the Transaction Type","لا بد من تحديد نوع العملية"),
	CustomerError(7,"You must Define the Customer","لا بد من تحديد العميل"),
	OwnerBalanceError(8,"Owner balance doesnot allow the operation ","عفوا.. رصيد هذا العميل لا يسمح باتمام العمليه"),
	/* Order Exception Errors **/
	SLotChoosenBefore(9,"This Slot has been Choosen before","عفوا.. لقد تم اختيار هذة القطعة من الحارة من قبل "),
	BinUndefined(10,"You must choose the bin ","عفوا.. لا بد من تحديد الحارة "),
	SlotUndefined(11,"You must choose the Slot ","عغوا .. لا بد من تحديد لقطعة داخل الحارة "),
	QsUndefined(12,"You must choose the Quality Status ","عفوا .. لا بد من تحديد حالة القطعة  "),
	ExceedUndefined(13,"You will Exceed Quantity Either next or cancel ","ستتم تجاوز كمية القطع لهذا الطلب يمكنك التجاوز او الالغاء  "),
	EmptyFirsetName(14 , "You Must Write The First Name" , "عفوا.. لا بد من كتاية الاسم الاول"),
	EmptyLastName(15 , "You Must Write The Last Name" , "عفوا.. لا بد من كتاية الاسم الاخير"),
	SiteHaveNoEnoughSpace(16 , "you have no enough space" , "عفوا..  لا يوجد مساحة كافية لتنفيذ هئا النوع "),
	PLEASESCANPALETS(17 , "please rescan the palet Serial" , "عفوا..  برجاء اعادة مسح البالته "),
	DOUBLE_SCAN_FOR_SAME_ITEM(18 , "please rescan the palet Serial" , "عفوا..  برجاء اعادة مسح البالته لأنك قمت بأدخال نفس البالتة اكتر من مرة "),
	SCAN_FOR_SAME_ITEM_DOES_NOT_EXEST(19 , "please rescan the palet Serial" , "عفوا..  برجاء اعادة مسح البالته لأنك قمت بأدخال بالتة غير صحيحة "),
	MOBILE_IS_MANDATORY(20, "Mobile number is a mandatory field", "عفوا.. لابد من ادخال رقم المحمول");
	
	
	
	public final int id;
	public final String valueEn;
	public final String valueAr;
	
	private ErrorsEnum(int id,String Value,String ValueAr) {
		this.id = id;
		this.valueEn=Value ;
		this.valueAr=ValueAr ;
	}
}