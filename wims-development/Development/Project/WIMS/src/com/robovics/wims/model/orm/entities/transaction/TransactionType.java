package com.robovics.wims.model.orm.entities.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Transaction_Type")
@Entity
public class TransactionType extends BaseEntity {

	private static final long serialVersionUID = -6122590442464757924L;
	private Integer id;
	private String description;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TTP_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TTP_Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}