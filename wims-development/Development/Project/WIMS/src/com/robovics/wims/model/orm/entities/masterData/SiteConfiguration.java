package com.robovics.wims.model.orm.entities.masterData;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Site_Configuration")
@Entity
public class SiteConfiguration extends BaseEntity {

	private static final long serialVersionUID = 6091764364048759069L;
	private Integer id;
	private Integer siteId;
	private Date dateTime;
	private Integer active;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CFG_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CFG_STE_Id")
	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	@Column(name = "CFG_DateTime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	
	@Column(name = "CFC_Active")
	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}
}