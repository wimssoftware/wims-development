package com.robovics.wims.model.orm.entities.transaction.execute;

import com.google.gwt.junit.client.WithProperties.Property;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.staff.StaffService;
import com.vaadin.data.fieldgroup.PropertyId;

public class SelectedSlot {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8931373777889625530L;
	@PropertyId (value = "id")
	private Integer id;
	@PropertyId (value = "groupSymbol")
	private String groupSymbol;
	@PropertyId (value = "binNo")
	private Integer binNo;
	@PropertyId (value = "x")
	private Integer x;
	@PropertyId (value = "y")
	private Integer y;	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGroupSymbol() {
		return groupSymbol;
	}
	public void setGroupSymbol(String groupSymbol) {
		this.groupSymbol = groupSymbol;
	}
	public Integer getBinNo() {
		return binNo;
	}
	public void setBinNo(Integer binNo) {
		this.binNo = binNo;
	}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}

}
