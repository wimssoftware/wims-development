package com.robovics.wims.model.orm.entities.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;
import com.vaadin.data.fieldgroup.PropertyId;

@NamedQueries(value = {
		
		@NamedQuery(name = "getSKUsOfOwner", query = "SELECT distinct s.id FROM CurrentBinItem c, StockKeepingUnit s"
				+ ", TransactionOrderItem t , ReceiveItemBin r WHERE s.id = t.stockKeepingUnitId"
				+ " and c.transactionOrderItemSerial = r.serial and r.transactionOrderItemId = t.id"
				+ " and c.ownerId = :P_OWN and c.siteId= :P_STE_NO " ),
		@NamedQuery(name = "getAvailableBalanceOfOwner", query = "SELECT count(c.id) FROM CurrentBinItem c, StockKeepingUnit s,"
				+ " TransactionOrderItem t , ReceiveItemBin r  WHERE s.id = t.stockKeepingUnitId and c.transactionOrderItemSerial"
				+ " = r.serial and r.transactionOrderItemId = t.id "
				+ "and c.ownerId = :P_OWN and c.siteId=:P_STE_NO and s.id =:P_SKU_Id" )
					
				//	 >=:P_Date
		})


@Table(name = "Owner")
@Entity
public class Owner extends BaseEntity {
	
	private static final long serialVersionUID = -6635980421803548893L;
	private Integer id;
	private Integer contactId;
	@PropertyId("name")
	private String name ;
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "OWN_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "OWN_CNT_Id")
	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	

	@Transient
	public String getName() {
		return name;
	}

	public void setName(String symbol) {
		this.name = symbol;
	}
}