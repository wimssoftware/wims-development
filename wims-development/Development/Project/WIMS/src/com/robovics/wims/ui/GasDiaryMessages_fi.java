package com.robovics.wims.ui;

import com.robovics.wims.model.orm.entities.masterData.Site;


public class GasDiaryMessages_fi extends GasDiaryMessages {
    private static final long serialVersionUID = 8250423030306357522L;

    @Override
    public Object[][] getContents() {
        return contents_fi;
    }
    static final Object[][] contents_fi = {
       
    	{switchLanguage , "تغيير اللغة"},
    	{ChooseOneFromthefollowing,"اختر  من الاتي"},
    	{WarehouseAdmin , "مدير المخازن"},
    	{SiteAdmin , "مشرف المخرن"},
    	{SiteOrders , "الطلبات"},
    	{AllSitesNames , "جميع اسماء المخازن"},
    	{back , "الرجوع"},
    	
    	{Reports , "التقارير"},
    	{LogOut , "تسجيل الخروج"},
    	{NewCont , "عميل حديد"},
    	{Delete , "حذف"},
    	{NewStaff , "عامل جديد"},
    	{CancelKey, "إلغاء"},
        {Save, "حفظ"},
        {NewContract , "عقد جديد"},
        {Update , "تعديل"},
        {NewSite , "موقع جديد"},
        {NewBin , "حارة جديده"},
        {NewSKU, "صنف جديد"},
        {ConfigureBin , "تعديل الحارة"},
        
        {code , "الرمز"},
        {Description , "الوصف"},
        {MaxCarton , "الحد الأقصى للكرتون"},
        {DefaultQualityStatus , "حالة الجودة الافتراضية"},
        {StackingNumber , "الحمولة"},
        {LifeTimeDays , "مدة الصلاحية باليوم"},       
        {SKUWindowCaption , "اضافة صنف جديد"},
        
        {firstName,"الاسم الاول"},
        {lastName, "الاسم الاخير"},
        {mobileNumber , "رقم الهاتف المحمول"} ,  
        {Address, "العنوان"},
        {Type, "النوع"},
        {ContactWindowCaption, "اضافة عميل جديد"},
        
        {SiteName, "اسم المخزن"},
        {StartDate, "تاريخ بداية العمل"},
        {EndDate, "تاريخ انتهاء العمل"},
        {Username, "اسم المستخدم"},
        {Password, "الرقم السري"},
        {Email, "البريد الالكتروني"},
        {privilege , "صلاحيات"},
        {StaffWindowCaption,"اضافة عامل جديد"},
    	 
        {Active , "فعال"}, 
        {Date , "التاريخ"},
        {symbols , "الرمز"},
        {Bins,"الحارات"},
        {serial, "الرقم المسلسل"},
        {slot , "الخانة"},
        {ItemName , "وصف الصنف"},
        {ItemCode , "رمز الصنف"},
        {width,"العرض"},
        {height,"الارتفاع"},
        {lenght,"الطول"},
        {SiteAddress , "عنوان المخزن"},
        {ActiveORinActive , "فعال/غير فعال"},
        {BinWindowCaption,"اضافة حاره جديدة"},
        {BinName , "اسم الحاره"},
        {BinNumber,"رقم الحاره"},
        {Sites, "المخازن"},
        {ConfigurationDate ,"تاريخ التعديل"},
        {SiteWindowCaption,"اضافة مخزن جديد"},
        {UpdateSite , "تعديل المخزن"},
        {UpdateBin,"نعديل الحاره"},
       
        {OwnerName,"اسم المالك"},
        {SKU ,"الصنف"},
        {ContractWindwoCaption,"اضافة عقد جديد"},
        {ContractDate,"تاريخ العقد"},
        
        {ClearAll ,"اعادة تعيين"},
        {Done ,"تم"},
        {Quantity,"الكمية"},
        {Edit,"تعديل"},
        {customer, "اسم العميل"},
        {OrderDate,"تاريخ الطلب"},
        {Setting, "الاعدادات"},
        {NotExecutedOrder,"طلبات لم يتم تنفيذها"},
        {NewOrder, "طلب جديد"},
        {From , "من"},
        {To, "الى"},
        {ProductionDate , "تاريخ الصنع"},
        {TransactionType,"نوع العملية"},
        {BatchNo, "رقم الباتش"},
        {TransactionDate,"تاريخ التنفيذ"},
        {UnitOfMeasurement,"وجدة القياس"},
        {CurrentIncubatedSlots,"الخانات الممتلئة حاليا"},
        {CurrentIncubatedPallets,"البالتات الممتلئة حاليا"},
        {CreateNewPallet,"اضافة بالته جديده"},
        
        
        
        
        {refresh,"تحديث"},
        {siteAdminName,"اسم مدير المخزن"},
        {ClickonRowtoExcutetheOrder,"اضغط على الصف لتنفيذ الطلب"},
        {FromBin,"من الحاره"},
        {FromSlotlength,"من الطول"},
        {FromSlotHeight,"من الارتفاع"},
        {ToBin,"الى الحاره"},
        {ToSlotlength,"الى الطول"},
        {ToSlotHeight,"الى الارتفاع"},
        
        {QualityStatus,"حالة الجودة"},
        {Scan ,"مسح"},
        {EnterMAnually,"الادخال يدويا"},
        {SlotSerial,"الرقم المسلسل للخانة"},
        {CartonNo,"عدد الكراتين"},
        {ItemSerial,"الرقم المسلسل للصنف"},
        {ExpirationDate,"تاريح الانتهاء"},
        
        {next,"التالي"},
        {EnterSerial,"ادخل الرقم المسلسل"},
        {AddCarton,"اضافة كرتون"},
        {ChooseSlot,"اختر الخانة"},
        
    	{OkKey, "OK"},
        
        {Reset, "Palauta"},
    
        // Application
        {AppTitle, "Bensapäivyri"},
        
        // LoginScreen
       
        {Login, "Sisäänkirjautuminen"},
        {LoginButton, "Kirjaudu"},
        {RegisterNewUser, "Rekisteröidy uudeksi käyttäjäksi"},
        {ForgotPassword, "Unohditko salasanasi?"},
        {InvalidUserOrPassword, "Väärä käyttäjätunnus tai salasana"},
        {InvalidUserOrPasswordLong, "Annettua käyttäjätunnusta ei ole olemassa tai sanasana on virheellinen"},
        {DemoUsernameHint, "Anna demo/demo esittelykirjautumista varten"},
        
        // UserEditor
        {UserNameError, "Virheellinen tai puuttuva käyttäjätunnus. Käyttäjätunnus saa sisältää vain pienellä kirjoitettuja kirjaimia a-z, numeroita ja alaviivan (_)."},
        {InvalidPasswordFormat, "Salasana voi sisältää kaikkia merkkejä paitsi välilyöntejä ja on oltava 6-20 merkkiä pitkä."},
        {PasswordAgain, "Salasana uudelleen"},
        {PasswordHint, "Salasanavihje"},
        
        {InvalidEmail, "Kelvoton sähköpostiosoite"},
        
        // UserView
        {MyCars, "Omat autot"},
        {NewCar, "Uusi auto..."},
        {EditCar, "Muokkaa..."},
        {DeleteCar, "Poista"},
        {CarDetails, "Auton tiedot"},

        // CarView
        {Fills, "Täytöt"},
        {NewFill, "Uusi täyttö"},
        {EditFill, "Muokkaa täyttöä"},
        {DateTimeFormat, "yyyy-MM-dd HH:mm"},
        {DateShort, "Päivämäärä"},
        {Station, "Huoltoasema"},
        {AmountShort, "Määrä"},
        {PartialFillShort, "Osittainen"},
        {TotalPriceShort, "Yhteensä"},
        {MeterShort, "Mittari"},
        {TripMeterShort, "Trippi"},
        {TripResetShort, "Nollattu"},
        
        // CarEditor
        {RegisterNumber, "Rekisterinumero"},
        {RegisterNumberError, "Rekisterinumero täytyy antaa ja sen täytyy olla käypä: kirjaimia, viiva ja numeroita. Esimerkiksi, ABC-123"},
        {MustBeGiven, "Täytyy antaa, ei saa olla tyhjä"},
        {MustBeInteger, "Täytyy olle käypä vuosiluku"},
        {Manufacturer, "Valmistaja"},
        {Model, "Malli"},
        {Year, "Vuosi"},
        
        // FillEditor
        {DateAndTime, "Päivämäärä ja aika"},
        {NotADate, "Syöte ei ole kelvollinen päivämäärä ja kellonaika"},
        {TooOldDate, "Päivämäärä on mahdottoman vanha"},
        {DateMustBeGiven, "Päivämäärä täytyy antaa, kellonaika on valinnainen (asetetaan keskiyöhön)"},
        {StationMustBeGiven, "Asema täytyy antaa"},
        {Amount, "Määrä (l)"},
        {MustBeAmount, "Määrän täytyy olla lukema kahdella desimaalilla käyttäen pilkkua desimaalierottimena, esim. 56,00."},
        {AmountMustBeGiven, "Määrä täytyy antaa"},
        {TotalPrice, "Kokonaishinta (€)"},
        {MustBePrice, "Hinnan täytyy olla lukema kahdella desimaalilla käyttäen pilkkua desimaalierottimena. Esim. 56,00."},
        {TotalMustBeGiven, "Kokonaishinta täytyy antaa"},
        {PartialFill, "Osittainen täyttö"},
        {Meter, "Matkamittarin lukema (km)"},
        {MustBeKmReading, "Syötteen täytyy olla kilometrilukema ilman desimaaleja, esim. ######."},
        {TripMeter, "Trippimittarin lukema (km)"},
        {MustBeKmTripReading, "Syötteen täytyy olla kilometrilukema korkeintaan yhdellä desimaalilla, esim. 123,4."},
        {TripReset, "Trippimittari nollattiin täytön jälkeen"},
        {Comment, "Kommentti"},
        {CommentPrompt, "Kirjoita valinnainen kommentti"},
        
        // FillEditor: station editor
        {NewStation, "Uusi huoltoasema"},
        {StationName, "Aseman nimi"},
        {EmptyStationName, "Aseman nimi ei saa olla tyhjä"},
    };
}