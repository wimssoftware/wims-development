package com.robovics.wims.model.service;

public class Palet_Slot_CartonCount_isnew {

	private Integer Palet_Id;
	private Integer Slot_Id;
	private Integer CartonCount;
	private boolean isNew;
	
	public Integer getPalet_Id() {
		return Palet_Id;
	}
	public void setPalet_Id(Integer palet_Id) {
		Palet_Id = palet_Id;
	}
	public Integer getSlot_Id() {
		return Slot_Id;
	}
	public void setSlot_Id(Integer slot_Id) {
		Slot_Id = slot_Id;
	}
	public Integer getCartonCount() {
		return CartonCount;
	}
	public void setCartonCount(Integer cartonCount) {
		CartonCount = cartonCount;
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	
	
}
