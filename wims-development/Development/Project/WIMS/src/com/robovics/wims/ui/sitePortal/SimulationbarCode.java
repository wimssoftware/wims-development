package com.robovics.wims.ui.sitePortal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.TextField;
import com.vaadin.annotations.Theme;
import com.vaadin.client.ui.orderedlayout.Slot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.masterData.BinSlot;
import com.robovics.wims.model.orm.entities.masterData.BinSlotConfiguration;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.SerialRecored;
import com.robovics.wims.model.orm.entities.masterData.SiteConfigurationBinGroup;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.BinSlotService;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.ui.GasDiaryMessages;
@SuppressWarnings("serial")
@Theme("wims")
public class SimulationbarCode extends Window {
	
	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
 	
	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	BinSlotService slotService = null ;
	    TextField txt;
        public Button Next ;
        public Button Done;
        public Button AddCarton;
        ComboBox SlotIDCombobox ;
        private MasterDataService mainService = new MasterDataService();
        List<SerialRecored> sRecords = new ArrayList<>(); 
        List <TextField> txt_ = new ArrayList<>();
        int count = 0 ;
        
        public SimulationbarCode(int type,int siteid) throws DatabaseException
        {
        	if (((Locale)VaadinSession.getCurrent().getAttribute("Locale"))==null){
    		 	i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
    		 	i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
    		 	VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
    	 }else {
    		Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
    		 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
    		 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
    	 }
        	 txt = new TextField(this.getMessage(GasDiaryMessages.EnterSerial));
             Next = new Button(this.getMessage(GasDiaryMessages.next));
             Done = new Button(this.getMessage(GasDiaryMessages.Done));
             AddCarton = new Button(this.getMessage(GasDiaryMessages.AddCarton));
             SlotIDCombobox = new ComboBox(this.getMessage(GasDiaryMessages.ChooseSlot));
        	if (type==1){//Pallet
        	VerticalLayout v1 = new VerticalLayout();
        	slotService = (BinSlotService) ServiceFactory.getInstance()
    				.getService(ServicesEnum.BIN_SLOT_Service);
        	List<Integer> Slotsid = mainService.GetAllSlotsInSiteConfig(mainService.GetLastSiteConfigIdForSite(siteid));
        	List<BinSlot> Slots = new ArrayList<BinSlot>(Slotsid.size());
        	for(Integer i : Slotsid){
        		BinSlot temp = dao.getEntityById(i, BinSlot.class, "id");
        		BinSlotConfiguration slotConfig = dao.getEntityById(temp.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
    			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
    			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
    			temp.setSlotSymbol(groupConfig.getSymbol()+group.getBinNumber()+ " " + temp.getY()+"_"+temp.getZ());
        		Slots.add(temp);
        	}
        	        	
        	BeanItemContainer<BinSlot> beansSlots = new BeanItemContainer<BinSlot>(BinSlot.class);			
        	beansSlots.addAll(Slots);
        	SlotIDCombobox.setContainerDataSource(beansSlots);
        	SlotIDCombobox.setItemCaptionPropertyId("slotSymbol"); 
        	v1.addComponents(txt , SlotIDCombobox , Next , Done);
        	
        	v1.setMargin(true);
        	v1.setSpacing(true);
     		
        	 
        	 /*Action Listeners*
        	  */
        	 Done.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					try {
						mainService.addnewSerialToBarCode(sRecords);
						dao = DAOFactory.getInstance().getDataAccess();
						//Page.getCurrent().getJavaScript().execute("window.location.reload();");
						close();
						
					} catch (BusinessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
        	 Next.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					// TODO Auto-generated method stub
					try {
					Integer slotId = ((BinSlot) SlotIDCombobox.getValue()).getId();
					String  serial  = txt.getValue();
					SerialRecored s = new SerialRecored();
					s.setSerial(serial);
					
					s.getInfoFromSerial();
					
					sRecords.add(s); 
					s= new SerialRecored(); 
					s.setSerial(slotId+"");
					sRecords.add(s);
					SlotIDCombobox.setValue(null);
					txt.setValue("");
					Notification.show("Added Successfully");
					}catch(Exception e){
						Notification.show("خطا في الادخال ",Notification.TYPE_ERROR_MESSAGE);
						SlotIDCombobox.setValue(null);
						txt.setValue("");
						return ; 
					}
				}
			});
     		this.setContent(v1);
        	 this.center();
        	 this.setWidth("400px");
        	 this.setHeight("300px");
	
        }else if (type == 2){//receive carton
        	final VerticalLayout v1 = new VerticalLayout();
        	slotService = (BinSlotService) ServiceFactory.getInstance()
    				.getService(ServicesEnum.BIN_SLOT_Service);
        	List<Integer> Slotsid = mainService.GetAllSlotsInSiteConfig(mainService.GetLastSiteConfigIdForSite(siteid));
        	List<BinSlot> Slots = new ArrayList<BinSlot>(Slotsid.size());
        	for(Integer i : Slotsid){
        		BinSlot temp = dao.getEntityById(i, BinSlot.class, "id");
        		BinSlotConfiguration slotConfig = dao.getEntityById(temp.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
    			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
    			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
    			temp.setSlotSymbol(groupConfig.getSymbol()+group.getBinNumber()+ " " + temp.getY()+"_"+temp.getZ());
        		Slots.add(temp);
        	}
        	
        	BeanItemContainer<BinSlot> beansSlots = new BeanItemContainer<BinSlot>(BinSlot.class);			
        	beansSlots.addAll(Slots);
        	
        	 SlotIDCombobox.setContainerDataSource(beansSlots);
        	 SlotIDCombobox.setItemCaptionPropertyId("slotSymbol"); 
        	 v1.addComponents(SlotIDCombobox,AddCarton,Next,Done);
        	
        	 v1.setMargin(true);
        	 v1.setSpacing(true);
     		
        	 
        	 /*Action Listeners*
        	  */
        	 AddCarton.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					// TODO Auto-generated method stub
					txt_.add(new TextField("Enter Serial"));
					v1.addComponent(txt_.get(count) );
					//setHeight(getHeight(),Unit.PIXELS);
					count++;
				}
			});
        	 Done.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					try {
						mainService.addnewSerialToBarCode(sRecords);
						dao = DAOFactory.getInstance().getDataAccess();
						//Page.getCurrent().getJavaScript().execute("window.location.reload();");
						close();
						
					} catch (BusinessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
        	 Next.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					// TODO Auto-generated method stub
					try {
					Integer slotId = ((BinSlot) SlotIDCombobox.getValue()).getId();
					
					SerialRecored s = new SerialRecored();
					
					s.setSerial(slotId+"");
					sRecords.add(s);
					SlotIDCombobox.setValue(null);
					
					for (int i = 0 ; i<count;i++){
					String  serial  = txt_.get(i).getValue();
					s= new SerialRecored(); 
					s.setSerial(serial);
					s.getInfoFromSerial();
					sRecords.add(s); 
					}
					txt.setValue("");
					txt_= new ArrayList<>();
					Notification.show("Added Successfully");
					}catch(Exception e){
						Notification.show("خطا في الادخال ",Notification.TYPE_ERROR_MESSAGE);
						SlotIDCombobox.setValue(null);
						txt.setValue("");
						return ; 
					}
				}
			});
     		this.setContent(v1);
        	 this.center();
        	 this.setWidth("400px");
        	 this.setHeight("300px");
        }
        	}
        
        @Override
        public void setLocale(Locale locale) {
            super.setLocale(locale);
            i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
        }
        
        /** Returns the bundle for the current locale. */
        public ResourceBundle getBundle() {
            return i18nBundle;
        }
     
        /**
         * Returns a localized message from the resource bundle
         * with the current application locale.
         **/ 
        public String getMessage(String key) {
            return i18nBundle.getString(key);
        }
        
}
