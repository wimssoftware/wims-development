package com.robovics.wims.ui;


public class GasDiaryMessages_en extends GasDiaryMessages {
    private static final long serialVersionUID = -7071215163996244034L;

    @Override
    public Object[][] getContents() {
        return contents_en;
    }
    static final Object[][] contents_en = {
        
    	{switchLanguage , "Switch Language"},
    	{ChooseSiteName,"Choose Site Name"},
    	{ChooseOneFromthefollowing,"Choose Portal of the following"},
    	{WarehouseAdmin , "Warehouse Admin"},
    	{SiteAdmin , "Site Admin"},
    	{SiteOrders , "Site Orders"},
    	{AllSitesNames , " All Sites Names"},
    	{back , "Back"},
    	
    	{Reports , "Reports"},
    	{LogOut , "LogOut"}, 
    	
    	{NewCont , "New Contact"},
    	{Delete,"Delete"},
    	{NewStaff, "New Staff"},
    	{CancelKey, "Cancel"},
        {Save, "Save"},
        {NewContract , "New Contract"},
        {Update , "Update"},
        {NewSite , "New Site"},
        {NewBin , "New Bin"},
        {NewSKU , "New SKU"},
        {ConfigureBin , "Configure Bin"},
        
        {code , "Code"},
        {Description , "Description"},
        {MaxCarton , "Max Carton"},
        {DefaultQualityStatus , "Default Quality Status"},
        {StackingNumber , "Stacking Number"},
        {LifeTimeDays , "LifeTime Days"},        
        {SKUWindowCaption , "Add New SKU"},
        
        {firstName,"First Name"},
        {lastName, "Last Name"},
        {mobileNumber , "Mobile Number"} ,  
        {Address, "Address"},
        {Type, "Type"},
        {ContactWindowCaption,"Add New Contact"},
        
        {SiteName, "Site Name"},
        {StartDate, "Start Date"},
        {EndDate, "End Date"},
        {Username, "Username"},
        {Password, "Password"},
        {Email, "Email Address"},
        {privilege , "Privilege"},
        {StaffWindowCaption,"Add New Staff"},
    	
        {Active , "Active"},
        {Date , "Date"},
        {symbols, "Symbol"},
        {Bins ,"Bins"},
        {serial, "Serial"},
        {slot , "Slot"},
        {ItemName , "Item Name"},
        {ItemCode , "Item Code"},
        {width,"Width"},
        {height,"Height"},
        {lenght,"Lenght"},
        {SiteAddress,"Site Address"},
        {ActiveORinActive,"Active/inActive"},
        {BinWindowCaption , "Add New Bin"},
        {BinName,"Bin Name"},
        {BinNumber,"Bin Number"},
        {Sites , "Sites"},
        {ConfigurationDate,"Configuration Date"},
        {SiteWindowCaption,"Add New Site"},
        {UpdateSite,"Update Site"},
        {UpdateBin,"UpdateBin"},
        
        {OwnerName,"Owner Name"},
        {SKU , "SKU"},
        {ContractWindwoCaption,"Add New Contract"},
        {ContractDate,"Contract Date"},
        
        {ClearAll,"Clear All"},
        {Done , "Done"},
        {Quantity,"Quantity"},
        {Edit,"Edit"},
        {customer , "Customer"},
        {OrderDate,"Order Date"},
        {Setting,"Setting"},
        {NotExecutedOrder,"Not Executed Order"},
        {NewOrder,"New Order"},
        {From,"From"},
        {To,"To"},
        {ProductionDate,"Production Date"},
        {TransactionType,"Transaction Type"},
        {BatchNo,"Batch No"},
        {TransactionDate,"Transaction Date"},
        {UnitOfMeasurement,"Unit Of Measurement"},
        {CurrentIncubatedSlots,"Current Incubated Slots"},
        {CurrentIncubatedPallets,"Current Incubated Pallets"},
        {CreateNewPallet , "Create New Pallet"},
        
        
        {refresh,"Refresh"},
        {siteAdminName,"Site Admin Name"},
        {ClickonRowtoExcutetheOrder,"Click on Row to Execute the Order"},
        {FromBin,"From Bin"},
        {FromSlotlength,"From Slot Length"},
        {FromSlotHeight,"From Slot Height"},
        {ToBin,"To Bin"},
        {ToSlotlength,"To Slot Length"},
        {ToSlotHeight,"To Slot Height"},
        {QualityStatus,"Quality Status"},
        {Scan,"Scan"},
        {EnterMAnually,"Enter Manually"},
        {SlotSerial ,"Slot Serial"},
        {CartonNo,"Cartons No"},
        {ItemSerial,"Item Serial"},
        {ExpirationDate,"Expiration Date"},
        
        {next,"Next"},
        {EnterSerial,"Enter Serial"},
        {AddCarton,"Add Carton"},
        {ChooseSlot,"Choose Slot"},
        
        {OkKey, "OK"},
        
        {Reset, "Reset"},
    
        
        // Application
        {AppTitle, "Gas Diary"},
        
        // LoginScreen
      
        {Login, "Login"},
        {LoginButton, "Login"},
        {RegisterNewUser, "Register a new user"},
        {ForgotPassword, "Forgot your password?"},
        {InvalidUserOrPassword, "Invalid user or password"},
        {InvalidUserOrPasswordLong, "A user with the given user name does not exist or the password is incorrect"},
        {DemoUsernameHint, "Use demo/demo for a demonstration"},

        // UserEditor
        {UserNameError, "Invalid or missing username. Username may contain only lower-case alphabets, numbers, and underscore (_)."},
        {InvalidPasswordFormat, "Password can contain any letters except spaces and be 6-20 characters long."},
        {PasswordAgain, "Password Again"},
        {PasswordHint, "Password Hint"},
       
        {InvalidEmail, "Invalid email address"},
        
        // UserView
        {MyCars, "My Cars"},
        {NewCar, "New Car..."},
        {EditCar, "Edit Car..."},
        {DeleteCar, "Delete Car"},
        {CarDetails, "Car Details"},
        
        // CarView
        {Fills, "Fills"},
        {NewFill, "New Fill"},
        {EditFill, "Edit Fill"},
        {DateTimeFormat, "yyyy/MM/dd HH:mm"},
        {DateShort, "Date"},
        {Station, "Station"},
        {AmountShort, "Amount"},
        {PartialFillShort, "Partial"},
        {TotalPriceShort, "Total"},
        {MeterShort, "Meter"},
        {TripMeterShort, "Trip"},
        {TripResetShort, "Reset?"},
        
        // CarEditor
        {RegisterNumber, "Register Number"},
        {RegisterNumberError, "Register must be given and be a valid register number: letters, dash, and numbers, for example, ABC-123"},
        {MustBeGiven, "Must be given, may not be empty"},
        {MustBeInteger, "Must be a proper year number"},
        {Manufacturer, "Manufacturer"},
        {Model, "Model"},
        {Year, "Year"},
        
        // FillEditor
        {DateAndTime, "Date and Time"},
        {NotADate, "Not a Date"},
        {TooOldDate, "Date is unrealistically old"},
        {DateMustBeGiven, "Date must be given, time is optional (is set to midnight)"},
        {StationMustBeGiven, "Station must be given"},
        {Amount, "Amount (l)"},
        {MustBeAmount, "Must be a positive amount ##.##, with period as decimal separator"},
        {AmountMustBeGiven, "Amount must be given"},
        {TotalPrice, "Total Price"},
        {MustBePrice, "Must be a price ##.## �, with period as decimal separator"},
        {TotalMustBeGiven, "Total price must be given"},
        {PartialFill, "Partial fill"},
        {Meter, "Meter reading"},
        {MustBeKmReading, "Must be a kilometer reading ######"},
        {TripMeter, "Trip meter reading"},
        {MustBeKmTripReading, "Must be a kilometer reading with at most one decimal ###.#"},
        {TripReset, "Was trip meter reset after filling?"},
        {Comment, "Comment"},
        {CommentPrompt, "Enter an optional comment"},
        
        // FillEditor: station editor
        {NewStation, "New Station"},
        {StationName, "Station Name"},
        {EmptyStationName, "Station name must not be empty"},
    };
}