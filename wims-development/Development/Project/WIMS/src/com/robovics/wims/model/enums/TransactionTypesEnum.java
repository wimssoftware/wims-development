package com.robovics.wims.model.enums;

public enum TransactionTypesEnum {

	ISSUE(1), RECEIVE(2), TRANSFER(3);

	public final int id;

	private TransactionTypesEnum(int id) {
		this.id = id;
	}
}