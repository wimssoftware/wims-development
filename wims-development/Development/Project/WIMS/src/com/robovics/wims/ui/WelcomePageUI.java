package com.robovics.wims.ui;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
@Theme("wims")
public class WelcomePageUI extends UI {
	Button switchLanguage;
	Button WarehouseAdmin;
	Button SiteAdmin;
	Button SiteOrders;
	ComboBox AllSitesNames;
	Button back;

	ResourceBundle i18n; // Automatic Localization
	ResourceBundle i18nBundle; // Automatic localization

	MasterDataService masterDataService = null;

	@WebServlet(value = { "/*" }, asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = WelcomePageUI.class)

	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		if (((Locale) VaadinSession.getCurrent().getAttribute("Locale")) == null) {
			i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
			i18n = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
			VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
		} else {
			Locale c = ((Locale) VaadinSession.getCurrent().getAttribute("Locale"));
			i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
			i18n = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
		}
		switchLanguage = new Button(this.getMessage(GasDiaryMessages.switchLanguage));
		WarehouseAdmin = new Button(this.getMessage(GasDiaryMessages.WarehouseAdmin));
		SiteAdmin = new Button(this.getMessage(GasDiaryMessages.SiteAdmin));
		SiteOrders = new Button(this.getMessage(GasDiaryMessages.SiteOrders));
		AllSitesNames = new ComboBox(this.getMessage(GasDiaryMessages.AllSitesNames));
		back = new Button(this.getMessage(GasDiaryMessages.back));
		setContent(BuildUI());
	}

	public VerticalLayout BuildUI() {
		masterDataService = (MasterDataService) ServiceFactory.getInstance()
				.getService(ServicesEnum.MASTER_DATA_SERVICE);
		List<Site> sites = masterDataService.getAllSites();
		final BeanItemContainer<Site> beansSitess = new BeanItemContainer<Site>(Site.class);
		beansSitess.addAll(sites);
		final Panel MainPanel = new Panel();
		MainPanel.setCaption(this.getMessage(GasDiaryMessages.ChooseOneFromthefollowing));
		VerticalLayout vertical = new VerticalLayout();
		VerticalLayout MainLayout = new VerticalLayout();
		HorizontalLayout header = new HorizontalLayout();
		HorizontalLayout footer = new HorizontalLayout();
		header.addComponent(new Image(null, new ThemeResource("NTLC-Logo.JPG")));
		header.addComponent(switchLanguage);

		footer.addComponent(new Image(null, new ThemeResource("roboVics.png")));

		MainPanel.setHeight("400px");
		MainPanel.setWidth("400px");
		WarehouseAdmin.setWidth("200px");
		WarehouseAdmin.setHeight("100px");
		SiteAdmin.setWidth("200px");
		SiteAdmin.setHeight("100px");
		SiteOrders.setWidth("200px");
		SiteOrders.setHeight("100px");
		vertical.addComponent(WarehouseAdmin);
		vertical.addComponent(SiteAdmin);
		vertical.addComponent(SiteOrders);
		vertical.addComponent(AllSitesNames);
		vertical.addComponent(back);
		vertical.setComponentAlignment(back, Alignment.MIDDLE_CENTER);
		AllSitesNames.setVisible(false);
		back.setVisible(false);
		vertical.setComponentAlignment(WarehouseAdmin, Alignment.BOTTOM_CENTER);
		vertical.setComponentAlignment(SiteAdmin, Alignment.BOTTOM_CENTER);
		vertical.setComponentAlignment(SiteOrders, Alignment.BOTTOM_CENTER);
		vertical.setComponentAlignment(AllSitesNames, Alignment.MIDDLE_CENTER);
		vertical.setMargin(true);
		vertical.setSpacing(true);
		MainLayout.addComponent(header);
		MainLayout.addComponent(MainPanel);
		MainLayout.addComponent(footer);
		MainPanel.setContent(vertical);
		MainLayout.setComponentAlignment(MainPanel, Alignment.MIDDLE_CENTER);

		WarehouseAdmin.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("WPHI");

			}
		});

		SiteAdmin.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().setLocation("AdminPage");

			}
		});
		SiteOrders.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				WarehouseAdmin.setVisible(false);
				SiteAdmin.setVisible(false);
				SiteOrders.setVisible(false);
				AllSitesNames.setVisible(true);
				back.setVisible(true);
			}
		});

		AllSitesNames.setContainerDataSource(beansSitess);
		AllSitesNames.setItemCaptionPropertyId("name");
		AllSitesNames.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				getUI().getPage().setLocation("siteOrders?siteId=" + ((Site) AllSitesNames.getValue()).getId());
			}
		});

		back.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				WarehouseAdmin.setVisible(true);
				SiteAdmin.setVisible(true);
				SiteOrders.setVisible(true);
				AllSitesNames.setVisible(false);
				back.setVisible(false);
			}
		});

		switchLanguage.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				setLocale(new Locale("en"));
				// rebuild it with the new locale.
				if (((Locale) VaadinSession.getCurrent().getAttribute("Locale")).getLanguage().equals("fi")) {
					VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
				} else {
					VaadinSession.getCurrent().setAttribute("Locale", new Locale("fi"));
					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("fi"));
				}
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
			}
		});

		return MainLayout;
	}

	@Override
	public void setLocale(Locale locale) {
		super.setLocale(locale);
		i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	}

	/**
	 * Returns a localized message from the resource bundle with the current
	 * application locale.
	 **/
	public String getMessage(String key) {
		return i18nBundle.getString(key);
	}
}
