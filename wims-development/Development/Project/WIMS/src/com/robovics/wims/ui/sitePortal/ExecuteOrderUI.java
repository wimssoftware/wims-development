package com.robovics.wims.ui.sitePortal;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.robovics.wims.user_session.user_session;

import javax.servlet.annotation.WebServlet;

import org.eclipse.jdt.core.dom.ThisExpression;

import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.masterData.BINSCOUNT;
import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.exceptions.ExecuteException;
import com.robovics.wims.model.enums.ErrorsEnum;
import com.robovics.wims.model.enums.QualityStatusEnum;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Customer;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.masterData.BinSlot;
import com.robovics.wims.model.orm.entities.masterData.BinSlotConfiguration;
import com.robovics.wims.model.orm.entities.masterData.CurrentBinItem;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteIssue;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteReceive;
import com.robovics.wims.model.orm.entities.transaction.itemBin.IssueItemBin;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.BinSlotService;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.masterData.BinSlotService.BinSlotDetails;
import com.robovics.wims.model.service.masterData.MasterDataService.CurrentBinsFilterationParameter;
import com.robovics.wims.model.service.receive.ReceiveService;
import com.robovics.wims.model.service.transaction.execute.TransactionExecuteIssueService;
import com.robovics.wims.model.service.transaction.execute.TransactionExecuteReceiveService;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.ui.admin.adminMainUI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Item;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Container;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.ClassResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionDestroyEvent;
import com.vaadin.server.SessionDestroyListener;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.Calendar;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


@SuppressWarnings("serial")
@Theme("wims")


public class ExecuteOrderUI extends Window{
	
	private MasterDataService mainService = new MasterDataService();
	int count = 0 ; 
	int SiteNo = 15 ; 
	int todId ; 
	int Quantity; 
	
	public int getQuantity() {
		return Quantity;
	}
	public void setQuantity(int quantity) {
		Quantity = quantity;
	}
	public int getTodId() {
		return todId;
	}
	public void setTodId(int todId) {
		this.todId = todId;
	}
	public int getSiteNo() {
		return SiteNo;
	}
	public void setSiteNo(int siteNo) {
		SiteNo = siteNo;
	}

	private StockKeepingUnit currentSKU;
	
	public StockKeepingUnit getCurrentSKU() {
		return currentSKU;
	}
	
	public void setCurrentSKU(StockKeepingUnit currentSKU) {
		this.currentSKU = currentSKU;
	}
	
	private List <TransactionOrderItem> itemIds;
	
	public List<TransactionOrderItem> getItemIds() {
		return itemIds;
	}
	
	public void setItemIds(List<TransactionOrderItem> itemIds) {
		this.itemIds = itemIds;
	}
	
	public List<TransactionOrderIssue> getIorderids() {
		return Iorderids;
	}
	public void setIorderids(List<TransactionOrderIssue> iorderids) {
		Iorderids = iorderids;
	}
	public List<TransactionOrderReceive> getRorderids() {
		return Rorderids;
	}
	public void setRorderids(List<TransactionOrderReceive> rorderids) {
		Rorderids = rorderids;
	}

	private List <TransactionOrderIssue> Iorderids;
	private List <TransactionOrderReceive> Rorderids;

	private List <ReceiveItemBin> ribS = new ArrayList<>();
	private List <IssueItemBin> iibS = new ArrayList<>();
	private List <CurrentBinItem> cbiS = new ArrayList<>();
	private List <TransactionExecuteIssue> teiS = new ArrayList<>();
	private List <TransactionExecuteReceive> terS = new ArrayList<>();
	private HorizontalLayout headerLayout = new HorizontalLayout();
	private HorizontalLayout footerLayout = new HorizontalLayout();
	private VerticalLayout HLayout = new VerticalLayout();
	private FormLayout ExecuteFormLayout= new FormLayout();
	private HorizontalLayout ListHorizontalLayout = new HorizontalLayout(); 
	private ListSelect SerialsList = new ListSelect("List Of All Serials");
	private Button Read = new Button("Read");
	private Button Next = new Button("Next");
	private Button Delete = new Button("Delete");
	private Button bprevious = new Button("<<");
	private Button bnext = new Button(">>");
	//@PropertyId("")
	private DateField productionDate = new DateField("Production Date");
	private HorizontalLayout hNextPrev = new HorizontalLayout();
	PropertysetItem itemsProperty = new PropertysetItem();
	private int transactionType ; 
	private int unitType ; 	
	private  MasterDataService masterService;
	private List<GroupBin> groubBins ; 
	BeanItemContainer<GroupBin> groubBinsBean ;
	
	private  BinSlotService binSlotService;
	private List< BinSlot> binSlots ; 
	BeanItemContainer<BinSlot> binSlotsBean ;
	private List< Integer> selectedBinSlots = new ArrayList<Integer>(); 
	
	public int getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}
	public int getUnitType() {
		return unitType;
	}
	public void setUnitType(int unitType) {
		this.unitType = unitType;
	}

	@PropertyId("binNo")
	private ComboBox BinNoText = new ComboBox("Bin");
	
	@PropertyId("slotNo")
	private ComboBox SlotNoText = new ComboBox("Slot");
	
	@PropertyId("availablePallet")
	private ComboBox AvailablePallet = new ComboBox("Available Pallet");
	
	@PropertyId("qualityStatus")
	private ComboBox QualityStatus = new ComboBox("Quality Status");
	
	@PropertyId("serial")
	private TextField SerialText = new TextField("Serial");
			
	public ExecuteOrderUI(){
		masterService = (MasterDataService) ServiceFactory.getInstance()
				.getService(ServicesEnum.MASTER_DATA_SERVICE);
		
		
		
		groubBinsBean = new BeanItemContainer<GroupBin>(GroupBin.class);
		
		binSlotService = (BinSlotService) ServiceFactory.getInstance()
				.getService(ServicesEnum.BIN_SLOT_Service);
		
		binSlotsBean = new BeanItemContainer<BinSlot>(BinSlot.class);
		
		BinNoText.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				// TODO Auto-generated method stub
				if(event.getProperty().getValue()== null){
						return;
				}
				
				GroupBin selectedGbn =(GroupBin)event.getProperty().getValue();
				System.out.println(selectedGbn.getId());
				if (transactionType==2){
					List<BinSlotDetails> emptySlots = binSlotService.getDetailsOfEmptySlotOfSite(SiteNo);
					for(BinSlotDetails b: emptySlots){
						binSlots.add(binSlotService.getBinSlotById(b.getSlotid()));
					}
					//binSlots = binSlotService.getEmptySlotOfBin(selectedGbn.getId(), new Date ());
				for(BinSlot sl : binSlots){
					System.out.print(sl.getId()+"sdf");
				}
				}else if(transactionType==1){
					binSlots =binSlotService.getFullSlotOfBin(selectedGbn.getId(), new Date());	
					for(BinSlot sl : binSlots){
						System.out.print(sl.getId()+" :");
					}
				}
				if (binSlots == null | binSlots.isEmpty() ){
					System.out.println("Nullllll");
					//bins =masterService.getEmptyBinsBySite(1);
				}
				binSlotsBean.removeAllItems();
				binSlotsBean.addAll(binSlots);
				SlotNoText.setContainerDataSource(binSlotsBean);
				SlotNoText.setItemCaptionPropertyId("slotSymbol");
			}
		});
		
		List <String> QSTs =Arrays.asList("Released","Quantiled","Blocked");
		QualityStatus.setContainerDataSource(new IndexedContainer(QSTs));
	}
	
	public Layout integrateAllLayouts () throws DatabaseException
	{
		VerticalLayout Vlay = new VerticalLayout();
		HorizontalLayout MainLayout = new HorizontalLayout();
		VerticalLayout tempLayout = new  VerticalLayout();	 
		Delete.setVisible(false);
		ExecuteFormLayout = IntergrateFormLayout();
		SerialsList = IntegrateListSelect();
		//tempLayout.addComponent(ExecuteFormLayout);
		hNextPrev.addComponent(bprevious);
		hNextPrev.addComponent(bnext);
		Vlay.addComponent(SerialsList);
		Vlay.addComponent(hNextPrev);
		MainLayout.addComponent(ExecuteFormLayout);
		MainLayout.addComponent(Vlay);
		tempLayout.addComponent(MainLayout);
		tempLayout.addComponent(Next);
		tempLayout.setComponentAlignment(Next, Alignment.BOTTOM_CENTER);
		//MainLayout.setComponentAlignment(SerialsList, Alignment.MIDDLE_RIGHT);
		Next.addClickListener(new Button.ClickListener() {

			   @Override
			   public void buttonClick(ClickEvent event) {
				TransactionExecuteReceiveService terService = (TransactionExecuteReceiveService) ServiceFactory.getInstance()
						.getService(ServicesEnum.TRANSACTION_EXECUTE_RECEIVE_SERVICE);
				TransactionExecuteIssueService teiService = (TransactionExecuteIssueService) ServiceFactory.getInstance()
						.getService(ServicesEnum.TRANSACTION_EXECUTE_ISSUE_SERVICE);
				
				
				try {
					 if (transactionType == 2){
					terService.submitExecution(terS, ribS, todId , cbiS);
					 }
					 else  if (transactionType == 1){
				   teiService.submitExecution(teiS, iibS, todId , cbiS);	 
					 }
				} catch (BusinessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			   getUI().getPage().setLocation("laborLogin");
				  				     
				  
				   
			   }
			  });
		
		Read.addClickListener(new Button.ClickListener() {
			 
				 @Override
			   public void buttonClick(ClickEvent event) {
						ReceiveItemBin rib = new ReceiveItemBin();
						IssueItemBin iib = new IssueItemBin();
						 TransactionExecuteReceive tre = new TransactionExecuteReceive();
						 TransactionExecuteIssue tri = new TransactionExecuteIssue();
						 CurrentBinItem cbI =new CurrentBinItem();
			
				try{
						if (SlotNoText.getValue()==null){ throw(new ExecuteException(11));}
						 if (QualityStatus.getValue()==null){ throw(new ExecuteException(12));} 
						 
						if (selectedBinSlots.contains(((BinSlot)SlotNoText.getValue()).getId())){
							throw(new ExecuteException(9));
						}
						if (Quantity==count){
							throw(new ExecuteException(ErrorsEnum.ExceedUndefined.id));
						}
			selectedBinSlots.add(((BinSlot)SlotNoText.getValue()).getId());   
			  if (transactionType == 2){
				  
				  SerialsList.addItem(SerialText.getValue());
				  rib.setSerial(SerialText.getValue());
				  rib.setSlotId(((BinSlot)SlotNoText.getValue()).getId());
				  tre.setSiteLaborId(1);
				  tre.setTransactionOrderReceiveId(Rorderids.get(count).getId());
				  rib.setTransactionOrderItemId(itemIds.get(count).getId());
				  cbI.setSiteId(SiteNo);
				  cbI.setSlotId(rib.getSlotId());
				  cbI.setTransactionOrderItemSerial(rib.getSerial());
				  cbI.setOwnerId(Rorderids.get(count).getOwnerId());
				  //rib.setQstId(getFromString((String)QualityStatus.getValue()));
				  rib.setProduDate((Date) productionDate.getValue());
				  count++;
				  ribS.add(rib);
				  terS.add(tre);
				  cbiS.add(cbI);
				  
				 
			  }else if (transactionType == 1){
				  try {
					
					  CurrentBinItem cbi = masterService.getCBIBySlotId(((BinSlot)SlotNoText.getValue()).getId());
					  String srial = cbi.getTransactionOrderItemSerial();
					  SerialsList.addItem(srial);
					  iib.setSerial(srial);
					  iib.setSlotId(((BinSlot)SlotNoText.getValue()).getId());
					  tri.setSiteLaborId(1);
					  tri.setTransactionOrderIssueId(Iorderids.get(count).getId());
					  iib.setTransactionOrderItemId(itemIds.get(count).getId());
					 
					  //iib.setQstId(getFromString((String)QualityStatus.getValue()));					
					  count++;
					  iibS.add(iib);
					  teiS.add(tri);
					  cbiS.add(cbi);
				
					  
				  } catch (UnsupportedOperationException | BusinessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				  //Remove redandunt enter. 
				  
			  }
				}catch(ExecuteException e){
					Notification.show(ErrorsEnum.values()[e.getType()-1].valueAr,Type.ERROR_MESSAGE);
					return ;
				}
			  
			
			   SerialText.setValue(""); 
			   productionDate.setValue(null);
			  BinNoText.setValue(null);
			  SlotNoText.setValue(null);
			  AvailablePallet.setValue(null);
			  QualityStatus.setValue(null);
			  binSlotsBean.removeAllItems();
			  
				   
			   }
			  });
		
		MainLayout.setMargin(true);
		MainLayout.setSpacing(true);
		tempLayout.setMargin(true);
		tempLayout.setSpacing(true);
		return tempLayout;
	}
	
	
	
	private FormLayout IntergrateFormLayout()
	{
		if (this.transactionType==2){
			groubBins = masterService.getGroupBins(this.SiteNo, new Date()) ;	
		}else if (this.transactionType==1){
			try {
				groubBins = masterService.getCurrentBinsBySKUId(this.currentSKU.getId(),this.SiteNo);
				System.out.println("bEa "+groubBins.size());
			} catch (BusinessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		groubBinsBean.removeAllItems();
		groubBinsBean.addAll(groubBins);
		BinNoText.setContainerDataSource(groubBinsBean);
		BinNoText.setItemCaptionPropertyId("symbol");
		
		HLayout.addComponent(Read);
		itemsProperty.addItemProperty("binNo",new ObjectProperty<String>("") );
		itemsProperty.addItemProperty("slotNo",new ObjectProperty<String>("") );
		itemsProperty.addItemProperty("qualityStatus",new ObjectProperty<String>("") );
		itemsProperty.addItemProperty("serial",new ObjectProperty<String>("") );
		
		ExecuteFormLayout.addComponent(this.BinNoText);
		ExecuteFormLayout.addComponent(this.SlotNoText);
		
		
		if(transactionType == 2 && unitType == 2){
			//ExecuteFormLayout.addComponent(this.AvailablePallet);
			AvailablePallet.addItem(" Add New Pallet");
			ExecuteFormLayout.addComponent(this.SerialText);
			ExecuteFormLayout.addComponent(productionDate); 
			}
		
		ExecuteFormLayout.addComponent(this.QualityStatus);
		
		ExecuteFormLayout.addComponent(this.HLayout);
		
		
		FieldGroup form = new FieldGroup(itemsProperty);
		form.bindMemberFields(ExecuteFormLayout);
		
		return ExecuteFormLayout;
	}
	
	
	private ListSelect IntegrateListSelect() throws DatabaseException
	{
		SerialsList.setWidth("200px");
		//SerialsList.addItem("ConfigrationId= "+mainService.GetLastSiteConfigIdForSite(SiteNo));
		//List<Integer> allbins = mainService.GetAllBinsInSiteConfig(mainService.GetLastSiteConfigIdForSite(SiteNo));	
		//for(Integer s: allbins){
		//	List<Integer> allSlots = mainService.GetAllSlotIdsInBinSlotConfigId(mainService.GetLatestBinSlotConfig(s).getId());
		//	for(Integer sl: allSlots){
		//		SerialsList.addItem("slot"+sl);
		//	}
		//
		//List<Integer> allbins = mainService.GetAllBinsInSiteConfig(mainService.GetLastSiteConfigIdForSite(SiteNo));
		//for(Integer b: allbins){
		//	SerialsList.addItem("bin "+b);
		//	Integer slotConfig = mainService.GetLatestBinSlotConfig(b);
		//	SerialsList.addItem("SlotConfig "+slotConfig);				
			
		//}
		List<Integer> allslots = mainService.GetAllSlotsInSiteConfig(mainService.GetLastSiteConfigIdForSite(SiteNo));
		List<Integer> equpied = mainService.GetAllSlotsWhichcontainSomethingInSite(SiteNo);
		List<Integer> EmptySlotList = mainService.GetAllEmptySlotsInSite(SiteNo);
		//List<Integer> good = mainService.GetSlots_FilteredBySku(SiteNo,2);
		
		SerialsList.addItem("no of slots="+allslots.size());
		for(Integer s: allslots){
			SerialsList.addItem(s);
		}
		SerialsList.addItem("no of Empty slots="+EmptySlotList.size());
		for(Integer e: EmptySlotList){
			SerialsList.addItem(e);
		}
		SerialsList.addItem("no of equpied slots="+equpied.size());
		for(Integer q: equpied){
			SerialsList.addItem("Slot no:"+q);
			SerialsList.addItem("SKUID: "+mainService.GetSkuOfSlot(q));
		}

		//SerialsList.addItem("no of good slots="+good.size());
		//for(Integer s: good){
		//	SerialsList.addItem(s);
		//}
		
		SerialsList.addValueChangeListener(new ValueChangeListener()
		{		@Override
				public void valueChange(final ValueChangeEvent event) {
				Delete.setVisible(true);
			 }
		});	
		return SerialsList;
	}
	
	private int getFromString (String str_){
		if (str_.equals("Released")) return 1 ;
		else if (str_.equals("Quantiled")) return 2 ;
		else if (str_.equals("Blocked")) return 3 ;
		else return 0 ;
	}	
}
