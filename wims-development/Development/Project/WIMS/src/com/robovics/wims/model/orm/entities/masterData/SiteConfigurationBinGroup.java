package com.robovics.wims.model.orm.entities.masterData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;


@NamedQueries(value = {
		@NamedQuery(name = "sCBG_get_sCBG", query = " select scb.id from  SiteConfigurationBinGroup scb "
				+ " where scb.siteConfigurationId =:P_SiteConfigId "
				+ " and scb.symbol =:P_symbol "
				),
		
		@NamedQuery(name = "getAllBinGroupsInSiteConfig", query = " select scb.id from  SiteConfigurationBinGroup scb "
						+ " where scb.siteConfigurationId =:P_SiteConfigId "
						)
				
		})		


@Table(name = "Site_Configuration_Bin_Group")
@Entity
public class SiteConfigurationBinGroup extends BaseEntity {

	private static final long serialVersionUID = -9025086282649374759L;
	private Integer id;
	private String symbol;
	private Integer siteConfigurationId; 

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CBG_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CBG_Symbol")
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	@Column(name = "CBG_CFG_Id")
	public Integer getSiteConfigurationId() {
		return siteConfigurationId;
	}

	public void setSiteConfigurationId(Integer id) {
		this.siteConfigurationId = id;
	}

	
}