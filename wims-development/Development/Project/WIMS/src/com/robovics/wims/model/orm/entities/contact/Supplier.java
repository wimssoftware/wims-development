package com.robovics.wims.model.orm.entities.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Supplier")
@Entity
public class Supplier extends BaseEntity {

	private static final long serialVersionUID = 3583442407559871398L;
	private Integer id;
	private Integer contactId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "SUP_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "SUP_CNT_Id")
	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
}