package com.robovics.wims.exceptions;

public class OrderException extends Exception {

	private static final long serialVersionUID = -4802887938013524376L;
	private int type; 
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public OrderException(String message) {
		super(message);
	}
	public OrderException(int type) {
		this.type = type;
	}
}