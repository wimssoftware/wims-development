package com.robovics.wims.model.orm.entities.transaction.orderItem;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Item_Quality_Status")
@Entity
public class ItemQualityStatus extends BaseEntity {

	private static final long serialVersionUID = 6569967404786085450L;
	private Integer id;
	private Integer siteAdminId;
	private Date date;
	private Integer qualityStatusTypeId;
	private Integer transactionOrderItemId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "IQS_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "IQS_SAD_Id")
	public Integer getSiteAdminId() {
		return siteAdminId;
	}

	public void setSiteAdminId(Integer siteAdminId) {
		this.siteAdminId = siteAdminId;
	}

	@Column(name = "IQS_Date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "IQS_QST_Id")
	public Integer getQualityStatusTypeId() {
		return qualityStatusTypeId;
	}

	public void setQualityStatusTypeId(Integer qualityStatusTypeId) {
		this.qualityStatusTypeId = qualityStatusTypeId;
	}

	@Column(name = "IQS_TOI_Id")
	public Integer getTransactionOrderItemId() {
		return transactionOrderItemId;
	}

	public void setTransactionOrderItemId(Integer transactionOrderItemId) {
		this.transactionOrderItemId = transactionOrderItemId;
	}
}