package com.robovics.wims.model.service.transaction;

import java.util.List;

import org.apache.tools.ant.taskdefs.SQLExec.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteReceive;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.issue.IssueService;
import com.robovics.wims.model.service.receive.ReceiveService;
import com.robovics.wims.model.service.transaction.itemBin.ReceiveItemBinService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderIssueService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderService;
import com.robovics.wims.model.service.transaction.orderItem.TransactionOrderItemService;

@Service(value = "transactionEntityService")
public class TransactionEntityService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	@Autowired
	private TransactionOrderService transactionOrderService;
	@Autowired
	private ReceiveItemBinService receiveItemBinService;
	
	public void addTransactionExecuteReceive(
			TransactionExecuteReceive transactionExecuteReceive)
			throws BusinessException {
		try {
			dao.addEntity(transactionExecuteReceive);
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void submitExecution(
			TransactionExecuteReceive transactionExecuteReceive,
			List<ReceiveItemBin> receiveItemBins,
			TransactionOrder transactionOrder) throws BusinessException {
		transactionExecuteReceive
				.setTransactionOrderReceiveId(transactionOrderService
						.getTransactionOrderReceiveByTransactionOrderId(
								transactionOrder.getId()).getId());
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			dao.addEntity(transactionExecuteReceive, session);
			for (ReceiveItemBin receiveItemBin : receiveItemBins) {
				receiveItemBin
						.setTransactionExecuteReceiveId(transactionExecuteReceive
								.getId());
				receiveItemBinService
						.addReceiveItemBin(receiveItemBin, session);
			}
			transactionOrder.setExecuted(1);
			transactionOrderService.updateTransactionOrder(transactionOrder,
					session);

			session.commitTransaction();
		} catch (DatabaseException e) {
			session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
}