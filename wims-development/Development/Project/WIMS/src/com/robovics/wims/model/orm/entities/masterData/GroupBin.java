package com.robovics.wims.model.orm.entities.masterData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;
import com.vaadin.data.fieldgroup.PropertyId;

@NamedQueries(value = {
		@NamedQuery(name = "GroupBin_getAllBins", query = " select distinct b.id from  GroupBin b ,  "
				+ "SiteConfiguration sc , SiteConfigurationBinGroup scb where b.siteConfigurationBinGroupId = scb.id "
				+ "and sc.siteId =:P_SiteNo and scb.siteConfigurationId = sc.id "
				+ "and sc.dateTime = ( select max(h.dateTime) from SiteConfiguration h) "
					) ,
		@NamedQuery(name = "GroupBin_getEmptyBins", query = " select distinct b.id from  GroupBin b ,   "
				+ "SiteConfiguration sc , SiteConfigurationBinGroup scb where b.siteConfigurationBinGroupId = scb.id "
				+ "and sc.siteId =:P_SiteNo and scb.siteConfigurationId = sc.id and b.id not in"
				+ " (select c.slotId from CurrentBinItem c )  "
				+ "and sc.dateTime = ( select max(h.dateTime) from SiteConfiguration h ) "
					) ,
		@NamedQuery(name = "Site_getLatestConfiguration", query = " select max(h.dateTime) from SiteConfiguration h where h.siteId =:P_SiteNo  "
					),
		@NamedQuery(name = "getGroupBinsOfSite", query = " select distinct b.id from SiteConfiguration s , SiteConfigurationBinGroup scb , GroupBin b "
				+ " where b.siteConfigurationBinGroupId = scb.id and scb.siteConfigurationId = s.id  and "
				+ "  s.dateTime in (select max(f.dateTime) from SiteConfiguration f where f.siteId =:P_SiteNo and f.dateTime <= :P_Date )   "
					),
				//	 >=:P_Date 
		@NamedQuery(name = "getAllBinsInBinGroupConfig", query = " select b.id from GroupBin b "
				+ " where b.siteConfigurationBinGroupId =:P_BinGroupConfigrationID "
					)
		})		


@Table(name = "Group_Bin")
@Entity
public class GroupBin extends BaseEntity {

	private static final long serialVersionUID = -7608611587694146908L;
	private Integer id;
	private Integer siteConfigurationBinGroupId;
	private Integer binNumber;
	@PropertyId("symbol")
	private String symbol;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "GBN_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "GBN_CBG_Id")
	public Integer getSiteConfigurationBinGroupId() {
		return siteConfigurationBinGroupId;
	}

	public void setSiteConfigurationBinGroupId(
			Integer siteConfigurationBinGroupId) {
		this.siteConfigurationBinGroupId = siteConfigurationBinGroupId;
	}

	@Column(name = "GBN_Number")
	public Integer getBinNumber() {
		return binNumber;
	}

	public void setBinNumber(Integer binNumber) {
		this.binNumber = binNumber;
	}

	@Transient
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}