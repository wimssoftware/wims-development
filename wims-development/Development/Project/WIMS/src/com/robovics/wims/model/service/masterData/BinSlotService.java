package com.robovics.wims.model.service.masterData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.masterData.BinSlot;
import com.robovics.wims.model.orm.entities.masterData.BinSlotConfiguration;
import com.robovics.wims.model.orm.entities.masterData.CurrentBinItem;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.orm.entities.masterData.SiteConfiguration;
import com.robovics.wims.model.orm.entities.masterData.SiteConfigurationBinGroup;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

@Service(value = "slotService")
public class BinSlotService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	@Autowired
	private MasterDataService masterDataService;

	public class BinSlotDetails {
		private Integer id;
		private Integer slotid;
		private String itemserial;
		private String itemname;
		private String itemcode;
		private Date dateTime;
		private int y = 0;
		private int z = 0;
		private int amount = 0 ; 
 
		
		public int getAmount() {
			return amount;
		}

		public void setAmount(int amount) {
			this.amount = amount;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getSlotid() {
			return slotid;
		}

		public void setSlotid(Integer slotid) {
			this.slotid = slotid;
		}

		public String getItemserial() {
			return itemserial;
		}

		public void setItemserial(String itemserial) {
			this.itemserial = itemserial;
		}

		public String getItemname() {
			return itemname;
		}

		public void setItemname(String item_serial) {
			StockKeepingUnit sSku = masterDataService
					.getSKUBySerial(item_serial);
			itemname = sSku.getDescription();
		}

		public String getItemcode() {
			return itemcode;
		}

		public void setItemcode(String item_serial) {
			StockKeepingUnit sSku = masterDataService
					.getSKUBySerial(item_serial);
			itemcode = sSku.getCode();
		}

		public Date getDateTime() {
			return dateTime;
		}

		public void setDateTime(Date dateTime) {
			this.dateTime = dateTime;
		}

		public int getY() {
			return y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public int getZ() {
			return z;
		}

		public void setZ(int z) {
			this.z = z;
		}

	}

	public List<BinSlot> getAllSlotS() {
		List<BinSlot> slTs = dao.getAllEntities(BinSlot.class);
		for (BinSlot slt : slTs) {
			BinSlotConfiguration bscId = dao.getEntityById(slt.getBinSlotConfigurationId(),
					BinSlotConfiguration.class, "id");
			GroupBin gbnId = dao.getEntityById(bscId.getGroupBinId(),
					GroupBin.class, "id");
			SiteConfigurationBinGroup CBGId = dao.getEntityById(gbnId.getSiteConfigurationBinGroupId(),
					SiteConfigurationBinGroup.class, "id");
			
			slt.setBinSymbol(dao.getEntityById(slt.getBinSlotConfigurationId(),
					BinSlotConfiguration.class, "id").getGroupBinId());
			slt.setSlotSymbol(CBGId.getSymbol()+gbnId.getBinNumber()+" "+ slt.getY()+"_"+slt.getZ());
		}

		return slTs;
	}
	
	public List<BinSlot> getAllSlotS(int bscId) {
		List<BinSlot> slTs_ = dao.getAllEntities(BinSlot.class);
		List<BinSlot> slTs = new ArrayList<>();
		
		
		for (BinSlot slt : slTs_) {
			if(slt.getBinSlotConfigurationId()==bscId){
			slt.setBinSymbol(dao.getEntityById(slt.getBinSlotConfigurationId(),
					BinSlotConfiguration.class, "id").getGroupBinId());
			slTs.add(slt);
			
			}
		}

		return slTs;
	}

	
	
	public BinSlot getSlotById(int Id) {
		BinSlot slt = dao.getEntityById(Id, BinSlot.class, "id");
		slt.setBinSymbol(dao.getEntityById(slt.getBinSlotConfigurationId(),
				BinSlotConfiguration.class, "id").getGroupBinId());
		return slt;
	}
	
	

	public String getSerialOfSlot(int slotId) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_GBN_ID", slotId);
			List<String> Ids = dao.executeNamedQuery(String.class,
					QueryNamesEnum.GET_ALL_SERIALS_OF_SLOT.queryName,
					parameters);
			if (Ids != null && Ids.size() != 0)
				return Ids.get(0);

		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return null;
	}

	public BinSlot getBinSlotById(Integer id) {
		BinSlot binSlot = dao.getEntityById(id, BinSlot.class, "id");
		binSlot.setSlotSymbol("" + binSlot.getY() + "_" + binSlot.getZ());
		return binSlot;
	}

	public List<BinSlot> getSlotOfBinconfig(int binNoconfig) {
		List<BinSlot> binSlots = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("GBN_Id", binNoconfig);

			List<Integer> slotIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_ALL_SLOTS_OF_SITE_CONFIG.queryName, parameters);
			for (Integer Id : slotIds) {
				binSlots.add(this.getBinSlotById(Id));
			}
			return binSlots;

		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return binSlots;
	}
	
	public List<BinSlot> getSlotOfBin_(int binNo, Date binDate) {
		List<BinSlot> binSlots = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("GBN_Id", binNo);
			parameters.put("P_Date", binDate);
			List<Integer> slotIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_SLOT_BY_BIN_ID.queryName, parameters);
			for (Integer Id : slotIds) {
				binSlots.add(this.getBinSlotById(Id));
			}
			return binSlots;

		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return binSlots;
	}
	
	public List<BinSlot> getSlotOfBin(int binNo, Date binDate) {
		List<BinSlot> binSlots = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("GBN_Id", binNo);

			List<Integer> slotIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_SLOT_BY_BIN_ID.queryName, parameters);
			for (Integer Id : slotIds) {
				binSlots.add(this.getBinSlotById(Id));
			}
			return binSlots;

		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return binSlots;
	}

	public List<BinSlotDetails> getSlotsDeatialsOfBin(int binNo, Date binDate) {
		List<BinSlotDetails> binSlots = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("GBN_Id", binNo);
			parameters.put("P_Date", binDate);

			List<Integer> slotIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_SLOT_BY_BIN_ID.queryName, parameters);
			for (Integer Id : slotIds) {
				try {
					CurrentBinItem cbi = masterDataService.getCBIBySlotId(Id);
					BinSlot s = this.getBinSlotById(Id);
					BinSlotDetails bsd = new BinSlotDetails();
					bsd.setSlotid(s.getId());
					bsd.setItemserial(cbi.getTransactionOrderItemSerial());
					bsd.setItemcode(cbi.getTransactionOrderItemSerial());
					bsd.setItemname(cbi.getTransactionOrderItemSerial());
					bsd.setY(s.getY());
					bsd.setZ(s.getZ());
					binSlots.add(bsd);
				} catch (Exception E) {
					E.printStackTrace();
					Notification.show("Empty Group Bin", Type.ERROR_MESSAGE);
					return null;
				}
			}
			return binSlots;

		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return binSlots;
	}

	public List<BinSlotDetails> getDetailsOfEmptySlotOfBin(int binNo,
			Date binDate) {
		List<BinSlot> binSlots = new ArrayList<>();
		List<BinSlot> binSlotsB = new ArrayList<>();
		List<BinSlotDetails> binSlotsD = new ArrayList<>();
		binSlotsB = this.getSlotOfBin_(binNo, binDate);
		for (BinSlot bSlot : binSlotsB)
			if (this.getSerialOfSlot(bSlot.getId()) == null) {
				binSlots.add(bSlot);
			}
		for (BinSlot b : binSlots) {
			try {
				BinSlotDetails bsd = new BinSlotDetails();
				bsd.setSlotid(b.getId());
				bsd.setY(b.getY());
				bsd.setZ(b.getZ());
				binSlotsD.add(bsd);
			} catch (Exception E) {
				E.printStackTrace();
				Notification.show("Empty Group Bin", Type.ERROR_MESSAGE);
				return null;
			}

		}

		return binSlotsD;
	}

	public List<BinSlotDetails> getDetailsOfFullSlotOfBin(int binNo,
			Date binDate) {
		List<BinSlot> binSlots = new ArrayList<>();
		List<BinSlot> binSlotsB = new ArrayList<>();
		List<BinSlotDetails> binSlotsD = new ArrayList<>();
		binSlotsB = this.getSlotOfBin_(binNo, binDate); 

		for (BinSlot bSlot : binSlotsB) {
			if (this.getSerialOfSlot(bSlot.getId()) != null) {
				binSlots.add(bSlot);
			}
		}
		for (BinSlot b : binSlots) {
			try {
				CurrentBinItem cbi = masterDataService
						.getCBIBySlotId(b.getId());
				BinSlotDetails bsd = new BinSlotDetails();
				bsd.setSlotid(b.getId());
				bsd.setItemserial(cbi.getTransactionOrderItemSerial());
				bsd.setItemcode(cbi.getTransactionOrderItemSerial());
				bsd.setItemname(cbi.getTransactionOrderItemSerial());
				bsd.setY(b.getY());
				bsd.setZ(b.getZ());
				binSlotsD.add(bsd);
			} catch (Exception E) {
				E.printStackTrace();
				continue;
			}

		}

		return binSlotsD;
	}	
	
	public List<BinSlotDetails> getDetailsOfFullSlotOfBin(int binSlotConfId) {
		List<BinSlot> binSlots = new ArrayList<>();
		List<BinSlot> binSlotsB = new ArrayList<>();
		List<BinSlotDetails> binSlotsD = new ArrayList<>();
		binSlotsB = this.getSlotOfBinconfig(binSlotConfId);

		for (BinSlot bSlot : binSlotsB) {
			if (this.getSerialOfSlot(bSlot.getId()) != null) {
				binSlots.add(bSlot);
			}
		}
		for (BinSlot b : binSlots) {
			try {
				CurrentBinItem cbi = masterDataService
						.getCBIBySlotId(b.getId());
				BinSlotDetails bsd = new BinSlotDetails();
				bsd.setSlotid(b.getId());
				bsd.setItemserial(cbi.getTransactionOrderItemSerial());
				bsd.setItemcode(cbi.getTransactionOrderItemSerial());
				bsd.setItemname(cbi.getTransactionOrderItemSerial());
				bsd.setY(b.getY());
				bsd.setZ(b.getZ());
				binSlotsD.add(bsd);
			} catch (Exception E) {
				E.printStackTrace();
				continue;
			}

		}

		return binSlotsD;
	}

	public List<BinSlot> getEmptySlotOfBin(int binNo, Date binDate) {
		List<BinSlot> binSlots = new ArrayList<>();
		List<BinSlot> binSlotsB = new ArrayList<>();
		binSlotsB = this.getSlotOfBin(binNo, binDate);
		for (BinSlot bSlot : binSlotsB)
			if (this.getSerialOfSlot(bSlot.getId()) == null) {
				binSlots.add(bSlot);
			}

		return binSlots;
	}

	public List<BinSlotDetails> getEmptySlotOFBin(int binNo, Date binDate) {
		List<BinSlotDetails> binSlots = new ArrayList<>();
		List<BinSlot> binSlotsB = new ArrayList<>();
		binSlotsB = this.getSlotOfBin(binNo, binDate);
		for (BinSlot bSlot : binSlotsB)
			if (this.getSerialOfSlot(bSlot.getId()) == null) {
				BinSlotDetails BSD = new BinSlotDetails();
				BSD.setY(bSlot.getY());
				BSD.setZ(bSlot.getZ());
				binSlots.add(BSD);
			}

		return binSlots;
	}

	public List<BinSlot> getFullSlotOfBin(int binNo, Date binDate) {
		List<BinSlot> binSlots = new ArrayList<>();
		List<BinSlot> binSlotsB = new ArrayList<>();
		binSlotsB = this.getSlotOfBin(binNo, binDate);

		for (BinSlot bSlot : binSlotsB) {
			System.out.println("slts is " + bSlot.getId());
			if (this.getSerialOfSlot(bSlot.getId()) != null) {
				System.out.println("slts is ser "
						+ this.getSerialOfSlot(bSlot.getId()));
				binSlots.add(bSlot);
			}
		}

		System.out.println("slts after is " + binSlots.size());
		return binSlots;
	}

	/*
	 * Formatting the project 13-2-2015 Tariq Shatat. Start Segment _ 1
	 */

	public ArrayList<BinSlotDetails> getSlotDetailsInTimeX(int binSlotCfgId) {
		ArrayList<BinSlotDetails> binSlotDetailsList = new ArrayList<>();
		List<ReceiveItemBin> recieveItemBinList = dao
				.getAllEntities(ReceiveItemBin.class);
		List<BinSlot> BinSlotList = dao.getAllEntities(BinSlot.class);
		for (BinSlot binSlot : BinSlotList) {
			if (binSlot.getBinSlotConfigurationId() == binSlotCfgId) {
				for (ReceiveItemBin recieveItemBin : recieveItemBinList) {
					if (recieveItemBin.getSlotId() == binSlot.getId()) {
						BinSlotDetails binSlotdetails = new BinSlotDetails();
						binSlotdetails.setId(binSlot.getId());
						binSlotdetails.setDateTime(recieveItemBin
								.getExecutionTime());
						binSlotdetails
								.setItemserial(recieveItemBin.getSerial());
						binSlotdetails.setItemname(recieveItemBin.getSerial());
						binSlotdetails.setItemcode(recieveItemBin.getSerial());
						binSlotDetailsList.add(binSlotdetails);
					}
				}
			}
		}
		return binSlotDetailsList;
	}

	public ArrayList<BinSlotDetails> getCurrentBinsSlots(int binCfgId) {
		ArrayList<Date> dateCnf = new ArrayList<>();
		ArrayList<BinSlotConfiguration> binSlotCnfgList = new ArrayList<>();
		List<GroupBin> groupBinList = dao.getAllEntities(GroupBin.class);
		List<BinSlotConfiguration> BinSlotConfigurationList = dao
				.getAllEntities(BinSlotConfiguration.class);
		for (GroupBin groupBin : groupBinList) {
			if (groupBin.getSiteConfigurationBinGroupId() == binCfgId) {
				for (BinSlotConfiguration binSlotCnfg : BinSlotConfigurationList) {
					if (groupBin.getId() == binSlotCnfg.getGroupBinId()) {
						binSlotCnfgList.add(binSlotCnfg);
						dateCnf.add(binSlotCnfg.getDateTime());
					}
				}
			}
		}
		ArrayList<BinSlotDetails> binSlotDetailsList = new ArrayList<>();
		if (dateCnf.size() != 0) {
			int indmaxDate = dateCnf.indexOf(Collections.max(dateCnf));
			int id = binSlotCnfgList.get(indmaxDate).getId();
			binSlotDetailsList = getSlotDetailsInTimeX(id);
		}
		return binSlotDetailsList;
	}

	public ArrayList<BinSlotDetails> getCurrentBinsEmptySlots(int binCfgId) {
		ArrayList<Date> dateCnf = new ArrayList<>();
		ArrayList<BinSlotConfiguration> binSlotCnfgList = new ArrayList<>();
		List<GroupBin> groupBinList = dao.getAllEntities(GroupBin.class);
		List<BinSlotConfiguration> BinSlotConfigurationList = dao
				.getAllEntities(BinSlotConfiguration.class);
		for (GroupBin groupBin : groupBinList) {
			if (groupBin.getSiteConfigurationBinGroupId() == binCfgId) {
				for (BinSlotConfiguration binSlotCnfg : BinSlotConfigurationList) {
					if (groupBin.getId() == binSlotCnfg.getGroupBinId()) {
						binSlotCnfgList.add(binSlotCnfg);
						dateCnf.add(binSlotCnfg.getDateTime());
					}
				}
			}
		}
		ArrayList<BinSlotDetails> binSlotDetailsList = new ArrayList<>();
		if (dateCnf.size() != 0) {
			int indmaxDate = dateCnf.indexOf(Collections.max(dateCnf));
			int id = binSlotCnfgList.get(indmaxDate).getId();
			binSlotDetailsList = getSlotDetailsInTimeX(id);
		}
		return binSlotDetailsList;
	}

	public void addAutomaticSlots(int sltCfg, int nWidth, int nLength,
			int nHeight) {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			for (int k = 0; k < nHeight; k++) {
				for (int i = 0; i < nWidth; i++) {
					for (int j = 0; j < nLength; j++) {
						BinSlot newSlot = new BinSlot();
						newSlot.setBinSlotConfigurationId(sltCfg);
						newSlot.setX(i + 1);
						newSlot.setY(j + 1);
						newSlot.setZ(k + 1);
						// To be updated with the lifting (stacking) number when
						// item entered .
						dao.addEntity(newSlot, session);
					}
				}
			}
			session.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			session.rollbackTransaction();
		}
	}

	// if id = -1 then return the most recent
	
	public List<BinSlotDetails> getDetailsOfEmptySlotOfSite(Integer site_id){
		Integer siteConfigid = masterDataService.GetLastSiteConfigIdForSite(site_id);
		List<GroupBin> GBINs = masterDataService.getAllBinsInSiteConfiguration(siteConfigid);
		List<BinSlotDetails> emptySlots = new ArrayList<BinSlotDetails>();
		for(GroupBin g :GBINs){
			List<BinSlotDetails> emptySlotsIngroup = this.getDetailsOfEmptySlotOfBin(g.getId(),new Date());
			emptySlots.addAll(emptySlotsIngroup);
		}
		return emptySlots;
	}
}