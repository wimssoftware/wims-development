package com.robovics.wims.model.service.transaction.orderItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.StockKeepingUnitService;

@Service(value = "transactionOrderItemService")
public class TransactionOrderItemService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	@Autowired
	private StockKeepingUnitService stockKeepingUnitService;

	@SuppressWarnings("unchecked")
	public List<TransactionOrderItem> getTransactionOrderItem(
			Integer transactionOrderId) throws BusinessException {
		List<TransactionOrderItem> returnorderItems = new ArrayList<TransactionOrderItem>();		
		CustomSession session = dao.openSession();
		List<TransactionOrderItem> orderItems = (List<TransactionOrderItem>) session
				.getSession().createCriteria(TransactionOrderItem.class)
				.add(Restrictions.eq("transactionOrderId", transactionOrderId))
				.list();

		for (TransactionOrderItem orderItem : orderItems) {
			StockKeepingUnit sku = stockKeepingUnitService.getSKUById(orderItem
					.getStockKeepingUnitId());
			orderItem.setStockKeepingUnitCode(sku.getCode());
			orderItem.setStockKeepingUnitDescription(sku.getDescription());
			if(orderItem.getIsContaining() == 0)
				returnorderItems.add(orderItem);
		}
		session.close();
		return returnorderItems;
	}
	
	@SuppressWarnings("deprecation")
	public boolean updateItemsDates(int toiId ,Date prodDate,CustomSession... session){
		TransactionOrderItem toi = dao.getEntityById(toiId, TransactionOrderItem.class, "id");
		StockKeepingUnit sku = dao.getEntityById(toi.getStockKeepingUnitId(), StockKeepingUnit.class, "id");
		@SuppressWarnings("deprecation")
		Date tempDate = new Date(sku.getLifeTimeYears(), sku.getLifeTimeMonths(), sku.getLifeTimeDays());
		
		Date expirationDate = new Date ();
		expirationDate.setYear(prodDate.getYear()+sku.getLifeTimeYears());
		expirationDate.setMonth(prodDate.getMonth()+sku.getLifeTimeMonths());
		expirationDate.setDate(prodDate.getDate()+sku.getLifeTimeDays());
		toi.setProductionDate(prodDate);
		toi.setExpirationDate(expirationDate);
		try{
			dao.updateEntity(toi, session);
		}catch(Exception e){
			e.printStackTrace();
			return false ; 
		}
		return true ;
	}
	
	public List<TransactionOrderItem>  getFirstExpiredTransactionOrderItemsOfSiteAndSKU(int siteNo,int skuId) {
		List <TransactionOrderItem> tois = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SKU_ID", skuId);
			parameters.put("P_STE_NO", skuId);
			List<Integer> Ids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_FIRSTEXPIRED_ORDER_ITEMS_OF_SITE_SKU.queryName,
					parameters);
			for (Integer Id : Ids){
				 tois.add(dao.getEntityById(Id, TransactionOrderItem.class, "id"));
			}
			return tois ; 
		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return null;
	}
	public List<TransactionOrderItem>  getFirstExpiredTransactionOrderItemsOfSiteAndSKUExceptDate(int siteNo,int skuId,Date date) {
		List <TransactionOrderItem> tois = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SKU_ID", skuId);
			parameters.put("P_STE_NO", skuId);
			parameters.put("P_DATE", date);
			List<Integer> Ids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_FIRSTEXPIRED_ORDER_ITEMS_OF_SITE_SKU_ExceptDate.queryName,
					parameters);
			for (Integer Id : Ids){
				 tois.add(dao.getEntityById(Id, TransactionOrderItem.class, "id"));
			}
			return tois ; 
		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	
}