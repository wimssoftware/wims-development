package com.robovics.wims.model.orm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.springframework.stereotype.Component;

import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.entities.BaseEntity;

@Component(value = "dao")
public class DataAccess {

	private SessionFactory sessionFactory;

	DataAccess() {
		Configuration configuration = new Configuration();
		configuration
				.configure("com/robovics/wims/model/orm/hibernate-mysql-config.xml");
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
				.applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	public CustomSession openSession() {
		return new CustomSession(sessionFactory.openSession());
	}

	public void closeSession(CustomSession session) {
		session.close();
		
	}

	public void addEntity(BaseEntity bean, CustomSession... useSession)
			throws DatabaseException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		Session session = isOpenedSession ? useSession[0].getSession()
				: sessionFactory.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();
			session.save(bean);
			if (!isOpenedSession)
				session.getTransaction().commit();
		} catch (Exception e) {
			if (!isOpenedSession)
				session.getTransaction().rollback();
			throw new DatabaseException(e.getMessage());
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}
	
	public void addEntities(Collection<? extends BaseEntity> beans,
			CustomSession... useSession) throws DatabaseException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		Session session = isOpenedSession ? useSession[0].getSession()
				: sessionFactory.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();
			Iterator<? extends BaseEntity> iter = beans.iterator();
			for (; iter.hasNext();)
				session.save(iter.next());
			if (!isOpenedSession)
				session.getTransaction().commit();
		} catch (Exception e) {
			if (!isOpenedSession)
				session.getTransaction().rollback();
			throw new DatabaseException(e.getMessage());
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public void updateEntity(BaseEntity bean, CustomSession... useSession)
			throws DatabaseException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		Session session = isOpenedSession ? useSession[0].getSession()
				: sessionFactory.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();
			session.saveOrUpdate(bean);
			if (!isOpenedSession)
				session.getTransaction().commit();
		} catch (Exception e) {
			if (!isOpenedSession)
				session.getTransaction().rollback();
			throw new DatabaseException(e.getMessage());
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public void updateEntities(Collection<? extends BaseEntity> beans,
			CustomSession... useSession) throws DatabaseException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		Session session = isOpenedSession ? useSession[0].getSession()
				: sessionFactory.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();
			Iterator<? extends BaseEntity> iter = beans.iterator();
			for (; iter.hasNext();)
				session.saveOrUpdate(iter.next());
			if (!isOpenedSession)
				session.getTransaction().commit();
		} catch (Exception e) {
			if (!isOpenedSession)
				session.getTransaction().rollback();
			throw new DatabaseException(e.getMessage());
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public void deleteEntity(BaseEntity bean, CustomSession... useSession)
			throws DatabaseException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		Session session = isOpenedSession ? useSession[0].getSession()
				: sessionFactory.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();
			session.delete(bean);
			if (!isOpenedSession)
				session.getTransaction().commit();
		} catch (Exception e) {
			if (!isOpenedSession)
				session.getTransaction().rollback();
			throw new DatabaseException(e.getMessage(), e);
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public void deleteEntities(Collection<? extends BaseEntity> beans,
			CustomSession... useSession) throws DatabaseException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		Session session = isOpenedSession ? useSession[0].getSession()
				: sessionFactory.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();
			Iterator<? extends BaseEntity> iter = beans.iterator();
			for (; iter.hasNext();)
				session.delete(iter.next());
			if (!isOpenedSession)
				session.getTransaction().commit();
		} catch (Exception e) {
			if (!isOpenedSession)
				session.getTransaction().rollback();
			throw new DatabaseException(e.getMessage());
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends BaseEntity> T getEntityById(Integer id, Class<T> entity,
			String idAttrName) {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(entity).add(
				Restrictions.eq(idAttrName, id));
		T entityObject = (T) criteria.list().get(0);
		session.close();
		return entityObject;
	}

	@SuppressWarnings("unchecked")
	public <T extends BaseEntity> List<T> getAllEntities(Class<T> entity) {
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(entity);
		List<T> entityObjects = (List<T>) criteria.list();
		session.close();
		return entityObjects;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> executeNamedQuery(Class<T> dataClass, String queryName,
			Map<String, Object> parameters) throws DatabaseException {
		Session session = sessionFactory.openSession();
		try {
			Query q = session.getNamedQuery(queryName);
			if (parameters != null) {

				for (Map.Entry<String, Object> paramEntry : parameters
						.entrySet()) {
					Object value = paramEntry.getValue();
					if (value instanceof Integer)
						q.setInteger(paramEntry.getKey(), (Integer) value);
					else if (value instanceof String)
						q.setString(paramEntry.getKey(), (String) value);
					else if (value instanceof Long)
						q.setLong(paramEntry.getKey(), (Long) value);
					else if (value instanceof Float)
						q.setFloat(paramEntry.getKey(), (Float) value);
					else if (value instanceof Double)
						q.setDouble(paramEntry.getKey(), (Double) value);
					else if (value instanceof Date)
						q.setDate(paramEntry.getKey(), (Date) value);
					else if (value instanceof Object[])
						q.setParameterList(paramEntry.getKey(),
								(Object[]) value);
				}
			}
			List<T> result = q.list();
			if (result == null)
				result = new ArrayList<T>();
			return result;
		} catch (Exception e) {
			throw new DatabaseException(e.getMessage());
		} finally {
			session.close();
		}
	}
}