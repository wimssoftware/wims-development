package com.robovics.wims.model.service.transaction;

import java.util.Date;
import java.util.List;

import com.google.gwt.junit.client.WithProperties.Property;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.staff.StaffService;
import com.vaadin.data.fieldgroup.PropertyId;


public class TransactionEntity {
	
	@PropertyId ("todId")
	private Integer TodId;
	@PropertyId(value = "orderDate")
	private Date orderDate;
	@PropertyId(value = "transactionTime")
	private Date transactionTime;
	
	private Integer siteAdminId;
	
	@PropertyId(value = "siteAdminName")
	private String siteAdminName;
	
	public String getSiteAdminName() {
		return siteAdminName;}

	public void setSiteAdminName(String siteAdminName) {
		this.siteAdminName = siteAdminName;
	}
	private Integer transactionTypeId;
	
	@PropertyId(value = "TransactionType")
	private String transactionTypeDes;
	
	public String getTransactionTypeDes() {
		if (transactionTypeId==1){
			return "Issue";
		}else if (transactionTypeId==2){
			return "Receive";
		}else {
			return "Transfer";
		}
	}

	
	public void setTransactionTypeDes(String transactionTypeDes) {
		this.transactionTypeDes = transactionTypeDes;
	}
	@PropertyId(value = "itemsQuantity")
	private Integer itemsQuantity;
	@PropertyId ("tors")
	private List <TransactionOrderReceive> Rorderids;
	@PropertyId ("tois")
	private List <TransactionOrderIssue> Iorderids;
	
	private Integer ownerId;
	private Integer customerId;
	private Integer supplierId;
	@PropertyId(value = "ownerName")
	public String ownerName;
	@PropertyId(value = "customerName")
	public String customerName;
	@PropertyId(value = "supplierName")
	public String supplierName;
	
	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getCustomerName() {
		return customerName ;
		}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getSupplierName() {
		return supplierName;
		}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	@PropertyId ("items")
	private List <TransactionOrderItem> itemIds;
	public List<TransactionOrderItem> getItemIds() {
		return itemIds;
	}
	
	public void setItemIds(List<TransactionOrderItem> itemIds) {
		this.itemIds = itemIds;
	}

	public Integer getStockKeepingUnitId() {
		return stockKeepingUnitId;
	}
	
	public void setStockKeepingUnitId(Integer stockKeepingUnitId) {
		this.stockKeepingUnitId = stockKeepingUnitId;
	}

	public Integer getUnitTypeId() {
		return unitTypeId;
	}

	public void setUnitTypeId(Integer unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

	public Integer getIsContained() {
		return isContained;
	}

	public void setIsContained(Integer isContained) {
		this.isContained = isContained;
	}

	public Integer getIsContaining() {
		return isContaining;
	}

	public void setIsContaining(Integer isContaining) {
		this.isContaining = isContaining;
	}

	public Date getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Date productionDate) {
		this.productionDate = productionDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	@PropertyId ("stockKeepingUnitId")
	private Integer stockKeepingUnitId;
	
	@PropertyId ("stockKeepingUnitDes")
	private String stockKeepingUnitDes;
	
	public String getStockKeepingUnitDes() {
		return stockKeepingUnitDes;
	}

	public void setStockKeepingUnitDes(String stockKeepingUnitDes) {
		this.stockKeepingUnitDes = stockKeepingUnitDes;
	}
	private Integer unitTypeId;
	private Integer isContained;
	private Integer isContaining;
	private Date productionDate;
	private Date expirationDate;
	
	public TransactionEntity(){}
	
	public Integer getTodId() {
		return TodId;
	}
	public void setTodId(Integer todId) {
		TodId = todId;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	public Integer getSiteAdminId() {
		return siteAdminId;
	}
	public void setSiteAdminId(Integer siteAdminId) {
		this.siteAdminId = siteAdminId;
	}
	public Integer getTransactionTypeId() {
		return transactionTypeId;
	}
	public void setTransactionTypeId(Integer transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	public Integer getItemsQuantity() {
		return itemsQuantity;
	}
	public void setItemsQuantity(Integer itemsQuantity) {
		this.itemsQuantity = itemsQuantity;
	}
	
	public List<TransactionOrderReceive> getRorderids() {
		return Rorderids;
	}

	public void setRorderids(List<TransactionOrderReceive> rorderids) {
		Rorderids = rorderids;
	}

	public List<TransactionOrderIssue> getIorderids() {
		return Iorderids;
	}

	public void setIorderids(List<TransactionOrderIssue> iorderids) {
		Iorderids = iorderids;
	}

	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}
	
	


}
