package com.robovics.wims.model.service.staff;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.orm.entities.staff.Contract;

public class ContractService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	public class ContractView {
		private int id = -1;
		private String ownerName = null;
		private String skuName = null;
		private String siteName = null;
		private String active = null;
		private Date datetime = null;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public void setOwnerName(int crtID) {
			for (Owner o : dao.getAllEntities(Owner.class)) {
				if (o.getId() == crtID) {
					Contact c = dao.getEntityById(o.getContactId(),
							Contact.class, "id");
					ownerName = c.getFirstName() + " " + c.getLastName();
				}
			}
		}

		public String getOwnerName() {
			return ownerName;
		}

		public void setSkuName(int crtID) {
			StockKeepingUnit o = dao.getEntityById(crtID,
					StockKeepingUnit.class, "id");
			skuName = o.getDescription();
		}

		public String getSkuName() {
			return skuName;
		}

		public void setSiteName(int crtID) {
			Site o = dao.getEntityById(crtID, Site.class, "id");
			siteName = o.getName();
		}

		public String getSiteName() {
			return siteName;
		}

		public void setDatetime(int crtID) {
			datetime = dao.getEntityById(crtID, Contract.class, "id")
					.getDatetime();
		}

		public Date getDatetime() {
			return datetime;
		}

		public void setActive(int crtID) {
			if (dao.getEntityById(crtID, Contract.class, "id").getActive() == 0) {
				active = "In Active";
			} else if (dao.getEntityById(crtID, Contract.class, "id")
					.getActive() == 1) {
				active = "Active";
			}
		}

		public String getActive() {
			return active;
		}
	}

	public int addcontract(Contract contract) {
		try {
			dao.addEntity(contract);
		} catch (DatabaseException e) {
			return 0;
		}
		return 1;
	}

	public int updatecontract(Contract contract) {
		try {
			dao.updateEntity(contract);
		} catch (DatabaseException e) {
			return 0;
		}
		return 1;
	}

	public List<Contract> listAllContractsWithoutView() {
		List<Contract> contractlist =dao.getAllEntities(Contract.class);
		
		return contractlist;
	}
	

	public ArrayList<ContractView> listAllContracts() {
		ArrayList<ContractView> contractlist = new ArrayList<>();
		for (Contract c : dao.getAllEntities(Contract.class)) {
			ContractView cv = new ContractView();
			cv.setId(c.getId());
			cv.setOwnerName(c.getOwnerid());
			cv.setActive(c.getId());
			cv.setDatetime(c.getId());
			cv.setSiteName(c.getSiteid());
			cv.setSkuName(c.getSkuid());
			contractlist.add(cv);
		}
		return contractlist;
	}
	
	public ArrayList<ContractView> listInActiveContracts() {
		ArrayList<ContractView> contractlist = new ArrayList<>();
		for (Contract c : dao.getAllEntities(Contract.class)) {
			if (c.getActive()==0){
			ContractView cv = new ContractView();
			cv.setId(c.getId());
			cv.setOwnerName(c.getOwnerid());
			cv.setActive(c.getId());
			cv.setDatetime(c.getId());
			cv.setSiteName(c.getSiteid());
			cv.setSkuName(c.getSkuid());
			contractlist.add(cv);
			}
		}
		return contractlist;
	}
	
	public ArrayList<ContractView> listActiveContracts() {
		ArrayList<ContractView> contractlist = new ArrayList<>();
		for (Contract c : dao.getAllEntities(Contract.class)) {
			if (c.getActive()==1){
			ContractView cv = new ContractView();
			cv.setId(c.getId());
			cv.setOwnerName(c.getOwnerid());
			cv.setActive(c.getId());
			cv.setDatetime(c.getId());
			cv.setSiteName(c.getSiteid());
			cv.setSkuName(c.getSkuid());
			contractlist.add(cv);
			}
		}
		return contractlist;
	}

	/*
	 * Tariq shatat 
	 * Added contract active validation to the Method
	 * */
	public ArrayList<Owner> listOwnersOfSite(int adminSiteId) {
		ArrayList<Owner> ownerlist = new ArrayList<>();
		ArrayList<Integer>ownersIds =new ArrayList<>();
		
		for (Contract c : dao.getAllEntities(Contract.class)) {
			if (c.getSiteid() == adminSiteId && c.getActive()==1) {
				Owner o = dao.getEntityById(c.getOwnerid(), Owner.class, "id");
				//ownerlist.indexOf(dao.getEntityById(o.getContactId(),Contact.class, "id")) == -1
				//The first has a serialization Id and will not be captured .
				if (!ownersIds.contains(o.getId())) {
					System.out.println("OWner Id is "+o.getContactId() );
					
					ownersIds.add(o.getId());
					ownerlist.add(o);
				}
			}
		}
		for (Owner ownr :ownerlist){
			Contact ctct=dao.getEntityById(ownr.getContactId(), Contact.class, "id");
			ownr.setName(ctct.getFirstName()+" "+ctct.getLastName());
		}
		return ownerlist;
	}

	public ArrayList<StockKeepingUnit> listSkusOfOwner(Owner contact,
			int adminSiteId) {
		int ownerId = contact.getId();
		
		/*for (Owner o : dao.getAllEntities(Owner.class)) {
			if (o.getContactId() == contact.getId()) {
				ownerId = o.getId();
			}
		}*/
		System.err.println("ownerId selected"+ownerId);
		ArrayList<StockKeepingUnit> skulist = new ArrayList<>();
		/*
		 * Tariq shatat 
		 * Added contract active validation to the Method
		 * */
		for (Contract c : dao.getAllEntities(Contract.class)) {
			if (c.getSiteid() == adminSiteId && c.getOwnerid() == ownerId &&c.getActive()==1) {
				if (skulist.indexOf(dao.getEntityById(c.getSkuid(),StockKeepingUnit.class, "id")) == -1) {
					skulist.add(dao.getEntityById(c.getSkuid(),
							StockKeepingUnit.class, "id"));
				}
			}
		}
		return skulist;
	}
}
