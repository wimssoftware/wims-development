package com.robovics.wims.model.orm.entities.transaction.orderItem;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Item_Contained_In")
@Entity
public class ItemContainedIn extends BaseEntity {

	private static final long serialVersionUID = -8681441032522865576L;
	private Integer id;
	private Integer containerTransactionOrderItem;
	private Integer containedTransactionOrderItem;
	private Date startDate;
	private Date endDate;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "ICI_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ICI_Container_TOI_Id")
	public Integer getContainerTransactionOrderItem() {
		return containerTransactionOrderItem;
	}

	public void setContainerTransactionOrderItem(
			Integer containerTransactionOrderItem) {
		this.containerTransactionOrderItem = containerTransactionOrderItem;
	}

	@Column(name = "ICI_Contained_TOI_Id")
	public Integer getContainedTransactionOrderItem() {
		return containedTransactionOrderItem;
	}

	public void setContainedTransactionOrderItem(
			Integer containedTransactionOrderItem) {
		this.containedTransactionOrderItem = containedTransactionOrderItem;
	}

	@Column(name = "ICI_StartDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "ICI_EndDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}