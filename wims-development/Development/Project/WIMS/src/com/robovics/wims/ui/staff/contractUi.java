package com.robovics.wims.ui.staff;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.orm.entities.masterData.SiteConfigurationBinGroup;
import com.robovics.wims.model.orm.entities.staff.Contract;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.contact.ContactService.ContactView;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.staff.ContractService;
import com.robovics.wims.model.service.staff.ContractService.ContractView;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class contractUi extends UI {
	ContractService contractservice = new ContractService();
	List<ContractView> contractList = null;
	int currentIndex = 0;
	int allContractViewSize = 0;
	BeanItemContainer<ContractView> beans = new BeanItemContainer<ContractView>(
			ContractView.class);
	final Table table = new Table("", beans);
	
	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
 	Button sOk ;
 	Action update;

	@Override
	protected void init(VaadinRequest request) {

	}

	public Layout build() {
		Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
		 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
		 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
		 Button new_contract = new Button(this.getMessage(GasDiaryMessages.NewContract));
		 sOk = new Button(this.getMessage(GasDiaryMessages.Save));
		 update = new Action(this.getMessage(GasDiaryMessages.Update));
		List<ContractView> contracts = null;
		contractList = contractservice.listAllContracts();
		allContractViewSize = contractList.size();

		contracts = getListOfContractView();

		HorizontalLayout Hlayout = new HorizontalLayout();

		VerticalLayout vlayout_tool = new VerticalLayout();
		vlayout_tool.setMargin(true);
		
		new_contract.setId("toolLayout_b1");

		HorizontalLayout searchLayout = new HorizontalLayout();
		TextField oname = new TextField();
		oname.setImmediate(true);
		oname.setInputPrompt(this.getMessage(GasDiaryMessages.OwnerName));
		TextField sname = new TextField();
		sname.setImmediate(true);
		sname.setInputPrompt(this.getMessage(GasDiaryMessages.SiteName));
		searchLayout.addComponent(oname);
		searchLayout.addComponent(sname);
		searchLayout.setId("searchLayout");

		vlayout_tool.addComponent(new_contract);
		vlayout_tool.addComponent(searchLayout);

		VerticalLayout tablelayout = new VerticalLayout();
		beans.addAll(contracts);
		table.setPageLength(contracts.size());
		table.setBuffered(false);
		table.setSelectable(true);
		table.setVisibleColumns(new Object[] { "ownerName", "skuName",
				"siteName", "datetime", "active" });
		table.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.OwnerName), this.getMessage(GasDiaryMessages.SKU), this.getMessage(GasDiaryMessages.SiteName), 
				this.getMessage(GasDiaryMessages.Date),this.getMessage(GasDiaryMessages.Active) });
		tablelayout.addComponent(table);

		table.addActionHandler(new Handler() {
			

			@Override
			public void handleAction(Action action, Object sender,
					final Object target) {
				if (action == update) {
					try {
						ContractView contact = (ContractView) target;
						Window subWindow = build_window(contact, true);
						
						UI.getCurrent().addWindow(subWindow);
					} catch (Exception e) {
						e.printStackTrace();
						Notification.show("Error", e.getMessage(),
								Notification.Type.TRAY_NOTIFICATION);
					}
				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				ArrayList<Action> actions = new ArrayList<Action>();
				// Enable deleting only if clicked on an item
				if (target != null)
					actions.add(update);
				return (Action[]) (actions.toArray(new Action[actions.size()]));
			}
		});

		HorizontalLayout hNextpervios = new HorizontalLayout();
		hNextpervios.setSizeUndefined();
		Button perviousb = new Button("<<");
		hNextpervios.addComponent(perviousb);
		Button nextb = new Button(">>");
		hNextpervios.addComponent(nextb);
		nextb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<ContractView> contracts = getListOfContractView();
				beans.removeAllItems();
				beans.addAll(contracts);
				table.setData(contracts);
				table.setPageLength(contracts.size());
				table.commit();
			}
		});
		perviousb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<ContractView> contacts = getPerviousListOfContractView();
				beans.removeAllItems();
				beans.addAll(contacts);
				table.setData(contacts);
				table.setPageLength(contacts.size());
				table.commit();
			}
		});
		tablelayout.addComponent(hNextpervios);

		oname.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("ownerName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});

		sname.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("siteName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});
		oname.setImmediate(true);
		sname.setImmediate(true);

		new_contract.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				ContractView sCBG = (ContractView) table.getValue();
				Window subWindow = build_window(sCBG, false);
				UI.getCurrent().addWindow(subWindow);
			}
		});

		Hlayout.setSizeUndefined();
		Hlayout.addComponent(vlayout_tool);
		Hlayout.addComponent(tablelayout);
		return Hlayout;
	}

	public List<ContractView> getListOfContractView() {
		ArrayList<ContractView> lContractView = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i < allContractViewSize; i++) {
			try {
				lContractView.add(contractList.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lContractView;
	}

	public List<ContractView> getPerviousListOfContractView() {
		ArrayList<ContractView> lContractView = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i >= 0; i--) {
			try {
				lContractView.add(contractList.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lContractView;
	}

	public Window build_window(final ContractView selectedContract,
			final boolean update) {
		DataAccess dao = DAOFactory.getInstance().getDataAccess();
		VerticalLayout subContent = new VerticalLayout();
		VerticalLayout subContent1 = new VerticalLayout();
		subContent.setMargin(true);
		subContent.setSpacing(false);
		final Window subWindow = new Window(this.getMessage(GasDiaryMessages.ContractWindwoCaption));
		subWindow.setDraggable(false);
		subWindow.setResizable(false);
		subWindow.setHeight("470px");
		subWindow.setWidth("300px");
		subWindow.setContent(subContent1);
		// //////////
		final ArrayList<Owner> owners = (ArrayList<Owner>) dao
				.getAllEntities(Owner.class);
		ArrayList<Contact> contacts = (ArrayList<Contact>) dao
				.getAllEntities(Contact.class);
		final ArrayList<String> owners_name = new ArrayList<>();
		for (Owner s : owners) {
			for (Contact c : contacts) {
				if (s.getContactId() == c.getId()) {
					owners_name.add(c.getFirstName() + " " + c.getLastName());
				}
			}
		}
		final ComboBox ownerCombo = new ComboBox(this.getMessage(GasDiaryMessages.OwnerName), owners_name);
		ownerCombo.setNullSelectionAllowed(false);
		subContent.addComponent(ownerCombo);
		// //////////
		final ArrayList<String> sites = new ArrayList<>();
		final List<Site> site = dao.getAllEntities(Site.class);
		for (Site o : site) {
			sites.add(o.getName());
		}
		final ComboBox siteCombo = new ComboBox(this.getMessage(GasDiaryMessages.SiteName), sites);
		siteCombo.setNullSelectionAllowed(false);
		subContent.addComponent(siteCombo);
		// //////////
		final ArrayList<String> skus = new ArrayList<>();
		final List<StockKeepingUnit> sku = dao
				.getAllEntities(StockKeepingUnit.class);
		for (StockKeepingUnit o : sku) {
			skus.add(o.getDescription());
		}
		final ComboBox skuCombo = new ComboBox(this.getMessage(GasDiaryMessages.SKU), skus);
		skuCombo.setNullSelectionAllowed(false);
		subContent.addComponent(skuCombo);
		// //////////
		final PopupDateField cdate = new PopupDateField();
		cdate.setImmediate(true);
		cdate.setCaption(this.getMessage(GasDiaryMessages.ContractDate));
		cdate.setInputPrompt(this.getMessage(GasDiaryMessages.ContractDate));
		cdate.setValue(new Date());
		subContent.addComponent(cdate);
		// //////////
		ArrayList<String> active = new ArrayList<>();
		active.add("Active");
		active.add("In Active");
		final ComboBox activeCombo = new ComboBox(this.getMessage(GasDiaryMessages.Active), active);
		activeCombo.setNullSelectionAllowed(false);
		subContent.addComponent(activeCombo);
		subWindow.center();
		if (update) {
			ownerCombo.select(selectedContract.getOwnerName());
			System.out.print(selectedContract.getOwnerName());
			siteCombo.select(selectedContract.getSiteName());
			skuCombo.select(selectedContract.getSkuName());
			activeCombo.select(selectedContract.getActive());
			cdate.setValue(selectedContract.getDatetime());
		}
		// //////////////////////
	
		final Contract cObj = new Contract();
		sOk.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				
				if (activeCombo.getValue().equals("Active")) {
					cObj.setActive(1);
				} else if (activeCombo.getValue().equals("In Active")) {
					cObj.setActive(0);
				}
				cObj.setDatetime(cdate.getValue());
				cObj.setSiteid(site.get(
						sites.indexOf((String) siteCombo.getValue())).getId());
				cObj.setSkuid(sku.get(
						skus.indexOf((String) skuCombo.getValue())).getId());
				cObj.setOwnerid(owners.get(
						owners_name.indexOf((String) ownerCombo.getValue()))
						.getId());
				List<ContractView> contracts = null;
				contractList = contractservice.listAllContracts();
				contracts = getListOfContractView();
				List <Contract> contractList_ =contractservice.listAllContractsWithoutView();
				for (Contract c : contractList_)
				{
					if (c.getOwnerid()== cObj.getOwnerid() && c.getActive()==cObj.getActive() 
							&& c.getSiteid() == cObj.getSiteid() && c.getSkuid() == cObj.getSkuid()){
						Notification.show("عفوا لقد تم ادخال عذا العقد من قبل",Notification.TYPE_ERROR_MESSAGE);
						return ; 
					}
				}
				
				if (update) {
					cObj.setId(selectedContract.getId());
					contractservice.updatecontract(cObj);
				} else {
					contractservice.addcontract(cObj);
				}
				beans = new BeanItemContainer<ContractView>(ContractView.class);
				
				allContractViewSize = contractList.size();
				contracts = getListOfContractView();
				for (ContractView c : contracts)
				{
					
				}
				beans.addAll(contracts);
				table.setPageLength(contracts.size());
				table.setBuffered(false);
				table.setSelectable(true);
				table.setVisibleColumns(new Object[] { "ownerName", "skuName",
						"siteName", "datetime", "active" });
				table.setColumnHeaders(new String[] { "Owner", "SKU", "Site",
						"Date", "Active" });
				table.setContainerDataSource(beans);
				subWindow.close();
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
			}
		});
		subContent.addComponent(sOk);
		subContent.setComponentAlignment(sOk, Alignment.BOTTOM_CENTER);
		subContent1.addComponent(subContent);
		subContent1.setComponentAlignment(subContent, Alignment.MIDDLE_CENTER);
		return subWindow;
	}
	
	 @Override
	    public void setLocale(Locale locale) {
	        super.setLocale(locale);
	        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	    }
	    
	    /** Returns the bundle for the current locale. */
	    public ResourceBundle getBundle() {
	        return i18nBundle;
	    }
	 
	    /**
	     * Returns a localized message from the resource bundle
	     * with the current application locale.
	     **/ 
	    public String getMessage(String key) {
	        return i18nBundle.getString(key);
	    }

}
