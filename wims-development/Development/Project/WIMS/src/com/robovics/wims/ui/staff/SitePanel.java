package com.robovics.wims.ui.staff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.masterData.BinSlotConfiguration;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.orm.entities.masterData.SiteConfiguration;
import com.robovics.wims.model.orm.entities.masterData.SiteConfigurationBinGroup;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.BinSlotService;
import com.robovics.wims.model.service.masterData.BinSlotService.BinSlotDetails;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SitePanel extends UI {
	private static final long serialVersionUID = -5283900239820303185L;
	private MasterDataService masterService;
	private BinSlotService binSlotService;
	private Table binsTable = new Table();
	private Table itemsSerialsTable = new Table();
	private Table slotsDetailsTable = new Table();
	private Table SiteConifgration = new Table();
	private List<Site> sites = new ArrayList<>();
	final GridLayout mainLayout = new GridLayout(5, 1);
	Table sitesTable = null;
	int sitesSize = 0;
	private List<GroupBin> grpBin = new ArrayList<>();
	int grbBinSize = 0;

	ResourceBundle i18n; // Automatic Localization
	ResourceBundle i18nBundle; // Automatic localization
	Button savebtn;

	Handler siteActionHandler = new Handler() {

		Action update = new Action("Update Site");
		Action Delete = new Action("Delete Site");

		@Override
		public void handleAction(Action action, Object sender, Object target) {

			if (action == update) {
				Window UpdateSite = UpdateSites((Site) target);
				UpdateSite.setDraggable(false);
				UpdateSite.setResizable(false);

				UI.getCurrent().addWindow(UpdateSite);
			}
			if (action == Delete) {
				int tablelength = sitesTable.getPageLength();
				try {
					Site DeletedSite = (Site) target;
					masterService.deleteSite(DeletedSite);

					sitesTable.removeItem(target);
					sitesTable.setPageLength(tablelength - 1);
					Notification.show("Info", "Record deleted successfully", Notification.Type.TRAY_NOTIFICATION);
					sites.remove(DeletedSite);
					sitesSize--;
				} catch (DatabaseException e) {
					sitesTable.setPageLength(tablelength);
					Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
				} catch (BusinessException e) {
					e.printStackTrace();
					Notification.show("You Cannot Delete This Site", e.getMessage(),
							Notification.Type.TRAY_NOTIFICATION);
				}
			}
		}

		@Override
		public Action[] getActions(Object target, Object sender) {
			ArrayList<Action> actions = new ArrayList<Action>();
			if (target != null)
				actions.add(update);

			actions.add(Delete);
			return (Action[]) (actions.toArray(new Action[actions.size()]));
		}
	};

	HorizontalLayout hMainLayout = new HorizontalLayout();

	ValueChangeListener selectedBinChaneged = new ValueChangeListener() {

		@Override
		public void valueChange(ValueChangeEvent event) {

			if (event.getProperty().getValue() == null) {
				System.out.println("deSelected__");
				itemsSerialsTable.setVisible(false);
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
				return;
			} else {
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
				GroupBin scbg = (GroupBin) binsTable.getValue();
				itemsSerialsTable = getAllSlotsInBin(scbg.getId());
				itemsSerialsTable.addValueChangeListener(selectedBinConfChaneged);
				mainLayout.addComponent(itemsSerialsTable, 3, 0);
				binsTable.setVisible(true);
			}
			itemsSerialsTable.setSizeUndefined();
		}
	};

	ValueChangeListener selectedSiteCFGChaneged = new ValueChangeListener() {

		@Override
		public void valueChange(ValueChangeEvent event) {

			if (event.getProperty().getValue() == null) {
				binsTable.setVisible(false);
				mainLayout.removeComponent(binsTable);
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
				return;
			} else {
				mainLayout.removeComponent(binsTable);
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
				SiteConfiguration selectedSiteCFG = (SiteConfiguration) SiteConifgration.getValue();
				binsTable = getItemsPerSite(selectedSiteCFG.getId());
				binsTable.addValueChangeListener(selectedBinChaneged);
				binsTable.addActionHandler(new Handler() {
					Action update = new Action("Update Bin");
					Action Delete = new Action("Delete Bin");

					@Override
					public void handleAction(Action action, Object sender, Object target) {
						if (action == update) {
							Window UpdateBin = UpdateBins((GroupBin) target);
							UpdateBin.setDraggable(false);
							UpdateBin.setResizable(false);

							UI.getCurrent().addWindow(UpdateBin);
						}
						if (action == Delete) {
							int tablelength = binsTable.getPageLength();
							try {
								GroupBin DeletedBin = (GroupBin) target;
								System.out.print(DeletedBin.getSiteConfigurationBinGroupId());
								grpBin = masterService
										.getAllBinsInSiteConfiguration(DeletedBin.getSiteConfigurationBinGroupId());
								grbBinSize = grpBin.size();
								masterService.deleteBin(DeletedBin);
								binsTable.removeItem(target);
								binsTable.setPageLength(tablelength - 1);
								Notification.show("Info", "Record deleted successfully",
										Notification.Type.TRAY_NOTIFICATION);
								grpBin.remove(DeletedBin);
								grbBinSize--;
							} catch (DatabaseException e) {
								sitesTable.setPageLength(tablelength);
								Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
							} catch (BusinessException e) {
								e.printStackTrace();
								Notification.show("You Cannot Delete This Bin", e.getMessage(),
										Notification.Type.TRAY_NOTIFICATION);
							}
						}
					}

					@Override
					public Action[] getActions(Object target, Object sender) {
						ArrayList<Action> actions = new ArrayList<Action>();
						if (target != null)
							actions.add(update);
						actions.add(Delete);

						return (Action[]) (actions.toArray(new Action[actions.size()]));
					}

				});
				mainLayout.removeComponent(2, 0);
				mainLayout.addComponent(binsTable, 2, 0);
				binsTable.setVisible(true);
			}
		}
	};

	ValueChangeListener selectedSiteChaneged = new ValueChangeListener() {

		@Override
		public void valueChange(ValueChangeEvent event) {
			if (event.getProperty().getValue() == null) {
				SiteConifgration.setVisible(false);
				mainLayout.removeComponent(SiteConifgration);
				mainLayout.removeComponent(binsTable);
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
				return;
			} else {
				mainLayout.removeComponent(SiteConifgration);
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
				mainLayout.removeComponent(binsTable);
				Site selectedSite = (Site) sitesTable.getValue();
				SiteConifgration = getAllSiteConfigrations(selectedSite.getId());
				SiteConifgration.addValueChangeListener(selectedSiteCFGChaneged);
				mainLayout.removeComponent(1, 0);
				mainLayout.addComponent(SiteConifgration, 1, 0);
				SiteConifgration.setVisible(true);
			}
		}
	};

	ValueChangeListener selectedBinConfChaneged = new ValueChangeListener() {
		@Override
		public void valueChange(ValueChangeEvent event) {
			if (event.getProperty().getValue() == null) {
				slotsDetailsTable.setVisible(false);
				mainLayout.removeComponent(slotsDetailsTable);
				return;
			} else {
				mainLayout.removeComponent(slotsDetailsTable);
				BinSlotConfiguration bsc = (BinSlotConfiguration) itemsSerialsTable.getValue();
				slotsDetailsTable = getAllSlotsDetails(bsc.getId(), bsc.getDateTime());
				mainLayout.addComponent(slotsDetailsTable, 4, 0);
				slotsDetailsTable.setVisible(true);
			}
			itemsSerialsTable.setSizeUndefined();
		}
	};

	@SuppressWarnings("serial")
	public Layout buildForm() {

		Locale c = ((Locale) VaadinSession.getCurrent().getAttribute("Locale"));
		i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
		i18n = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
		final Button b1 = new Button(this.getMessage(GasDiaryMessages.NewSite));
		final Button b2 = new Button(this.getMessage(GasDiaryMessages.NewBin));
		savebtn = new Button(this.getMessage(GasDiaryMessages.Save));
		final Button b3 = new Button(this.getMessage(GasDiaryMessages.ConfigureBin));
		hMainLayout.setSizeUndefined();

		masterService = (MasterDataService) ServiceFactory.getInstance().getService(ServicesEnum.MASTER_DATA_SERVICE);
		binSlotService = (BinSlotService) ServiceFactory.getInstance().getService(ServicesEnum.BIN_SLOT_Service);
		sites = masterService.getAllSites();
		mainLayout.setSizeUndefined();
		sitesTable = initSitesList(sites);

		sitesSize = sites.size();

		sitesTable.addActionHandler(siteActionHandler);

		mainLayout.addComponent(sitesTable, 0, 0);
		sitesTable.addValueChangeListener(selectedSiteChaneged);

		final VerticalLayout allLayout = new VerticalLayout();
		allLayout.addComponent(mainLayout);

		VerticalLayout vToolLayout = new VerticalLayout();
		vToolLayout.setSizeUndefined();

		b1.setId("toolLayout_b1");

		b1.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Window subWindow = buid_window();
				subWindow.setDraggable(false);
				subWindow.setResizable(false);

				UI.getCurrent().addWindow(subWindow);
			}
		});

		b2.setId("toolLayout_b1");

		b2.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				SiteConfiguration selectedSite = (SiteConfiguration) SiteConifgration.getValue();
				if (selectedSite != null) {
					Window subWindow = buid_bin_window(selectedSite);
					UI.getCurrent().addWindow(subWindow);
				}
			}
		});

		b3.setId("toolLayout_b1");

		b3.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				GroupBin sCBG = (GroupBin) binsTable.getValue();
				if (sCBG != null) {
					Window subWindow = buid_configure_bin_window(sCBG);

					UI.getCurrent().addWindow(subWindow);
				}
			}
		});

		HorizontalLayout searchLayout = new HorizontalLayout();
		searchLayout.setSizeUndefined();
		TextField Sname = new TextField();
		Sname.setImmediate(true);
		Sname.setInputPrompt(this.getMessage(GasDiaryMessages.SiteName));
		searchLayout.addComponent(Sname);
		searchLayout.setId("searchLayout");
		vToolLayout.addComponent(b1);
		vToolLayout.addComponent(b2);
		vToolLayout.addComponent(b3);
		vToolLayout.addComponent(searchLayout);

		Sname.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) sitesTable.getContainerDataSource();
				if (filter != null)
					f.removeContainerFilter(filter);
				filter = new Like("name", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				sitesTable.setPageLength(f.size());
				mainLayout.removeComponent(binsTable);
				mainLayout.removeComponent(itemsSerialsTable);
				mainLayout.removeComponent(slotsDetailsTable);
			}
		});

		Sname.setImmediate(true);

		hMainLayout.addComponent(vToolLayout);
		hMainLayout.addComponent(allLayout);

		return hMainLayout;
	}

	private Table initSitesList(List<Site> sites) {
		BeanItemContainer<Site> beansSites = new BeanItemContainer<Site>(Site.class);
		beansSites.addAll(sites);
		Table sitesTable = new Table(this.getMessage(GasDiaryMessages.Sites), beansSites);
		sitesTable.setPageLength(beansSites.size());
		sitesTable.setVisibleColumns(new Object[] { "name", "address", "active" });
		sitesTable.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.SiteName),
				this.getMessage(GasDiaryMessages.Address), this.getMessage(GasDiaryMessages.Active) });
		sitesTable.setSizeFull();
		sitesTable.setBuffered(false);
		sitesTable.setSelectable(true);
		sitesTable.setImmediate(true);

		return sitesTable;
	}

	private Table getAllSlotsInBin(int binConfId) {
		BeanItemContainer<BinSlotConfiguration> beansBins = new BeanItemContainer<BinSlotConfiguration>(
				BinSlotConfiguration.class);
		ArrayList<BinSlotConfiguration> binViewList = masterService.getAllBinsSlotsConfInSite(binConfId);
		beansBins.addAll(binViewList);
		Table tableItems = new Table(this.getMessage(GasDiaryMessages.ConfigurationDate), beansBins);
		tableItems.setPageLength(beansBins.size());
		tableItems.setSizeFull();
		tableItems.setBuffered(false);
		tableItems.setVisibleColumns(new Object[] { "dateTime" });
		tableItems.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.Date) });
		tableItems.setSelectable(true);
		tableItems.setImmediate(true);
		return tableItems;
	}

	private Table getAllSiteConfigrations(int SiteId) {
		BeanItemContainer<SiteConfiguration> beansBins = new BeanItemContainer<SiteConfiguration>(
				SiteConfiguration.class);
		ArrayList<SiteConfiguration> ConfigSiteList = masterService.getAllSiteConfigurations(SiteId);
		beansBins.addAll(ConfigSiteList);
		Table tableItems = new Table(this.getMessage(GasDiaryMessages.ConfigurationDate), beansBins);
		tableItems.setPageLength(beansBins.size());
		tableItems.setSizeFull();
		tableItems.setBuffered(false);
		tableItems.setVisibleColumns(new Object[] { "dateTime" });
		tableItems.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.Date) });
		tableItems.setSelectable(true);
		tableItems.setImmediate(true);
		return tableItems;
	}

	private Table getItemsPerSite(int siteCfgId) {
		masterService = (MasterDataService) ServiceFactory.getInstance().getService(ServicesEnum.MASTER_DATA_SERVICE);
		BeanItemContainer<GroupBin> beansBins = new BeanItemContainer<GroupBin>(GroupBin.class);
		ArrayList<GroupBin> binViewList = masterService.getAllBinsInSiteConfiguration(siteCfgId);
		System.out.println("888 " + binViewList.size());
		beansBins.addAll(binViewList);
		final Table tableItems = new Table(this.getMessage(GasDiaryMessages.Bins), beansBins);
		tableItems.setPageLength(beansBins.size() + 3);
		tableItems.setSizeFull();
		tableItems.setBuffered(false);
		tableItems.setVisibleColumns(new Object[] { "symbol" });
		tableItems.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.symbols) });
		tableItems.setSelectable(true);
		tableItems.setImmediate(true);
		return tableItems;
	}

	private Table getAllSlotsDetails(int binSlotConfId, Date binDateCfg) {

		BeanItemContainer<BinSlotDetails> beansSlots = new BeanItemContainer<BinSlotDetails>(BinSlotDetails.class);
		ArrayList<BinSlotDetails> binSlotDetailsList = (ArrayList<BinSlotDetails>) binSlotService
				.getDetailsOfFullSlotOfBin(binSlotConfId);

		ArrayList<BinSlotDetails> binSlotDetailsListEmpty = (ArrayList<BinSlotDetails>) binSlotService
				.getEmptySlotOFBin(binSlotConfId, binDateCfg);

		beansSlots.addAll(binSlotDetailsList);
		beansSlots.addAll(binSlotDetailsListEmpty);
		Table tableItems = new Table(this.getMessage(GasDiaryMessages.slot), beansSlots);
		tableItems.setPageLength(beansSlots.size());
		tableItems.setSizeFull();
		tableItems.setBuffered(false);
		tableItems.setVisibleColumns(new Object[] { "itemserial", "itemname", "itemcode", "dateTime", "y", "z" });
		tableItems.setColumnHeaders(
				new String[] { this.getMessage(GasDiaryMessages.serial), this.getMessage(GasDiaryMessages.ItemName),
						this.getMessage(GasDiaryMessages.ItemCode), this.getMessage(GasDiaryMessages.Date),
						this.getMessage(GasDiaryMessages.lenght), this.getMessage(GasDiaryMessages.height) });
		tableItems.setSelectable(true);
		tableItems.setImmediate(true);
		return tableItems;
	}

	@Override
	protected void init(VaadinRequest request) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings({ "serial" })
	public Window buid_window() {
		final Window subWindow = new Window(this.getMessage(GasDiaryMessages.SiteWindowCaption));
		VerticalLayout subContent = new VerticalLayout();
		VerticalLayout Content1 = new VerticalLayout();
		subContent.setMargin(true);
		subWindow.setContent(Content1);

		final TextField Sname = new TextField();
		Sname.setImmediate(true);
		Sname.setInputPrompt(this.getMessage(GasDiaryMessages.SiteName));
		Sname.setCaption(this.getMessage(GasDiaryMessages.SiteName));

		/*
		 * final PopupDateField Sdate = new PopupDateField();
		 * Sdate.setImmediate(true); Sdate.setInputPrompt("Configuration date");
		 * Sdate.setValue(new Date()); Sdate.setCaption("Configration Date");
		 */

		final TextField Saddress = new TextField();
		Saddress.setImmediate(true);
		Saddress.setInputPrompt(this.getMessage(GasDiaryMessages.SiteAddress));
		Saddress.setCaption(this.getMessage(GasDiaryMessages.SiteAddress));

		final ComboBox activeTypeCombo = new ComboBox(null, Arrays.asList("Active", "inActive"));
		activeTypeCombo.setNullSelectionAllowed(false);
		activeTypeCombo.select("Active");
		activeTypeCombo.setCaption(this.getMessage(GasDiaryMessages.ActiveORinActive));

		savebtn.setId("toolLayout_b1");

		subContent.addComponent(Sname);
		// subContent.addComponent(Sdate);
		subContent.addComponent(Saddress);
		subContent.addComponent(activeTypeCombo);
		subContent.addComponent(savebtn);

		Content1.addComponent(subContent);
		Content1.setComponentAlignment(subContent, Alignment.MIDDLE_CENTER);
		subContent.setSpacing(true);
		subContent.setMargin(true);
		subWindow.center();
		subWindow.setWidth("300px");

		savebtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				MasterDataService masterService = (MasterDataService) ServiceFactory.getInstance()
						.getService(ServicesEnum.MASTER_DATA_SERVICE);
				Site siteView = new Site();
				siteView.setAddress(Saddress.getValue());
				siteView.setName(Sname.getValue());
				String activeTypeComboval = (String) activeTypeCombo.getValue();
				int active = 1;
				if (activeTypeComboval.equals("Active")) {
					active = 1;

				} else if (activeTypeComboval.equals("inActive")) {
					active = 0;
				}
				siteView.setActive(active);
				try {
					subWindow.close();
					masterService.add_site(siteView, active);
					Notification.show("Info", "Record Inserted successfully", Notification.Type.TRAY_NOTIFICATION);
					mainLayout.removeComponent(binsTable);
					mainLayout.removeComponent(itemsSerialsTable);
					mainLayout.removeComponent(slotsDetailsTable);
					mainLayout.removeComponent(sitesTable);
					// sites = masterService.getAllSitesConfiguration();
					sites = masterService.getAllSites();
					mainLayout.setSizeUndefined();
					sitesTable = initSitesList(sites);
					mainLayout.addComponent(sitesTable, 0, 0);
					sitesTable.addValueChangeListener(selectedSiteChaneged);
					sitesTable.addActionHandler(siteActionHandler);
					Page.getCurrent().getJavaScript().execute("window.location.reload();");
				} catch (BusinessException e) {
					e.printStackTrace();
					Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
				}
			}
		});
		return subWindow;
	}

	public Window buid_bin_window(final SiteConfiguration selectedSite) {
		final Window subWindow = new Window(this.getMessage(GasDiaryMessages.BinWindowCaption));
		subWindow.setResizable(false);
		if (selectedSite != null) {
			VerticalLayout subContent = new VerticalLayout();
			subContent.setMargin(true);
			subWindow.setContent(subContent);

			final TextField Bname = new TextField();
			Bname.setImmediate(true);
			Bname.setInputPrompt(this.getMessage(GasDiaryMessages.BinName));
			Bname.setCaption(this.getMessage(GasDiaryMessages.BinName));

			final TextField Bnumber = new TextField();
			Bnumber.setImmediate(true);
			Bnumber.setInputPrompt(this.getMessage(GasDiaryMessages.BinNumber));
			Bnumber.setCaption("Bin number, or numbers separated by a -");
			savebtn.setId("toolLayout_b1");

			subContent.addComponent(Bname);
			subContent.addComponent(Bnumber);
			subContent.addComponent(savebtn);
			subContent.setSpacing(true);
			subContent.setMargin(true);
			subWindow.setWidth("250px");
			subWindow.center();

			savebtn.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					SiteConfigurationBinGroup sCBG = new SiteConfigurationBinGroup();
					sCBG.setSiteConfigurationId(selectedSite.getId());
					sCBG.setSymbol(Bname.getValue());
					int binNumberStart, binNumberEnd;
					try {
						String numbers = Bnumber.getValue();
						if (numbers.contains("-")) {
							String[] nums = numbers.split("-");
							if (nums.length > 2)
								throw new Exception("You entered invalid format");
							binNumberStart = Integer.parseInt(nums[0]);
							binNumberEnd = Integer.parseInt(nums[1]);
						} else {
							binNumberEnd = binNumberStart = Integer.parseInt(numbers);
						}
					} catch (Exception e) {
						Notification.show("You've entered an invalid format for the bin numbers", Type.ERROR_MESSAGE);
						return;
					}
					List<BusinessException> errors = new ArrayList<>();
					for (int i = binNumberStart; i <= binNumberEnd; i++) {
						GroupBin gB = new GroupBin();
						gB.setBinNumber(i);
						gB.setSymbol(sCBG.getSymbol() + gB.getBinNumber());
						try {
							masterService.add_sitebin(sCBG, gB);
						} catch (BusinessException e) {
							e.printStackTrace();
							errors.add(e);
						}
					}
					if (errors.size() > 0) {
						Notification.show("Some bins have not been added", errors.toString(), Type.ERROR_MESSAGE);
						mainLayout.removeComponent(2, 0);
						mainLayout.removeComponent(itemsSerialsTable);
						mainLayout.removeComponent(slotsDetailsTable);
						mainLayout.setSizeUndefined();
						binsTable = getItemsPerSite(selectedSite.getId());

						binsTable.addValueChangeListener(selectedBinChaneged);
						sitesTable.addValueChangeListener(selectedSiteChaneged);
						SiteConifgration.unselect(selectedSite.getId());
						binsTable.setImmediate(true);
						binsTable.commit();
						masterService = (MasterDataService) ServiceFactory.getInstance()
								.getService(ServicesEnum.MASTER_DATA_SERVICE);
						subWindow.close();
						return;
					}
					Page.getCurrent().getJavaScript().execute("window.location.reload();");
				}
			});
		}
		return subWindow;
	}

	public Window buid_configure_bin_window(final GroupBin gB) {
		final Window subWindow = new Window(this.getMessage(GasDiaryMessages.ConfigureBin));
		subWindow.setResizable(false);
		VerticalLayout subContent = new VerticalLayout();
		subContent.setMargin(true);
		subWindow.setContent(subContent);

		final PopupDateField Bdate = new PopupDateField();
		Bdate.setImmediate(true);
		Bdate.setInputPrompt(this.getMessage(GasDiaryMessages.ConfigurationDate));
		Bdate.setValue(new Date());
		Bdate.setCaption(this.getMessage(GasDiaryMessages.ConfigurationDate));

		final TextField Blength = new TextField();
		Blength.setImmediate(true);
		Blength.setInputPrompt(this.getMessage(GasDiaryMessages.lenght));
		Blength.setCaption(this.getMessage(GasDiaryMessages.lenght));

		final TextField Bwidth = new TextField();
		Bwidth.setImmediate(true);
		Bwidth.setInputPrompt(this.getMessage(GasDiaryMessages.width));
		Bwidth.setCaption(this.getMessage(GasDiaryMessages.width));

		final TextField Bheight = new TextField();
		Bheight.setImmediate(true);
		Bheight.setInputPrompt(this.getMessage(GasDiaryMessages.height));
		Bheight.setCaption(this.getMessage(GasDiaryMessages.height));

		// Button savebtn = new Button("Save");
		savebtn.setId("toolLayout_b1");

		subContent.addComponent(Bdate);
		subContent.addComponent(Bwidth);
		subContent.addComponent(Blength);
		subContent.addComponent(Bheight);
		subContent.addComponent(savebtn);
		subContent.setSpacing(true);
		subContent.setMargin(true);
		subWindow.setWidth("300px");
		subWindow.center();

		savebtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				MasterDataService masterService = (MasterDataService) ServiceFactory.getInstance()
						.getService(ServicesEnum.MASTER_DATA_SERVICE);
				BinSlotConfiguration binSlotCnfg = new BinSlotConfiguration();
				binSlotCnfg.setGroupBinId(gB.getId());
				binSlotCnfg.setDateTime(Bdate.getValue());
				// Needs to check on the Comparison between the new and old
				// width and length.
				binSlotCnfg.setLength(Integer.parseInt(Blength.getValue()));
				binSlotCnfg.setWidth(Integer.parseInt(Bwidth.getValue()));
				binSlotCnfg.setHeight(Integer.parseInt(Bheight.getValue()));
				binSlotCnfg.setSelected(false);
				try {
					subWindow.close();
					masterService.addBinConfig(binSlotCnfg);
					/*
					 * Tariq Shatat automatic adding slot based on width and
					 * length
					 */
					binSlotService.addAutomaticSlots(binSlotCnfg.getId(), Integer.parseInt(Bwidth.getValue()),
							Integer.parseInt(Blength.getValue()), Integer.parseInt(Bheight.getValue()));
					mainLayout.removeComponent(itemsSerialsTable);
					mainLayout.removeComponent(slotsDetailsTable);
					// sites = masterService.getAllSitesConfiguration();
					mainLayout.setSizeUndefined();
					itemsSerialsTable = getAllSlotsInBin(gB.getId());
					itemsSerialsTable.addValueChangeListener(selectedBinConfChaneged);
					mainLayout.addComponent(itemsSerialsTable, 2, 0);
					Page.getCurrent().getJavaScript().execute("window.location.reload();");
				} catch (BusinessException e) {
					e.printStackTrace();
				}
			}
		});

		return subWindow;
	}

	public Window UpdateSites(final Site siteView) {
		// final SiteView siteView = (SiteView) sitesTable.getValue();
		final Window UpdateSite = new Window(this.getMessage(GasDiaryMessages.UpdateSite));
		VerticalLayout v1 = new VerticalLayout();
		VerticalLayout Content1 = new VerticalLayout();
		v1.setMargin(true);
		UpdateSite.setContent(Content1);

		final TextField Sname = new TextField();
		Sname.setImmediate(true);
		Sname.setCaption(this.getMessage(GasDiaryMessages.SiteName));
		Sname.setValue(siteView.getName());

		final TextField Saddress = new TextField();
		Saddress.setImmediate(true);
		Saddress.setCaption(this.getMessage(GasDiaryMessages.SiteAddress));
		Saddress.setValue(siteView.getAddress());

		final ComboBox activeTypeCombo = new ComboBox(null, Arrays.asList("Active", "inActive"));
		activeTypeCombo.setNullSelectionAllowed(false);
		activeTypeCombo.select("Active");
		activeTypeCombo.setCaption(this.getMessage(GasDiaryMessages.ActiveORinActive));
		activeTypeCombo.setValue(siteView.getActive());

		// Button savebtn = new Button("Save");
		savebtn.setId("toolLayout_b1");

		v1.addComponent(Sname);
		v1.addComponent(Saddress);
		v1.addComponent(activeTypeCombo);
		v1.addComponent(savebtn);

		Content1.addComponent(v1);
		Content1.setComponentAlignment(v1, Alignment.MIDDLE_CENTER);
		v1.setSpacing(true);
		v1.setMargin(true);
		UpdateSite.center();
		UpdateSite.setWidth("300px");

		savebtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				Site newSite = new Site();
				newSite.setAddress(Saddress.getValue());
				newSite.setName(Sname.getValue());
				newSite.setId(siteView.getId());

				String activeTypeComboval = (String) activeTypeCombo.getValue();
				int active = 1;
				if (activeTypeComboval.equals("Active")) {
					active = 1;
				} else if (activeTypeComboval.equals("inActive")) {
					active = 0;
				}
				newSite.setActive(activeTypeComboval);
				sitesTable.commit();
				try {
					UpdateSite.close();
					masterService.updateSite(newSite);
					Notification.show("Info", "Record updated successfully", Notification.Type.TRAY_NOTIFICATION);
					Page.getCurrent().getJavaScript().execute("window.location.reload();");
				} catch (BusinessException e) {
					e.printStackTrace();
					Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
				}

			}

		});
		return UpdateSite;
	}

	public Window UpdateBins(final GroupBin BinGrpView) {
		final Window UpdateBin = new Window(this.getMessage(GasDiaryMessages.UpdateBin));
		UpdateBin.setResizable(false);

		VerticalLayout subContent = new VerticalLayout();
		subContent.setMargin(true);
		UpdateBin.setContent(subContent);

		final TextField Bname = new TextField();
		Bname.setImmediate(true);
		Bname.setCaption(this.getMessage(GasDiaryMessages.BinName));
		// Bname.setValue(BinGrpView.getSymbol());// error here
		Bname.setValue(masterService.getSiteConfigurationBinGroupById(BinGrpView.getSiteConfigurationBinGroupId())
				.getSymbol());

		final TextField Bnumber = new TextField();
		Bnumber.setImmediate(true);
		Bnumber.setCaption(this.getMessage(GasDiaryMessages.BinNumber));
		Bnumber.setValue(BinGrpView.getBinNumber().toString());

		// Button savebtn = new Button("Save");
		savebtn.setId("toolLayout_b1");

		subContent.addComponent(Bname);
		subContent.addComponent(Bnumber);
		subContent.addComponent(savebtn);
		subContent.setSpacing(true);
		subContent.setMargin(true);
		UpdateBin.setWidth("250px");
		UpdateBin.center();
		savebtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				GroupBin group = new GroupBin();
				group.setSymbol(Bname.getValue());
				group.setBinNumber(Integer.parseInt(Bnumber.getValue()));

				try {
					UpdateBin.close();
					mainLayout.removeComponent(binsTable);
					SiteConfiguration selectSiteConfig = (SiteConfiguration) SiteConifgration.getValue();
					masterService.updateBin(BinGrpView, group, selectSiteConfig);
					Notification.show("Info", "Record updated successfully", Notification.Type.TRAY_NOTIFICATION);
					binsTable.commit();

					masterService = (MasterDataService) ServiceFactory.getInstance()
							.getService(ServicesEnum.MASTER_DATA_SERVICE);
					Page.getCurrent().getJavaScript().execute("window.location.reload();");
				} catch (BusinessException e) {
					e.printStackTrace();
					Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
				}

			}

		});

		return UpdateBin;
	}

	@Override
	public void setLocale(Locale locale) {
		super.setLocale(locale);
		i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	}

	/** Returns the bundle for the current locale. */
	public ResourceBundle getBundle() {
		return i18nBundle;
	}

	/**
	 * Returns a localized message from the resource bundle with the current
	 * application locale.
	 **/
	public String getMessage(String key) {
		return i18nBundle.getString(key);
	}
}