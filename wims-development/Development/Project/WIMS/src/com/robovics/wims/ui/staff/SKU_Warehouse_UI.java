package com.robovics.wims.ui.staff;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.QualityStatus;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.service.QualityStatusServices;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("wims")
public class SKU_Warehouse_UI extends UI {
	private static final String TABLE_CODE_COLUMN = "code";
	private static final String TABLE_QUALITY_STATUS_COLUMN = "qualityStatus";
	private static final String TABLE_MAX_CARTON_COLUMN = "maxCartonCount";
	private static final String TABLE_LIFE_TIME_COLUMN = "lifeTimeDays";
	private static final String TABLE_STACK_COUNT_COLUMN = "liftingNumber";
	private static final String TABLE_DESCRIPTION_COLUMN = "description";
	

	ResourceBundle i18n; // Automatic Localization
	ResourceBundle i18nBundle; // Automatic localization
	Action delete;
	StockKeepingUnitService skuService = null;
	List<StockKeepingUnit> allSKUs = null;
	int currentIndex = 0;
	int allSKUsize = 0;
	
	List<QualityStatus> qualityStatusValues;

	@Override
	protected void init(VaadinRequest request) {

	}

	@SuppressWarnings("deprecation")
	public Layout buildSKU() {
		Locale c = ((Locale) VaadinSession.getCurrent().getAttribute("Locale"));
		i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
		i18n = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
		
		final Button newSKUBtn = new Button(this.getMessage(GasDiaryMessages.NewSKU));
		final Button submit = new Button(this.getMessage(GasDiaryMessages.Save));
		Button cancel = new Button(this.getMessage(GasDiaryMessages.CancelKey));
		delete = new Action(this.getMessage(GasDiaryMessages.Delete));

		skuService = (StockKeepingUnitService) ServiceFactory.getInstance().getService(ServicesEnum.SKU_SERVICE);
		QualityStatusServices qualityStatusService = (QualityStatusServices) ServiceFactory.getInstance().getService(ServicesEnum.QUALITY_STATUS_SERVCE);
		qualityStatusValues = qualityStatusService.getQualityStatusValues();
		
		
		allSKUs = skuService.getAllSKU();
		allSKUsize = allSKUs.size();
		System.out.println(allSKUsize+ "  Size is My Size  ");
		addQualityStatusToSKUs();
		
		
		VerticalLayout vlayout = new VerticalLayout();
		vlayout.setMargin(true);
		
		// A layout for the table and form
		VerticalLayout layout = new VerticalLayout();
		final Table table = new Table();
		table.setBuffered(false);
	
		table.addContainerProperty(TABLE_CODE_COLUMN, String.class, "", getMessage(GasDiaryMessages.code), null, Align.CENTER);
		table.addContainerProperty(TABLE_DESCRIPTION_COLUMN, String.class, "", getMessage(GasDiaryMessages.Description), null, Align.CENTER);
		table.addContainerProperty(TABLE_MAX_CARTON_COLUMN, Integer.class, "", getMessage(GasDiaryMessages.MaxCarton), null, Align.CENTER);
		table.addContainerProperty(TABLE_QUALITY_STATUS_COLUMN, String.class, "", getMessage(GasDiaryMessages.QualityStatus), null, Align.CENTER);
		table.addContainerProperty(TABLE_STACK_COUNT_COLUMN, Integer.class, "", getMessage(GasDiaryMessages.StackingNumber), null, Align.CENTER);
		table.addContainerProperty(TABLE_LIFE_TIME_COLUMN, Integer.class, "", getMessage(GasDiaryMessages.LifeTimeDays), null, Align.CENTER);
		
		List<StockKeepingUnit> skus = getListOfSKU();
		table.setPageLength(skus.size());
		
		setTableData(table, skus);
		
		layout.addComponent(table);

		final Window SKUWindow = new Window();
		SKUWindow.center();
		SKUWindow.setHeight("400px");
		SKUWindow.setWidth("400px");
		SKUWindow.setResizable(false);
		SKUWindow.setDraggable(false);
		SKUWindow.setCaption(this.getMessage(GasDiaryMessages.SKUWindowCaption));

		table.addActionHandler(new Handler() {

			@Override
			public void handleAction(Action action, Object sender, final Object target) {
				if (action == delete) {
					int tableLength = table.getPageLength();
					
					try {
						
						StockKeepingUnit sku = allSKUs.get((int)target-1);
						skuService.deleteSKU(sku);
						table.removeItem(target);
						table.setPageLength(tableLength - 1);
						Notification.show("Info", "Record deleted successfully", Notification.Type.TRAY_NOTIFICATION);
						allSKUs.remove(sku);
						allSKUsize--;
					} catch (BusinessException e) {
						table.setPageLength(tableLength);
						Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
					}
				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				return new Action[]{delete};
			}
		});

		final FormLayout formLayout = new FormLayout();

		PropertysetItem itemsProperty = new PropertysetItem();
		itemsProperty.addItemProperty(TABLE_CODE_COLUMN, new ObjectProperty<String>(""));
		itemsProperty.addItemProperty(TABLE_DESCRIPTION_COLUMN, new ObjectProperty<String>(""));
		itemsProperty.addItemProperty(TABLE_MAX_CARTON_COLUMN, new ObjectProperty<Integer>(0));
		itemsProperty.addItemProperty(TABLE_QUALITY_STATUS_COLUMN, new ObjectProperty<String>(""));
		itemsProperty.addItemProperty(TABLE_STACK_COUNT_COLUMN, new ObjectProperty<Integer>(3));
		itemsProperty.addItemProperty(TABLE_LIFE_TIME_COLUMN, new ObjectProperty<Integer>(0));

		final FieldGroup form = new FieldGroup(itemsProperty);
		formLayout.addComponent(form.buildAndBind(this.getMessage(GasDiaryMessages.code), TABLE_CODE_COLUMN));
		formLayout.addComponent(form.buildAndBind(this.getMessage(GasDiaryMessages.Description), TABLE_DESCRIPTION_COLUMN));
		formLayout.addComponent(form.buildAndBind(this.getMessage(GasDiaryMessages.MaxCarton), TABLE_MAX_CARTON_COLUMN));
		
		final ComboBox qualityStatusSelector = new ComboBox(getMessage(GasDiaryMessages.DefaultQualityStatus), qualityStatusValues);		
		qualityStatusSelector.setNullSelectionAllowed(false);
		
		formLayout.addComponent(qualityStatusSelector);
		formLayout.addComponent(form.buildAndBind(this.getMessage(GasDiaryMessages.StackingNumber), TABLE_STACK_COUNT_COLUMN));
		formLayout.addComponent(form.buildAndBind(this.getMessage(GasDiaryMessages.LifeTimeDays), TABLE_LIFE_TIME_COLUMN));

		formLayout.setVisible(false);
		form.setBuffered(true);
		formLayout.setSpacing(true);
		formLayout.setMargin(true);
		SKUWindow.setContent(formLayout);

		table.addListener(new ItemClickListener() {

			@Override
			public void itemClick(ItemClickEvent event) {
				if (event.getButton() != ItemClickEvent.BUTTON_RIGHT) {
					SKUWindow.setCaption("Update Stock Keeping Unit");
					getCurrent().addWindow(SKUWindow);
				}
			}
		});

		HorizontalLayout hNextpervios = new HorizontalLayout();
		hNextpervios.setSizeUndefined();
		Button perviousb = new Button("<<");
		hNextpervios.addComponent(perviousb);
		Button nextb = new Button(">>");
		hNextpervios.addComponent(nextb);
		nextb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<StockKeepingUnit> lSKU = getListOfSKU();
				if (lSKU.size() > 0) {
					table.setData(lSKU);
					table.setPageLength(lSKU.size());
					table.commit();
				}
			}
		});
		perviousb.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				List<StockKeepingUnit> lSKU = getPerviousListOfSKU();
				if (lSKU.size() > 0) {
					setTableData(table, lSKU);
					table.setPageLength(lSKU.size());
					table.commit();
				}
			}
		});
		layout.addComponent(hNextpervios);

		table.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				if (event.getProperty().getValue() == null) {
					SKUWindow.close();
					return;
				}
				Object value = table.getValue();
				Item i = table.getItem(value);
				String qs = (String) i.getItemProperty(TABLE_QUALITY_STATUS_COLUMN).getValue();
				qualityStatusSelector.setValue(qualityStatusForText(qs));
				form.setItemDataSource(i);
				
				formLayout.setVisible(true);
				table.setData(null);
			}
		});
		table.setSelectable(true);
		table.setImmediate(true);
		
		newSKUBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				StockKeepingUnit newSKU = new StockKeepingUnit();
				newSKU.setCode("");
				newSKU.setDescription("");
				newSKU.setLiftingNumber(0);
				newSKU.setLifeTimeDays(0);
				newSKU.setMaxCartonCount(1);
				newSKU.qualityStatusText = qualityStatusValues.get(0).getDescription();
				allSKUs.add(newSKU);
				Item item = addSkuToTable(table, newSKU);
				form.setItemDataSource(item);
				table.setPageLength(table.getPageLength() + 1);
				
				table.select(allSKUs.size());
				table.setEnabled(false);
				newSKUBtn.setEnabled(false);
				SKUWindow.setClosable(false);
				formLayout.setVisible(true);
				getCurrent().addWindow(SKUWindow);
			}
		});

		submit.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				StockKeepingUnit skuToValidate = new StockKeepingUnit();
				skuToValidate.setCode((String) form.getField(TABLE_CODE_COLUMN).getValue());
				skuToValidate.setDescription((String) form.getField(TABLE_DESCRIPTION_COLUMN).getValue());
				skuToValidate.setLiftingNumber(Integer.parseInt((String) form.getField(TABLE_STACK_COUNT_COLUMN).getValue()));
				skuToValidate.setLifeTimeDays(Integer.parseInt((String) form.getField(TABLE_LIFE_TIME_COLUMN).getValue()));
				skuToValidate.setMaxCartonCount(Integer.parseInt((String) form.getField(TABLE_MAX_CARTON_COLUMN).getValue()));
				skuToValidate.qualityStatusText = qualityStatusSelector.getValue().toString();
				skuToValidate.setDefaultQualityStatus(qualityStatusForText(skuToValidate.qualityStatusText).getId());
				
				List<String> errorMessages = skuService.validateSKU(skuToValidate);

				if (errorMessages.size() > 0) {
					Collections.reverse(errorMessages);
					for (String errorMessage : errorMessages)
						Notification.show("Error", errorMessage, Notification.Type.TRAY_NOTIFICATION);
					return;
				}

				Integer index = (Integer) table.getValue() - 1;
				StockKeepingUnit sku = allSKUs.get(index);
				skuToValidate.setId(sku.getId());
				sku = skuToValidate;
				try {
					if (sku.getId() == null) {
						List<StockKeepingUnit> SKU = skuService.getAllSKU();
						for (StockKeepingUnit SKU_ : SKU) {
							if (SKU_.getCode().equals(skuToValidate.getCode())) {
								Notification.show("SKU does Exist", Notification.TYPE_ERROR_MESSAGE);
								return;
							}
						}
						skuService.addSKU(sku);
						Notification.show("Info", "Record inserted successfully", Notification.Type.TRAY_NOTIFICATION);
						allSKUsize++;
					} else {
						skuService.updateSKU(sku);
						Notification.show("Info", "Record updated successfully", Notification.Type.TRAY_NOTIFICATION);
					}
					allSKUs.set(index, sku);
				} catch (BusinessException e) {
					Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
				}
				newSKUBtn.setEnabled(true);
				
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
				formLayout.setVisible(false); 
				SKUWindow.close();
				table.commit();
				table.setEnabled(true);
			}
		});
		
		HorizontalLayout formFooter = new HorizontalLayout();
		formFooter.addComponent(submit);

		cancel.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.discard(); 
				formLayout.setVisible(false); 
				SKUWindow.close();
				Object o = table.getValue();
				int index = (Integer) o - 1;
				StockKeepingUnit sku = allSKUs.get(index);
				if (sku.getId() == null) {
					table.removeItem(o);
					allSKUs.remove(index);
					table.setPageLength(table.getPageLength() - 1);
				} else
					table.unselect(o);

				table.discard(); 
				table.setEnabled(true);
				newSKUBtn.setEnabled(true);
			}
		});
		formFooter.addComponent(cancel);
		formLayout.addComponent(formFooter);
		vlayout.addComponent(newSKUBtn);

		HorizontalLayout filterLayout = new HorizontalLayout();
		TextField codeFilter = new TextField(this.getMessage(GasDiaryMessages.code));
		TextField descriptionFilter = new TextField(this.getMessage(GasDiaryMessages.Description));
		filterLayout.addComponent(codeFilter);
		filterLayout.addComponent(descriptionFilter);

		codeFilter.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();

				if (filter != null)
					f.removeContainerFilter(filter);
				filter = new Like("code", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});

		descriptionFilter.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();

				if (filter != null)
					f.removeContainerFilter(filter);
				filter = new Like("description", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});
		codeFilter.setImmediate(true);
		descriptionFilter.setImmediate(true);

		vlayout.addComponent(filterLayout);
		HorizontalLayout Hlayout = new HorizontalLayout();
		Hlayout.setSizeUndefined();
		Hlayout.addComponent(vlayout);
		Hlayout.addComponent(layout);

		return Hlayout;
	}
	
	private QualityStatus qualityStatusForText(String text) {
		for(QualityStatus qs : qualityStatusValues) {
			if(qs.getDescription().equals(text))
				return qs;
		}
		return null;
	}
	
	private void setTableData(Table table, List<StockKeepingUnit> skus) {
		for(StockKeepingUnit unit : skus) {
			addSkuToTable(table, unit);
		}
	}
	
	@SuppressWarnings("unchecked")
	private Item addSkuToTable(Table table, StockKeepingUnit unit) {
		try {
			Integer itemID = allSKUs.indexOf(unit) + 1;
			 Object id = table.addItem(new Object[]{unit.getCode(), unit.getDescription(), unit.getMaxCartonCount(),
					unit.qualityStatusText, unit.getLiftingNumber(), unit.getLifeTimeDays()}, itemID);
			return table.getItem(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String qualityStatusForID(Integer id) {
		for(QualityStatus qs : qualityStatusValues) {
			if(qs.getId() == id) return qs.getDescription();
		}
		return "";
	}
	
	private void addQualityStatusToSKUs() {
		for(StockKeepingUnit unit : allSKUs) {
			unit.qualityStatusText = qualityStatusForID(unit.getDefaultQualityStatus());
		}
	}
	

	public List<StockKeepingUnit> getListOfSKU() {
		ArrayList<StockKeepingUnit> lSKU = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i < allSKUsize; i++) {
			try {
				lSKU.add(allSKUs.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i + 1;
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lSKU;
	}

	public List<StockKeepingUnit> getPerviousListOfSKU() {
		ArrayList<StockKeepingUnit> lSKU = new ArrayList<>();
		int count = 0;
		for (int i = currentIndex; i >= 0; i--) {
			try {
				lSKU.add(allSKUs.get(i));
				count++;
				System.err.println(count);
				if (count == 10) {
					currentIndex = i;
					System.err.println("break");
					break;
				}
			} catch (Exception ex) {
			}
		}
		return lSKU;
	}

	@Override
	public void setLocale(Locale locale) {
		super.setLocale(locale);
		i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	}

	/** Returns the bundle for the current locale. */
	public ResourceBundle getBundle() {
		return i18nBundle;
	}

	/**
	 * Returns a localized message from the resource bundle with the current
	 * application locale.
	 **/
	public String getMessage(String key) {
		return i18nBundle.getString(key);
	}
}
