package com.robovics.wims.model.service.transaction.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.masterData.BinSlot;
import com.robovics.wims.model.orm.entities.masterData.CurrentBinItem;
import com.robovics.wims.model.orm.entities.staff.SiteAdmin;
import com.robovics.wims.model.orm.entities.staff.SiteLabor;
import com.robovics.wims.model.orm.entities.staff.Staff;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteTransfer;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.itemBin.TransferItemBin;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderTransfer;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.masterData.MasterDataService;


@Service(value = "transactionOrderTransferService")
public class TransactionOrderTransferService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	@Autowired
	private MasterDataService masterService;
	public class TransferTransView {
		private Integer id;
		private Integer idSotFrom;
		private Integer idSotTo;
		private String binSymbolFrom;
		private String binSymbolTo;
		private Integer yfrom;
		private Integer yto;
		private Integer zfrom;
		private Integer zto;
		private Integer toi;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getIdSotFrom() {
			return idSotFrom;
		}

		public void setIdSotFrom(Integer idSotFrom) {
			this.idSotFrom = idSotFrom;
		}

		public Integer getIdSotTo() {
			return idSotTo;
		}

		public void setIdSotTo(Integer idSotTo) {
			this.idSotTo = idSotTo;
		}

		public String getBinSymbolFrom() {
			return binSymbolFrom;
		}

		public void setBinSymbolFrom(Integer idSotFrom) {
			this.binSymbolFrom = masterService.getBinSymbolFromSlotId(idSotFrom);
		}

		public String getBinSymbolTo() {
			return binSymbolTo;
		}

		public void setBinSymbolTo(Integer idSotTo) {
			this.binSymbolTo = masterService.getBinSymbolFromSlotId(idSotTo);
		}

		public Integer getYfrom() {
			return yfrom;
		}

		public void setYfrom(Integer yfrom) {
			this.yfrom = yfrom;
		}

		public Integer getYto() {
			return yto;
		}

		public void setYto(Integer yto) {
			this.yto = yto;
		}

		public Integer getZfrom() {
			return zfrom;
		}

		public void setZfrom(Integer zfrom) {
			this.zfrom = zfrom;
		}

		public Integer getZto() {
			return zto;
		}

		public void setZto(Integer zto) {
			this.zto = zto;
		}

		public Integer getToi() {
			return toi;
		}

		public void setToi(Integer to) {
			for (TransactionOrderItem TOI : dao
					.getAllEntities(TransactionOrderItem.class)) {
				if (TOI.getTransactionOrderId() == to) {
					this.toi = TOI.getId();
				}
			}
		}
	}

	// kamel
	public ArrayList<TransferTransView> getAllTransferOrders(int site_Id) {
		ArrayList<TransferTransView> TTV_List = new ArrayList<>();
		List<TransactionOrder> TO_List = dao
				.getAllEntities(TransactionOrder.class);
		for (TransactionOrder TO : TO_List) {
			if (TO.getTransactionTypeId() == 3 && TO.getExecuted() == 0 && dao.getEntityById(TO.getSiteAdminId()
					,SiteAdmin.class , "id").getSiteId()==site_Id) {
				for (TransactionOrderTransfer TOT : dao
						.getAllEntities(TransactionOrderTransfer.class)) {
					if (TOT.getTransactionOrderId() == TO.getId()) {
						TransferTransView TTV = new TransferTransView();
						TTV.setBinSymbolFrom(TOT.getFromSlotId());
						TTV.setBinSymbolTo(TOT.getToSlotId());
						TTV.setIdSotFrom(TOT.getFromSlotId());
						TTV.setIdSotTo(TOT.getToSlotId());
						BinSlot SotFrom = dao.getEntityById(
								TOT.getFromSlotId(), BinSlot.class, "id");
						BinSlot SotTo = dao.getEntityById(TOT.getToSlotId(),
								BinSlot.class, "id");
						TTV.setYfrom(SotFrom.getY());
						TTV.setYto(SotTo.getY());
						TTV.setZfrom(SotFrom.getZ());
						TTV.setZto(SotTo.getZ());
						TTV.setId(TOT.getId());
						TTV.setToi(TO.getId());
						TTV_List.add(TTV);
					}
				}
			}
		}
		return TTV_List;
	}

	// kamel
	public int authenticate_labor(String username, String pass) {
		for (Staff s : dao.getAllEntities(Staff.class)) {
			if (s.getUname().equals(username) && s.getPassword().equals(pass)) {
				if (s.getPriviladge() == 3) {
					for (SiteLabor sl : dao.getAllEntities(SiteLabor.class)) {
						if (sl.getStaffId() == s.getId()) {
							return sl.getId();
						}
					}
				}
			}
		}
		return -1;
	}

	// kamel
	public void execute_transfer_order(int sitelabor_Id, int TOT_Id,int TOI_Id, int from_Sot, int to_Sot) throws BusinessException {
		
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			TransactionExecuteTransfer TET = new TransactionExecuteTransfer();
			TET.setSiteLaborId(1);
			TET.setTransactionOrderTransferId(TOT_Id);
			dao.addEntity(TET, session);
			
			int TET_Id = TET.getId();
			
			TransactionOrderItem TOI_Obj = dao.getEntityById(TOI_Id,TransactionOrderItem.class, "id");
			TransactionOrder tod = dao.getEntityById(TOI_Obj.getTransactionOrderId(), TransactionOrder.class, "id");
			
			int SKU_Id = TOI_Obj.getStockKeepingUnitId();
			StockKeepingUnitService skuserv = new StockKeepingUnitService();
			StockKeepingUnit sku_ob = skuserv.getSKUById(SKU_Id);
			String serial = sku_ob.getDescription();
			TransferItemBin TIB = new TransferItemBin();
			TIB.setTransactionExecuteTransferId(TET_Id);
			TIB.setTransactionOrderItemId(TOI_Id);
			TIB.setFormslotId(from_Sot);
			TIB.setToslotId(to_Sot);
			TIB.setSerial(serial);
			dao.addEntity(TIB, session);
			// please update:
			// Update the row which contains the Old SOT_Id with the new SOT_Id
			// update the transaction_order table and set the "is_Executed" field to one .
			TransactionOrderTransfer tot = dao.getEntityById(TOT_Id, TransactionOrderTransfer.class, "id");
			CurrentBinItem cbi =  masterService.getCBIBySlotId(tot.getFromSlotId());
			cbi.setSlotId(tot.getToSlotId());
			tod.setExecuted(1);
			dao.updateEntity(tod, session);
			dao.updateEntity(cbi, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	
	/*
	 * Modification by Tariq Shata 
	 * removed weired siteCfg
	 * */
	
	public void execute_transfer_transaction(int adminId, String item_Serial,int toSlotId , int fromSlotID) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			/*
			 * Tariq Shatat 
			 * Comments for m.Kamel Code .  
			 * 
			 * */
			TransactionOrder TO = new TransactionOrder();
			TO.setExecuted(0);
			TO.setItemsQuantity(1);
			TO.setOrderDate(new Date());
			TO.setTransactionTime(new Date());
			TO.setSiteAdminId(adminId);
			TO.setTransactionTypeId(3);
			session.beginTransaction();
			dao.addEntity(TO, session);
			int OrderID = TO.getId();
			/*Weired Code 
			 * Fatal Mistake occurs 
			 * Supported Two SKUS
			 * 
			 * */
			/*
			int SKU_CODE =-1 ;
			//masterService.getSKUBySerial(	);
			
			
			for (StockKeepingUnit SKU : dao.getAllEntities(StockKeepingUnit.class)) {
				if (SKU.getCode().equals(SKU_code)) {
					SKU_CODE = SKU.getId();
				}
			}
			
			TransactionOrderItem TOI = null;
			for (TransactionOrderItem TOI_iterate : dao.getAllEntities(TransactionOrderItem.class)) {
				if (TOI_iterate.getStockKeepingUnitId() == SKU_CODE) {
					TOI = TOI_iterate;
				}
			}
			*/
			
			/// Add the new Transaction Order Item  . 
			ReceiveItemBin rib = null;
			for (ReceiveItemBin rib_iterate : dao.getAllEntities(ReceiveItemBin.class)) {
				if (rib_iterate.getSerial().equals(item_Serial)) {
					rib = rib_iterate;
				}
			}
			TransactionOrderItem TOI = dao.getEntityById(rib.getTransactionOrderItemId(), TransactionOrderItem.class, "id");
			
			TransactionOrderItem TOI_Inserted = new TransactionOrderItem();
			TOI_Inserted.setTransactionOrderId(OrderID);
			TOI_Inserted.setStockKeepingUnitId(TOI.getStockKeepingUnitId());
			TOI_Inserted.setUnitTypeId(TOI.getUnitTypeId());
			TOI_Inserted.setIsContained(TOI.getIsContained());
			TOI_Inserted.setIsContaining(TOI.getIsContaining());
			TOI_Inserted.setProductionDate(TOI.getProductionDate());
			TOI_Inserted.setExpirationDate(TOI.getExpirationDate());
			dao.addEntity(TOI_Inserted, session);
			
			
			//int binCfg = masterService.getCurrentBinConf(siteCfg);
			TransactionOrderTransfer TOT = new TransactionOrderTransfer();
			TOT.setTransactionOrderId(OrderID);
			TOT.setToSlotId(toSlotId);
			TOT.setFromSlotId(fromSlotID);
			dao.addEntity(TOT, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}


}