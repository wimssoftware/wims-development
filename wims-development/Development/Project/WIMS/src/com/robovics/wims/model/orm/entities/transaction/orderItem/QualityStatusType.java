package com.robovics.wims.model.orm.entities.transaction.orderItem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Quality_Status_Type")
@Entity
public class QualityStatusType extends BaseEntity {

	private static final long serialVersionUID = 866955019089608329L;
	private Integer id;
	private String description;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "QST_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "QST_Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}