package com.robovics.wims.model.orm.entities.transaction.itemBin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Issue_Item_Bin")
@Entity
public class IssueItemBin extends BaseEntity {

	private static final long serialVersionUID = -8695744811451228998L;
	private Integer id;
	private Integer transactionExecuteIssueId;
	private Integer transactionOrderItemId;
 	private Integer slotId;
	private Date executionTime;
	private String serial; 
	
	
	

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "SIB_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "SIB_TES_Id")
	public Integer getTransactionExecuteIssueId() {
		return transactionExecuteIssueId;
	}

	public void setTransactionExecuteIssueId(Integer transactionExecuteIssueId) {
		this.transactionExecuteIssueId = transactionExecuteIssueId;
	}

	@Column(name = "SIB_TOI_Id")
	public Integer getTransactionOrderItemId() {
		return transactionOrderItemId;
	}

	public void setTransactionOrderItemId(Integer transactionOrderItemId) {
		this.transactionOrderItemId = transactionOrderItemId;
	}

	@Column(name = "SIB_SOT_Id")
	public Integer getSlotId() {
		return slotId;
	}

	public void setSlotId(Integer slotId) {
		this.slotId = slotId;
	}
	
	@Column(name = "SIB_Serial")
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	
	@Column(name = "SIB_ExecuteTime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}
}