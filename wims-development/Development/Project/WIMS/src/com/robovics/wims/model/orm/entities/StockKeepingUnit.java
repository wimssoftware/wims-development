package com.robovics.wims.model.orm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@NamedQueries(value = {
		
		@NamedQuery(name = "GetmaxCartonCount", query = " select sku.maxCartonCount "+
				" from StockKeepingUnit sku "+
				" where sku.id = :P_skuId "
					) 
					
		})


@Table(name = "Stock_Keeping_Unit")
@Entity
public class StockKeepingUnit extends BaseEntity {

	private static final long serialVersionUID = 4323565460988709579L;
	private Integer id;
	private String code;
	private String description;
	private Integer liftingNumber;
	private Integer maxCartonCount;
	private Integer lifeTimeYears;
	private Integer lifeTimeMonths;
	private Integer lifeTimeDays;
	private Integer defaultQualityStatus;	
	
	@Transient
	public String qualityStatusText;;
 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "SKU_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "SKU_Code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "SKU_Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@Column(name = "Max_Carton_Count")
	public Integer getMaxCartonCount() {
		return maxCartonCount;
	}

	public void setMaxCartonCount(Integer maxCartonCount) {
		this.maxCartonCount = maxCartonCount;
	}
	
	@Column(name = "SKU_LiftingNumber")
	public Integer getLiftingNumber() {
		return liftingNumber;
	}

	public void setLiftingNumber(Integer liftingNumber) {
		this.liftingNumber = liftingNumber;
	}

	@Column(name = "SKU_LifeTimeYears")
	public Integer getLifeTimeYears() {
		return lifeTimeYears;
	}

	public void setLifeTimeYears(Integer lifeTimeYears) {
		this.lifeTimeYears = lifeTimeYears;
	}

	@Column(name = "SKU_LifeTimeMonths")
	public Integer getLifeTimeMonths() {
		return lifeTimeMonths;
	}

	public void setLifeTimeMonths(Integer lifeTimeMonths) {
		this.lifeTimeMonths = lifeTimeMonths;
	}

	@Column(name = "SKU_LifeTimeDays")
	public Integer getLifeTimeDays() {
		return lifeTimeDays;
	}

	public void setLifeTimeDays(Integer lifeTimeDays) {
		this.lifeTimeDays = lifeTimeDays;
	}
	
	@Column(name = "SKU_QST_Id")
	public Integer getDefaultQualityStatus() {
		return defaultQualityStatus;
	}

	public void setDefaultQualityStatus(Integer defaultQualityStatus) {
		this.defaultQualityStatus = defaultQualityStatus;
	}
	
}