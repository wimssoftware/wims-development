package com.robovics.wims.user_session;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;



//@Component(value = "userSession")

public  class user_session {
	private static user_session instance;
	public static Integer user_id ;
	public static boolean IsLogin ;
	public static Integer user_priviladge;
	public static List<Integer> site_ids;
	public static Locale current_local; 
	public static user_session getInstance() {
		if (instance == null)
			instance = new user_session();
		return (user_session)instance;
	}
	private user_session(){
	    user_id = -1;
	    IsLogin = false;
	    user_priviladge = -1;
	    site_ids = new ArrayList<>();
	    current_local = new Locale ("en");
	}
}
