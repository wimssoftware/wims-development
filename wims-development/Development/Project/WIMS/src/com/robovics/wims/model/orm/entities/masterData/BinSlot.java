package com.robovics.wims.model.orm.entities.masterData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;
import com.vaadin.data.fieldgroup.PropertyId;

@NamedQueries(value = {
		
		@NamedQuery(name = "getSlotsOfSitebyConfig", query = " select distinct b.id "
				+ " from BinSlot b  "
				+ " where b.binSlotConfigurationId = :GBN_Id "
					),
					
		@NamedQuery(name = "getSlotsOfSite", query = " select distinct b.id from BinSlotConfiguration s , BinSlot b  "
				+ " where b.binSlotConfigurationId = s.id and  s.groupBinId = :GBN_Id   and "
				+ "  s.dateTime in (select max(f.dateTime) from BinSlotConfiguration f where f.groupBinId = :GBN_Id and f.dateTime <= :P_Date )   "
					),					
		@NamedQuery(name = "getAllSlotIdsInBinSlotConfigId", query = " select b.id from BinSlot b "
							+ " where b.binSlotConfigurationId =:P_BinSlotConfigId "
					)			
		})


@Table(name = "slot")
@Entity
public class BinSlot extends BaseEntity {

	private static final long serialVersionUID = -7608611587694146908L;
	private Integer id;
	private Integer binSlotConfigurationId;
	private Integer x;
	private Integer y;
	private Integer z;
	private Integer binSymbol;
	private String Serial;
	
	@Column(name = "SOT_Serial")
	public String getSerial() {
		return Serial;
	}

	public void setSerial(String binSerial) {
		this.Serial = binSerial;
	}

	@PropertyId(value = "slotSymbol") 
	private String slotSymbol ;
	


	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "SOT_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "SOT_BSC_Id")
	public Integer getBinSlotConfigurationId() {
		return binSlotConfigurationId;
	}

	public void setBinSlotConfigurationId(Integer binSlotConfigurationId) {
		this.binSlotConfigurationId = binSlotConfigurationId;
	}

	@Column(name = "SOT_X")
	
	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}
	@Column(name = "SOT_Y")
	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}
	
	@Column(name = "SOT_Z")
	public Integer getZ() {
		return z;
	}

	public void setZ(Integer z) {
		this.z = z;
	}
	@Transient
	public String getSlotSymbol() {
		return slotSymbol;
	}

	public void setSlotSymbol(String slotSymbol) {
		this.slotSymbol = slotSymbol;
	}
	@Transient  
	public Integer getBinSymbol() {
		return binSymbol;
	}

	public void setBinSymbol(Integer binSymbol) {
		this.binSymbol = binSymbol;
	}
}