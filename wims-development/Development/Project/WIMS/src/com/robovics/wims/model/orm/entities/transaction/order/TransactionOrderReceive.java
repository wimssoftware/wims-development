package com.robovics.wims.model.orm.entities.transaction.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		@NamedQuery(name = "getReceiveOrdersElements", query = "select a.id from TransactionOrderReceive a  where a.transactionOrderId =:P_ID")
		}) 

@Table(name = "Transaction_Order_Receive")
@Entity
public class TransactionOrderReceive extends BaseEntity {

	private static final long serialVersionUID = 5406220280969109183L;
	private Integer id;
	private Integer transactionOrderId;
	private Integer ownerId;
	private Integer supplierId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TOR_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TOR_TOD_Id")
	public Integer getTransactionOrderId() {
		return transactionOrderId;
	}

	public void setTransactionOrderId(Integer transactionOrderId) {
		this.transactionOrderId = transactionOrderId;
	}

	@Column(name = "TOR_OWN_Id")
	public Integer getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}

	@Column(name = "TOR_SUP_Id")
	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}
}