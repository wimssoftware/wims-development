package com.robovics.wims.model.orm.entities.transaction.itemBin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {

		@NamedQuery(name = "getItemLocation", query = " select o.slotId from ReceiveItemBin o"
				+ " where o.transactionOrderItemId =:P_itemId ")
		})


@Table(name = "Receive_Item_Bin")
@Entity
public class ReceiveItemBin extends BaseEntity {

	private static final long serialVersionUID = 8852899365585557684L;
	private Integer id;
	private Integer transactionExecuteReceiveId;
	private Integer transactionOrderItemId;
	private Integer slotId;
	private Date executionTime;
	private String serial; 
	private Date produDate;
	
	

	

	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "RIB_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "RIB_TER_Id")
	public Integer getTransactionExecuteReceiveId() {
		return transactionExecuteReceiveId;
	}

	public void setTransactionExecuteReceiveId(
			Integer transactionExecuteReceiveId) {
		this.transactionExecuteReceiveId = transactionExecuteReceiveId;
	}

	@Column(name = "RIB_TOI_Id")
	public Integer getTransactionOrderItemId() {
		return transactionOrderItemId;
	}

	public void setTransactionOrderItemId(Integer transactionOrderItemId) {
		this.transactionOrderItemId = transactionOrderItemId;
	}

	@Column(name = "RIB_SOT_Id")
	public Integer getSlotId() {
		return slotId;
	}

	public void setSlotId(Integer slotId) {
		this.slotId = slotId;
	}


	
	@Column(name = "RIB_Serial")
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	


	@Column(name = "RIB_ExecuteTime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}
	
	@Transient
	public Date getProduDate() {
		return produDate;
	}

	public void setProduDate(Date produDate) {
		this.produDate = produDate;
	}

	
}