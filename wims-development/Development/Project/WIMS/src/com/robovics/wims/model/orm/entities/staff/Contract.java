package com.robovics.wims.model.orm.entities.staff;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "contract")
@Entity
public class Contract extends BaseEntity {

/**
	 * 
	 */
	private static final long serialVersionUID = -5813475077867962059L;
	private Integer id;
	private Integer ownerid;
	private Integer skuid;
	private Integer siteid;
	private Integer supplierid;
	private Integer active;
	private Date datetime;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CRT_ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CRT_Own_ID")
	public Integer getOwnerid() {
		return ownerid;
	}

	public void setOwnerid(Integer ownerid) {
		this.ownerid = ownerid;
	}

	@Column(name = "CRT_SKU_ID")
	public Integer getSkuid() {
		return skuid;
	}

	public void setSkuid(Integer skuid) {
		this.skuid = skuid;
	}

	@Column(name = "CRT_STE_ID")
	public Integer getSiteid() {
		return siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

	@Column(name = "CRT_supplier_ID")
	public Integer getSupplierid() {
		return supplierid;
	}

	public void setSupplierid(Integer supplierid) {
		this.supplierid = supplierid;
	}

	@Column(name = "CRT_Active")
	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Column(name = "CRT_DateTime")
	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
}
