package com.robovics.wims.model.enums;

public enum QueryNamesEnum {

	GET_UN_EXECUTED_TRANSACTION_ORDERS_BY_TYPE("TransactionOrder_getUnExecutedByType"),
	GET_SLOTS_SKU_ID("SLot_getBySKUId") , 
	GET_ALL_PINS_IN_SSITE("CurrentBinItem_getAllItemBySiteNo"), 
	GET_ALL_CARTONS_IN_SLOT_ID("GetAllCartonsInSlotId"),
	GET_NO_OF_CARTONS_IN_SLOTID("GetNoOfCartonsInSlotId"),
			GET_ALL_SlOTS_OF_SITE("CurrentBinItem_getAllSlots") , 
			GET_ALL_SLOTS_OF_SITE_CONFIG("getSlotsOfSitebyConfig"),
			GET_COUNT_PER_BIN("CurrentBin_getCount"), 
			GET_ALL_SERIALS_OF_SLOT("CurrentBin_getSerials") ,GET_Latest_BINS_OF_SITE_EMPTY_non_EMPTY("GroupBin_getAllBins") ,
			GET_EMPTY_BINS("GroupBin_getEmptyBins"), GET_Latest_Configuration("Site_getLatestConfiguration"),Login("UserInfo"),GET_Site_Admin_Id("GET_Site_Id_By_Admin_Id"),
			GET_Site_Worker_Id("GET_Site_Id_By_Worker_Id"),GET_ALL_RECEIVE_ORDERS("getReceiveOrdersElements")	,
			GET_ALL_ISSUE_ORDERS("getIssueOrdersElements")	,GET_GROUBBINS_BY_SITE_ID("getGroupBinsOfSite")	, GET_SKU_OF_SLOT("GetSKUofSlot"),
			GET_SLOT_BY_BIN_ID("getSlotsOfSite"), GET_CBI_BY_SLOT_ID("CurrentBinItem_getbySlotId"), 
			GET_SKU_OF_OWNER("getSKUsOfOwner"),GET_Available_Credit_Of_Owner("getAvailableBalanceOfOwner"),
			GET_FIRSTEXPIRED_ORDER_ITEMS_OF_SITE_SKU("getFirstExpiredOrderItemsInSiteFromSKU"),
			GET_FIRSTEXPIRED_ORDER_ITEMS_OF_SITE_SKU_ExceptDate("getFirstExpiredOrderItemsInSiteFromSKUExceptDate"),
			GET_SCBG_GET_SCBG("sCBG_get_sCBG"),
			GET_ALL_BIN_GROUPS_IN_SITE_CONFIG("getAllBinGroupsInSiteConfig"),
			GET_ALL_BINS_IN_BIN_GROUP_CONFIG("getAllBinsInBinGroupConfig"),
			GET_ALL_SLOTS_IN_BIN_SLOT_CONFIG("getAllSlotIdsInBinSlotConfigId"),
			GET_SLOT_ID_OF_ITEM("GetSlotIdOfItem"),
			GET_ALL_RECIVED_ITEM_IDS("getAllRecivedItemIds"),
			GET_ALL_ISSUE_ITEM_IDS("getAllIssueItemIds"),
			GET_ALL_TRANSFER_ITEM_IDS("getAllTransferItemIds"),
			GET_ITEM_LOCATION("getItemLocation"),
			GET_ITEM_IN_SLOT_ID("GetItemInSlotId"),
			GET_CURRENT_BIN_ITEM_OF_ITEM_Id("CurrentBinItem_getbyItemId"),
			GET_ALL_TRANSFER_ITEM_IDS_OF_ITEM_ID("getAllTransferItemIdForItemId"),
			GET_BIN_GROUP("getBingroup"),
			GET_CARTONINPALET_ID("GetCartonInPalletId"),	
			GET_TO_PAlLET_ID("topaletid"),
			LOGIN_LABOR("Login_Labor"),
			GET_OWNER_ID_OF_SLOT("GetOwnerIdofSlot"),
			GET_NO_OF_CARTONS_IN_PALET("GetNoOfCartonsIn"),
			GET_OWNER_ID_OF_ITEM("GetOwnerOfItem"),
			GET_SITE_ID_OF_ITEM("GetSiteIdOfItem"),
			GET_MAX_CARTON_COUNT("GetmaxCartonCount"),
			GET_TRANSFER_ITEM_LOCATION("getTranferItemLocation");
	public final String queryName;

	private QueryNamesEnum(String queryName) {
		this.queryName = queryName;
	}
}