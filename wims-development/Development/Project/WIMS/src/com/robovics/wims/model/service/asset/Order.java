package com.robovics.wims.model.service.asset;

import java.util.Date;



import com.robovics.wims.model.orm.entities.*;
import com.robovics.wims.model.enums.*;

public class Order {
	
	public Order(){}
	
	public UnitOfMeasurement uom ;
	public TransactionTypesEnum type; 
	public StockKeepingUnit relevantSKU = new StockKeepingUnit();
	public int Quantity ;
	public String batchNo;
	public int binNo;
	public String binSymbol ;
	public Date transactionDate ;
	public int customerId ; 
	public int ownerId ;
}
