package com.robovics.wims.model.enums;

public enum StaffTypeEnum {

	WAREHOUSEADMIN(1), SITEADMIN(2), LABOR(3);

	public final int id;

	private StaffTypeEnum(int id) {
		this.id = id;
	}
}