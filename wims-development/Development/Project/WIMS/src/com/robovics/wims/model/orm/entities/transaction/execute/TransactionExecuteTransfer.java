package com.robovics.wims.model.orm.entities.transaction.execute;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {

		@NamedQuery(name = "getAllTransferItemIdForItemId", query = " select e.id from TransactionExecuteTransfer e "
				+ " where e.transactionOrderTransferId =:P_itemId ")
		})

@Table(name = "Transaction_Execute_Transfer")
@Entity
public class TransactionExecuteTransfer extends BaseEntity {

	private static final long serialVersionUID = 1146045085736709301L;
	private Integer id;
	private Integer siteLaborId;
	private Integer transactionOrderTransferId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TET_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TET_SLB_Id")
	public Integer getSiteLaborId() {
		return siteLaborId;
	}

	public void setSiteLaborId(Integer siteLaborId) {
		this.siteLaborId = siteLaborId;
	}

	@Column(name = "TET_TOT_Id")
	public Integer getTransactionOrderTransferId() {
		return transactionOrderTransferId;
	}

	public void setTransactionOrderTransferId(Integer transactionOrderTransferId) {
		this.transactionOrderTransferId = transactionOrderTransferId;
	}
}