package com.robovics.wims.model.orm.entities.transaction.execute;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {

		@NamedQuery(name = "getAllIssueItemIds", query = " select o.transactionOrderId from TransactionExecuteIssue e , TransactionOrderIssue o "
				+ " where e.transactionOrderIssueId = o.id ")
		})


@Table(name = "Transaction_Execute_Issue")
@Entity
public class TransactionExecuteIssue extends BaseEntity {

	private static final long serialVersionUID = 8931373775909625530L;
	private Integer id;
	private Integer siteLaborId;
	private Integer transactionOrderIssueId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TES_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TES_SLB_Id")
	public Integer getSiteLaborId() {
		return siteLaborId;
	}

	public void setSiteLaborId(Integer siteLaborId) {
		this.siteLaborId = siteLaborId;
	}

	@Column(name = "TES_TOS_Id")
	public Integer getTransactionOrderIssueId() {
		return transactionOrderIssueId;
	}

	public void setTransactionOrderIssueId(Integer transactionOrderIssueId) {
		this.transactionOrderIssueId = transactionOrderIssueId;
	}
}