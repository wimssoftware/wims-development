package com.robovics.wims.ui.sitePortal;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import oracle.jdbc.util.Login;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.service.staff.StaffService;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window;

public class LaborLoginUI extends UI{

	
	private HorizontalLayout headerLayout = new HorizontalLayout();
	private HorizontalLayout footerLayout = new HorizontalLayout();
	private FormLayout layout = new FormLayout();
	
	PropertysetItem itemsProperty = new PropertysetItem();
	
	@PropertyId("userName")
	   private TextField UserName = new TextField("Labor Name");
	
	 @PropertyId("password")
	   private PasswordField Password = new PasswordField("Password");
	 
	 Button ok = new Button("Commit");
	   
	   
	@WebServlet(value = "/laborLogin/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = LaborLoginUI.class)
	public static class Servlet extends VaadinServlet {
	}
	
	@Override
	protected void init(VaadinRequest request) {
		// TODO Auto-generated method stub
		
		addWindow(buildForm());
	}
	
	
	private Window buildForm()
	{
		VerticalLayout Vlay = new VerticalLayout();
		layout = IntergrateFormLayout();
		final Window LoginWindow = new Window("Login Window");
		LoginWindow.setHeight("250px");
		LoginWindow.setWidth("350px");
		LoginWindow.center();
		LoginWindow.setResizable(false);
		LoginWindow.setClosable(false);
		LoginWindow.setDraggable(false);
		
		//headerLayout.addComponent(new Label("Header"));
				
		Vlay.addComponent(headerLayout);
		Vlay.addComponent(layout);
		Vlay.setComponentAlignment(layout, Alignment.MIDDLE_RIGHT);
		Vlay.addComponent(ok);
		Vlay.setComponentAlignment(ok, Alignment.BOTTOM_CENTER);
		Vlay.addComponent(footerLayout);
		Vlay.setMargin(true);
		Vlay.setSpacing(true);
		ok.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				StaffService ss = new StaffService();
				try {
					Integer labor_id = ss.authenticateLabor(
							UserName.getValue(), Password.getValue());
					
					System.out.println("labor id: "+labor_id);
					
					if (labor_id != -1) {
						LoginWindow.close();
						// TODO:Add call back 
					} else {
						
						Notification not = new Notification(
								"Notification error",
								"login failed' please try again");
						not.show(Page.getCurrent());
						
					}
				} catch (BusinessException e) {
					Notification not = new Notification("Notification error", e
							.getMessage());
					not.show(Page.getCurrent());
					e.printStackTrace();
				}

			}
		});
		 LoginWindow.setContent(Vlay);
		 
			return LoginWindow;
	}
	private FormLayout IntergrateFormLayout()
	{
		itemsProperty.addItemProperty("userName",new ObjectProperty<String>("") );
       	itemsProperty.addItemProperty("password",new ObjectProperty<String>("") );
		
              	
       	layout.addComponent(this.UserName);
       	layout.addComponent(this.Password);
      
       	
    	FieldGroup form = new FieldGroup(itemsProperty);
       	form.bindMemberFields(layout);
       	
       	return layout;
		
	}
}

