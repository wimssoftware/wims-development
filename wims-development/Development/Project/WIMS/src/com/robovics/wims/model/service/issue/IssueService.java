package com.robovics.wims.model.service.issue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.contact.Supplier;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.orm.entities.transaction.orderItem.UnitType;
import com.robovics.wims.model.service.transaction.TransactionEntity;

@Service(value = "issueService")
public class IssueService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	public List<StockKeepingUnit> getAllStockKeepingUnits() {
		return dao.getAllEntities(StockKeepingUnit.class);
	}

	public List<Owner> getAllOwners() {
		return dao.getAllEntities(Owner.class);
	}

	public List<Supplier> getAllSuppliers() {
		return dao.getAllEntities(Supplier.class);
	}

	public List<UnitType> getAllUnitTypes() {
		return dao.getAllEntities(UnitType.class);
	}

	public List<TransactionOrderIssue> getAllChidOrders(int todId){
		
		List<TransactionOrderIssue> toIssue = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_ID", todId);
			List<Integer> Ids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_ALL_ISSUE_ORDERS.queryName,
					parameters);
			
			for (Integer id : Ids){
				toIssue.add(dao.getEntityById(id, TransactionOrderIssue.class, "id"));
			}
			
						
		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return toIssue ;
	}
	public void addTransactionOrder(TransactionOrder transactionOrder,
			ArrayList<TransactionOrderItem> items,
			HashSet<TransactionOrderReceive> tors) throws BusinessException {
		try {
			dao.addEntity(transactionOrder);
			Integer transactionOrderId = transactionOrder.getId();
			for (TransactionOrderItem item : items) {
				try {
					item.setTransactionOrderId(transactionOrderId);
					dao.addEntity(item);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			for (TransactionOrderReceive tor : tors) {
				try {
					tor.setTransactionOrderId(transactionOrderId);
					dao.addEntity(tor);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}
	
	public void addNewOrder (TransactionEntity trE){
		TransactionOrder tod = new TransactionOrder(); 
		tod.setExecuted(0);
		tod.setSiteAdminId(trE.getSiteAdminId());
		System.out.println(trE.getItemsQuantity());
		tod.setItemsQuantity(trE.getItemsQuantity());
		System.out.println(trE.getOrderDate().toString());
		tod.setOrderDate(trE.getOrderDate());
		tod.setTransactionTime(trE.getTransactionTime());
		tod.setTransactionTypeId(trE.getTransactionTypeId());
		try {
			dao.addEntity(tod);
			trE.setTodId(tod.getId());
			
			System.out.println("Susccess2");
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fail2");
		}
		if(tod.getTransactionTypeId()==1){
			addIssueOrder(trE);
			System.out.println("Susccess3");
		}
	}

	public void addIssueOrder (TransactionEntity trE) {
		
			for (int i=0 ; i<trE.getItemsQuantity();i++) {
				try {
					TransactionOrderItem item = new TransactionOrderItem();
					TransactionOrderIssue tor = new TransactionOrderIssue();
					item.setTransactionOrderId(trE.getTodId());
					item.setIsContained(trE.getIsContained());
					item.setIsContaining(trE.getIsContaining());
					item.setStockKeepingUnitId(trE.getStockKeepingUnitId());
					item.setUnitTypeId(trE.getUnitTypeId());
					dao.addEntity(item);
					tor.setTransactionOrderId(trE.getTodId());
					tor.setOwnerId(trE.getOwnerId());
					tor.setCustomerId(trE.getCustomerId());
					dao.addEntity(tor);
					
				} catch (DatabaseException e) {
					e.printStackTrace();
					try {
						throw new BusinessException("Internal Database Error");
					} catch (BusinessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}	
	}
}