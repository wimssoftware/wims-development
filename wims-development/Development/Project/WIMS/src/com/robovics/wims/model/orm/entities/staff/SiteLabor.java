package com.robovics.wims.model.orm.entities.staff;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		
		@NamedQuery(name = "Login_Labor", query = " select sl.id "+
				" from SiteLabor sl , Staff s"+
				" where sl.staffId = s.id and s.uname = :P_userName and s.password = :P_password "
			 		) 

		})

@Table(name = "Site_Labor")
@Entity
public class SiteLabor extends BaseEntity {

	private static final long serialVersionUID = 4036428290142134569L;
	private Integer id;
	private Integer staffId;
	private Integer siteId;
	private Date startDate;
	private Date endDate;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "SLB_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "SLB_STF_Id")
	public Integer getStaffId() {
		return staffId;
	}

	public void setStaffId(Integer staffId) {
		this.staffId = staffId;
	}

	@Column(name = "SLB_STE_Id")
	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	@Column(name = "SLB_StartDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "SLB_EndDate")
//	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}