package com.robovics.wims.exceptions;

public class ExecuteException extends Exception {

	private static final long serialVersionUID = -4802887938013524376L;
	private int type; 
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public ExecuteException(String message) {
		super(message);
	}
	public ExecuteException(int type) {
		this.type = type;
	}
}