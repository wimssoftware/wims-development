package com.robovics.wims.ui.sitePortal;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.annotation.WebServlet;









import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.TransactionTypesEnum;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.transaction.TransactionEntity;
import com.robovics.wims.model.service.transaction.TransactionEntityService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderTransferService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderTransferService.TransferTransView;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window;

@Theme("wims")
public class SiteOrdersUI extends UI  {


	/**
	 * 
	 */
	Button switchLanguage;
	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
	private static final long serialVersionUID = 1L;
	List<TransactionOrder> transactionOrders = new ArrayList<>();
	List<TransactionOrder> transactionOrdersR = new ArrayList<>();
	List<TransactionOrder> transactionOrdersI = new ArrayList<>();
	List<TransactionEntity> transactions = new ArrayList<>();
	// to be changed soon
	Integer site_id;
	
	BeanItemContainer<TransactionEntity> transactionOrderBeans = new BeanItemContainer<TransactionEntity>(
			TransactionEntity.class);
	
	private TransactionOrderService transactionOrderService;
	final TransactionOrderTransferService transferService =(TransactionOrderTransferService) ServiceFactory.getInstance()
			.getService(ServicesEnum.TRANSACTION_ORDER_TRANSFER_SERVICE);
	private HorizontalLayout headerLayout = new HorizontalLayout();
	private HorizontalLayout footerLayout = new HorizontalLayout();
	private VerticalLayout layout = new VerticalLayout();
	Table transactionOrdersTable;
	Table TransferExecute ;
	Button refreshButton ;
	
	@WebServlet(value = "/siteOrders/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = SiteOrdersUI.class)
	public static class Servlet extends VaadinServlet {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	}
	@Override
	protected void init(VaadinRequest request) {
		if (((Locale)VaadinSession.getCurrent().getAttribute("Locale"))==null){
		 	i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
		 	i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
		 	VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
	 }else {
		Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
		 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
		 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
	 }
		 switchLanguage = new Button(this.getMessage(GasDiaryMessages.switchLanguage));
		refreshButton = new Button(this.getMessage(GasDiaryMessages.refresh));
		
		transactionOrderService = (TransactionOrderService) ServiceFactory
				.getInstance().getService(ServicesEnum.TRANSACTION_ORDER_SERVICE);
			
		    
		System.out.println("___________");
		System.out.println("___________");
		System.out.println("___________");
		Map<String,String[]> att = request.getParameterMap();
		System.out.println(att.get("siteId")[0]);
		site_id = Integer.parseInt(att.get("siteId")[0]);
		System.out.println(site_id);
		System.out.println("___________");
		System.out.println("___________");
		System.out.println("___________");
		getPage().setTitle("Site "+site_id);	 
		setContent(buildForm()); 
	}
	
	@SuppressWarnings("deprecation")
	private Layout buildForm()
	{
		headerLayout.addComponent(new Image(null, new ThemeResource(
				"NTLC-Logo.JPG")));
		headerLayout.addComponent(switchLanguage);
		
		footerLayout.addComponent(new Image(null, new ThemeResource(
				 "roboVics.png")));
		
		Button refreshButton = new Button(this.getMessage(GasDiaryMessages.refresh));
		Button refreshTrasferButton = new Button(this.getMessage(GasDiaryMessages.refresh));
		
		switchLanguage.addClickListener(new ClickListener() {
  			public void buttonClick(ClickEvent event) {
  				setLocale(new Locale("en")); 
  			   // rebuild it with the new locale.
  				 if (((Locale)VaadinSession.getCurrent().getAttribute("Locale")).getLanguage().equals("fi")){
  					VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
  					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
  				}
  				else{
  					VaadinSession.getCurrent().setAttribute("Locale", new Locale("fi"));
  				i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),new Locale("fi") );
  				}
  				Page.getCurrent().getJavaScript().execute("window.location.reload();");		
  			} 
  		});
		refreshTrasferButton.addClickListener(new ClickListener() {
			

			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
			}
		});
		transactionOrdersTable = new Table("Waiting Transaction Orders");
		IntegrateTransferTable();
        TransferExecute = IntegrateTransferTable();
		
		transactionOrdersTable.setContainerDataSource(transactionOrderBeans);
		
		 

		transactionOrdersTable.setVisibleColumns(new Object[] { "orderDate", "transactionTime",
				"stockKeepingUnitDes","siteAdminName", "transactionTypeDes", "itemsQuantity","ownerName","customerName","supplierName" });
		
		transactionOrdersTable.setColumnHeaders(new String[] {this.getMessage(GasDiaryMessages.OrderDate) , this.getMessage(GasDiaryMessages.TransactionDate),
				this.getMessage(GasDiaryMessages.SKU),this.getMessage(GasDiaryMessages.siteAdminName), this.getMessage(GasDiaryMessages.TransactionType),this.getMessage(GasDiaryMessages.Quantity),this.getMessage(GasDiaryMessages.OwnerName),this.getMessage(GasDiaryMessages.customer),this.getMessage(GasDiaryMessages.customer) });
		 transactionOrdersTable.setPageLength(transactionOrdersTable.size()+1);
		 transactionOrdersTable.setSizeFull();   
		 transactionOrdersTable.setSelectable(true);
		 transactionOrdersTable.setDescription(this.getMessage(GasDiaryMessages.ClickonRowtoExcutetheOrder));
		 updateTransactionTable();
		 transactionOrdersTable.setPageLength(transactionOrdersTable.size()+1);
		 
		 transactionOrdersTable.addListener(new ItemClickListener() {

			 private static final long serialVersionUID = 1L;

				@Override
			    public void itemClick(ItemClickEvent event) {

			    	com.vaadin.data.Property itemProperty =  event.getItem().getItemProperty("transactionTypeId");
			    	System.out.println(itemProperty);
	    
			    	ExecuteOredergui executeWindow = new ExecuteOredergui();
			    	
			    	// Set window size.
			    		System.out.println("Execute window send null");
			    		executeWindow.setHeight("630px");
			    		executeWindow.setWidth("1300px");
			    		executeWindow.center();
				    	executeWindow.setResizable(false);
				    	executeWindow.setSiteNo(site_id);
				    	// Set window position.
				        if(itemProperty.getValue().equals(1))//Issue   
				        {
				        	executeWindow.setCurrentSKU(((TransactionEntity) event.getItemId()).getStockKeepingUnitId());
				        	executeWindow.setCurrentOwnerId(((TransactionEntity) event.getItemId()).getOwnerId());
				        	executeWindow.setTransactionOrderIssueIds(((TransactionEntity) event.getItemId()).getIorderids());
				        	executeWindow.setCurrentCustomerId(((TransactionEntity) event.getItemId()).getCustomerId());
				        	executeWindow.setType(1);
				        	executeWindow.setMeasure_type(((TransactionEntity) event.getItemId()).getUnitTypeId());
				        	executeWindow.setCurrentOrderId(((TransactionEntity) event.getItemId()).getTodId());
				        }
				        else if(itemProperty.getValue().equals(2))//Receive
				        {
				        	executeWindow.setCurrentSKU(((TransactionEntity) event.getItemId()).getStockKeepingUnitId());
				        	executeWindow.setCurrentOwnerId(((TransactionEntity) event.getItemId()).getOwnerId());
				        	executeWindow.setTransactionOrderReceiveIds(((TransactionEntity) event.getItemId()).getRorderids());
				        	executeWindow.setType(2);
				        	executeWindow.setMeasure_type(((TransactionEntity) event.getItemId()).getUnitTypeId());
				        	executeWindow.setCurrentOrderId(((TransactionEntity) event.getItemId()).getTodId());
				        }
				        System.out.println("no of Items = "+((TransactionEntity) event.getItemId()).getItemIds().size());
						System.out.println("no of Items = "+((TransactionEntity) event.getItemId()).getItemIds().size());
						System.out.println("no of Items = "+((TransactionEntity) event.getItemId()).getItemIds().size());
						System.out.println("no of Items = "+((TransactionEntity) event.getItemId()).getItemIds().size());
						System.out.println("no of Items = "+((TransactionEntity) event.getItemId()).getItemIds().size());
				        
				        executeWindow.setItemIds(((TransactionEntity) event.getItemId()).getItemIds());
				        try {
				        	Layout executeOrder = executeWindow.FormWindow();
				        	if(executeOrder != null){
				        		executeWindow.setContent(executeOrder);
				        		UI.getCurrent().addWindow(executeWindow);
				        	}else{
				        		System.out.println("execute window send null");
				        	}
				        	
				        } catch (DatabaseException e) {
				    	    e.printStackTrace();
				        }
			    	}
			});
		 
		 	refreshButton.addClickListener(new ClickListener() {
			
				@Override
				public void buttonClick(ClickEvent event) {
					// TODO Auto-generated method stub
					updateTransactionTable();
					transactionOrdersTable.setPageLength(transactionOrdersTable.size()+1);
				}
			}); 
			layout.addComponent(headerLayout);
			layout.addComponent(transactionOrdersTable);
			layout.addComponent(refreshButton);
			layout.addComponent(TransferExecute);
			layout.addComponent(refreshTrasferButton);
			layout.addComponent(footerLayout);
			layout.setComponentAlignment(footerLayout, Alignment.MIDDLE_CENTER);
			layout.setMargin(true);
			layout.setSpacing(true);
			
			return layout;	
	}
	
	private void updateTransactionTable(){
		transactionOrders.removeAll(transactionOrders);
		transactions.removeAll(transactions);
		transactionOrderBeans.removeAllItems();
		transactionOrdersTable.removeAllItems();
		try{
			transactionOrdersR = transactionOrderService.getUnExecutedOrdersByType(TransactionTypesEnum.RECEIVE,site_id);
			//System.out.println("R is "+transactionOrdersR.get(0).getSiteAdminName());
			for(TransactionOrder tod  :transactionOrdersR ){
				transactions.add(transactionOrderService.getTransactionEntity(tod));
			}
			transactionOrdersI =transactionOrderService.getUnExecutedOrdersByType(TransactionTypesEnum.ISSUE,site_id);
			for(TransactionOrder tod  :transactionOrdersI ){
				transactions.add(transactionOrderService.getTransactionEntity(tod));
			}
			transactionOrders.addAll(transactionOrdersR);
			transactionOrders.addAll(transactionOrdersI);
		} catch (BusinessException e) { 
			// TODO Auto-generated catch block
			System.out.println("Faieddddd");
		
			e.printStackTrace();
		} 
		transactionOrderBeans.addAll(transactions);
	}
	
	
	private Table IntegrateTransferTable()
	{
		ArrayList<TransferTransView> TTV_List = transferService.getAllTransferOrders(site_id);
		BeanItemContainer<TransferTransView> TTVBean = new BeanItemContainer<TransferTransView>(
				TransferTransView.class);
		TTVBean.addAll(TTV_List);
		final Table TransferExecute = new Table("Waiting Transfer Orders",
				TTVBean);

		TransferExecute.setVisibleColumns(new Object[] { "binSymbolFrom",
				"yfrom", "zfrom", "binSymbolTo", "yto", "zto" });
		TransferExecute.setColumnHeaders(new String[] { this.getMessage(GasDiaryMessages.FromBin),
				 this.getMessage(GasDiaryMessages.FromSlotlength),  this.getMessage(GasDiaryMessages.FromSlotHeight),
				 this.getMessage(GasDiaryMessages.ToBin), this.getMessage(GasDiaryMessages.ToSlotlength),  this.getMessage(GasDiaryMessages.ToSlotHeight) });

		TransferExecute.setPageLength(TTVBean.size());
		TransferExecute.setSizeFull();
		TransferExecute.setSelectable(true);
		TransferExecute.setDescription( this.getMessage(GasDiaryMessages.ClickonRowtoExcutetheOrder));

		TransferExecute.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				TransferTransView TTV_selected = (TransferTransView) TransferExecute
						.getValue();
				final int TOT_Id = TTV_selected.getId();
				final int TOI_Id = TTV_selected.getToi();
				final int From_SOT_Id = TTV_selected.getIdSotFrom();
				final int To_SOT_Id = TTV_selected.getIdSotTo();
				System.err.println(TOT_Id + "//" + TOI_Id + "//" + From_SOT_Id
						+ "//" + To_SOT_Id + "//");
				final Window subWindow = new Window("Labor login");
				VerticalLayout subContent = new VerticalLayout();
				subContent.setMargin(true);
				subWindow.setContent(subContent);
				final TextField username = new TextField();
				username.setInputPrompt("username");
				final TextField password = new TextField();
				password.setInputPrompt("password");
				Button sOk = new Button("Login");
				subContent.addComponent(username);
				subContent.addComponent(password);
				subContent.addComponent(sOk);
				sOk.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
						int sitelabor = transferService.authenticate_labor(
								username.getValue(), password.getValue());
						if (sitelabor == -1) {
							new Notification("Notification error",
									"login failed' please try again").show(Page
									.getCurrent());
						} else {
							try {
								transferService.execute_transfer_order(sitelabor,
										TOT_Id, TOI_Id, From_SOT_Id, To_SOT_Id);
								new Notification("Notification",
										"Executed").show(Page.getCurrent());
							} catch (BusinessException e) {
								new Notification("Notification error",
										"Error Done").show(Page.getCurrent());
							}
							subWindow.close();
						}
					}
				});
				subWindow.center();
				UI.getCurrent().addWindow(subWindow);
			}
		});
		return TransferExecute;
	}
	
	@Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
    }
    
    /** Returns the bundle for the current locale. */
    public ResourceBundle getBundle() {
        return i18nBundle;
    }
 
    /**
     * Returns a localized message from the resource bundle
     * with the current application locale.
     **/ 
    public String getMessage(String key) {
        return i18nBundle.getString(key);
    }
}
