package com.robovics.wims.model.service.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.orm.entities.contact.Customer;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.contact.Supplier;
import com.robovics.wims.model.orm.entities.staff.Staff;
import com.vaadin.ui.UI;

@Service(value = "contactService")
public class ContactService {

	public class ContactView {
		private Integer id;
		private String firstName;
		private String lastName;
		private String mobileNumber;
		private String address;
		private String type;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getMobileNumber() {
			return mobileNumber;
		}

		public void setMobileNumber(String mobileNumber) {
			this.mobileNumber = mobileNumber;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public void setType(int id) {
			List<Customer> c = dao.getAllEntities(Customer.class);
			List<Owner> O = dao.getAllEntities(Owner.class);
			List<Supplier> S = dao.getAllEntities(Supplier.class);
			for (Customer cc : c) {
				if (cc.getContactId() == id) {
					type = "Customer";
				}
			}
			for (Owner OO : O) {
				if (OO.getContactId() == id) {
					type = "Owner";
				}
			}
			for (Supplier SS : S) {
				if (SS.getContactId() == id) {
					type = "Supplier";
				}
			}
		}

		ContactView getContact() {
			ContactView contact = new ContactView();
			contact.setId(getId());
			contact.setFirstName(getFirstName());
			contact.setLastName(getLastName());
			contact.setMobileNumber(getMobileNumber());
			contact.setAddress(getAddress());
			contact.setType(getId());
			return contact;
		}

		void setContact(Contact contact) {
			setId(contact.getId());
			setFirstName(contact.getFirstName());
			setLastName(contact.getLastName());
			setMobileNumber(contact.getMobileNumber());
			setAddress(contact.getAddress());
			setType(contact.getId());
		}
	}

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	public void addContact(Contact contact, boolean[] types)
			throws BusinessException {
		try {
			dao.addEntity(contact);
			int contactId = contact.getId();
			if (types[0]) {// contact is a customer
				Customer customer = new Customer();
				customer.setContactId(contactId);
				try {
					dao.addEntity(customer);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			if (types[1]) {// contact is an owner
				Owner owner = new Owner();
				owner.setContactId(contactId);
				try {
					dao.addEntity(owner);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			if (types[2]) {// contact is a supplier
				Supplier supplier = new Supplier();
				supplier.setContactId(contactId);
				try {
					dao.addEntity(supplier);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void addContact(Contact con, String types) throws BusinessException {
		try {
			dao.addEntity(con);
			int contactId = con.getId();
			if (types.equals("Customer")) {// contact is a customer
				Customer customer = new Customer();
				customer.setContactId(contactId);
				try {
					dao.addEntity(customer);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			if (types.equals("Owner")) {// contact is an owner
				Owner owner = new Owner();
				owner.setContactId(contactId);
				try {
					dao.addEntity(owner);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			if (types.equals("Supplier")) {// contact is a supplier
				Supplier supplier = new Supplier();
				supplier.setContactId(contactId);
				try {
					dao.addEntity(supplier);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void updateContact(Contact con) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.updateEntity(con);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}

	public List<ContactView> getAllContacts() {
		ArrayList<ContactView> contacts = new ArrayList<>();
		List<Contact> c = dao.getAllEntities(Contact.class);
		for (Contact cc : c) {
			ContactView v = new ContactView();
			v.setContact(cc);
			contacts.add(v);
		}
		return contacts;
	}

	public void deleteContact(ContactView contact) throws DatabaseException {
		String type = contact.getType();
		if (type.equals("Customer")) {
			Customer c = dao.getEntityById(contact.getId(), Customer.class,
					"contactId");
			dao.deleteEntity(c);
		} else if (type.equals("Owner")) {
			Owner c = dao.getEntityById(contact.getId(), Owner.class,
					"contactId");
			dao.deleteEntity(c);
		} else if (type.equals("Supplier")) {
			Supplier c = dao.getEntityById(contact.getId(), Supplier.class,
					"contactId");
			dao.deleteEntity(c);
		}
		Contact con = dao.getEntityById(contact.getId(), Contact.class, "id");
		dao.deleteEntity(con);
	}
	public List<Contact> getAllContacts_() {
		return dao.getAllEntities(Contact.class);
	}
	
	public List <Customer> getAllCustomers(){
		List <Customer> cmrs =  dao.getAllEntities(Customer.class);
		Contact ctct;
		for (Customer cmr :cmrs){
			ctct=dao.getEntityById(cmr.getContactId(), Contact.class, "id");
			cmr.setName(ctct.getFirstName()+" "+ctct.getLastName());
		}
		return cmrs;
	}
	public List <Owner> getAllOwners(){
		List<Owner> ownrs =   dao.getAllEntities(Owner.class);
		Contact ctct;
		for (Owner ownr :ownrs){
			ctct=dao.getEntityById(ownr.getContactId(), Contact.class, "id");
			ownr.setName(ctct.getFirstName()+" "+ctct.getLastName());
		}
		return ownrs;
	}
	public List <Supplier> getAllSuppliers(){
		return dao.getAllEntities(Supplier.class);
	}
	
	public Contact getContactOfCustomer(int id){
		Customer ctct = dao.getEntityById(id, Customer.class, "id");
	return dao.getEntityById(ctct.getContactId(), Contact.class, "id")	;
	}
	
	public Contact getContactOfOwner(int id){
		Owner ctct = dao.getEntityById(id, Owner.class, "id");
		return dao.getEntityById(ctct.getContactId(), Contact.class, "id");	
		}
	public Contact getContactOfSupplier(int id){
		Supplier ctct = dao.getEntityById(id, Supplier.class, "id");
		return dao.getEntityById(ctct.getContactId(), Contact.class, "id");	
	}
	public List<StockKeepingUnit> getSKUSOfOwner(int own_id , int siteNo){
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_STE_NO", siteNo);
			parameters.put("P_OWN", own_id);
			List<Integer> skuIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_SKU_OF_OWNER.queryName,parameters);
			List<StockKeepingUnit>skuS =new ArrayList<>();
			for (Integer id : skuIds)
				skuS.add(dao.getEntityById(id, StockKeepingUnit.class, "id"));
			return skuS;
		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return null ;
		}
		}
		
		public Long getAvailableBalanceOfOwner(int own_id , int siteNo ,int sku_id){
			try {
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("P_STE_NO", siteNo);
				parameters.put("P_OWN", own_id);
				parameters.put("P_SKU_Id", sku_id);
				List<Long> Ids = dao.executeNamedQuery(Long.class,
						QueryNamesEnum.GET_Available_Credit_Of_Owner.queryName,parameters);
				return Ids.get(0);
			} catch (DatabaseException e) {
				e.printStackTrace();
				try {
					throw new BusinessException("Internal Database Error");
				} catch (BusinessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return null ;
				
			}
		
		
	}
	
	
}