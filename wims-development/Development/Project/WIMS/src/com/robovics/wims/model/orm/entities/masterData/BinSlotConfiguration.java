package com.robovics.wims.model.orm.entities.masterData;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		@NamedQuery(name = "getBingroup", query = 
				" select distinct b.groupBinId"+
				" from BinSlotConfiguration b "+
				" where  b.id = :P_Slot_Config_ID"
						)  
							
		})


@Table(name = "bin_slot_configuration")
@Entity
public class BinSlotConfiguration extends BaseEntity {

	private static final long serialVersionUID = 6091764364048759069L;
	private Integer id;
	private Integer groupBinId;
	private Date dateTime;
	private Integer width;
	private Integer length;
	private Integer height;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "BSC_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "BSC_GBN_Id")
	public Integer getGroupBinId() {
		return groupBinId;
	}

	public void setGroupBinId(Integer groupBinid_) {
		this.groupBinId = groupBinid_;
	}

	@Column(name = "BSC_DateTime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	@Column(name = "BSC_Width")
	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	@Column(name = "BSC_Length")
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	@Column(name = "BSC_Height")
	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}
}