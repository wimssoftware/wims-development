package com.robovics.wims.ui.staff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.model.service.staff.StaffService.StaffView;
import com.robovics.wims.ui.dialog.ConfirmationDialog;
import com.robovics.wims.ui.dialog.ConfirmationDialog.Callback;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Container.Filterable;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.util.filter.Like;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("wims")
public class StaffUI extends UI {

	private StaffService staffService;
	private MasterDataService masterDataService;
	private static final String SITE_ADMIN_TYPE = "Site Admin";
	private static final String SITE_LABOR_TYPE = "Site Labor";

	@WebServlet(value = "/Staff/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = StaffUI.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		staffService = (StaffService) ServiceFactory.getInstance().getService(
				ServicesEnum.STAFF_SERVICE);
		masterDataService = (MasterDataService) ServiceFactory.getInstance()
				.getService(ServicesEnum.MASTER_DATA_SERVICE);
		List<StaffView> staffList = staffService.getAllStaff();
		getPage().setTitle("Staff");
		setContent(buildForm(staffList));
	}

	@SuppressWarnings("deprecation")
	private Layout buildForm(List<StaffView> staffList) {

		VerticalLayout vlayout = new VerticalLayout();
		vlayout.setMargin(true);
		final BeanItemContainer<StaffView> beans = new BeanItemContainer<StaffView>(
				StaffView.class);
		beans.addAll(staffList);
		// A layout for the table and form
		VerticalLayout layout = new VerticalLayout();
		// layout.setMargin(true);
		// Bind a table to it
		final Table table = new Table("", beans);
		table.setPageLength(staffList.size());
		table.setBuffered(false);
		table.setVisibleColumns(new Object[] { "firstName", "lastName",
				"mobileNumber", "siteName", "staffType", "startDate", "endDate" });
		table.setColumnHeaders(new String[] { "First Name", "Last Name",
				"Mobile Number", "Site Name", "Staff Type", "Start Date",
				"EndvDate" });
		layout.addComponent(table);
		
		final Window StaffWindow = new Window();
		StaffWindow.center();
		StaffWindow.setHeight("550px");
		StaffWindow.setWidth("400px");
		StaffWindow.setResizable(false);
		StaffWindow.setDraggable(false);

		table.addActionHandler(new Handler() {
			Action delete = new Action("Delete Staff");

			@Override
			public void handleAction(Action action, Object sender,
					final Object target) {
				if (action == delete) {
					final ConfirmationDialog confirmationDialog = new ConfirmationDialog(
							"Delete Confirmation",
							"Are you sure you want to delete this record ?",
							new Callback() {
								@Override
								public void onDialogResult(boolean resultIsYes) {
									if (resultIsYes) {
										int tableLength = table.getPageLength();
										try {
											StaffView staff = (StaffView) target;
											staffService.deleteStaff(staff);
											table.removeItem(target);
											table.setPageLength(tableLength - 1);
											Notification
													.show("Info",
															"Record deleted successfully",
															Notification.Type.TRAY_NOTIFICATION);
										} catch (BusinessException e) {
											table.setPageLength(tableLength);
											Notification.show(
													"Error",
													e.getMessage(),
													Notification.Type.TRAY_NOTIFICATION);
										}
									}
								}
							});
					addWindow(confirmationDialog);
				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				ArrayList<Action> actions = new ArrayList<Action>();
				// Enable deleting only if clicked on an item
				if (target != null)
					actions.add(delete);
				return (Action[]) (actions.toArray(new Action[actions.size()]));
			}
		});

		// Create a form for editing a selected or new item.
		// It is invisible until actually used.
		final FormLayout formLayout = new FormLayout();

		PropertysetItem itemsProperty = new PropertysetItem();
		itemsProperty.addItemProperty("firstName", new ObjectProperty<String>(
				""));
		itemsProperty.addItemProperty("lastName",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("mobileNumber",
				new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("startDate", new ObjectProperty<Date>(
				new Date()));
		itemsProperty.addItemProperty("endDate", new ObjectProperty<Date>(
				new Date()));

		final FieldGroup form = new FieldGroup(itemsProperty);
		formLayout.addComponent(form.buildAndBind("First Name", "firstName"));
		formLayout.addComponent(form.buildAndBind("Last Name", "lastName"));
		formLayout.addComponent(form.buildAndBind("Mobile Number",
				"mobileNumber"));
		formLayout.addComponent(form.buildAndBind("Start Date", "startDate"));
		formLayout.addComponent(form.buildAndBind("End Date", "endDate"));

		formLayout.setVisible(false);
		form.setBuffered(true);
		formLayout.setSpacing(true);
		formLayout.setMargin(true);
        StaffWindow.setContent(formLayout);
		
		table.addListener(new ItemClickListener() {

		    @Override
		    public void itemClick(ItemClickEvent event) {
		    	getCurrent().addWindow(StaffWindow);	
		    }
	});
		//layout.addComponent(formLayout);
		
		
		// When the user selects an item, show it in the form
		table.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				// Close the form if the item is deselected
				if (event.getProperty().getValue() == null) {
					//formLayout.setVisible(false);
					StaffWindow.close();
					return;
				}
				// Bind the form to the selected item
				form.setItemDataSource(table.getItem(table.getValue()));
				formLayout.setVisible(true);
				// The form was opened for editing an existing item
				table.setData(null);
			}
		});
		table.setSelectable(true);
		table.setImmediate(true);

		HorizontalLayout newStaffLayout = new HorizontalLayout();

		final Button newBeanBtn = new Button("New Staff");
		final ComboBox newStaffTypeCombo = new ComboBox(null, Arrays.asList(
				SITE_ADMIN_TYPE, SITE_LABOR_TYPE));
		newStaffTypeCombo.select(SITE_ADMIN_TYPE);
		newStaffTypeCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (newStaffTypeCombo.getValue() == null
						|| ((String) newStaffTypeCombo.getValue()).isEmpty())
					newBeanBtn.setEnabled(false);
				else
					newBeanBtn.setEnabled(true);
			}
		});
		// Creates a new bean for editing in the form before adding it to the
		// table. Adding is handled after committing the form.
		newBeanBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				// Create a new item, this will create a new bean
				StaffView staff = staffService.new StaffView();
				staff.setFirstName("");
				staff.setLastName("");
				staff.setMobileNumber("");
				Site site = masterDataService.getCurrentSite();
				staff.setSiteId(site.getId());
				staff.setSiteName(site.getName());
				String staffType = (String) newStaffTypeCombo.getValue();
				staff.setStaffType(staffType);
				if (staffType.equals(SITE_ADMIN_TYPE))
					staff.setSiteAdmin(true);
				else if (staffType.equals(SITE_LABOR_TYPE))
					staff.setSiteLabor(true);
				Date currentDate = new Date();
				staff.setStartDate(currentDate);

				beans.addBean(staff);
				table.setPageLength(table.getPageLength() + 1);

				form.setItemDataSource(table.getItem(staff));
				// Make the form a bit nicer
				// The form was opened for editing a new item
				table.setData(staff);
				table.select(staff);
				table.setEnabled(false);
				newBeanBtn.setEnabled(false);
				//formLayout.setVisible(true);
				getCurrent().addWindow(StaffWindow);
			}
		});
		// When OK button is clicked, commit the form to the bean
		final Button saveBtn = new Button("Save");
		saveBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				StaffView staffView = staffService.new StaffView();
				staffView.setFirstName((String) form.getField("firstName")
						.getValue());
				staffView.setLastName((String) form.getField("lastName")
						.getValue());
				staffView.setMobileNumber((String) form
						.getField("mobileNumber").getValue());
				staffView.setStartDate((Date) form.getField("startDate")
						.getValue());
				staffView
						.setEndDate((Date) form.getField("endDate").getValue());
				List<String> errorMessages = staffService
						.validateStaff(staffView);
				if (errorMessages.size() > 0) {
					Collections.reverse(errorMessages);
					for (String errorMessage : errorMessages)
						Notification.show("Error", errorMessage,
								Notification.Type.TRAY_NOTIFICATION);
					return;
				}
				try {
					form.commit();
				} catch (CommitException e1) {
					Notification
							.show("Error",
									"Error saving Staff, please return to technical support",
									Notification.Type.TRAY_NOTIFICATION);
				}
				formLayout.setVisible(false); // and close it
				StaffWindow.close();
				// New items have to be added to the container
				table.commit();
				table.setEnabled(true);
				StaffView staff = (StaffView) table.getValue();
				try {
					if (staff.getId() == null) {
						staffService.addStaff(staff);
						Notification.show("Info",
								"Record inserted successfully",
								Notification.Type.TRAY_NOTIFICATION);
					} else {
						staffService.updateStaff(staff);
						Notification.show("Info",
								"Record updated successfully",
								Notification.Type.TRAY_NOTIFICATION);
					}
				} catch (BusinessException e) {
					Notification.show("Error", e.getMessage(),
							Notification.Type.TRAY_NOTIFICATION);
				}
				newBeanBtn.setEnabled(true);
			}
		});
		HorizontalLayout formFooter = new HorizontalLayout();
		formFooter.addComponent(saveBtn);

		// // Make modification to enable/disable the Save button
		// form.setFormFieldFactory(new DefaultFieldFactory() {
		// @SuppressWarnings("rawtypes")
		// @Override
		// public Field createField(Item item, Object propertyId, Component
		// uiContext) {
		// final AbstractField field = (AbstractField)
		// super.createField(item, propertyId, uiContext);
		// field.addValueChangeListener(new ValueChangeListener() {
		// @Override
		// public void valueChange(ValueChangeEvent event) {
		// submit.setEnabled(form.isModified());
		// }
		// });
		// if (field instanceof TextField) {
		// final TextField tf = (TextField) field;
		// tf.addTextChangeListener(new TextChangeListener() {
		// @Override
		// public void textChange(TextChangeEvent event) {
		// if (form.isModified() ||
		// !event.getText().equals(tf.getValue())) {
		// submit.setEnabled(true);
		//
		// // Not needed after first event unless
		// // want to detect also changes back to
		// // unmodified value.
		// tf.removeListener(this);
		//
		// // Has to be reset because the
		// // removeListener() setting causes
		// // updating the field value from the
		// // server-side.
		// tf.setValue(event.getText());
		// }
		// }
		// });
		// }
		// field.setImmediate(true);
		//
		// return field;
		// }
		// });

		Button cancelBtn = new Button("Cancel");
		cancelBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.discard(); // Not really necessary
				formLayout.setVisible(false); // and close it
				StaffWindow.close();
				
				StaffView staff = (StaffView) table.getValue();
				if (staff.getId() == null) {
					beans.removeItem(staff);
					table.setPageLength(table.getPageLength() - 1);
				} else
					table.unselect(staff);

				table.discard(); // Discards possible addItem()
				table.setEnabled(true);
				newBeanBtn.setEnabled(true);
			}
		});

		formFooter.addComponent(cancelBtn);
		formLayout.addComponent(formFooter);

		newStaffLayout.addComponent(newBeanBtn);
		newStaffLayout.addComponent(newStaffTypeCombo);

		HorizontalLayout filterLayout = new HorizontalLayout();
		TextField firstNameFilter = new TextField("First Name");
		TextField lastNameFilter = new TextField("Last Name");
		filterLayout.addComponent(firstNameFilter);
		filterLayout.addComponent(lastNameFilter);

		firstNameFilter.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("firstName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});

		lastNameFilter.addTextChangeListener(new TextChangeListener() {
			Like filter = null;

			public void textChange(TextChangeEvent event) {
				Filterable f = (Filterable) table.getContainerDataSource();
				// Remove old filter
				if (filter != null)
					f.removeContainerFilter(filter);
				// Set new filter for the "Name" column
				filter = new Like("lastName", "%" + event.getText() + "%");
				filter.setCaseSensitive(false);
				f.addContainerFilter(filter);
				table.setPageLength(f.size());
			}
		});
		firstNameFilter.setImmediate(true);
		lastNameFilter.setImmediate(true);

		vlayout.addComponent(newStaffLayout);
		vlayout.addComponent(filterLayout);

		
		HorizontalLayout Hlayout = new HorizontalLayout();
		Hlayout.setSizeUndefined();
		Hlayout.addComponent(vlayout);
		Hlayout.addComponent(layout);

		return Hlayout;
	}
}