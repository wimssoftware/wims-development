package com.robovics.wims.exceptions;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = -1189111037686106157L;

	public DatabaseException(String message) {
		super(message);
	}

	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}
}