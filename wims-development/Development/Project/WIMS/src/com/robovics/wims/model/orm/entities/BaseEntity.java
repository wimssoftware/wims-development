package com.robovics.wims.model.orm.entities;

import java.io.Serializable;

import javax.persistence.Transient;

public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 6046934434935489816L;
	private boolean selected;

	@Transient
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}