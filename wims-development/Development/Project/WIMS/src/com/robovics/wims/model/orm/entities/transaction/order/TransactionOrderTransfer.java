package com.robovics.wims.model.orm.entities.transaction.order;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Transaction_Order_Transfer")
@Entity
public class TransactionOrderTransfer extends BaseEntity {

	private static final long serialVersionUID = -5688749758033389783L;
	private Integer id;
	private Integer transactionOrderId;
	private Integer fromSlotId;
	private Integer toSlotId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TOT_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TOT_TOD_Id")
	public Integer getTransactionOrderId() {
		return transactionOrderId;
	}

	public void setTransactionOrderId(Integer transactionOrderId) {
		this.transactionOrderId = transactionOrderId;
	}

	@Column(name = "TOT_From_SOT_Id")
	public Integer getFromSlotId() {
		return fromSlotId;
	}

	public void setFromSlotId(Integer fromSlotId) {
		this.fromSlotId = fromSlotId;
	}

	@Column(name = "TOT_To_SOT_Id")
	public Integer getToSlotId() {
		return toSlotId;
	}

	public void setToSlotId(Integer toSlotId) {
		this.toSlotId = toSlotId;
	}
}