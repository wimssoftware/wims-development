package com.robovics.wims.exceptions;

public class OpenDialogFailedException extends Exception {

	private static final long serialVersionUID = 14693312577672629L;

	public OpenDialogFailedException(String message) {
		super(message);
	}
}