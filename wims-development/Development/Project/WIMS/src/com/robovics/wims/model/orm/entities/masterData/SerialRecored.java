package com.robovics.wims.model.orm.entities.masterData;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "serial_record_table")
@Entity
public class SerialRecored extends BaseEntity {
	
	private static final long serialVersionUID = -7608611887694146778L;
	
	private Integer id;
	private String serial;
	private Integer type;//carton or palet
	private Integer sku;//sku id
	private Date date;
	
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "SRT_ID")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "SRT_Serial")
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	@Transient
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
	@Transient
	public Integer getSku() {
		return sku;
	}
	public void setSku(Integer sku) {
		this.sku = sku;
	}
	
	@Transient
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Transient
	public void getInfoFromSerial() throws Exception{
		try {
		
		String[] data = serial.split(";");
		type = Integer.parseInt(data[0]);
		
		String[] d_m_y = data[1].split("/");
		int day = Integer.parseInt(d_m_y[0]);  //25
		int month = Integer.parseInt(d_m_y[1]); //12
		int year = Integer.parseInt(d_m_y[2]); // 1988
		java.util.Calendar c = java.util.Calendar.getInstance();
		c.set(year, month-1, day, 0, 0); 
		date =  c.getTime();
		
		sku = Integer.parseInt(data[2]);
		}catch(Exception R){
			R.printStackTrace();
			throw(R);
			
		} 
	}
}
