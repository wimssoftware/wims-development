package com.robovics.wims.ui.dialog.staff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.model.service.staff.StaffService.StaffView;
import com.robovics.wims.ui.dialog.ConfirmationDialog;
import com.robovics.wims.ui.dialog.ConfirmationDialog.Callback;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class StaffDialog extends Window implements ClickListener {

	private static final long serialVersionUID = -1423228382735681143L;
	private Callback staffDialogCallback;
	private VerticalLayout container = new VerticalLayout();
	private StaffView selectStaff;
	private static final String SITE_ADMIN_TYPE = "Site Admin";
	private static final String SITE_LABOR_TYPE = "Site Labor";
	private StaffService staffService;
	private MasterDataService masterDataService;

	public StaffDialog(String caption, Integer StaffType,
			Callback staffDialogCallback) {
		super(caption);

		staffService = (StaffService) ServiceFactory.getInstance().getService(
				ServicesEnum.STAFF_SERVICE);
		masterDataService = (MasterDataService) ServiceFactory.getInstance()
				.getService(ServicesEnum.MASTER_DATA_SERVICE);

		final List<StaffView> staff = staffService
				.getAllStaffByStaffType(StaffType);
		final List<String> staffNames = new ArrayList<String>();
		for (StaffView staffIt : staff)
			staffNames.add(staffIt.getFirstName());

		setModal(true);

		this.staffDialogCallback = staffDialogCallback;

		setContent(container);

		setContent(buildForm(staff));
	}

	@SuppressWarnings("deprecation")
	private Layout buildForm(List<StaffView> staffList) {
		VerticalLayout vlayout = new VerticalLayout();
		vlayout.setMargin(true);
		final BeanItemContainer<StaffView> beans = new BeanItemContainer<StaffView>(
				StaffView.class);
		beans.addAll(staffList);
		// A layout for the table and form
		HorizontalLayout layout = new HorizontalLayout();
		// layout.setMargin(true);
		// Bind a table to it
		final Table table = new Table("Staff List", beans);
		table.setPageLength(10);
		table.setBuffered(false);
		table.setVisibleColumns(new Object[] { "firstName", "lastName",
				"mobileNumber", "siteName", "staffType" });
		table.setColumnHeaders(new String[] { "First Name", "Last Name",
				"Mobile Number", "Site Name", "Staff Type" });
		table.setMultiSelect(true);
		table.setSelectable(true);
		table.setImmediate(true);

		layout.addComponent(table);

		// Create a form for editing a selected or new item.
		// It is invisible until actually used.
		final Form form = new Form();
		// form.setCaption("Edit Staff");
		form.setVisible(false);
		form.setBuffered(true);
		layout.addComponent(form);

		// When the user selects an item, show it in the form
		table.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				// Close the form if the item is deselected
				if (event.getProperty().getValue() == null) {
					form.setVisible(false);
					return;
				}
				// Bind the form to the selected item
				// selectStaff=(StaffView) table.getItem(table.getValue());
				// close();
				// staffDialogCallback.onDialogResult(true,selectStaff);
				// selectStaff=(StaffView)((ItemClickEvent) event.getItemId();

			}
		});

		table.addListener(new ItemClickListener() { // With ItemClickListener

			public void itemClick(ItemClickEvent event) {
				if (event.getButton() == ItemClickEvent.BUTTON_LEFT) {
					// you can handle left/right/middle -mouseclick
					selectStaff = (StaffView) event.getItemId();
					close();
					staffDialogCallback.onDialogResult(true, selectStaff);
				}

			}
		});

		table.addActionHandler(new Handler() {
			Action edit = new Action("Edit Staff");

			@Override
			public void handleAction(Action action, Object sender,
					final Object target) {
				if (action == edit) {
					form.setItemDataSource(table.getItem(target));
					form.setVisibleItemProperties(new Object[] { "firstName",
							"lastName", "mobileNumber" });
					form.setVisible(true);
					// The form was opened for editing an existing item
					table.setData(null);

				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				ArrayList<Action> actions = new ArrayList<Action>();
				// Enable deleting only if clicked on an item
				if (target != null)
					actions.add(edit);
				return (Action[]) (actions.toArray(new Action[actions.size()]));
			}
		});

		HorizontalLayout newStaffLayout = new HorizontalLayout();

		final Button newBeanBtn = new Button("New Staff");
		final ComboBox newStaffTypeCombo = new ComboBox(null, Arrays.asList(
				SITE_ADMIN_TYPE, SITE_LABOR_TYPE));
		newStaffTypeCombo.select(SITE_ADMIN_TYPE);
		newStaffTypeCombo.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (newStaffTypeCombo.getValue() == null
						|| ((String) newStaffTypeCombo.getValue()).isEmpty())
					newBeanBtn.setEnabled(false);
				else
					newBeanBtn.setEnabled(true);
			}
		});
		// Creates a new bean for editing in the form before adding it to the
		// table. Adding is handled after committing the form.
		newBeanBtn.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				// Create a new item, this will create a new bean
				StaffView staff = staffService.new StaffView();
				staff.setFirstName("");
				staff.setLastName("");
				staff.setMobileNumber("");
				Site site = masterDataService.getCurrentSite();
				staff.setSiteId(site.getId());
				staff.setSiteName(site.getName());
				String staffType = (String) newStaffTypeCombo.getValue();
				staff.setStaffType(staffType);
				if (staffType.equals(SITE_ADMIN_TYPE))
					staff.setSiteAdmin(true);
				else if (staffType.equals(SITE_LABOR_TYPE))
					staff.setSiteLabor(true);

				beans.addBean(staff);
				form.setItemDataSource(table.getItem(staff));
				// Make the form a bit nicer
				form.setVisibleItemProperties(new Object[] { "firstName",
						"lastName", "mobileNumber" });
				// The form was opened for editing a new item
				table.setData(staff);
				table.select(staff);
				table.setEnabled(false);
				newBeanBtn.setEnabled(false);
				form.setVisible(true);
			}
		});
		// When OK button is clicked, commit the form to the bean
		final Button submit = new Button("Save");
		submit.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.commit();
				form.setVisible(false); // and close it
				// New items have to be added to the container
				table.commit();
				table.setEnabled(true);
				StaffView staff = (StaffView) table.getValue();
				try {
					if (staff.getId() == null) {
						staffService.addStaff(staff);
						Notification.show("Info",
								"Record inserted successfully",
								Notification.Type.TRAY_NOTIFICATION);
					} else {
						staffService.updateStaff(staff);
						Notification.show("Info",
								"Record updated successfully",
								Notification.Type.TRAY_NOTIFICATION);
					}
				} catch (BusinessException e) {
					Notification.show("Error", e.getMessage(),
							Notification.Type.TRAY_NOTIFICATION);
				}
				newBeanBtn.setEnabled(true);
			}
		});
		form.getFooter().addComponent(submit);

		Button cancel = new Button("Cancel");
		cancel.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				form.discard(); // Not really necessary
				form.setVisible(false); // and close it
				StaffView staff = (StaffView) table.getValue();
				if (staff.getId() == null)
					beans.removeItem(staff);
				table.discard(); // Discards possible addItem()
				table.setEnabled(true);
				newBeanBtn.setEnabled(true);
			}
		});
		form.getFooter().addComponent(cancel);

		newStaffLayout.addComponent(newBeanBtn);
		newStaffLayout.addComponent(newStaffTypeCombo);

		vlayout.addComponent(newStaffLayout);
		vlayout.addComponent(layout);

		return vlayout;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (getParent() != null)
			((UI) getParent()).removeWindow(this);
		staffDialogCallback.onDialogResult(true, selectStaff);
	}

	public interface Callback {

		public void onDialogResult(boolean resultIsYes, StaffView selectStaff);
	}

}