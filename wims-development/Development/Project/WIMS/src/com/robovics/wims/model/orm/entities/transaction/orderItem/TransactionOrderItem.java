package com.robovics.wims.model.orm.entities.transaction.orderItem;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		
		
		
		@NamedQuery(name = "getFirstExpiredOrderItemsInSiteFromSKU", query = "select toi1.id from TransactionOrderItem toi1 where"
				+ " toi1.expirationDate in ( "
				+ "SELECT min(toi.expirationDate) FROM TransactionOrderItem toi ,"
				+ " TransactionOrder tod , SiteAdmin sad WHERE "
				+ "toi.transactionOrderId = tod.id and toi.stockKeepingUnitId = :P_SKU_ID and tod.siteAdminId = sad.id "
				+ " and sad.siteId = :P_STE_NO and tod.transactionTypeId =2 )   "
					),
		@NamedQuery(name = "GetSKUofSlot", query = " select i.stockKeepingUnitId "+
				" from CurrentBinItem c , TransactionOrderItem i "+
				" where c.itemId = i.id and c.slotId = :P_Current_Pin_id"
			 		) ,
		@NamedQuery(name = "getFirstExpiredOrderItemsInSiteFromSKUExceptDate", query = "select toi1.id from TransactionOrderItem toi1 where"
				+ " toi1.id <> :P_TOI_ID and toi1.expirationDate in ( "
				+ "SELECT min(toi.expirationDate) FROM TransactionOrderItem toi ,"
				+ " TransactionOrder tod , SiteAdmin sad WHERE toi.expirationDate >:P_DATE and "
				+ "toi.transactionOrderId = tod.id and toi.stockKeepingUnitId = :P_SKU_ID and tod.siteAdminId = sad.id "
				+ " and sad.siteId = :P_STE_NO and tod.transactionTypeId =2 )   "
					),
		@NamedQuery(name = "GetOwnerOfItem", query = " select r.ownerId "+
				" from TransactionOrderReceive r , TransactionOrderItem i "+
				" where i.transactionOrderId = r.transactionOrderId and i.id = :P_itemId"
			 		) ,
		@NamedQuery(name = "GetSiteIdOfItem", query = " select a.siteId "+
				" from TransactionOrder o , TransactionOrderItem i , SiteAdmin a"+
				" where i.id = :P_itemId and i.transactionOrderId = o.id and o.siteAdminId = a.id"
					) 
					
				//	 >=:P_Date
		})

@Table(name = "Transaction_Order_Item")
@Entity
public class TransactionOrderItem extends BaseEntity {

	private static final long serialVersionUID = 7135624474039110139L;
	private Integer id;
	private Integer transactionOrderId;
	private Integer stockKeepingUnitId;
	private Integer unitTypeId;
	private Integer isContained;
	private Integer isContaining;
	private Date productionDate;
	private Date expirationDate;
	private String serial;
	private String stockKeepingUnitCode;
	private String stockKeepingUnitDescription;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TOI_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TOI_TOD_Id")
	public Integer getTransactionOrderId() {
		return transactionOrderId;
	}

	public void setTransactionOrderId(Integer transactionOrderId) {
		this.transactionOrderId = transactionOrderId;
	}

	@Column(name = "TOI_SKU_Id")
	public Integer getStockKeepingUnitId() {
		return stockKeepingUnitId;
	}

	public void setStockKeepingUnitId(Integer stockKeepingUnitId) {
		this.stockKeepingUnitId = stockKeepingUnitId;
	}

	@Column(name = "TOI_UTP_Id")
	public Integer getUnitTypeId() {
		return unitTypeId;
	}

	public void setUnitTypeId(Integer unitTypeId) {
		this.unitTypeId = unitTypeId;
	}

	@Column(name = "TOI_IsContained")
	public Integer getIsContained() {
		return isContained;
	}

	public void setIsContained(Integer isContained) {
		this.isContained = isContained;
	}

	@Column(name = "TOI_Serial")
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	@Column(name = "TOI_IsContaining")
	public Integer getIsContaining() {
		return isContaining;
	}

	public void setIsContaining(Integer isContaining) {
		this.isContaining = isContaining;
	}

	@Column(name = "TOI_ProductionDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Date productionDate) {
		this.productionDate = productionDate;
	}

	@Column(name = "TOI_ExpirationDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Transient
	public String getStockKeepingUnitCode() {
		return stockKeepingUnitCode;
	}

	public void setStockKeepingUnitCode(String stockKeepingUnitCode) {
		this.stockKeepingUnitCode = stockKeepingUnitCode;
	}

	@Transient
	public String getStockKeepingUnitDescription() {
		return stockKeepingUnitDescription;
	}

	public void setStockKeepingUnitDescription(
			String stockKeepingUnitDescription) {
		this.stockKeepingUnitDescription = stockKeepingUnitDescription;
	}
}