package com.robovics.wims.model.orm.entities.transaction.itemBin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {

		@NamedQuery(name = "getTranferItemLocation", query = " select o.toslotId from TransferItemBin o"
				+ " where o.transactionExecuteTransferId =:P_itemTransferId ")
		})


@Table(name = "Transfer_Item_Bin")
@Entity
public class TransferItemBin extends BaseEntity {

	private static final long serialVersionUID = -6580395198925890866L;
	private Integer id;
	private Integer transactionExecuteTransferId;
	private Integer transactionOrderItemId;
	private Integer toslotId;
	private Integer formslotId;
	private Date executionTime;
	private String serial; 
	
	

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TIB_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TIB_TET_Id")
	public Integer getTransactionExecuteTransferId() {
		return transactionExecuteTransferId;
	}

	public void setTransactionExecuteTransferId(
			Integer transactionExecuteTransferId) {
		this.transactionExecuteTransferId = transactionExecuteTransferId;
	}

	@Column(name = "TIB_TOI_Id")
	public Integer getTransactionOrderItemId() {
		return transactionOrderItemId;
	}

	public void setTransactionOrderItemId(Integer transactionOrderItemId) {
		this.transactionOrderItemId = transactionOrderItemId;
	}

	@Column(name = "TIB_From_SOT_Id")
	public Integer getFormslotId() {
		return formslotId;
	}

	public void setFormslotId(Integer formslotId) {
		this.formslotId = formslotId;
	}

	@Column(name = "TIB_To_SOT_Id")
	public Integer getToslotId() {
		return toslotId;
	}

	public void setToslotId(Integer toslotId) {
		this.toslotId = toslotId;
	}



	@Column(name = "TIB_Serial")
	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	@Column(name = "TIB_ExecuteTime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Date executionTime) {
		this.executionTime = executionTime;
	}
}