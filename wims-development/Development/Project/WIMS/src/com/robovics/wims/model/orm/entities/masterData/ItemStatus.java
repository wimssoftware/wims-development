package com.robovics.wims.model.orm.entities.masterData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.BaseEntity;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.service.contact.ContactService.ContactView;

@Table(name = "items_status")
@Entity
public class ItemStatus extends BaseEntity {

	private static final long serialVersionUID = 7191629462969191322L;
	private Integer id;
	private Integer qualityStatusId;
	private Integer transactionOrderItemId;
	private Date date;
	private String note;
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "IS_Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "IS_QST_Id")
	public Integer getQualityStatusId() {
		return qualityStatusId;
	}
	public void setQualityStatusId(Integer qualityStatusId) {
		this.qualityStatusId = qualityStatusId;
	}
	
	@Column(name = "IS_TOI_Id")
	public Integer getTransactionOrderItemId() {
		return transactionOrderItemId;
	}
	public void setTransactionOrderItemId(Integer transactionOrderItemId) {
		this.transactionOrderItemId = transactionOrderItemId;
	}
	
	@Column(name = "IS_Date")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Column(name = "IS_Note")
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
}