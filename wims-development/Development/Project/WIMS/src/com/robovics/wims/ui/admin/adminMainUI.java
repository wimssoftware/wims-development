package com.robovics.wims.ui.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.OrderException;
import com.robovics.wims.model.enums.ErrorsEnum;
import com.robovics.wims.model.enums.TransactionTypesEnum;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Customer;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.staff.Staff;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.issue.IssueService;
import com.robovics.wims.model.service.masterData.BinSlotService;
import com.robovics.wims.model.service.masterData.BinSlotService.BinSlotDetails;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.receive.ReceiveService;
import com.robovics.wims.model.service.staff.ContractService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.model.service.transaction.TransactionEntity;
import com.robovics.wims.model.service.transaction.order.TransactionOrderService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderTransferService;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionDestroyEvent;
import com.vaadin.server.SessionDestroyListener;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("wims")
public class adminMainUI extends UI {
	static Boolean loggin = false;
	static Staff sAStaff;
	static int windowCount;
	private static StaffService staffService;

	Button switchLanguage;
	ResourceBundle i18n; // Automatic Localization
	ResourceBundle i18nBundle; // Automatic localization

	private List<GroupBin> groubBins;
	BeanItemContainer<GroupBin> groubBinsBean;

	private List<BinSlotDetails> binSlots;
	BeanItemContainer<BinSlotDetails> binSlotsBean = new BeanItemContainer<BinSlotDetails>(BinSlotDetails.class);
	BeanItemContainer<BinSlotDetails> binSlotsBeanTo = new BeanItemContainer<BinSlotDetails>(BinSlotDetails.class);
	int adminSiteId;
	private BinSlotService binSlotService;

	HorizontalLayout MainLayout = new HorizontalLayout();
	private VerticalLayout tabsLayout = new VerticalLayout();
	private VerticalLayout sideLayout = new VerticalLayout();
	private HorizontalLayout headerLayout = new HorizontalLayout();
	private HorizontalLayout footerLayout = new HorizontalLayout();

	private VerticalLayout settingsLayout = new VerticalLayout();

	private VerticalLayout notExecutedOrdersLayout = new VerticalLayout();
	private FormLayout notExecutedOrder = new FormLayout();

	private VerticalLayout newOrderLayout = new VerticalLayout();
	private FormLayout newOrderForm = new FormLayout();
	private HorizontalLayout buttonsLayout = new HorizontalLayout();

	private VerticalLayout TransferWindow = new VerticalLayout();
	Button back;
	Action NewPallet;

	PropertysetItem itemsProperty = new PropertysetItem();

	Button ClearAll;
	Button Done;

	@PropertyId("unitOfMeasurement")
	private ComboBox PalletCarton;

	@PropertyId("transactionType")
	ComboBox IssueReceive;

	@PropertyId("skuClass")
	ComboBox SKU;

	@PropertyId("quantity")
	TextField quatityText;

	@PropertyId("batchNo")
	TextField BatchNoText;

	@PropertyId("binNo")
	ComboBox BinNoText;

	@PropertyId("transactionDate")
	DateField TransactionCalender;

	@PropertyId("orderDate")
	DateField orderCalender;

	@PropertyId("customer")
	ComboBox CustomerComboBox;

	@PropertyId("owner")
	ComboBox OwnerComboBox;

	@PropertyId("userName")
	TextField userName;

	@PropertyId("password")
	PasswordField password;

	@PropertyId("mobile nmber")
	TextField mobile;

	@PropertyId("email")
	TextField email;

	Button edit;

	private MasterDataService masterService;
	private StockKeepingUnitService skuService;
	private ContactService contactService;
	private ReceiveService receiveOrderService;
	private IssueService issueOrderService;
	private TransactionOrderTransferService transferService;
	private TransactionOrderService transactionOrderService;
	private TabSheet mainSheet = new TabSheet();

	private List<StockKeepingUnit> skuS;

	BeanItemContainer<GroupBin> beansBins = new BeanItemContainer<>(GroupBin.class);

	SelectedTabChangeListener tabCh = new SelectedTabChangeListener() {

		@Override
		public void selectedTabChange(SelectedTabChangeEvent event) {
			if (event.getTabSheet().getSelectedTab().getId().equals("New Order")) {
				IntegrateNewOrderLayout();

			} else if (event.getTabSheet().getSelectedTab().getId().equals("Not Executed Orders")) {
				IntegrateNotExecutedOrderLayout();

			} else if (event.getTabSheet().getSelectedTab().getId().equals("Settings")) {
				IntegrateSettingLayout();

			}
		}

	};

	@WebServlet(value = "/AdminPage/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = adminMainUI.class)
	public static class Servlet extends VaadinServlet implements SessionInitListener, SessionDestroyListener {

		@Override
		public void sessionDestroy(SessionDestroyEvent event) {
		}

		@Override
		public void sessionInit(SessionInitEvent event) throws ServiceException {
			windowCount = 0;
		}
	}

	@Override
	protected void init(VaadinRequest request) {

		if (VaadinSession.getCurrent().getAttribute("IsLogin") == null
				|| VaadinSession.getCurrent().getAttribute("user_priviladge") == null) {
			authintecate(request);
		} else {
			if (VaadinSession.getCurrent().getAttribute("IsLogin").equals("false")
					|| !VaadinSession.getCurrent().getAttribute("user_priviladge").equals("2")) {
				authintecate(request);
			} else {
				Locale c = ((Locale) VaadinSession.getCurrent().getAttribute("Locale"));
				i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
				i18n = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c);
				switchLanguage = new Button(this.getMessage(GasDiaryMessages.switchLanguage));
				back = new Button(this.getMessage(GasDiaryMessages.back));
				ClearAll = new Button(this.getMessage(GasDiaryMessages.ClearAll));
				Done = new Button(this.getMessage(GasDiaryMessages.Done));
				SKU = new ComboBox(this.getMessage(GasDiaryMessages.SKU));
				SKU.setNullSelectionAllowed(false);
				quatityText = new TextField(this.getMessage(GasDiaryMessages.Quantity));
				BinNoText = new ComboBox(this.getMessage(GasDiaryMessages.Bins));
				BinNoText.setNullSelectionAllowed(false);
				OwnerComboBox = new ComboBox(this.getMessage(GasDiaryMessages.OwnerName));
				OwnerComboBox.setNullSelectionAllowed(false);
				userName = new TextField(this.getMessage(GasDiaryMessages.Username));
				password = new PasswordField(this.getMessage(GasDiaryMessages.Password));
				mobile = new TextField(this.getMessage(GasDiaryMessages.mobileNumber));
				email = new TextField(this.getMessage(GasDiaryMessages.Email));
				edit = new Button(this.getMessage(GasDiaryMessages.Edit));
				orderCalender = new DateField(this.getMessage(GasDiaryMessages.OrderDate));
				CustomerComboBox = new ComboBox(this.getMessage(GasDiaryMessages.customer));
				CustomerComboBox.setNullSelectionAllowed(false);
				IssueReceive = new ComboBox(this.getMessage(GasDiaryMessages.TransactionType));
				IssueReceive.setNullSelectionAllowed(false);
				BatchNoText = new TextField(this.getMessage(GasDiaryMessages.BatchNo));
				TransactionCalender = new DateField(this.getMessage(GasDiaryMessages.TransactionDate));
				NewPallet = new Action(this.getMessage(GasDiaryMessages.CreateNewPallet));
				Build_UI();
			}
		}
	}

	public void authintecate(final VaadinRequest request) {
		getPage().setTitle("Warehouse Portal");
		final Window LoginWindow = new Window();
		LoginWindow.center();
		LoginWindow.setClosable(false);
		LoginWindow.setDraggable(false);
		LoginWindow.setResizable(false);

		VerticalLayout vlayout = new VerticalLayout();
		Panel panel = new Panel();
		panel.setSizeUndefined();
		vlayout.addComponent(panel);
		CustomLayout custom = new CustomLayout("login_temp");

		custom.addStyleName("customlayoutexample");
		panel.setContent(custom);
		final TextField username = new TextField();
		custom.addComponent(username, "username");
		final PasswordField password = new PasswordField();
		custom.addComponent(password, "password");
		Button ok = new Button("Login");
		ok.setClickShortcut(KeyCode.ENTER);
		ok.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				StaffService ss = new StaffService();
				try {
					List<Integer> site_id = ss.authenticateStaff(username.getValue(), password.getValue());

					if (site_id != null && !site_id.isEmpty()) {
						LoginWindow.close();
						if (VaadinSession.getCurrent().getAttribute("user_priviladge").equals("2")) {
							init(request);
						}
					} else {
						Notification not = new Notification("Notification error", "login failed' please try again");
						not.show(Page.getCurrent());
					}
				} catch (BusinessException e) {
					Notification not = new Notification("Notification error", e.getMessage());
					not.show(Page.getCurrent());
					e.printStackTrace();
				}
			}
		});
		custom.addComponent(ok, "okbutton");
		LoginWindow.setContent(custom);
		getCurrent().addWindow(LoginWindow);
	}

	public void Build_UI() {
		staffService = (StaffService) ServiceFactory.getInstance().getService(ServicesEnum.STAFF_SERVICE);

		Integer idSi = Integer.parseInt(VaadinSession.getCurrent().getAttribute("user_id").toString());
		sAStaff = staffService.getStaffById(idSi);
		masterService = (MasterDataService) ServiceFactory.getInstance().getService(ServicesEnum.MASTER_DATA_SERVICE);
		binSlotService = (BinSlotService) ServiceFactory.getInstance().getService(ServicesEnum.BIN_SLOT_Service);

		skuService = (StockKeepingUnitService) ServiceFactory.getInstance().getService(ServicesEnum.SKU_SERVICE);

		contactService = (ContactService) ServiceFactory.getInstance().getService(ServicesEnum.CONTACT_SERVICE);
		receiveOrderService = (ReceiveService) ServiceFactory.getInstance().getService(ServicesEnum.RECEIVE_SERVICE);
		issueOrderService = (IssueService) ServiceFactory.getInstance()
				.getService(ServicesEnum.TRANSACTION_ORDER_ISSUE);

		transactionOrderService = (TransactionOrderService) ServiceFactory.getInstance()
				.getService(ServicesEnum.TRANSACTION_ORDER_SERVICE);
		transferService = (TransactionOrderTransferService) ServiceFactory.getInstance()
				.getService(ServicesEnum.TRANSACTION_ORDER_TRANSFER_SERVICE);
		initComboBoxs();
		IssueReceive.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (event.getProperty().getValue() == "Issue") {
					CustomerComboBox.setVisible(true);
					OwnerComboBox.setVisible(true);
				} else if (event.getProperty().getValue() == "Receive") {
					OwnerComboBox.setVisible(true);
					CustomerComboBox.setVisible(false);
				} else if (event.getProperty().getValue() == "Transfer") {
					OwnerComboBox.setVisible(false);
					IssueReceive.setVisible(false);
					PalletCarton.setVisible(false);
					SKU.setVisible(false);
					quatityText.setVisible(false);
					orderCalender.setVisible(false);
					TransactionCalender.setVisible(false);
					CustomerComboBox.setVisible(false);
					TransferWindow = TransferLayout();
					Done.setVisible(false);
					ClearAll.setVisible(false);
					newOrderLayout.addComponent(TransferWindow);
					back.addClickListener(new Button.ClickListener() {
						public void buttonClick(ClickEvent event) {
							TransferWindow.setVisible(false);
							OwnerComboBox.setVisible(true);
							IssueReceive.setVisible(true);
							IssueReceive.select(null);
							PalletCarton.setVisible(true);
							SKU.setVisible(true);
							quatityText.setVisible(true);
							orderCalender.setVisible(true);
							TransactionCalender.setVisible(true);
							CustomerComboBox.setVisible(false);
							Done.setVisible(true);
							ClearAll.setVisible(true);
						}
					});
				}
			}
		});

		OwnerComboBox.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (event.getProperty().getValue() == null) {
					SKU.removeAllItems();
					return;
				} else {
					int adminSiteId = Integer.parseInt((String) VaadinSession.getCurrent().getAttribute("Site_Id"));
					Owner contactobj = (Owner) OwnerComboBox.getValue();
					skuS = new ContractService().listSkusOfOwner(contactobj, adminSiteId);
					BeanItemContainer<StockKeepingUnit> beansSkus = new BeanItemContainer<StockKeepingUnit>(
							StockKeepingUnit.class);
					beansSkus.addAll(skuS);
					SKU.setContainerDataSource(beansSkus);
					SKU.setItemCaptionPropertyId("description");
				}
			}
		});

		mainSheet.addSelectedTabChangeListener(tabCh);
		headerLayout.addComponent(new Image(null, new ThemeResource("NTLC-Logo.JPG")));
		headerLayout.setSizeFull();

		getPage().setTitle("Home");
		Done.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				TransactionEntity trE = new TransactionEntity();
				int Quantity = -1;
				StockKeepingUnit sku_;
				Owner selOwner;

				try {
					if (IssueReceive.getValue() == null) {
						throw (new OrderException(6));
					}

					try {
						Quantity = Integer.parseInt(quatityText.getValue());
						if (Quantity > 0) {
							trE.setItemsQuantity(Integer.parseInt(quatityText.getValue()));
						} else {
							throw (new OrderException(1));
						}
					} catch (Exception e) {
						throw (new OrderException(1));
					}

					Date transactionTime = (Date) TransactionCalender.getValue();
					if (transactionTime == null) {
						throw (new OrderException(2));
					}
					trE.setTransactionTime(transactionTime);

					Date orderTime = (Date) orderCalender.getValue();
					if (orderTime == null) {
						trE.setOrderDate(new Date());
					} else {
						trE.setOrderDate(orderTime);
					}

					selOwner = (Owner) OwnerComboBox.getValue();
					if (selOwner == null) {
						throw (new OrderException(3));
					}
					trE.setOwnerId(selOwner.getId());

					sku_ = (StockKeepingUnit) SKU.getValue();
					if (sku_ == null) {
						throw (new OrderException(4));
					}
					Integer skuId = sku_.getId();
					trE.setStockKeepingUnitId(skuId);
					trE.setSiteAdminId(staffService.getSiteAdminByStaffId(sAStaff.getId(), new Date()).get(0).getId());
					if (PalletCarton.getValue() == null) {
						throw (new OrderException(5));
					}
					if (PalletCarton.getValue().equals("Pallet")) {
						trE.setUnitTypeId(1);
						trE.setIsContained(0);
						trE.setIsContaining(0);
					} else if (PalletCarton.getValue().equals("Carton")) {
						trE.setUnitTypeId(2);
						trE.setIsContained(1);
						trE.setIsContaining(0);
					}
				} catch (OrderException e) {
					Notification.show(ErrorsEnum.values()[e.getType() - 1].valueAr, Type.ERROR_MESSAGE);
					return;
				}
				try {

					if (IssueReceive.getValue().equals("Receive")) {
						trE.setTransactionTypeId(2);
						receiveOrderService.addNewOrder(trE);
						Notification.show("Success!", "New transaction receive order successfully created",
								Notification.Type.HUMANIZED_MESSAGE);
					} else if (IssueReceive.getValue().equals("Issue")) {
						long maxQuan = contactService.getAvailableBalanceOfOwner(selOwner.getId(),
								staffService.getSiteAdminByStaffId(sAStaff.getId(), new Date()).get(0).getSiteId(),
								sku_.getId());
					
						if (Quantity > maxQuan) {

							throw (new OrderException(8));

						}
						trE.setTransactionTypeId(1);
						Customer selCustomer = (Customer) CustomerComboBox.getValue();
						if (selCustomer == null) {
							throw (new OrderException(7));
						}

						trE.setCustomerId(selCustomer.getId());
						issueOrderService.addNewOrder(trE);
						Notification.show("Success!", "New transaction receive order successfully created",
								Notification.Type.HUMANIZED_MESSAGE);
					}
				} catch (OrderException e) {
					Notification.show(ErrorsEnum.values()[e.getType() - 1].valueAr, Type.ERROR_MESSAGE);
					return;
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Fail1");
					Notification.show("Test", Type.ERROR_MESSAGE);
				}
			}
		});

		ClearAll.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				PalletCarton.setValue(null);
				IssueReceive.setValue(null);
				SKU.setValue(null);
				quatityText.setValue("");
				BatchNoText.setValue("");
				BinNoText.setValue(null);
				TransactionCalender.setValue(null);
				CustomerComboBox.setValue(null);
				OwnerComboBox.setValue(null);
			}
		});

		if (!UI.getCurrent().getWindows().contains(integrateAllLayouts())) {
			addWindow(integrateAllLayouts());
		}

	}

	@SuppressWarnings("deprecation")
	private void initComboBoxs() {
		quatityText.setRequired(true);
		quatityText.addValidator(new com.vaadin.data.validator.IntegerValidator("Must be number"));
		TransactionCalender.setRequired(true);
		this.OwnerComboBox.setRequired(true);
		this.CustomerComboBox.setRequired(true);
		SKU.setRequired(true);
		List<String> uomType = Arrays.asList("Pallet", "Carton");
		PalletCarton = new ComboBox(this.getMessage(GasDiaryMessages.UnitOfMeasurement), new IndexedContainer(uomType));
		PalletCarton.setNullSelectionAllowed(false);

		List<String> TraType = Arrays.asList("Issue", "Receive", "Transfer");
		IssueReceive = new ComboBox(this.getMessage(GasDiaryMessages.TransactionType), new IndexedContainer(TraType));
		IssueReceive.setNullSelectionAllowed(false);

		this.PalletCarton.setRequired(true);
		this.IssueReceive.setRequired(true);

		List<Customer> cmrs = contactService.getAllCustomers();
		BeanItemContainer<Customer> beansCmrs = new BeanItemContainer<Customer>(Customer.class);
		beansCmrs.addAll(cmrs);
		CustomerComboBox.setContainerDataSource(beansCmrs);
		CustomerComboBox.setItemCaptionPropertyId("name");

		int adminSiteId = Integer.parseInt((String) VaadinSession.getCurrent().getAttribute("Site_Id"));
		try {
			List<Owner> ownrs = new ContractService().listOwnersOfSite(adminSiteId);
			BeanItemContainer<Owner> beansOwnrss = new BeanItemContainer<Owner>(Owner.class);
			beansOwnrss.addAll(ownrs);
			OwnerComboBox.setContainerDataSource(beansOwnrss);
			OwnerComboBox.setItemCaptionPropertyId("name");
		} catch (Exception e) {
			Notification.show("Error", e.getMessage(), Notification.Type.TRAY_NOTIFICATION);
		}

	}

	private Window integrateAllLayouts() {
		Window subWindow = new Window();
		subWindow.center();
		subWindow.setClosable(false);
		subWindow.setResizable(false);
		subWindow.setDraggable(false);
		subWindow.setSizeFull();
		getCurrent().addWindow(subWindow);
		VerticalLayout tempLay = new VerticalLayout();
		IntegrateSideLayout();
		settingsLayout.setId("Settings");
		notExecutedOrdersLayout.setId("Not Executed Orders");
		newOrderLayout.setId("New Order");
		mainSheet.addTab(settingsLayout, this.getMessage(GasDiaryMessages.Setting));
		mainSheet.addTab(notExecutedOrdersLayout, this.getMessage(GasDiaryMessages.NotExecutedOrder));
		mainSheet.addTab(newOrderLayout, this.getMessage(GasDiaryMessages.NewOrder));

		mainSheet.setWidth("600px");
		tabsLayout.addComponent(mainSheet);
		tabsLayout.setSpacing(true);
		tabsLayout.setMargin(true);
		MainLayout.addComponent(tabsLayout);
		sideLayout.setSpacing(true);
		sideLayout.setMargin(true);
		MainLayout.addComponent(sideLayout);
		MainLayout.setComponentAlignment(sideLayout, Alignment.TOP_RIGHT);

		tempLay.addComponent(headerLayout);
		footerLayout.addComponent(new Image(null, new ThemeResource("roboVics.png")));
		tempLay.addComponent(MainLayout);
		headerLayout.addComponent(switchLanguage);
		tempLay.setComponentAlignment(MainLayout, Alignment.MIDDLE_CENTER);
		tempLay.addComponent(footerLayout);

		switchLanguage.addClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				setLocale(new Locale("en"));
				if (((Locale) VaadinSession.getCurrent().getAttribute("Locale")).getLanguage().equals("fi")) {
					VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
				} else {
					VaadinSession.getCurrent().setAttribute("Locale", new Locale("fi"));
					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("fi"));
				}
				Page.getCurrent().getJavaScript().execute("window.location.reload();");
			}
		});
		MainLayout.setMargin(true);
		MainLayout.setSpacing(true);
		tempLay.setSpacing(true);
		tempLay.setMargin(true);
		subWindow.setContent(tempLay);
		return subWindow;
	}

	private void IntegrateSettingLayout() {
		if (sAStaff != null) {
			settingsLayout.removeAllComponents();
			FormLayout settingFormLayout = new FormLayout();
			HorizontalLayout buttonSettingLayout = new HorizontalLayout();

			final Button save = new Button(this.getMessage(GasDiaryMessages.Save));
			final Button cancel = new Button(this.getMessage(GasDiaryMessages.CancelKey));
			System.out.println(sAStaff.getMobileNumber());
			mobile.setValue(sAStaff.getMobileNumber());
			email.setValue(sAStaff.getEmail());
			password.setValue(sAStaff.getPassword());
			userName.setValue(sAStaff.getUname());

			mobile.setNullSettingAllowed(false);
			email.addValidator(new EmailValidator("Not Valid Email"));
			userName.setIcon(FontAwesome.USER);

			itemsProperty.addItemProperty("userName", new ObjectProperty<String>(""));
			itemsProperty.addItemProperty("password", new ObjectProperty<String>(""));
			itemsProperty.addItemProperty("mobile Number", new ObjectProperty<String>(""));
			itemsProperty.addItemProperty("email", new ObjectProperty<String>(""));

			settingFormLayout.addComponent(userName);
			settingFormLayout.addComponent(password);
			settingFormLayout.addComponent(mobile);
			settingFormLayout.addComponent(email);
			buttonSettingLayout.addComponent(edit);

			userName.setReadOnly(true);
			password.setReadOnly(true);
			mobile.setReadOnly(true);
			email.setReadOnly(true);
			buttonSettingLayout.addComponent(save);
			buttonSettingLayout.addComponent(cancel);

			save.setVisible(false);
			cancel.setVisible(false);

			edit.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					save.setVisible(true);
					cancel.setVisible(true);
					password.setReadOnly(false);
					mobile.setReadOnly(false);
					email.setReadOnly(false);
				}
			});

			save.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					sAStaff.setEmail(email.getValue());
					sAStaff.setMobileNumber(mobile.getValue());
					sAStaff.setPassword(password.getValue());
					try {
						staffService.updateStaff(sAStaff);
					} catch (BusinessException e) {
						e.printStackTrace();
					}
					save.setVisible(false);
					cancel.setVisible(false);
					password.setReadOnly(true);
					mobile.setReadOnly(true);
					email.setReadOnly(true);
				}
			});

			cancel.addClickListener(new Button.ClickListener() {
				public void buttonClick(ClickEvent event) {
					save.setVisible(false);
					cancel.setVisible(false);
					password.setReadOnly(true);
					mobile.setReadOnly(true);
					email.setReadOnly(true);
				}
			});

			FieldGroup form = new FieldGroup(itemsProperty);
			form.bindMemberFields(settingFormLayout);

			settingsLayout.addComponent(settingFormLayout);
			settingsLayout.addComponent(buttonSettingLayout);

			buttonSettingLayout.setMargin(true);
			buttonSettingLayout.setSpacing(true);
		}
	}

	private void IntegrateNotExecutedOrderLayout() {
		notExecutedOrdersLayout.removeAllComponents();
		Table NotexcutedOrderTable = new Table();

		NotexcutedOrderTable.addContainerProperty("orderDate", Date.class, null);
		NotexcutedOrderTable.addContainerProperty("transactionTime", String.class, null);
		NotexcutedOrderTable.addContainerProperty("transactionTypeId", String.class, null);
		NotexcutedOrderTable.addContainerProperty("itemsQuantity", Integer.class, null);
		List<TransactionOrder> transactionOrders = new ArrayList<>();
		try {
			transactionOrders = transactionOrderService.getUnExecutedOrdersByType(TransactionTypesEnum.RECEIVE,
					staffService.getSiteId(sAStaff));
			BeanItemContainer<TransactionOrder> ordC = new BeanItemContainer<TransactionOrder>(TransactionOrder.class);
			ordC.addAll(transactionOrders);
			NotexcutedOrderTable.setContainerDataSource(ordC);
		} catch (BusinessException e) {
			e.printStackTrace();
		}

		NotexcutedOrderTable.setPageLength(NotexcutedOrderTable.size());
		NotexcutedOrderTable.setWidth("600px");

		notExecutedOrder.addComponent(NotexcutedOrderTable);
		notExecutedOrdersLayout.addComponent(NotexcutedOrderTable);

	}

	private void IntegrateNewOrderLayout() {
		itemsProperty.addItemProperty("unitOfMeasurement", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("transactionType", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("skuClass", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("quantity", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("batchNo", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("customer", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("owner", new ObjectProperty<String>(""));
		itemsProperty.addItemProperty("transactionDate", new ObjectProperty<String>(""));

		newOrderForm.addComponent(this.IssueReceive);
		newOrderForm.addComponent(PalletCarton);
		newOrderForm.addComponent(this.OwnerComboBox);
		newOrderForm.addComponent(this.SKU);
		newOrderForm.addComponent(this.quatityText);
		newOrderForm.addComponent(this.orderCalender);
		newOrderForm.addComponent(this.TransactionCalender);
		OwnerComboBox.setVisible(true);
		newOrderForm.addComponent(this.CustomerComboBox);
		CustomerComboBox.setVisible(false);

		FieldGroup form = new FieldGroup(itemsProperty);
		form.bindMemberFields(newOrderForm);

		buttonsLayout.addComponent(ClearAll);
		buttonsLayout.addComponent(Done);
		buttonsLayout.setSpacing(true);
		buttonsLayout.setMargin(true);

		newOrderLayout.addComponent(newOrderForm);
		newOrderLayout.addComponent(buttonsLayout);

	}

	private void IntegrateSideLayout() {
		Label siteAdminName = new Label();
		if (sAStaff != null) {
			siteAdminName.setCaption(sAStaff.getFirstName() + " " + sAStaff.getLastName());
		}
		siteAdminName.setImmediate(true);
		Button logOut = new Button(this.getMessage(GasDiaryMessages.LogOut));
		sideLayout.addComponent(siteAdminName);
		sideLayout.addComponent(logOut);
		logOut.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				VaadinSession.getCurrent().setAttribute("IsLogin", "false");
				VaadinSession.getCurrent().setAttribute("user_id", "-1");
				VaadinSession.getCurrent().setAttribute("user_priviladge", "-1");
				VaadinSession.getCurrent().setAttribute("Site_Id", "-1");
				getPage().setLocation("AdminPage");
			}
		});
	}

	private VerticalLayout TransferLayout() {
		final GridLayout TransferWindow_1 = new GridLayout(1, 2);
		final VerticalLayout TransferWindow_2 = new VerticalLayout();
		TransferWindow_1.setSizeUndefined();
		HorizontalLayout HLayout = new HorizontalLayout();
		Button Done1 = new Button(this.getMessage(GasDiaryMessages.Done));
		Button cancle1 = new Button(this.getMessage(GasDiaryMessages.CancelKey));

		Panel PanelFrom = new Panel(this.getMessage(GasDiaryMessages.From));
		PanelFrom.setWidth("500px");
		PanelFrom.setHeight("500px");
		Panel PanelTo = new Panel(this.getMessage(GasDiaryMessages.To));
		PanelTo.setWidth("500px");
		final Table TreeFromGb = new Table(this.getMessage(GasDiaryMessages.Bins));
		final Table TreeFromSlt = new Table(this.getMessage(GasDiaryMessages.CurrentIncubatedSlots));
		final Table TreeFromPallet = new Table(this.getMessage(GasDiaryMessages.CurrentIncubatedPallets));
		final Table TreeToGb = new Table(this.getMessage(GasDiaryMessages.Bins));
		final Table TreeToSlt = new Table(this.getMessage(GasDiaryMessages.CurrentIncubatedSlots));

		TreeToSlt.setPageLength(TreeToSlt.size());

		adminSiteId = (staffService.getSiteAdminByStaffId(sAStaff.getId(), new Date()).get(0).getId());
		System.out.println("siteId " + adminSiteId);
		groubBins = masterService.getGroupBins(
				(staffService.getSiteAdminByStaffId(sAStaff.getId(), new Date()).get(0).getSiteId()), new Date());
		groubBinsBean = new BeanItemContainer<GroupBin>(GroupBin.class);
		groubBinsBean.addAll(groubBins);

		TreeToGb.setContainerDataSource(groubBinsBean);
		TreeFromGb.setContainerDataSource(groubBinsBean);

		TreeToGb.setPageLength(3);
		TreeToGb.setBuffered(false);
		TreeToGb.setVisibleColumns(new Object[] { "symbol" });
		TreeToGb.setColumnHeaders(new String[] { "" });
		TreeToGb.setSelectable(true);
		TreeToGb.setImmediate(true);

		TreeFromGb.setPageLength(3);
		TreeFromGb.setBuffered(false);
		TreeFromGb.setVisibleColumns(new Object[] { "symbol" });
		TreeFromGb.setColumnHeaders(new String[] { "" });
		TreeFromGb.setSelectable(true);
		TreeFromGb.setImmediate(true);

		TreeToSlt.addActionHandler(new Handler() {

			public void handleAction(Action action, Object sender, Object target) {
				if (action == NewPallet) {
					Window CreateNew = CreateNewPallet();
					CreateNew.setDraggable(false);
					CreateNew.setResizable(false);
					UI.getCurrent().addWindow(CreateNew);
				}

			}

			public Action[] getActions(Object target, Object sender) {
				ArrayList<Action> actions = new ArrayList<Action>();
				// Enable deleting only if clicked on an item
				if (target != null)
					actions.add(NewPallet);

				return (Action[]) (actions.toArray(new Action[actions.size()]));
			}
		});

		TreeFromSlt.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				TreeFromPallet.setSelectable(true);
				TreeFromPallet.setImmediate(true);
			}
		});

		TreeFromGb.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				GroupBin selectedGbn = (GroupBin) (event.getProperty().getValue());
				System.out.println("Group  " + selectedGbn.getId());
				binSlots = binSlotService.getDetailsOfFullSlotOfBin(selectedGbn.getId(), new Date());
				if (binSlots == null | binSlots.isEmpty()) {

					System.out.println("Nullllll");
				}

				binSlotsBean.removeAllItems();
				binSlotsBean.addAll(binSlots);
				TreeFromSlt.setContainerDataSource(binSlotsBean);
				TreeFromSlt.setPageLength(binSlotsBean.size());
				TreeFromSlt.setBuffered(false);
				TreeFromSlt.setVisibleColumns(new Object[] { "itemserial", "itemname", "itemcode", "y", "z" });
				TreeFromSlt.setColumnHeaders(new String[] { "Serial", "Item Name", "Item Code", "Length", "Height" });
				TreeFromSlt.setSelectable(true);
				TreeFromSlt.setImmediate(true);

			}
		});

		TreeToGb.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				GroupBin selectedGbn = ((GroupBin) event.getProperty().getValue());

				binSlots = binSlotService.getDetailsOfEmptySlotOfBin(selectedGbn.getId(), new Date());
				if (binSlots == null | binSlots.isEmpty()) {
					System.out.println("Nullllll");
				}
				binSlotsBeanTo.removeAllItems();
				binSlotsBeanTo.addAll(binSlots);
				TreeToSlt.setContainerDataSource(binSlotsBeanTo);
				TreeToSlt.setPageLength(binSlotsBeanTo.size());
				TreeToSlt.setBuffered(false);
				TreeToSlt.setVisibleColumns(new Object[] { "y", "z", "itemserial", "amount" });
				TreeToSlt.setColumnHeaders(new String[] { "Length", "Height", "Serial", "Amount" });
				TreeToSlt.setSelectable(true);
				TreeToSlt.setImmediate(true);

			}
		});

		HorizontalLayout vlTo = new HorizontalLayout();
		HorizontalLayout vlFrom = new HorizontalLayout();

		vlTo.addComponent(TreeToGb);
		vlTo.addComponent(TreeToSlt);

		vlFrom.addComponent(TreeFromGb);
		vlFrom.addComponent(TreeFromSlt);
		vlFrom.addComponent(TreeFromPallet);

		PanelFrom.setContent(vlFrom);
		PanelTo.setContent(vlTo);

		HLayout.addComponent(PanelFrom);
		HLayout.addComponent(PanelTo);
		HLayout.setSpacing(true);
		HLayout.setMargin(true);
		TransferWindow_1.setSpacing(true);
		TransferWindow_1.setMargin(true);
		TransferWindow_1.addComponent(PanelFrom, 0, 0);
		TransferWindow_1.addComponent(PanelTo, 0, 1);

		HorizontalLayout done_cancle_layout = new HorizontalLayout();
		done_cancle_layout.addComponent(Done1);
		done_cancle_layout.addComponent(cancle1);
		done_cancle_layout.setSpacing(true);
		done_cancle_layout.setMargin(true);

		Label from = new Label("Transfer Order (Please choose from slot to un incubated slot)");
		TransferWindow_2.addComponent(back);
		TransferWindow_2.addComponent(from);
		TransferWindow_2.addComponent(TransferWindow_1);
		TransferWindow_2.addComponent(done_cancle_layout);
		TransferWindow_2.setSpacing(true);
		TransferWindow_2.setMargin(true);

		Done1.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				GroupBin selectedFromGbn = (GroupBin) (TreeFromGb.getValue());
				final BinSlotDetails selectedFromSlt = ((BinSlotDetails) TreeFromSlt.getValue());
				final BinSlotDetails selectedToSlt = ((BinSlotDetails) TreeToSlt.getValue());

				GroupBin selectedToGbn = ((GroupBin) TreeToGb.getValue());

				if (selectedFromGbn == null) {
					new Notification("Notification error", "Please select a From Bin").show(Page.getCurrent());
				} else if (selectedFromSlt == null) {
					new Notification("Notification error", "Please select a From Slot").show(Page.getCurrent());
				} else if (selectedToGbn == null) {
					new Notification("Notification error", "Please select a To Bin").show(Page.getCurrent());
				} else {
					TransferWindow_2.setVisible(false);
					PalletCarton.setVisible(true);
					IssueReceive.setVisible(true);
					SKU.setVisible(true);
					quatityText.setVisible(true);
					orderCalender.setVisible(true);
					TransactionCalender.setVisible(true);
					Done.setVisible(true);
					ClearAll.setVisible(true);
					try {
						int toto = selectedToSlt.getSlotid();
						int fromfrom = selectedFromSlt.getSlotid();
						System.out.println(VaadinSession.getCurrent().getAttribute("user_id").toString());
						transferService.execute_transfer_transaction(adminSiteId, selectedFromSlt.getItemserial(), toto,
								fromfrom);
						new Notification("Information", "Order Has been added to the execution level");
					} catch (BusinessException e) {
						new Notification("Notification error", "Error in Submitting Order");
					}
				}
			}
		});
		return TransferWindow_2;
	}

	/*
	 * TODO: Add If SKU is Already defined SO a new Parameter shall be paassed .
	 * Change the SKUs TO be Defined.
	 */

	public Window CreateNewPallet() {
		final Window CreateNew = new Window(this.getMessage(GasDiaryMessages.CreateNewPallet));
		List<StockKeepingUnit> skus = skuService.getAllSKU();

		CreateNew.setResizable(false);
		VerticalLayout V1 = new VerticalLayout();
		final ComboBox SKU = new ComboBox();
		final DateField Production = new DateField();
		SKU.setCaption(this.getMessage(GasDiaryMessages.SKU));
		BeanItemContainer<StockKeepingUnit> newDataSource = new BeanItemContainer<StockKeepingUnit>(
				StockKeepingUnit.class);
		newDataSource.addAll(skus);
		SKU.setContainerDataSource(newDataSource);
		SKU.setItemCaptionPropertyId("description");
		Production.setCaption(this.getMessage(GasDiaryMessages.ProductionDate));
		V1.addComponent(SKU);
		V1.addComponent(Production);

		CreateNew.setContent(V1);
		V1.setSpacing(true);
		V1.setMargin(true);
		CreateNew.setWidth("300px");
		CreateNew.setHeight("250px");

		CreateNew.center();

		return CreateNew;
	}

	@Override
	public void setLocale(Locale locale) {
		super.setLocale(locale);
		i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
	}

	/** Returns the bundle for the current locale. */
	public ResourceBundle getBundle() {
		return i18nBundle;
	}

	/**
	 * Returns a localized message from the resource bundle with the current
	 * application locale.
	 **/
	public String getMessage(String key) {
		return i18nBundle.getString(key);
	}
}