package com.robovics.wims.model.orm;

import org.hibernate.Session;

public class CustomSession {
	private Session session;

	// Package visibility to be used only by Data Access Class.
	CustomSession(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return this.session;
	}

	// -----------------------------------------------------------------------------

	public void beginTransaction() {
		session.beginTransaction();
	}

	public void commitTransaction() {
		session.getTransaction().commit();
	}

	public void rollbackTransaction() {
		session.getTransaction().rollback();
	}

	public void close() {
		session.close();
	}
}