package com.robovics.wims.model.orm.entities.transaction.execute;

import com.google.gwt.junit.client.WithProperties.Property;
import com.robovics.wims.model.orm.entities.contact.Contact;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.staff.StaffService;
import com.vaadin.data.fieldgroup.PropertyId;


public class ExecutedItem{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8931373778889625530L;
	@PropertyId (value = "id")
	private Integer id;
	@PropertyId (value = "Serial")
	private Integer Serial;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSerial() {
		return Serial;
	}
	public void setSerial(Integer serial) {
		Serial = serial;
	}
	
	
}
