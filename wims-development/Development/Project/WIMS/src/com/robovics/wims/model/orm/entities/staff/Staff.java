package com.robovics.wims.model.orm.entities.staff;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {
		@NamedQuery(name = "GET_Site_Id_By_Admin_Id", query = "select a.siteId from SiteAdmin a , Staff s where  a.staffId=s.id and s.id =:p_STF_Id"),
		@NamedQuery(name = "GET_Site_Id_By_Worker_Id", query = "select a.siteId from SiteLabor a , Staff s where  a.staffId=s.id and s.id =:p_STF_Id"),
		@NamedQuery(name = "UserInfo", query = "select s.id from Staff s where  s.password=:p_password and s.uname =:p_uname")	
		}) 

@Table(name = "Staff")
@Entity
public class Staff extends BaseEntity {

	private static final long serialVersionUID = 5059555221986319869L;
	private Integer id;
	private String firstName;
	private String lastName;
	private String mobileNumber;
	private String uname;
	private String password;
	private Integer priviladge;
	private String email ; 

	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "STF_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "STF_Fname")
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "STF_Lname")
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "STF_MobileNumber")
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "STF_Uname")
	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	@Column(name = "STF_Pass")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "STF_Priviladge")
	public Integer getPriviladge() {
		return priviladge;
	}

	public void setPriviladge(Integer priviladge) {
		this.priviladge = priviladge;
	}
	
	@Column(name = "STF_Email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}