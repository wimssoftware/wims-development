package com.robovics.wims.ui.staff;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.annotation.WebServlet;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.staff.StaffService;
import com.robovics.wims.ui.GasDiaryMessages;
import com.robovics.wims.ui.contact.ContactUI;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("wims")
public class WareHouseUI extends UI implements View {
 	Button switchLanguage;
 	Button b1 ;
 	Button b2 ;
 	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
 	
	private HorizontalLayout headerLayout = new HorizontalLayout();
	private HorizontalLayout footerLayout = new HorizontalLayout();
	public static final String NAME = "";

	private StockKeepingUnitService skuService;

	@WebServlet(value = "/WPHI/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = WareHouseUI.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		
		if (VaadinSession.getCurrent().getAttribute("IsLogin") == null
				|| VaadinSession.getCurrent().getAttribute("user_priviladge") == null) {
			authintecate();
		} else {
			if (VaadinSession.getCurrent().getAttribute("IsLogin")
					.equals("false")
					|| !VaadinSession.getCurrent()
							.getAttribute("user_priviladge").equals("1")) {
				
				authintecate();
			} else {
				Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
				 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
				 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
				switchLanguage = new Button(this.getMessage(GasDiaryMessages.switchLanguage));
				 b1 = new Button(this.getMessage(GasDiaryMessages.Reports));
				  b2 = new Button(this.getMessage(GasDiaryMessages.LogOut));
				 
				Build_UI();
			}

		}
	}

	public void Build_UI() {
		getPage().setTitle("Warehouse Portal");
		skuService = (StockKeepingUnitService) ServiceFactory.getInstance()
				.getService(ServicesEnum.SKU_SERVICE);
		List<StockKeepingUnit> skus = skuService.getAllSKU();
		setContent(buildForm(skus));
	}

	public void authintecate() {
		getPage().setTitle("Warehouse Portal");
		final Window LoginWindow = new Window();
		LoginWindow.center();
		LoginWindow.setClosable(false);
		LoginWindow.setDraggable(false);
		LoginWindow.setResizable(false);
		getCurrent().addWindow(LoginWindow);
		VerticalLayout vlayout = new VerticalLayout();
		Panel panel = new Panel();
		panel.setSizeUndefined();
		vlayout.addComponent(panel);
		CustomLayout custom = new CustomLayout("login_temp");
		custom.addStyleName("customlayoutexample");
		panel.setContent(custom);
		final TextField username = new TextField();
		custom.addComponent(username, "username");
		final PasswordField password = new PasswordField();
		custom.addComponent(password, "password");
		Button ok = new Button("Login");
		ok.setClickShortcut(KeyCode.ENTER);
		ok.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				
				StaffService ss = new StaffService();
				try {
					List<Integer> site_id = ss.authenticateStaff(
							username.getValue(), password.getValue());
					if (site_id != null && !site_id.isEmpty()) {
						LoginWindow.close();
						if (VaadinSession.getCurrent()
								.getAttribute("user_priviladge").equals("1")) {
							Page.getCurrent().reload();	
						} else {
							Notification not = new Notification(
									"Login error",
									Notification.TYPE_ERROR_MESSAGE);
							not.show(Page.getCurrent());
						} 
					} else {
						Notification not = new Notification(
								"Login error",
								Notification.TYPE_ERROR_MESSAGE);
						not.show(Page.getCurrent());
					}  
				} catch (BusinessException e) {
					Notification not = new Notification("Notification error", e
							.getMessage());
					not.show(Page.getCurrent());
					e.printStackTrace();
				}

			}
		});
		custom.addComponent(ok, "okbutton");
		LoginWindow.setContent(custom);
	}

	@SuppressWarnings("deprecation")
	private VerticalLayout buildForm(List<StockKeepingUnit> skus) {

		VerticalLayout vlayout = new VerticalLayout();
		Accordion accordion = new Accordion();
		accordion.setSizeFull();
		headerLayout.addComponent(new Image(null, new ThemeResource(
				"NTLC-Logo.JPG")));
		headerLayout.setSizeFull();
		footerLayout.addComponent(new Image(null, new ThemeResource(
				 "roboVics.png")));
		footerLayout.setSizeFull();
		VerticalLayout mainLayout = new VerticalLayout();
		headerLayout.addComponent(switchLanguage);

		Layout SKU = new SKU_Warehouse_UI().buildSKU();
		Layout contact = new ContactUI().buildForm();
		Layout Staff = new StaffPanel().buildStaff();
		Layout siteLyout = new SitePanel().buildForm();
		Layout contractUi = new contractUi().build();

		accordion.addTab(SKU, "SKU");
		accordion.addTab(contact, "Contact");
		accordion.addTab(Staff, "Staff");
		accordion.addTab(siteLyout, "Site");
		accordion.addTab(contractUi, "Contract");
		
		HorizontalLayout toolLayout = new HorizontalLayout();
		
		
		b1.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				getUI().getPage().setLocation("Reports");
			}
		});
		
		
		b1.setId("toolLayout_b1");
		b2.setId("toolLayout_b2");
		toolLayout.addComponent(b1);
		
		toolLayout.addComponent(b2);
		
		toolLayout.setSizeUndefined();
		vlayout.addComponent(toolLayout);
		vlayout.addComponent(accordion);
		
		b2.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				// set the value of new fields
				VaadinSession.getCurrent().setAttribute("IsLogin", "false");
				VaadinSession.getCurrent().setAttribute("user_id", "-1");
				VaadinSession.getCurrent()
						.setAttribute("user_priviladge", "-1");
				VaadinSession.getCurrent().setAttribute("Site_Id", "-1");
				getPage().setLocation("WPHI");
			}
		});
        
		switchLanguage.addClickListener(new ClickListener() {
  			public void buttonClick(ClickEvent event) {
  				setLocale(new Locale("en")); 
  			   // rebuild it with the new locale.
  				 if (((Locale)VaadinSession.getCurrent().getAttribute("Locale")).getLanguage().equals("fi")){
  					VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
  					i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
  				}
  				else{
  					VaadinSession.getCurrent().setAttribute("Locale", new Locale("fi"));
  				i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),new Locale("fi") );
  				}
  				Page.getCurrent().getJavaScript().execute("window.location.reload();");		
  			} 
  		});
		
		Panel panel = new Panel("Warehouse Admin Portal");
		panel.setContent(vlayout);
		mainLayout.addComponent(headerLayout);
		mainLayout.addComponent(panel);
		mainLayout.addComponent(footerLayout);
		mainLayout.setComponentAlignment(headerLayout, Alignment.TOP_CENTER);
		mainLayout.setComponentAlignment(footerLayout, Alignment.MIDDLE_CENTER);
		panel.setWidth("100%");
		panel.setHeight("100%");
		return mainLayout;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
    }
    
    /** Returns the bundle for the current locale. */
    public ResourceBundle getBundle() {
        return i18nBundle;
    }
 
    /**
     * Returns a localized message from the resource bundle
     * with the current application locale.
     **/ 
    public String getMessage(String key) {
        return i18nBundle.getString(key);
    }

}