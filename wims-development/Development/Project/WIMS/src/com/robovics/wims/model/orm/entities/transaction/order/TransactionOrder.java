package com.robovics.wims.model.orm.entities.transaction.order;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = { @NamedQuery(name = "TransactionOrder_getUnExecutedByType", query = " Select t.id from TransactionOrder t where "
		+ " (t.executed is null or t.executed = 0) "
		+ " and t.transactionTypeId = :P_TRANSACTION_TYPE_ID "
		+ " and t.transactionTime <= CURRENT_DATE "
		+ " and t.siteAdminId = :P_SiteAdminId "
		+ " order by t.transactionTime ") })
@Table(name = "Transaction_Order")
@Entity
public class TransactionOrder extends BaseEntity {

	private static final long serialVersionUID = -3583635149191036979L;
	private Integer id;
	private Date orderDate;
	private Date transactionTime;
	private Integer siteAdminId;
	private Integer transactionTypeId;
	private Integer itemsQuantity;
	private Integer executed;
	private String siteAdminName;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TOD_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TOD_OrderDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	@Column(name = "TOD_TransactionTime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTransactionTime() {
		return transactionTime;
	}

	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}

	@Column(name = "TOD_SAD_Id")
	public Integer getSiteAdminId() {
		return siteAdminId;
	}

	public void setSiteAdminId(Integer siteAdminId) {
		this.siteAdminId = siteAdminId;
	}

	@Column(name = "TOD_TTP_Id")
	public Integer getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Integer transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	@Column(name = "TOD_ItemsQuantity")
	public Integer getItemsQuantity() {
		return itemsQuantity;
	}

	public void setItemsQuantity(Integer itemsQuantity) {
		this.itemsQuantity = itemsQuantity;
	}

	@Column(name = "TOD_IsExecuted")
	public Integer getExecuted() {
		return executed;
	}

	public void setExecuted(Integer executed) {
		this.executed = executed;
	}

	@Transient
	public String getSiteAdminName() {
		return siteAdminName;
	}

	public void setSiteAdminName(String siteAdminName) {
		this.siteAdminName = siteAdminName;
	}
}