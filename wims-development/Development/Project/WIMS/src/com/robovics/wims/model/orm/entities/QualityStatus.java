package com.robovics.wims.model.orm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "quality_status_type")
@Entity
public class QualityStatus extends BaseEntity {

	private static final long serialVersionUID = 4323565460988709579L;
	private Integer id;
	private String description;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "QST_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "QST_Description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return description;
	}
	
}