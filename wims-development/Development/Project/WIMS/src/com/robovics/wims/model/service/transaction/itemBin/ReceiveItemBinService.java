package com.robovics.wims.model.service.transaction.itemBin;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;

@Service(value = "receiveItemBinService")
public class ReceiveItemBinService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	public void addReceiveItemBin(ReceiveItemBin receiveItemBin,
			CustomSession session) throws BusinessException {
		try {
			receiveItemBin.setExecutionTime(new Date());
			dao.addEntity(receiveItemBin, session);
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}
}