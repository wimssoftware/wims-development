package com.robovics.wims.model.orm.entities.contact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.robovics.wims.model.orm.entities.BaseEntity;

@Table(name = "Customer")
@Entity
public class Customer extends BaseEntity {

	private static final long serialVersionUID = 6396592762431387307L;
	private Integer id;
	private Integer contactId;
	private String name ; 

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "CMR_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CMR_CNT_Id")
	public Integer getContactId() {
		return contactId;
	}

	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	

	@Transient
	public String getName() {
		return name;
	}

	public void setName(String symbol) {
		this.name = symbol;
	}
}
