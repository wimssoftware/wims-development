package com.robovics.wims.ui.staff;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;


import com.robovics.wims.model.enums.ErrorsEnum;











import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import com.google.gwt.user.datepicker.client.DateBox;
import com.mysql.jdbc.Connection;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.StockKeepingUnitService;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.contact.ContactService;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.staff.ContractService;
import com.robovics.wims.model.service.staff.StaffService.StaffView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout.MarginHandler;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;


@SuppressWarnings("serial")
@Theme("wims")
public class reports extends UI {
	 ListSelect list = new ListSelect ("Reports");
	 Panel DatePanel = new Panel();
	 Panel win = new Panel("Reports Of item per site");
	byte test [] = new byte[4000]; 
	 ContactService contactService = null;
	
	 MasterDataService masterDataService = null;
	GridLayout mainLayout = new GridLayout(2, 1);
	@WebServlet(value = { "/Reports/*" }, asyncSupported = true)
	
	@VaadinServletConfiguration(productionMode = false, ui = reports.class)
	public static class Servlet extends VaadinServlet {
		
		
	}

	@Override
	protected void init(VaadinRequest request) {
		
		final VerticalLayout nla= new VerticalLayout();
				nla.addComponent(list);
		 
		list.addItem(1);
		list.addItem(2);
		list.setItemCaption(1 , "Items Per Sites");
		list.setItemCaption(2 , "Serials Per Sites");
		list.setNullSelectionAllowed(false);
		list.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent e){ 
			 Notification.show("Value changed:",
                String.valueOf(e.getProperty().getValue()),
                Type.TRAY_NOTIFICATION);
			 if(list.getValue().toString().equals("1"))
			 {
			// getCurrent().addWindow(itemsPersite());
				 nla.removeComponent(DatePanel);
				 nla.addComponent( itemsPersite());
				
			 }
			 else if (list.getValue().toString().equals("2"))
			 {
				 nla.removeComponent(win);
				 nla.addComponent( SerialPerSite());
			 }
		}
	});
		
		
	//	nla.addComponent(nButton);
		//nla.setSizeFull();
		nla.setMargin(true);
		nla.setSpacing(true);
		mainLayout.addComponent(nla, 0, 0);
		mainLayout.setSizeFull();
		setContent(mainLayout);
	}
	
	public static Connection establishConnection()
	  {
	  
	  Connection connection = null;
	  
	  try
	  
	  {
	  
	  Class.forName("com.mysql.jdbc.Driver");
	  
	  String oracleURL = "jdbc:mysql://localhost:3306/wims";
	  
	  connection = (Connection) DriverManager.getConnection(oracleURL,"root","");
	  
	  connection.setAutoCommit(false);
	  
	  }
	  
	  catch(SQLException exception)
	  
	  {
	  
	  exception.printStackTrace();
	  
	  } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  return connection;
	  
	  
	    }
	
	private Panel viewDocument() 
	{
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    final String retrievalName = "test3.pdf"; 

	   Panel ViewPDF = new Panel() ;
	    //Window window = new Window();
	   ViewPDF.setCaption("View PDF");
	    final StreamResource resource = new StreamResource(new StreamResource.StreamSource()
	    {
	        public InputStream getStream()
	        {
	            try
	            {
	                byte[] DocContent = test;
	                
	                //DocContent = getFileBytes("test3.pdf");
	                return new ByteArrayInputStream(DocContent);
	            }
	            catch (Exception e1)
	            {
	                e1.printStackTrace();
	                return null;
	            }
	        }
	    }, retrievalName)  ;

	    Embedded c = new Embedded("", resource);
	    c.setSizeFull();
	    resource.setMIMEType("application/pdf");
	    c.setType(Embedded.TYPE_BROWSER);
	    ViewPDF.setContent(c);

	    //window.setModal(true);
	    ViewPDF.setWidth("90%");
	    ViewPDF.setHeight("90%");
	    return ViewPDF ; 
	   //UI.getCurrent().addWindow(window);
	}

	/**
	 * getFileBytes
	 * 
	 * @author NBochkarev
	 * 
	 * @param fileOut
	 * @return
	 * @throws IOException
	 */
	public static byte[] getFileBytes(String fileName) throws IOException
	{
	    ByteArrayOutputStream ous = null;
	    InputStream ios = null;
	    try
	    {
	        byte[] buffer = new byte[4096];
	        ous = new ByteArrayOutputStream();
	        ios = new FileInputStream(new java.io.File(fileName));
	        int read = 0;
	        while ((read = ios.read(buffer)) != -1)
	            ous.write(buffer, 0, read);
	    }
	    finally
	    {
	        try
	        {
	            if (ous != null)
	                ous.close();
	        }
	        catch (IOException e)
	        {
	            // swallow, since not that important
	        }
	        try
	        {
	            if (ios != null)
	                ios.close();
	        }
	        catch (IOException e)
	        {
	            // swallow, since not that important
	        }
	    }
	    return ous.toByteArray();
	}

	
	public Panel itemsPersite()

	{
		  contactService = (ContactService) ServiceFactory.getInstance()
				.getService(ServicesEnum.CONTACT_SERVICE);
		List<Owner> owners =  contactService.getAllOwners();
		BeanItemContainer<Owner> beansOwnrss = new BeanItemContainer<Owner>(Owner.class);			
		beansOwnrss.addAll(owners);
		masterDataService = (MasterDataService) ServiceFactory.getInstance()
				.getService(ServicesEnum.MASTER_DATA_SERVICE);
		List<Site> sites = masterDataService.getAllSites();
		BeanItemContainer<Site> beansSitess = new BeanItemContainer<Site>(Site.class);
		beansSitess.addAll(sites);
		
		VerticalLayout v1 = new VerticalLayout();
		HorizontalLayout ownerLayout = new HorizontalLayout();
		HorizontalLayout siteLayout = new HorizontalLayout();
		final ComboBox owner = new ComboBox();
		owner.setCaption("Owners");
		final ComboBox site = new ComboBox();
		site.setCaption("Sites");
		final CheckBox AllOwners = new CheckBox("All Owners");
		final CheckBox AllSites = new CheckBox("All Sites");
		final CheckBox SKUlist = new CheckBox("List All SKUs");
		final CheckBox SKUCount = new CheckBox("Count All SKUs");
			
		Button nButton = new Button("Generate Report");
		//v1.addComponent(owner);
		beansOwnrss.addAll(owners);
		owner.setContainerDataSource(beansOwnrss);
		owner.setItemCaptionPropertyId("name");
		//owner.addItems(owners);
		//owner.addItem("All owners");
		//v1.addComponent(site);
		beansSitess.addAll(sites);
		site.setContainerDataSource(beansSitess);
		site.setItemCaptionPropertyId("name");
		//site.addItem("All Sites");
		//v1.addComponent(AllOwners);
		//v1.addComponent(AllSites);
		ownerLayout.addComponent(owner);
		ownerLayout.addComponent(AllOwners);
		siteLayout.addComponent(site);
		siteLayout.addComponent(AllSites);
		ownerLayout.setComponentAlignment(AllOwners, Alignment.BOTTOM_RIGHT);
		siteLayout.setComponentAlignment(AllSites, Alignment.BOTTOM_RIGHT);
		//ownerLayout.setMargin(true);
		ownerLayout.setSpacing(true);
		//siteLayout.setMargin(true);
		siteLayout.setSpacing(true);
		v1.addComponent(ownerLayout);
		v1.addComponent(siteLayout);
		v1.addComponent(SKUlist);
		v1.addComponent(SKUCount);
		v1.addComponent(nButton);
		v1.setComponentAlignment(nButton, Alignment.BOTTOM_CENTER);
		v1.setSpacing(true);
		v1.setMargin(true);
		win.setContent(v1);
		win.setHeight("320px");
		win.setWidth("400px");
		
		 
		
		nButton.addClickListener(new Button.ClickListener() {
			   @SuppressWarnings({ "null", "unused" })
			@Override
			   public void buttonClick(ClickEvent event)  {
				   test = new byte[4096];
				   Map<String, Object> map = new HashMap<String, Object>();
				  // listORcount.getValue() ;
				   boolean success = (new File
					         ("test3.pdf")).delete();
					         if (success) {
					            System.out.println("The file has  been successfully deleted"); 
					         }
				   Site ste = (Site) site.getValue();
				   Owner own  = (Owner) owner.getValue();

				   
				  try{
					  
						if (own == null && ste == null &&  !AllOwners.getValue() && !AllSites.getValue() && !SKUlist.getValue() && !SKUCount.getValue() )
								  {
									  Notification.show("you must choose parameter first", Type.ERROR_MESSAGE);
								  }
						else if (own == null && ste == null &&  AllOwners.getValue() || AllSites.getValue() && !SKUlist.getValue() && !SKUCount.getValue())
						  {
							  Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
						  }
						else if (own != null && ste != null &&  AllOwners.getValue() || AllSites.getValue())
						  {
							  Notification.show("you must choose either specific site of specific owner or all of them first", Type.ERROR_MESSAGE);
						  }
						else if (own == null && ste != null &&  !AllOwners.getValue() && !AllSites.getValue() && SKUlist.getValue() && SKUCount.getValue())
						{
							 Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
						}
						else if (own == null && ste == null && !AllOwners.getValue() && AllSites.getValue() && SKUlist.getValue() && SKUCount.getValue())
						{

							 Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
						}
						else if (own == null && ste == null && AllOwners.getValue() && AllSites.getValue() && SKUlist.getValue() && SKUCount.getValue())
						{
							Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
						}
						else if (own != null && ste != null &&  !AllOwners.getValue() && !AllSites.getValue() && SKUlist.getValue() && SKUCount.getValue())
						{
							Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
						}
						else if (own != null && ste != null &&  AllOwners.getValue() && AllSites.getValue() && SKUlist.getValue() && SKUCount.getValue())
						{
							Notification.show("you must choose specific report", Type.ERROR_MESSAGE);
						}
				
		
						else if (own == null && ste != null &&  !AllOwners.getValue() && !AllSites.getValue()  )
				   {
					   if (!SKUlist.getValue() && SKUCount.getValue())
					   {
						   
					   System.out.println("Entered One"); 
					    map.put("SteName", ste.getName());
					   JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\CountSKUperSpecificSite.jrxml");
					    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport ,map, establishConnection());
				    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
				    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					   }
					   
					   else if (SKUlist.getValue() && !SKUCount.getValue())
					   {
						   System.out.println("Entered Three"); 
						    map.put("SteName", ste.getName());
						   JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\ListSKUsperSpecificSite.jrxml");
						    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport ,map, establishConnection());
					    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
					    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					   }
					   else if (!SKUlist.getValue() && !SKUCount.getValue())
					   {
						   Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
					   }
					   
				   } 
					
					
					
				else if (own == null && ste == null && !AllOwners.getValue() && AllSites.getValue())
				{
					if(!SKUlist.getValue() && SKUCount.getValue())
					{
					System.out.println("Count All SKU Per All  Sites");
					 JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\CountAllSKUsperAllSites.jrxml");
					    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport ,map, establishConnection());
				    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
				    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					}
					else if(SKUlist.getValue() && !SKUCount.getValue())
					{
						System.out.println("List All SKU Per All  Sites");
						 JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\ListSKUsperAllSites.jrxml");
						    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport ,map, establishConnection());
					    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
					    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					}
					else if (!SKUlist.getValue() && !SKUCount.getValue())
					   {
						   Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
					   }
				    	
				}
				else if (own == null && ste == null && AllOwners.getValue() && AllSites.getValue())
				{
					if(SKUlist.getValue() && !SKUCount.getValue())
					{
					System.out.println("List All SKU per All Sites per All owners");
					JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\CountAllSKUsforAllSitesforAllOwners.jrxml");
				    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
			    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
			    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					}
					else if (!SKUlist.getValue() && SKUCount.getValue())
					{
						System.out.println("Count All SKU per All Sites per All owners");
						JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\CountAllSKUsperAllSitesperAllOwners.jrxml");
					    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
				    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
				    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					}
					else if (!SKUlist.getValue() && !SKUCount.getValue())
					   {
						   Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
					   }
					
				}
				else if (own != null && ste != null &&  !AllOwners.getValue() && !AllSites.getValue()){
					   if (SKUlist.getValue() && !SKUCount.getValue())
					   {
					   System.out.println("Entered Two");
					   
					   map.put("OwnID", own.getId());
					   map.put("SteName", ste.getName()); 
				     	JasperReport jasperReport = JasperCompileManager.compileReport("C:\\ireports\\ListSKUsperSpecificSiteforSpecificOwner.jrxml"); 
					    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
				    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
				    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					   }
					   else if (!SKUlist.getValue() && SKUCount.getValue())
					   {
						   System.out.println("Entered Four");
						   
						   map.put("OwnID", own.getId());
						   map.put("SteName", ste.getName());
					     	JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\CountSKUsperSpecificSiteperSpicifOwner.jrxml");
						    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
					    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
					    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					   }
					   else if (!SKUlist.getValue() && !SKUCount.getValue())
					   {
						   Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
					   }
				   }
				else if (own == null && ste != null &&  AllOwners.getValue() && !AllSites.getValue())
				{
					if (SKUlist.getValue() && !SKUCount.getValue())
					   {
					   System.out.println("Entered Two");
					   
					   map.put("SteName", ste.getName()); 
				     	JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\ListSKUsforSpecificSiteForAllOwners.jrxml");
					    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
				    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
				    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					   }
					   else if (!SKUlist.getValue() && SKUCount.getValue())
					   {
						   System.out.println("Entered Four");
						   
						   
						   map.put("SteName", ste.getName());
					     	JasperReport jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\CountSKUperSiteforAllOwners.jrxml");
						    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
					    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
					    	test = JasperExportManager.exportReportToPdf(jasperPrint);
					   }
					   
					   else if (!SKUlist.getValue() && !SKUCount.getValue())
					   {
						   Notification.show("you must choose list or count first", Type.ERROR_MESSAGE);
					   }
				}
						
				    	
				   }catch(Exception e){
					   e.printStackTrace();  
				   }
				  
				    	Panel p = viewDocument();
						p.setSizeFull();
						mainLayout.removeComponent(1, 0);
						
						mainLayout.addComponent(p,1,0);
						
						
			
			   }
			   
			});
		return win ;
	}
	
	public Panel SerialPerSite()
	{
		VerticalLayout v2 = new VerticalLayout();
		final DateField ExpirtionDate= new DateField();
		ExpirtionDate.setCaption("Date");
		Button nButton = new Button("Generate Report");
		v2.addComponents(ExpirtionDate, nButton);
		v2.setComponentAlignment(nButton, Alignment.BOTTOM_CENTER);
		v2.setMargin(true);
		v2.setSpacing(true);
		DatePanel.setContent(v2);
		DatePanel.setCaption("Choose Date");
		DatePanel.setWidth("250px");
		DatePanel.setHeight("200px");
		nButton.addClickListener(new Button.ClickListener(){

			@Override
			public void buttonClick(ClickEvent event) {
				ExpirtionDate.getValue() ;
				test = new byte[4096];
				   Map<String, Object> map = new HashMap<String, Object>();
				  // listORcount.getValue() ;
				   boolean success = (new File
					         ("test3.pdf")).delete();
					         if (success) {
					            System.out.println("The file has  been successfully deleted"); 
					         }
				
					         JasperReport jasperReport;
							try {
								System.out.println("Entered seven");
								map.put("ExpirationDate", ExpirtionDate.getValue());
								jasperReport = JasperCompileManager.compileReport(".\\.\\.\\ireports\\Serialsand SlotParameter.jrxml");
								JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,map, establishConnection());
						    	JasperExportManager.exportReportToPdfFile(jasperPrint, "test3.pdf");
						    	test = JasperExportManager.exportReportToPdf(jasperPrint);
						    	Panel p = viewDocument();
								p.setSizeFull();
								mainLayout.removeComponent(1, 0);
								mainLayout.addComponent(p,1,0);
							} catch (JRException e) {
								// TODO Auto-generated catch block
								e.printStackTrace(); 
								 
							}
							    

				// TODO Auto-generated method stub
				
			}
			
		}
		);
	
		return DatePanel;
		
	}
}
		
