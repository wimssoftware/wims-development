package com.robovics.wims.model.service.transaction.execute;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.masterData.CurrentBinItem;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteIssue;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteReceive;
import com.robovics.wims.model.orm.entities.transaction.itemBin.IssueItemBin;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.transaction.itemBin.ReceiveItemBinService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderService;

@Service(value = "transactionExecuteIssueService")
public class TransactionExecuteIssueService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	@Autowired
	private TransactionOrderService transactionOrderService;
	@Autowired
	private ReceiveItemBinService receiveItemBinService;
	@Autowired
	private MasterDataService masterService;

	public void addTransactionExecuteReceive(
			TransactionExecuteReceive transactionExecuteReceive)
			throws BusinessException {
		try {
			dao.addEntity(transactionExecuteReceive);
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public void submitExecution(
			TransactionExecuteReceive transactionExecuteReceive,
			List<ReceiveItemBin> receiveItemBins,
			TransactionOrder transactionOrder) throws BusinessException {
		transactionExecuteReceive
				.setTransactionOrderReceiveId(transactionOrderService
						.getTransactionOrderReceiveByTransactionOrderId(
								transactionOrder.getId()).getId());
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			dao.addEntity(transactionExecuteReceive, session);
			for (ReceiveItemBin receiveItemBin : receiveItemBins) {
				receiveItemBin
						.setTransactionExecuteReceiveId(transactionExecuteReceive
								.getId());
				receiveItemBinService
						.addReceiveItemBin(receiveItemBin, session);
			}
			transactionOrder.setExecuted(1);
			transactionOrderService.updateTransactionOrder(transactionOrder,
					session);

			session.commitTransaction();
		} catch (DatabaseException e) {
			session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	public void submitExecution(
			List<TransactionExecuteIssue> teiS,
			List<IssueItemBin> iibS,
			Integer transactionOrderid , List<CurrentBinItem> cbiS) throws BusinessException {
		
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();

			
			System.out.println(teiS.size());
			System.out.println(iibS.size());
				int i =0 ;
				for (TransactionExecuteIssue transactionExecuteIssue : teiS ){
				
					dao.addEntity(transactionExecuteIssue, session);
					iibS.get(i).setTransactionExecuteIssueId(transactionExecuteIssue.getId());
					i++;
					
				}
				for (IssueItemBin issueItemBin :iibS ){
					dao.addEntity(issueItemBin, session);
					
				}
				for (CurrentBinItem cbi : cbiS){
					masterService.deleteCurrentBinItem(cbi,session); 
				}
			
			TransactionOrder transactionOrder = dao.getEntityById(transactionOrderid, TransactionOrder.class, "id");
			transactionOrder.setExecuted(1);
			transactionOrderService.updateTransactionOrder(transactionOrder,session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	
}