package com.robovics.wims.model.enums;

public enum QualityStatusEnum {

	Released(1), Quantiled(2), Blocked(3);

	public final int id;
	
	private QualityStatusEnum(int id) {
		this.id = id;
	}
}