package com.robovics.wims.ui.dialog;

import java.util.ArrayList;
import java.util.List;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.OpenDialogFailedException;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.masterData.MasterDataService.CurrentBinsFilterationParameter;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class GroupBinSelectionDialog extends Window implements ClickListener {

	private static final long serialVersionUID = -1423228382735681143L;
	private Callback callback;
	private Button selectBtn = new Button("Select", this);
	private Button cancelBtn = new Button("Cancel", this);
	private VerticalLayout container = new VerticalLayout();
	private GroupBin selectGroupBin;
	private List<GroupBin> groupBins;

	public GroupBinSelectionDialog(String caption, Callback callback,
			CurrentBinsFilterationParameter parameterKey, Object parameterValue)
			throws OpenDialogFailedException {
		super(caption);

		MasterDataService masterDataService = (MasterDataService) ServiceFactory
				.getInstance().getService(ServicesEnum.MASTER_DATA_SERVICE);
		try {
			groupBins = masterDataService.getCurrentBins(parameterKey,
					(Integer) parameterValue);
			
		} catch (BusinessException e) {
			throw new OpenDialogFailedException(e.getMessage());
		}
		final List<String> groupBinNames = new ArrayList<String>();
		for (GroupBin groupBin : groupBins)
			groupBinNames.add(groupBin.getSymbol());

		setModal(true);

		this.callback = callback;

		final ComboBox selectGroupBinCombo = new ComboBox(caption,
				groupBinNames);
		selectGroupBinCombo.select(groupBinNames.get(0));
		selectGroupBin = groupBins.get(0);

		selectGroupBinCombo.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1530363925162554859L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (selectGroupBinCombo.getValue() == null
						|| ((String) selectGroupBinCombo.getValue()).isEmpty()) {
					selectGroupBin = null;
					selectBtn.setEnabled(false);
				} else {
					selectGroupBin = groupBins.get(groupBinNames
							.indexOf(((String) selectGroupBinCombo.getValue())));
					selectBtn.setEnabled(true);
				}
			}
		});

		container.addComponent(selectGroupBinCombo);

		HorizontalLayout layout = new HorizontalLayout();
		layout.addComponent(selectBtn);
		layout.addComponent(cancelBtn);

		layout.setMargin(true);
		container.addComponent(layout);

		container.setMargin(true);

		setContent(container);
	}

	public void buttonClick(ClickEvent event) {
		if (getParent() != null)
			((UI) getParent()).removeWindow(this);
		callback.onDialogResult(event.getSource() == selectBtn, selectGroupBin);
	}

	public interface Callback {

		public void onDialogResult(boolean resultIsYes,
				GroupBin selectedGroupBin);
	}
}