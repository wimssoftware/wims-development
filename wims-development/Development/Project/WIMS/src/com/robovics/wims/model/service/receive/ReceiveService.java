package com.robovics.wims.model.service.receive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.contact.Supplier;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.orm.entities.transaction.orderItem.UnitType;
import com.robovics.wims.model.service.transaction.TransactionEntity;

@Service(value = "receiveService")
public class ReceiveService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	public List<StockKeepingUnit> getAllStockKeepingUnits() {
		return dao.getAllEntities(StockKeepingUnit.class);
	}

	public List<Owner> getAllOwners() {
		return dao.getAllEntities(Owner.class);
	}

	public List<Supplier> getAllSuppliers() {
		return dao.getAllEntities(Supplier.class);
	}

	public List<UnitType> getAllUnitTypes() {
		return dao.getAllEntities(UnitType.class);
	}

	public void addTransactionOrder(TransactionOrder transactionOrder,
			ArrayList<TransactionOrderItem> items,
			HashSet<TransactionOrderReceive> tors) throws BusinessException {
		try {
			dao.addEntity(transactionOrder);
			Integer transactionOrderId = transactionOrder.getId();
			for (TransactionOrderItem item : items) {
				try {
					item.setTransactionOrderId(transactionOrderId);
					dao.addEntity(item);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			for (TransactionOrderReceive tor : tors) {
				try {
					tor.setTransactionOrderId(transactionOrderId);
					dao.addEntity(tor);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}
public List<TransactionOrderReceive> getAllChidOrders(int todId){
		
		List<TransactionOrderReceive> toReceive = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_ID", todId);
			List<Integer> Ids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_ALL_RECEIVE_ORDERS.queryName,
					parameters);
			
			for (Integer id : Ids){
				toReceive.add(dao.getEntityById(id, TransactionOrderReceive.class, "id"));
			}
			
						
		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return toReceive ;
	}
	
	public void addNewOrder (TransactionEntity trE){
		TransactionOrder tod = new TransactionOrder(); 
		tod.setExecuted(0);
		tod.setSiteAdminId(trE.getSiteAdminId());
		System.out.println(trE.getItemsQuantity());
		tod.setItemsQuantity(trE.getItemsQuantity());
		System.out.println(trE.getOrderDate().toString());
		tod.setOrderDate(trE.getOrderDate());
		tod.setTransactionTime(trE.getTransactionTime());
		tod.setTransactionTypeId(trE.getTransactionTypeId());
		try {
			dao.addEntity(tod);
			trE.setTodId(tod.getId());
			
			System.out.println("Susccess2");
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fail2");
		}
		if(tod.getTransactionTypeId()==2){
			addReceiveOrder(trE);
			System.out.println("Susccess3");
		}
	}

	public void addReceiveOrder (TransactionEntity trE) {
		
			for (int i=0 ; i<trE.getItemsQuantity();i++) {
				try {
					TransactionOrderItem item = new TransactionOrderItem();
					TransactionOrderReceive tor = new TransactionOrderReceive();
					
					item.setTransactionOrderId(trE.getTodId());
					item.setIsContained(trE.getIsContained());
					item.setIsContaining(trE.getIsContaining());
					item.setStockKeepingUnitId(trE.getStockKeepingUnitId());
					item.setUnitTypeId(trE.getUnitTypeId());
					dao.addEntity(item);
					tor.setTransactionOrderId(trE.getTodId());
					tor.setOwnerId(trE.getOwnerId());
					if (trE.getSupplierId()!=null){
						tor.setSupplierId(trE.getSupplierId());
					}
					dao.addEntity(tor);
					
				} catch (DatabaseException e) {
					e.printStackTrace();
					try {
						throw new BusinessException("Internal Database Error");
					} catch (BusinessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}	
	}
}