package com.robovics.wims.model.orm;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DAOFactory {

	public AnnotationConfigApplicationContext context;

	public static DAOFactory instance;

	private DAOFactory() {
		context = new AnnotationConfigApplicationContext();
		context.register(DataAccess.class);
		context.refresh();
	}

	public static  DAOFactory getInstance() {
		if (instance == null)
			instance = new DAOFactory();
		return instance;
	}

	public static void  refresh(){
		
	}
	public DataAccess getDataAccess() {
		return (DataAccess) context.getBean("dao");
	}
}