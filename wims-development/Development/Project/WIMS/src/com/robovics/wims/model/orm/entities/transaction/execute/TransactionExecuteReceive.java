package com.robovics.wims.model.orm.entities.transaction.execute;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.robovics.wims.model.orm.entities.BaseEntity;

@NamedQueries(value = {

		@NamedQuery(name = "getAllRecivedItemIds", query = " select o.transactionOrderId from TransactionExecuteReceive e , TransactionOrderReceive o "
				+ " where e.transactionOrderReceiveId = o.id ")
		})


@Table(name = "Transaction_Execute_Receive")
@Entity
public class TransactionExecuteReceive extends BaseEntity {

	private static final long serialVersionUID = -6946850686289911232L;
	private Integer id;
	private Integer siteLaborId;
	private Integer transactionOrderReceiveId;

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "TER_Id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TER_SLB_Id")
	public Integer getSiteLaborId() {
		return siteLaborId;
	}

	public void setSiteLaborId(Integer siteLaborId) {
		this.siteLaborId = siteLaborId;
	}

	@Column(name = "TER_TOR_Id")
	public Integer getTransactionOrderReceiveId() {
		return transactionOrderReceiveId;
	}

	public void setTransactionOrderReceiveId(Integer transactionOrderReceiveId) {
		this.transactionOrderReceiveId = transactionOrderReceiveId;
	}
}