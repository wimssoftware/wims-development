package com.robovics.wims.model.service.masterData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.List;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.masterData.BinSlot;
import com.robovics.wims.model.orm.entities.masterData.BinSlotConfiguration;
import com.robovics.wims.model.orm.entities.masterData.CurrentBinItem;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.SerialRecored;
import com.robovics.wims.model.orm.entities.masterData.Site;
import com.robovics.wims.model.orm.entities.masterData.SiteConfiguration;
import com.robovics.wims.model.orm.entities.masterData.SiteConfigurationBinGroup;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteTransfer;
import com.robovics.wims.model.orm.entities.transaction.itemBin.IssueItemBin;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.itemBin.TransferItemBin;
import com.robovics.wims.model.orm.entities.transaction.orderItem.CartonInPalet;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.Palet_Slot_CartonCount_isnew;

@Service(value = "masterDataService")
public class MasterDataService {

	@Autowired
	private BinSlotService bSlotService;

	/*public class SiteView {
		private Integer id;
		private Integer siteId;
		private Date dateTime;
		private String name;
		private String address;
		private String active;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getSiteId() {
			return siteId;
		}

		public void setSiteId(Integer siteId) {
			this.siteId = siteId;
		}

		public Date getDateTime() {
			return dateTime;
		}

		public void setDateTime(Date dateTime) {
			this.dateTime = dateTime;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getActive() {
			return active;
		}

		public void setActive(int active) {
			if (active == 1) {
				this.active = "Active";
			} else if (active == 0) {
				this.active = "In Active";
			}
		}
	}
	*/
	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	@Deprecated
	private static final int CURRENT_SITE_ID = 1;

	@Deprecated
	public Site getCurrentSite() {
		return dao.getEntityById(CURRENT_SITE_ID, Site.class, "id");
	}

	public Site getSiteById(Integer id) {
		return dao.getEntityById(id, Site.class, "id");
	}

	public SiteConfigurationBinGroup getSiteConfigurationBinGroupById(Integer id) {
		return dao.getEntityById(id, SiteConfigurationBinGroup.class, "id");
	}

	public List<GroupBin> getGroupBins() {
		List<GroupBin> groupBins = dao.getAllEntities(GroupBin.class);
		for (GroupBin groupBin : groupBins)
			groupBin.setSymbol(getSiteConfigurationBinGroupById(
					groupBin.getSiteConfigurationBinGroupId()).getSymbol()
					+ groupBin.getBinNumber());
		return groupBins;
	}

	public List<GroupBin> getGroupBins(int siteNo, Date siteDate) {
		List<GroupBin> groupBins = new ArrayList<>();
		List<SiteConfiguration> sCnfigs = this.getAllSiteConfigurations(siteNo);
		for(SiteConfiguration s :sCnfigs){
			
		}
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteNo", siteNo);
			parameters.put("P_Date", siteDate);

			List<Integer> groupBinsIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_GROUBBINS_BY_SITE_ID.queryName,
					parameters);
			for (Integer Id : groupBinsIds) {
				groupBins.add(this.getGroupBinById(Id));
			}
			return groupBins;

		} catch (DatabaseException e) {
			e.printStackTrace();
			try {
				throw new BusinessException("Internal Database Error");
			} catch (BusinessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return groupBins;
	}

	public GroupBin getGroupBinById(Integer id) {
		GroupBin groupBin = dao.getEntityById(id, GroupBin.class, "id");
		groupBin.setSymbol(getSiteConfigurationBinGroupById(
				groupBin.getSiteConfigurationBinGroupId()).getSymbol()
				+ groupBin.getBinNumber());
		return groupBin;
	}

	/*
	 * Tariq Shatat -- Added a function to retrieve currentBinItem by ID .
	 */
	public CurrentBinItem getCurrentBinItem(int id) throws BusinessException {
		return dao.getEntityById(id, CurrentBinItem.class, "id");

	}

	/*
	 * -- Tariq Shatat -- Added a function to retrieve get All the items in all
	 * sites versus bin .
	 */

	public List<CurrentBinItem> getCurrentBinItemofAllSites() {
		List<CurrentBinItem> currentItems = dao
				.getAllEntities(CurrentBinItem.class);
		return currentItems;

	}

	public List<CurrentBinItem> getCurrentBinItemBySiteID(int siteId) {
		List<CurrentBinItem> currentItems = dao
				.getAllEntities(CurrentBinItem.class);
		List<CurrentBinItem> selectedItems = new ArrayList<>();// = new
																// List<CurrentBinItem>()
																// ;
		for (CurrentBinItem currentItem : currentItems) {
			if (currentItem.getSiteId() == siteId) {
				selectedItems.add(currentItem);
			}
		}

		return selectedItems;
	}

	public void addCurrentBinItem(CurrentBinItem currentBinItem,
			CustomSession... useSession) throws BusinessException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		CustomSession session = isOpenedSession ? useSession[0] : dao
				.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();

			dao.addEntity(currentBinItem, session);

			if (!isOpenedSession)
				session.commitTransaction();
			;
		} catch (Exception e) {
			if (!isOpenedSession)
				session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public void updateCurrentBinItem(CurrentBinItem currentBinItem,
			CustomSession... useSession) throws BusinessException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		CustomSession session = isOpenedSession ? useSession[0] : dao
				.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();

			dao.updateEntity(currentBinItem, session);

			if (!isOpenedSession)
				session.commitTransaction();
			;
		} catch (Exception e) {
			if (!isOpenedSession)
				session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public void deleteCurrentBinItem(CurrentBinItem currentBinItem,
			CustomSession... useSession) throws BusinessException {
		boolean isOpenedSession = false;
		if (useSession != null && useSession.length > 0)
			isOpenedSession = true;
		CustomSession session = isOpenedSession ? useSession[0] : dao
				.openSession();
		try {
			if (!isOpenedSession)
				session.beginTransaction();

			dao.deleteEntity(currentBinItem, session);

			if (!isOpenedSession)
				session.commitTransaction();
			;
		} catch (Exception e) {
			if (!isOpenedSession)
				session.rollbackTransaction();
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
			if (!isOpenedSession)
				session.close();
		}
	}

	public static enum CurrentBinsFilterationParameter {
		SKU(Integer.class), IsEmpty(boolean.class);

		private final Class<?> valueType;

		private CurrentBinsFilterationParameter(Class<?> type) {
			this.valueType = type;
		}
	}

	public List<GroupBin> getCurrentBins(
			CurrentBinsFilterationParameter parameter, Object value)
			throws BusinessException {
		if (parameter == CurrentBinsFilterationParameter.SKU)
			if (CurrentBinsFilterationParameter.SKU.valueType.isInstance(value))
				return getCurrentBinsBySKUId((Integer) value);

		return null;
	}

	public List<GroupBin> getCurrentBins(
			CurrentBinsFilterationParameter parameter, Object value,
			Integer siteId) throws BusinessException {
		if (parameter == CurrentBinsFilterationParameter.SKU) {
			if (CurrentBinsFilterationParameter.SKU.valueType.isInstance(value))
				return getCurrentBinsBySKUId((Integer) value);
		} else if (parameter == CurrentBinsFilterationParameter.IsEmpty) {
			if ((CurrentBinsFilterationParameter.SKU.valueType.isInstance(true))) {
				return this.getEmptyBinsBySite(siteId);
			}

		}
		return null;
	}

	public Date getLatestConfigurationDate(Integer siteId)
			throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteNo", siteId);
			List<Date> Ids = dao.executeNamedQuery(Date.class,
					QueryNamesEnum.GET_Latest_Configuration.queryName,
					parameters);
			return Ids.get(0);

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public List<String> getBinsOfSite(int siteId) throws BusinessException {

		List<Integer> Bins = this.getBinsIDsBySite(siteId);
		List<String> binNames = new ArrayList<>();
		GroupBin gB;
		SiteConfigurationBinGroup SGB;
		for (Integer binNo : Bins) {
			gB = dao.getEntityById(binNo, GroupBin.class, "id");
			SGB = dao.getEntityById(gB.getSiteConfigurationBinGroupId(),
					SiteConfigurationBinGroup.class, "id");
			binNames.add(SGB.getSymbol() + gB.getBinNumber());
		}
		return binNames;

	}

	public List<Integer> getBinsIDsBySite(Integer siteId)
			throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteNo", siteId);
			List<Integer> Ids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_ALL_SlOTS_OF_SITE.queryName, parameters);
			return Ids;

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public List<Long> getCountsPerBin(Integer siteId) throws BusinessException {
		List<Long> res = new ArrayList<>();
		try {

			Map<String, Object> parameters = new HashMap<String, Object>();
			List<Integer> idS = this.getBinsIDsBySite(siteId);

			parameters.put("P_SiteNo", siteId);
			for (Integer binId : idS) {
				parameters.put("P_GBN_ID", binId);

				List<Long> Ids = dao.executeNamedQuery(Long.class,
						QueryNamesEnum.GET_COUNT_PER_BIN.queryName, parameters);
				res.add(Ids.get(0));
			}
			return res;
			/*
			 * List<GroupBin> items = new ArrayList<GroupBin>(Ids.size()); for
			 * (Integer id :Ids) items.add(dao.getEntityById(id, GroupBin.class,
			 * "id"));
			 * 
			 * return items ;
			 */

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public List<GroupBin> getCurrentBinsBySKUId(Integer skuId)
			throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SKU_ID", skuId);
			List<Integer> binIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_SLOTS_SKU_ID.queryName, parameters);
			List<GroupBin> bins = new ArrayList<GroupBin>(binIds.size());
			for (Integer binId : binIds) {
				BinSlot slt = dao.getEntityById(binId, BinSlot.class, "id");
				bins.add(getGroupBinById(dao.getEntityById(
						slt.getBinSlotConfigurationId(),
						BinSlotConfiguration.class, "id").getGroupBinId()));
			}
			return bins;
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public List<GroupBin> getCurrentBinsBySKUId(Integer skuId, int siteNo)
			throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SKU_ID", skuId);
			List<Integer> sltIds = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_SLOTS_SKU_ID.queryName, parameters);
			List<GroupBin> bins = new ArrayList<GroupBin>(sltIds.size());
			List<Integer> binTest = new ArrayList<Integer>();
			for (Integer sltId : sltIds) {
				System.out.println("QId " + sltIds);
				BinSlot slt = dao.getEntityById(sltId, BinSlot.class, "id");
				BinSlotConfiguration bsc = dao.getEntityById(slt.getBinSlotConfigurationId()
						, BinSlotConfiguration.class, "id");
				GroupBin b = getGroupBinById(bsc.getGroupBinId());
						

				if (!binTest.contains(b.getId())) {
					bins.add(b);
					binTest.add(b.getId());
				}
			}
			System.out.println("bbId " + bins.size());
			List<GroupBin> binsNew = new ArrayList<GroupBin>(sltIds.size());
			for (GroupBin bin : bins) {
				SiteConfigurationBinGroup scbg = dao.getEntityById(
						bin.getSiteConfigurationBinGroupId(),
						SiteConfigurationBinGroup.class, "id");
				int Siteid = dao.getEntityById(scbg.getSiteConfigurationId(),
						SiteConfiguration.class, "id").getSiteId();
				if (Siteid == siteNo) {
					binsNew.add(bin);
				}
			}
			System.out.println("baId " + bins.size());
			return bins;
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public List<Site> getAllSites() {
		return dao.getAllEntities(Site.class);
	}
	
	public void deleteSite(Site  SteId) throws DatabaseException, BusinessException
	{
		
		CustomSession session = dao.openSession(); 
		try {
		session.beginTransaction();
		
		
	
		List <SiteConfiguration> cfgs = this.getAllSiteConfigurations(SteId.getId());
		
		for (SiteConfiguration cfg : cfgs){
			List <SiteConfigurationBinGroup> cbgs = this.getAllSiteConfigurationBinOfSiteConfiguration(cfg.getId());
			for  (SiteConfigurationBinGroup cbg : cbgs ){
					List<GroupBin> gbns = this.getAllBinGroup(cbg.getId());
					for  (GroupBin gbn : gbns ){
						List<BinSlotConfiguration> bscs = this.getAllBinsSlotsConfInSite(gbn.getId());
						for(BinSlotConfiguration bsc : bscs){
							List<BinSlot> slts = this.bSlotService.getAllSlotS(bsc.getId());
							
							dao.deleteEntities(slts);
							
							dao.deleteEntity(bsc, session);
						}
						dao.deleteEntity(gbn, session);
					}
					dao.deleteEntity(cbg, session);	
			}
			
			dao.deleteEntity(cfg, session);
		}
		dao.deleteEntity(SteId,session);
		
		
		session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	
	public void deleteBin(GroupBin grpId) throws DatabaseException, BusinessException
	{
		CustomSession session = dao.openSession(); 
		try {
		session.beginTransaction();
				
			List<BinSlotConfiguration> bscs = this.getAllBinsSlotsConfInSite(grpId.getId());
					for(BinSlotConfiguration bsc : bscs){
						List<BinSlot> slts = this.bSlotService.getAllSlotS(bsc.getId());
						
					  dao.deleteEntities(slts);
												
				    	dao.deleteEntity(bsc, session);
					}
					dao.deleteEntity(grpId, session);
					
		session.commitTransaction();
	} catch (DatabaseException e) {
		e.printStackTrace();
		session.rollbackTransaction();
		throw new BusinessException("Internal Database Error");
	} finally {
		session.close();
	}
	
	}
	public List<GroupBin> getAllBinGroup (int id)
	{
		List<GroupBin> gbn_ = new ArrayList<>();
		List<GroupBin> gbns = dao.getAllEntities(GroupBin.class);
		for (GroupBin gbn : gbns)
		{
			if(gbn.getBinNumber() == id)
				gbn_.add(gbn);
		}
		return gbn_;
	}
	
	public List<SiteConfigurationBinGroup> getAllSiteConfigurationBinOfSiteConfiguration(int id ){
		
		List<SiteConfigurationBinGroup> cbg_ = new ArrayList<>();
		List<SiteConfigurationBinGroup> cbgs = dao.getAllEntities(SiteConfigurationBinGroup.class); 
		for (SiteConfigurationBinGroup cbg : cbgs){
			
			if (cbg.getSiteConfigurationId() == id )
				cbg_.add(cbg);
		}
		
		return cbg_;
	}
		
		
		
		/*
		Integer ID  = Ste.getId() ;
		List<SiteConfiguration> siteConfList = dao
				.getAllEntities(SiteConfiguration.class);
		
		List<SiteConfigurationBinGroup> siteConfBingrp = dao
				.getAllEntities(SiteConfigurationBinGroup.class);
		
		List<BinSlotConfiguration> bisSlotConf = dao
				.getAllEntities(BinSlotConfiguration.class);
		
		for (SiteConfiguration siteConf : siteConfList) {
			if(siteConf.getSiteId() == ID)
			{
			int SitecofigrationID =	dao.getEntityById(siteConf.getSiteId(), SiteConfiguration.class, "id").getId();
				for (SiteConfigurationBinGroup siteConfBins : siteConfBingrp)
				{
					if (siteConfBins.getSiteConfigurationId() == SitecofigrationID) {
						for (GroupBin GB : dao.getAllEntities(GroupBin.class)) {
							if (siteConfBins.getId() == GB.getSiteConfigurationBinGroupId()) {
								
								int bisSlotCfgID  = dao.getEntityById(siteConfBins.getId() , BinSlotConfiguration.class, "id").getId();
								for (BinSlotConfiguration BSC : bisSlotConf )
								{
									if (BSC.getGroupBinId() == bisSlotCfgID)
										dao.deleteEntity(BSC);
									
								}
								
								GB.setSymbol(siteConfBins.getSymbol() + ""
										+ GB.getBinNumber());
								dao.deleteEntity(GB);
	
							}
						}
						dao.deleteEntity(siteConfBins);
					}
				}
					
				dao.deleteEntity(siteConf);
				Site S = dao.getEntityById(ID , Site.class, "id" );
				dao.deleteEntity(S);
			}
		
	}*/
		
		
/*
	public List<Site> getAllSitesConfiguration() {
		ArrayList<Site> siteViewList = new ArrayList<>();
		List<SiteConfiguration> siteConfList = dao
				.getAllEntities(SiteConfiguration.class);
		for (SiteConfiguration siteConf : siteConfList) {
			int siteId = siteConf.getSiteId();
			Site site = dao.getEntityById(siteId, Site.class, "id");
			Site siteView = new Site();
			siteView.setId(siteConf.getId());
			//siteView.setSiteId(siteId);
			siteView.setName(site.getName());
			siteView.setAddress(site.getAddress());
			//siteView.setDateTime(siteConf.getDateTime());
			siteView.setActive(siteConf.getActive());
			siteViewList.add(siteView);
		}
		return siteViewList;
		
		
	}
*/
	
	public ArrayList<SiteConfiguration> getAllSiteConfigurations(Integer SteId) {
		ArrayList<SiteConfiguration> siteConfig = new ArrayList<>();
		List<SiteConfiguration> siteConfList = dao
				.getAllEntities(SiteConfiguration.class);
		for (SiteConfiguration siteConf : siteConfList) {
			if (siteConf.getSiteId() == SteId)
			{
			//SiteConfiguration CFG = dao.getEntityById(SteId, SiteConfiguration.class, "id");
			siteConfig.add(siteConf);
			}
		}
		return siteConfig;
	}
	
	
	public ArrayList<GroupBin> getAllBinsInSiteConfiguration(int siteCfgId) {
		ArrayList<GroupBin> binViewList = new ArrayList<>();
		DAOFactory.getInstance().instance=null ;
		DAOFactory.instance = null ;
		dao = DAOFactory.getInstance().getDataAccess();
		List<SiteConfigurationBinGroup> siteConfList = dao
				.getAllEntities(SiteConfigurationBinGroup.class);
		for (SiteConfigurationBinGroup siteConf : siteConfList) {
			if (siteConf.getSiteConfigurationId() == siteCfgId) {
				for (GroupBin GB : dao.getAllEntities(GroupBin.class)) {
					if (siteConf.getId() == GB.getSiteConfigurationBinGroupId()) {
						GB.setSymbol(siteConf.getSymbol() + ""
								+ GB.getBinNumber());
						binViewList.add(GB);
					}
				}
			}
		}
		return binViewList;
	}

	public ArrayList<SiteConfigurationBinGroup> getAllBinsInSiteByAdminSiteId(
			int adminSiteId) {
		ArrayList<SiteConfigurationBinGroup> binViewList = new ArrayList<>();
		List<SiteConfigurationBinGroup> siteConfList = dao
				.getAllEntities(SiteConfigurationBinGroup.class);
		List<SiteConfiguration> ConfList = dao
				.getAllEntities(SiteConfiguration.class);
		for (SiteConfiguration confList : ConfList) {
			if (confList.getActive() == 1
					&& confList.getSiteId() == adminSiteId) {
				for (SiteConfigurationBinGroup siteconfList : siteConfList) {
					if (siteconfList.getSiteConfigurationId() == confList
							.getSiteId()) {
						binViewList.add(siteconfList);
					}
				}
			}
		}
		return binViewList;
	}

	public ArrayList<BinSlotConfiguration> getAllBinsSlotsConfInSite(
			int binCfgId) {
		ArrayList<BinSlotConfiguration> binSlotCnfgList = new ArrayList<>();
		List<BinSlotConfiguration> BinSlotConfigurationList = dao
				.getAllEntities(BinSlotConfiguration.class);
		for (BinSlotConfiguration binSlotCnfg : BinSlotConfigurationList) {
			if (binCfgId == binSlotCnfg.getGroupBinId()) {
				binSlotCnfgList.add(binSlotCnfg);
			}
		}
		return binSlotCnfgList;
	}

	private ArrayList<Integer> notExistInTransferTable(int id,
			Date executionTime, int binSlotCngfId) {
		ArrayList<Integer> res = new ArrayList<>();
		ArrayList<Date> dates = new ArrayList<>();
		List<TransferItemBin> transferItemBinList = dao
				.getAllEntities(TransferItemBin.class);
		if (transferItemBinList.size() == 0 || transferItemBinList.isEmpty()) {
			res.add(1);
			return res;
		}
		dates.add(executionTime);
		for (TransferItemBin transferItemBin : transferItemBinList) {
			if (transferItemBin.getFormslotId() == id) {
				Date execDate = transferItemBin.getExecutionTime();
				dates.add(execDate);
			}
		}
		int maxDatepos = dates.indexOf(Collections.max(dates));
		if (maxDatepos == 0) {
			res.add(1);
		} else {
			res.add(0);
			BinSlot slot = dao.getEntityById(
					transferItemBinList.get(maxDatepos - 1).getToslotId(),
					BinSlot.class, "id");
			if (notExistInIsssueTable(slot.getId(), dates.get(maxDatepos))) {
				int binSlotCngf = slot.getBinSlotConfigurationId();
				if (binSlotCngf == binSlotCngfId) {
					res.add(1);
					res.add(slot.getY());
					res.add(slot.getZ());
					res.add(slot.getId());
				}
			} else {
				res.add(-1);
			}
		}
		return res;
	}

	private Boolean notExistInIsssueTable(int id, Date executionTime) {
		ArrayList<Date> dates = new ArrayList<>();
		List<IssueItemBin> issueItembinList = dao
				.getAllEntities(IssueItemBin.class);
		if (issueItembinList.size() == 0 || issueItembinList.isEmpty()) {
			return true;
		}
		dates.add(executionTime);
		for (IssueItemBin issueItembin : issueItembinList) {
			if (issueItembin.getSlotId() == id) {
				Date execDate = issueItembin.getExecutionTime();
				dates.add(execDate);
			}
		}
		int maxDatepos = dates.indexOf(Collections.max(dates));
		if (maxDatepos == 0) {
			return true;
		}
		return false;
	}

	public int getCurrentBinConf(int binCfgId) {
		ArrayList<Date> dateCnf = new ArrayList<>();
		ArrayList<BinSlotConfiguration> binSlotCnfgList = new ArrayList<>();
		List<GroupBin> groupBinList = dao.getAllEntities(GroupBin.class);
		List<BinSlotConfiguration> BinSlotConfigurationList = dao
				.getAllEntities(BinSlotConfiguration.class);
		for (GroupBin groupBin : groupBinList) {
			if (groupBin.getSiteConfigurationBinGroupId() == binCfgId) {
				for (BinSlotConfiguration binSlotCnfg : BinSlotConfigurationList) {
					if (groupBin.getId() == binSlotCnfg.getGroupBinId()) {
						binSlotCnfgList.add(binSlotCnfg);
						dateCnf.add(binSlotCnfg.getDateTime());
					}
				}
			}
		}
		if (dateCnf.size() != 0) {
			int indmaxDate = dateCnf.indexOf(Collections.max(dateCnf));
			int id = binSlotCnfgList.get(indmaxDate).getId();
			return id;
		}
		return -1;
	}

	public List<BINSCOUNT> getALLBINSCountOfSite(int siteId)
			throws BusinessException, DatabaseException {
		List<BINSCOUNT> binsCountAll = new ArrayList<>();
		List<Integer> Bins = this.getBinsIDsBySite(siteId);
		GroupBin gB;
		SiteConfigurationBinGroup SGB;
		String Symbol;
		int id;
		Long count = (long) 0;
		for (Integer binNo : Bins) {
			gB = dao.getEntityById(binNo, GroupBin.class, "id");
			SGB = dao.getEntityById(gB.getSiteConfigurationBinGroupId(),
					SiteConfigurationBinGroup.class, "id");
			id = binNo;
			Symbol = SGB.getSymbol() + gB.getBinNumber();

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteNo", siteId);
			parameters.put("P_GBN_ID", binNo);
			List<Long> Ids = dao.executeNamedQuery(Long.class,
					QueryNamesEnum.GET_COUNT_PER_BIN.queryName, parameters);
			count = Ids.get(0);
			binsCountAll.add(new BINSCOUNT(id, Symbol, count));
		}

		return binsCountAll;
	}

	public List<GroupBin> getLatestBinsBySite(Integer siteId)
			throws BusinessException {
		List<GroupBin> GbnS = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteNo", siteId);
			List<Integer> Ids = dao
					.executeNamedQuery(
							Integer.class,
							QueryNamesEnum.GET_Latest_BINS_OF_SITE_EMPTY_non_EMPTY.queryName,
							parameters);
			for (Integer id : Ids)
				GbnS.add(getGroupBinById(id));
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
		return GbnS;
	}

	public List<GroupBin> getEmptyBinsBySite(Integer siteId)
			throws BusinessException {
		List<GroupBin> GbnS = new ArrayList<>();
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteNo", siteId);
			List<Integer> Ids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_EMPTY_BINS.queryName, parameters);
			for (Integer id : Ids)
				GbnS.add(getGroupBinById(id));
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
		return GbnS;
	}

	public List<String> getAllSerialsOfBin(int binId) throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_GBN_ID", binId);
			List<String> Itemserials = dao.executeNamedQuery(String.class,
					QueryNamesEnum.GET_ALL_SERIALS_OF_SLOT.queryName,
					parameters);

			return Itemserials;
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	public StockKeepingUnit getSKUBySerial(String itemSerial) {

		TransactionOrderItem sItem = new TransactionOrderItem();
		ReceiveItemBin sItemBi = new ReceiveItemBin();
		List<ReceiveItemBin> allreceived = dao
				.getAllEntities(ReceiveItemBin.class);

		for (ReceiveItemBin item : allreceived) {

			if (itemSerial.equals(item.getSerial())) {
				sItemBi = item;
				break;
			}
		}
		sItem = dao.getEntityById(sItemBi.getTransactionOrderItemId(),
				TransactionOrderItem.class, "id");
		StockKeepingUnit sSku = dao.getEntityById(
				sItem.getStockKeepingUnitId(), StockKeepingUnit.class, "id");
		return sSku;
	}

	public void addCurrentBinItem(CurrentBinItem currentBinItem)
			throws BusinessException {
		try {

			dao.addEntity(currentBinItem);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		} finally {
		}
	}

	public void updateCurrentBinItem(CurrentBinItem currentBinItem)
			throws BusinessException {

		try {

			dao.updateEntity(currentBinItem);

		} catch (Exception e) {
			throw new BusinessException("Internal Database Error");

		}
	}

	public void deleteCurrentBinItem(CurrentBinItem currentBinItem)
			throws BusinessException {

		try {

			dao.deleteEntity(currentBinItem);

		} catch (Exception e) {
			throw new BusinessException("Internal Database Error");
		}
	}

	public void add_site(Site site, int active) throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			Site addedSite = new Site();
			addedSite.setName(site.getName());
			addedSite.setAddress(site.getAddress());
			addedSite.setActive(site.getActive());
			dao.addEntity(addedSite, session);
			//site.setSiteId(addedSite.getId());
			SiteConfiguration addedSiteConfiguration = new SiteConfiguration();
			addedSiteConfiguration.setDateTime(new Date());
			addedSiteConfiguration.setActive(active);
			addedSiteConfiguration.setSiteId(addedSite.getId());
			addedSiteConfiguration.setSelected(false);
			dao.addEntity(addedSiteConfiguration, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}

	public void addBinConfig(BinSlotConfiguration binSlotCnfg)
			throws BusinessException {

		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.addEntity(binSlotCnfg, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}

	public CurrentBinItem getCBIBySlotId(int slotId) throws BusinessException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SOT_ID", slotId);
			List<Integer> slotids = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_CBI_BY_SLOT_ID.queryName, parameters);

			return this.getCurrentBinItem(slotids.get(0));
		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}

	// kamel
	public String getBinSymbolFromSlotId(int slotId) {
		int slotBinCnfg = dao.getEntityById(slotId, BinSlot.class, "id")
				.getBinSlotConfigurationId();
		int GB_ID = dao.getEntityById(slotBinCnfg, BinSlotConfiguration.class,
				"id").getGroupBinId();
		
		return this.getGroupBinById(GB_ID).getSymbol();
		/*
		int SCBG = dao.getEntityById(GB_ID, GroupBin.class, "id")
				.getSiteConfigurationBinGroupId();
		SiteConfigurationBinGroup SCBG_Obj = dao.getEntityById(SCBG,
				SiteConfigurationBinGroup.class, "id");
		return SCBG_Obj.getSymbol() + " " + SCBG_Obj.getSiteConfigurationId();*/
		
	}

	// // kamel
	public void add_sitebin(SiteConfigurationBinGroup sCBG, GroupBin gB)
			throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			List<GroupBin> GBNs = this.getAllBinsInSiteConfiguration(sCBG.getSiteConfigurationId());
			for(GroupBin gbn : GBNs){
				if(gbn.getSymbol().equals(sCBG.getSymbol()+gB.getBinNumber())){
					throw (new Exception());
				}
			}
			int id = -1;
			for (SiteConfigurationBinGroup scbg : dao
					.getAllEntities(SiteConfigurationBinGroup.class)) {
				if (scbg.getSymbol().equals(sCBG.getSymbol()) && scbg.getSiteConfigurationId() == sCBG.getSiteConfigurationId()) {
					id = scbg.getId();
					gB.setSiteConfigurationBinGroupId(scbg.getId());
				}
			}
			if (id == -1) {
				add_SiteConfigurationBinGroup(sCBG);
				gB.setSiteConfigurationBinGroupId(sCBG.getId());
				dao.addEntity(gB, session);
				session.commitTransaction();
			}else{			
				System.out.println("id: "+id);
				gB.setSiteConfigurationBinGroupId(id);
				dao.addEntity(gB, session);
				session.commitTransaction();
			}
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		}catch (Exception e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("This bin already exists");
		} 
		finally {
			session.close();
			dao = DAOFactory.getInstance().getDataAccess(); 
		}
	}
	
	public void updateSite(Site steV) throws BusinessException {
		
		Site ste = new Site();
		ste.setId(steV.getId());
		ste.setAddress(steV.getAddress());
		ste.setName(steV.getName());
		ste.setActive(steV.getActive());
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.updateEntity(ste);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	
public void updateBin(GroupBin oldBin,GroupBin newBin,SiteConfiguration sConfig) 
		throws BusinessException { 
	//1- check CBG of new bin already exist or not
	SiteConfigurationBinGroup temp = get_CBG(sConfig, newBin.getSymbol());
	//  new CBG
	if(temp == null){
		System.out.println("new CBG");
		CustomSession session1 = dao.openSession();
		try{
			session1.beginTransaction();
			SiteConfigurationBinGroup newSCBG = new SiteConfigurationBinGroup();
			System.out.println("newCBG SCid:"+sConfig.getId());
			System.out.println("newCBG SCid:"+newBin.getSymbol());
			newSCBG.setSiteConfigurationId(sConfig.getId());
			newSCBG.setSymbol(newBin.getSymbol());

			add_SiteConfigurationBinGroup(newSCBG);
			session1.commitTransaction();
			newBin.setSiteConfigurationBinGroupId(newSCBG.getId());
			newBin.setId(oldBin.getId());
			dao.updateEntity(newBin);
			session1.commitTransaction();
		}catch(DatabaseException e) {
			e.printStackTrace();
			session1.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		}finally {
			session1.close();
		}
	}// it's already there
	else{
		System.out.println("CBG already Exists");
		CustomSession session2 = dao.openSession();
		try{
			session2.beginTransaction();
			List<GroupBin> GBNs = this.getAllBinsInSiteConfiguration(temp.getSiteConfigurationId());
			System.out.println("Bins:");
			for(GroupBin gbn : GBNs){
				System.out.println("ID: "+newBin.getSymbol()+", "+gbn.getSymbol());
				if(gbn.getSymbol().equals(newBin.getSymbol()+newBin.getBinNumber())){
					throw (new Exception());
				}
			}
			newBin.setSiteConfigurationBinGroupId(temp.getId());
			newBin.setId(oldBin.getId());
			System.out.println("Bin ID: "+newBin.getId());
			System.out.println("Bin Number: "+newBin.getBinNumber());
			dao.updateEntity(newBin);
		} catch (DatabaseException e) {
			e.printStackTrace();
			session2.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		}catch (Exception e) {
			e.printStackTrace();
			session2.rollbackTransaction();
			throw new BusinessException("This bin already exists");
		} 
		finally {
			session2.close();
		}
	}
	
	//2- check if there any bins arrow to old CBG 
	System.out.println("CBG id: "+oldBin.getSiteConfigurationBinGroupId());
	List<GroupBin> GBNs = this.getAllBinsInSiteConfiguration(oldBin.getSiteConfigurationBinGroupId());
	System.out.println("Bins count: "+GBNs.size());
	System.out.println("Frist Bin: "+GBNs.get(0).getSymbol());
	
}
	
public void add_SiteConfigurationBinGroup(SiteConfigurationBinGroup sCBG)
	throws BusinessException {
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.addEntity(sCBG, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		}finally {
			session.close();
		}
}

public SiteConfigurationBinGroup get_CBG(SiteConfiguration sConfig,String Symbol){
	try {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_SiteConfigId", sConfig.getId());
		parameters.put("P_symbol", Symbol);

		List<Integer> sCBGIds = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_SCBG_GET_SCBG.queryName,
				parameters);
		
		return this.getSiteConfigurationBinGroupById(sCBGIds.get(0));
		
	}catch(Exception e){
		return null;
	}
}

	String HajarCanReturnSiteName(int sltId){
		// Return The slot record where id = sltId
		/*BinSlot slt = dao.getEntityById(sltId, BinSlot.class, "id");
		int bsc_Id = slt.getBinSlotConfigurationId(); 
		
		BinSlotConfiguration bsc = dao.getEntityById(bsc_Id, BinSlotConfiguration.class,"id" );
		int gbn_num =bsc.getGroupBinId();
		
		GroupBin gbn = dao.getEntityById(gbn_num, GroupBin.class, "id");
		int cbg_Id = gbn.getSiteConfigurationBinGroupId();
		
		SiteConfigurationBinGroup cbg = dao.getEntityById(cbg_Id, SiteConfigurationBinGroup.class, "id");
		int cfg_Id = cbg.getSiteConfigurationId();
		
		SiteConfiguration cfg = dao.getEntityById(cfg_Id, SiteConfiguration.class, "id");
		int ste_Id = cfg.getSiteId();
		*/
		BinSlot slt = dao.getEntityById(sltId, BinSlot.class, "id");		
		BinSlotConfiguration bsc = dao.getEntityById(
				slt.getBinSlotConfigurationId(), BinSlotConfiguration.class,"id" );
		GroupBin gbn = dao.getEntityById(bsc.getGroupBinId()
				, GroupBin.class, "id");
		SiteConfigurationBinGroup cbg = dao.getEntityById(gbn.getSiteConfigurationBinGroupId()
				, SiteConfigurationBinGroup.class, "id");
		SiteConfiguration cfg = dao.getEntityById(cbg.getSiteConfigurationId()
				, SiteConfiguration.class, "id");
		return dao.getEntityById(cfg.getSiteId(), Site.class, "id").getName();
	}
	
	public Integer GetLastSiteConfigIdForSite(Integer SiteId){
		List<SiteConfiguration>	SiteConfigrations = this.getAllSiteConfigurations(SiteId);
		SiteConfiguration lastOne = SiteConfigrations.get(0);
		for(SiteConfiguration s : SiteConfigrations){
			if(s.getDateTime().after(lastOne.getDateTime())){
				lastOne = s;
			}
		}
		return lastOne.getId();
	}
	
	public List<Integer> GetAllbinGroupsInSiteConfig(Integer siteConfigId){//return siteConfigrationbingroup id
		Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_SiteConfigId", siteConfigId);
		
		try{
			List<Integer> binGroupList = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_ALL_BIN_GROUPS_IN_SITE_CONFIG.queryName,
				parameters);
			return binGroupList;
		}catch(Exception e){
			return null;
		}
	}
	
	public List<Integer> GetAllBinsInBinGroup(Integer BinGroupConfigrationId){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_BinGroupConfigrationID", BinGroupConfigrationId);
	
		try{
			List<Integer> binsList = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_ALL_BINS_IN_BIN_GROUP_CONFIG.queryName,
				parameters);
			return binsList;
		}catch(Exception e){
			return null;
		}
	}
	
	public List<Integer> GetAllBinsInSiteConfig(Integer siteConfigId){
		List<Integer> allBins = new ArrayList<Integer>();
		
		List<Integer> binGroupIdList = GetAllbinGroupsInSiteConfig(siteConfigId);
		for(Integer binGroupId :binGroupIdList){
			List<Integer> binsInCurrentGroup = GetAllBinsInBinGroup(binGroupId);
			allBins.addAll(binsInCurrentGroup);
		}
		return allBins;
	}
	
	public List<BinSlotConfiguration> GetBinSlotConfigIdsForBin (Integer BinID)
		throws DatabaseException{
		List<BinSlotConfiguration> BinSlotConfigList = new ArrayList<BinSlotConfiguration>();
		try{
			List<BinSlotConfiguration> AllSlotConfigs = dao.getAllEntities(BinSlotConfiguration.class);
			System.out.println("sfd " + AllSlotConfigs.size());
			for(BinSlotConfiguration s :AllSlotConfigs ){
				System.out.println("---- "+BinID +" "+ s.getGroupBinId());
				if(s.getGroupBinId().equals(BinID)){
					
					BinSlotConfigList.add(s);
				}
			}
			System.out.println("sfdsd " + BinSlotConfigList.size());
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}
		return BinSlotConfigList;
	}
	
	public Integer GetLatestBinSlotConfig(Integer binId)
			throws DatabaseException{
		try{
			List<BinSlotConfiguration> BinSlotConfigList = GetBinSlotConfigIdsForBin(binId);
			if(BinSlotConfigList.size() <= 0){
				return -1;
			}
			BinSlotConfiguration LatestBinSlotConfig = BinSlotConfigList.get(0);
			for (BinSlotConfiguration s:BinSlotConfigList){
				if(s.getDateTime().after(LatestBinSlotConfig.getDateTime())){
					LatestBinSlotConfig = s;
				}	
			}
			return LatestBinSlotConfig.getId();
				
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}
	}
	
	
	
	public List<Integer> GetAllSlotIdsInBinSlotConfigId(Integer BinSlotConfigId){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_BinSlotConfigId", BinSlotConfigId);
	
		try{
			List<Integer> slotList = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_ALL_SLOTS_IN_BIN_SLOT_CONFIG.queryName,
				parameters);
			return slotList;
		}catch(Exception e){
			return null;
		}	
	}
	
	public List<Integer> GetAllSlotsInSiteConfig(Integer SiteConfigId) 
			throws DatabaseException{
		List<Integer> allslotIdsList = new ArrayList<Integer>();
		//get all bins
		List<Integer> allbinIdsList = GetAllBinsInSiteConfig(SiteConfigId);
		System.out.println(allbinIdsList.size());
		for(Integer bin :allbinIdsList){ 
			System.out.println("----"+bin);
			allslotIdsList.addAll(GetAllSlotIdsInBinSlotConfigId(GetLatestBinSlotConfig(bin)));
		}
		return allslotIdsList;
	}
	
public List<Integer> GetAllSlotsWhichcontainSomethingInSite(Integer SiteId)
	throws DatabaseException{
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_SiteNo", SiteId);

		try{
			List<Integer> CurrentSlotList = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_ALL_SlOTS_OF_SITE.queryName,
				parameters);
			
			return CurrentSlotList;
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}		
	}

	public List<Integer> GetAllEmptySlotsInSite(Integer SiteId)
		throws DatabaseException{
			
			List<Integer> AllSlots = GetAllSlotsInSiteConfig(this.GetLastSiteConfigIdForSite(SiteId));
			System.out.println(AllSlots.size());
			List<Integer> SlotsWichContainSomeThing = GetAllSlotsWhichcontainSomethingInSite(SiteId);
			 
			AllSlots.removeAll(SlotsWichContainSomeThing);
			return AllSlots;
	}
	
	public Integer GetSkuOfSlot(Integer SlotId) throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_Current_Pin_id", SlotId);

		try{
			List<Integer> sku = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_SKU_OF_SLOT.queryName,
				parameters);
			
			return sku.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}		
	}
	
	public Integer GetOwnerOfSlot(Integer SlotId) throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_slot_id", SlotId);

		try{
			List<Integer> OwnerId = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_OWNER_ID_OF_SLOT.queryName,
				parameters);
			
			return OwnerId.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}		
	}

	
	public Integer GetGroupBinConfigOfSlot(Integer SlotId) 
			throws DatabaseException{
		Integer bsc = GetLatestBinSlotConfig(SlotId);
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_Slot_Config_ID", bsc);

		try{
			List<Integer> binGroup = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_BIN_GROUP.queryName,
				parameters);
			
			return binGroup.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}			
	}
	
	public List<Integer> GetSlotsForRecievedItems(Integer SiteId,Integer skuId,Integer ItemSize) 
			throws DatabaseException{
		System.out.println("-------------------------");
		List<Integer> EqupiedSlotList = GetAllSlotsWhichcontainSomethingInSite(SiteId);
		System.out.println(EqupiedSlotList.size());
		List<Integer> EmptySlotList = GetAllEmptySlotsInSite(SiteId);
		System.out.println(EmptySlotList.size());  
		List<Integer> highPrioritySlotList = new ArrayList<Integer>();
		List<Integer> returnList = new ArrayList<Integer>();
		
		System.out.println("-------------------------");
		Integer Counter=0;
		
		for(Integer slot: EqupiedSlotList){
			Integer sku = GetSkuOfSlot(slot);
			if(sku == skuId){
				BinSlot binSlot = dao.getEntityById(slot, BinSlot.class, "id");
				List<Integer> havesameskuList = GetAllSlotIdsInBinSlotConfigId(binSlot.getBinSlotConfigurationId());
				for(Integer i:havesameskuList){
					System.out.println("Slot"+i);
					if(EmptySlotList.contains(i)){
						highPrioritySlotList.add(i);
						EmptySlotList.remove(i);
					}					
				}
			}else{//Different sku
				BinSlot binSlot = dao.getEntityById(slot, BinSlot.class, "id");
				List<Integer> RemoveThoseSlots = GetAllSlotIdsInBinSlotConfigId(binSlot.getBinSlotConfigurationId());
				EmptySlotList.removeAll(RemoveThoseSlots);
			}
		}
		if(ItemSize > 0){
			if(highPrioritySlotList.size() > 0){
				for(Integer Index: highPrioritySlotList){
					returnList.add(Index);
					Counter++;
					if(Counter >= ItemSize)
						return returnList;
				}
				for(Integer Index :	EmptySlotList){
					returnList.add(Index); 
					Counter++;
					if(Counter >= ItemSize)
						return returnList;
				}
			}else{
				for(Integer Index :	EmptySlotList){
					returnList.add(Index);
					Counter++;
					if(Counter >= ItemSize)
						return returnList;
				}
			}
		}		
		return new ArrayList<Integer>();
	}

	public List<Integer> GetAllCartons_In_Slot_id(Integer slotId)
		throws DatabaseException{
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_slot_id", slotId);	
	
			try{
				List<Integer> cartons = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_ALL_CARTONS_IN_SLOT_ID.queryName,
					parameters);
				
				return cartons;
			}catch(Exception e) {
				e.printStackTrace();
				throw new DatabaseException("Please check bin configration");
			}
	}
	
	public Long Get_No_of_Cartons_in_slot(Integer slotId)
		throws DatabaseException{
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_slot_id", slotId);
	
			try{
				List<Long> cartons = dao.executeNamedQuery(Long.class,
					QueryNamesEnum.GET_NO_OF_CARTONS_IN_SLOTID.queryName,
					parameters);
				
				return (cartons.get(0)-1);
			}catch(Exception e) {
				e.printStackTrace();
				throw new DatabaseException("Please check bin configration");
			}
	}
	
	
	public Map<Integer,Integer> GetCartonsForIssueOrder (Integer SiteId,Integer skuId,Integer ItemSize,Integer OwnerId)
		throws DatabaseException{
		List<Integer> EqupiedSlotList = GetAllSlotsWhichcontainSomethingInSite(SiteId);
		// Create new List with same capacity as original (for efficiency).
		List<Integer> EqupiedSlotListfilterbyOwner_sku = new ArrayList<Integer>(EqupiedSlotList.size());
		Map<Integer,Integer> Carton_Slot = new TreeMap<Integer, Integer>();
		Map<Integer,Integer> returnCarton_Slot = new TreeMap<Integer, Integer>();
		Map<Integer,Date> Carton_ExpiredDate = new TreeMap<Integer, Date>();
		Map<Integer, Date> Carton_ExpiredDate_after = new TreeMap<Integer, Date>();		
		
		
		for (Integer foo: EqupiedSlotList) {
			EqupiedSlotListfilterbyOwner_sku.add(foo);
		}
				
		for(Integer slot: EqupiedSlotList){
			Integer sku = GetSkuOfSlot(slot);
			Integer owner = GetOwnerOfSlot(slot);
			if(sku != skuId || owner != OwnerId || Get_No_of_Cartons_in_slot(slot)==0){
				EqupiedSlotListfilterbyOwner_sku.remove(slot);
			} 
		}
		
		Collections.reverse(EqupiedSlotListfilterbyOwner_sku);
		
		for(Integer slot: EqupiedSlotListfilterbyOwner_sku){
			List<Integer> CartonList = GetAllCartons_In_Slot_id(slot);
			Collections.reverse(CartonList);
			for(Integer i : CartonList){
				Carton_Slot.put(i, slot);
				TransactionOrderItem carton = dao.getEntityById(i, TransactionOrderItem.class, "id");
				Carton_ExpiredDate.put(i, carton.getExpirationDate());
				System.out.println("Carton id = "+i+" , slot = "+slot+" , ExpiredDate = "+carton.getExpirationDate());
			}
		}
		
		Carton_ExpiredDate_after = sortByValue(Carton_ExpiredDate);
		
		Integer count = 0;
		
		for (Map.Entry<Integer,Date> entry : Carton_ExpiredDate_after.entrySet()) {
			returnCarton_Slot.put(entry.getKey(), Carton_Slot.get(entry.getKey()));
			System.out.println("Carton id = "+entry.getKey()+" , slot = "+Carton_Slot.get(entry.getKey())+" , ExpiredDate = "+entry.getValue());
			count++;
			if(count >= ItemSize){
				return returnCarton_Slot;
			}
		}
		return returnCarton_Slot;		
	} 
	
	
	public List<Integer> GetSlotsForIssueitems(Integer SiteId,Integer skuId,Integer ItemSize,Integer OwnerId) 
		throws DatabaseException{	
		
		List<Integer> EqupiedSlotList = GetAllSlotsWhichcontainSomethingInSite(SiteId);
		// Create new List with same capacity as original (for efficiency).
		List<Integer> EqupiedSlotListfilterbyOwner_sku = new ArrayList<Integer>(EqupiedSlotList.size());
		List<Integer> returnList = new ArrayList<Integer>();
		
		
		for (Integer foo: EqupiedSlotList) {
			EqupiedSlotListfilterbyOwner_sku.add(foo);
		}
				
		for(Integer slot: EqupiedSlotList){
			Integer sku = GetSkuOfSlot(slot);
			Integer owner = GetOwnerOfSlot(slot);
			if(sku != skuId || owner != OwnerId || Get_No_of_Cartons_in_slot(slot)>0){
				EqupiedSlotListfilterbyOwner_sku.remove(slot);
			} 
		}

		if(ItemSize > EqupiedSlotListfilterbyOwner_sku.size())
			return null;
		
		Collections.reverse(EqupiedSlotListfilterbyOwner_sku);
		System.out.println("EqupiedSlotListfilterbyOwner_sku: "+EqupiedSlotListfilterbyOwner_sku.size()+"	slot");
		
		Map<Integer, Date> EqupiedSlotMapfilterby_ExpiredDate_befor = new TreeMap<Integer, Date>();
		Map<Integer, Date> EqupiedSlotMapfilterby_ExpiredDate_after = new TreeMap<Integer, Date>();
		
		for (Integer slot: EqupiedSlotListfilterbyOwner_sku) {
			TransactionOrderItem item = dao.getEntityById(GetItemIdOfSlot(slot),TransactionOrderItem.class,"id");
			EqupiedSlotMapfilterby_ExpiredDate_befor.put(slot, item.getExpirationDate());
		}
		System.out.println("EqupiedSlotMapfilterby_ExpiredDate: "+EqupiedSlotMapfilterby_ExpiredDate_befor.size()+"	slot");		
		EqupiedSlotMapfilterby_ExpiredDate_after = sortByValue(EqupiedSlotMapfilterby_ExpiredDate_befor);
				
		Integer Counter = 0;
		for (Map.Entry<Integer,Date> entry : EqupiedSlotMapfilterby_ExpiredDate_after.entrySet()) {
			returnList.add(entry.getKey());
			System.out.println("slot:"+entry.getKey());
			Counter++;
			if(Counter >= ItemSize){
				return returnList;
			}
		}
		return returnList;		
	}

	public Integer GetItemIdOfSlot(Integer slotId)
			throws DatabaseException{
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("P_slot_id", slotId);

				try{
					List<Integer> items = dao.executeNamedQuery(Integer.class,
						QueryNamesEnum.GET_ITEM_IN_SLOT_ID.queryName,
						parameters);
					
					return items.get(0);
				}catch(Exception e) {
					e.printStackTrace();
					throw new DatabaseException("Please check bin configration");
				}
		}
	
	
	public Integer GetOwnerIdForItem(Integer itemId)
		throws DatabaseException{
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_itemId", itemId);

			try{
				List<Integer> Owners = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_OWNER_ID_OF_ITEM.queryName,
					parameters);
				
				return Owners.get(0);
			}catch(Exception e) {
				e.printStackTrace();
				throw new DatabaseException("Please check bin configration");
			}
	}
	
	public Integer GetSiteIdForItem(Integer itemId)
			throws DatabaseException{
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("P_itemId", itemId);

				try{
					List<Integer> Sites = dao.executeNamedQuery(Integer.class,
						QueryNamesEnum.GET_SITE_ID_OF_ITEM.queryName,
						parameters);
					
					return Sites.get(0);
				}catch(Exception e) {
					e.printStackTrace();
					throw new DatabaseException("Please check bin configration");
				}
		}

	
	public List<Integer> GetAllISContainingPalet(Integer SiteId,Integer skuId,Integer OwnerId) 
			throws DatabaseException{
		List<TransactionOrderItem> allItems = dao.getAllEntities(TransactionOrderItem.class);
		List<Integer> isContainingItems = new ArrayList<Integer>();
		List<Integer> returnList = new ArrayList<Integer>();
		
		for(TransactionOrderItem item: allItems){
			if(item.getIsContaining() == 1 && item.getStockKeepingUnitId() == skuId
				&& GetOwnerIdForItem(item.getId()) == OwnerId && GetSiteIdForItem(item.getId()) == SiteId){
				
				isContainingItems.add(item.getId());
			}
		}
		List<CurrentBinItem> allCurrentBinItems = dao.getAllEntities(CurrentBinItem.class);
		for(CurrentBinItem cbi:allCurrentBinItems){
			if(isContainingItems.contains(cbi.getItemId())){
				returnList.add(cbi.getItemId());
			}
		}
		return returnList;
	}
	
	public Integer GetmaxCartonCount(Integer SkuId)
		throws DatabaseException{
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_skuId", SkuId);

			try{
				List<Integer> size = dao.executeNamedQuery(Integer.class,
					QueryNamesEnum.GET_MAX_CARTON_COUNT.queryName,
					parameters);
				
				return size.get(0);
			}catch(Exception e) {
				e.printStackTrace();
				throw new DatabaseException("Please check bin configration");
			}
	}
	
	public Integer CreateNewIsContainingPalet(Integer OrderId ,Integer skuId,Integer SiteId){
		TransactionOrderItem item = new TransactionOrderItem();
		item.setTransactionOrderId(OrderId);
		item.setStockKeepingUnitId(skuId);
		item.setUnitTypeId(1);
		item.setIsContained(0);
		item.setIsContaining(1);
		
		CustomSession session1 = dao.openSession();
		try {
			session1.beginTransaction();
			dao.addEntity(item, session1);
			session1.commitTransaction();
		} catch (DatabaseException e) {
		    e.printStackTrace();
			session1.rollbackTransaction();
		} finally {
			session1.close();
		}
		return item.getId();
	}
	
	public Integer GetNoOfCartonsInPalet(Integer itemId)
		throws DatabaseException{
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("P_PaletId", itemId);

			try{
				List<Long> CartonsInPalet = dao.executeNamedQuery(Long.class,
					QueryNamesEnum.GET_NO_OF_CARTONS_IN_PALET.queryName,
					parameters);
				
				if(CartonsInPalet == null || CartonsInPalet.size() == 0){
					System.out.println("no catrtons in palet of id="+itemId);
					return 0;
				}else{
					System.out.println("no catrtons in palet of id="+itemId);
					System.out.print(CartonsInPalet.size());
					return CartonsInPalet.get(0).intValue();
				}
			}catch(Exception e) {
				e.printStackTrace();
				throw new DatabaseException("Please check bin configration");
			}		
	}
	
	public Integer GetSlotOfItem(Integer itemId)
			throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_item_id", itemId);

		try{
			List<Integer> slots = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_SLOT_ID_OF_ITEM.queryName,
				parameters);
			
			return slots.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}
	}
	
	
	public List<Palet_Slot_CartonCount_isnew> GetIsContainingPaletForCartons(Integer SiteId,Integer skuId,Integer ItemSize,Integer OwnerId,Integer OrderId) 
			throws DatabaseException{
		
		List<Palet_Slot_CartonCount_isnew> returnList = new ArrayList<Palet_Slot_CartonCount_isnew>();// pallet id - no of cartons
		List<Integer> allIsContaningPalets = GetAllISContainingPalet(SiteId,skuId,OwnerId);
		Integer paletSize = GetmaxCartonCount(skuId);
		
		if(allIsContaningPalets.size() == 0){
			Integer no_of_palets = (ItemSize/paletSize)+1;
			List<Integer> slotsForNewPalets = GetSlotsForRecievedItems(SiteId,skuId,no_of_palets);
			if(slotsForNewPalets.size() < no_of_palets){
				//there is no place
				return new ArrayList<Palet_Slot_CartonCount_isnew>();
			}
			for(Integer i=0; i<no_of_palets;i++){
				Integer PaletId = CreateNewIsContainingPalet(OrderId,skuId,SiteId);
				Palet_Slot_CartonCount_isnew temp = new Palet_Slot_CartonCount_isnew();
				temp.setPalet_Id(PaletId);
				temp.setNew(true);
				temp.setSlot_Id(slotsForNewPalets.get(i));
				if(no_of_palets-i == 1){
					temp.setCartonCount(ItemSize%paletSize);
				}else{
					temp.setCartonCount(paletSize);
				}
				returnList.add(temp);
			}
			return returnList;
		}else{
			Integer noOfCartos = ItemSize;
			for(Integer i :allIsContaningPalets){
				Integer nofreeItems = paletSize - GetNoOfCartonsInPalet(i);
				if(nofreeItems > 0){
					if( noOfCartos >= nofreeItems){
						noOfCartos -= nofreeItems;
						Palet_Slot_CartonCount_isnew temp = new Palet_Slot_CartonCount_isnew();
						temp.setPalet_Id(i);
						temp.setNew(false);
						temp.setSlot_Id(GetSlotOfItem(i));
						temp.setCartonCount(nofreeItems);
						returnList.add(temp);
						if(noOfCartos <= 0)
							return returnList;
					}else if(noOfCartos < nofreeItems){
						Palet_Slot_CartonCount_isnew temp = new Palet_Slot_CartonCount_isnew();
						temp.setPalet_Id(i);
						temp.setNew(false);
						temp.setSlot_Id(GetSlotOfItem(i));
						temp.setCartonCount(noOfCartos);
						returnList.add(temp);
						noOfCartos = 0;
						return returnList;
					}
				}
			}
			//lsa fe cartons
			Integer no_of_palets = (noOfCartos/paletSize)+1;
			List<Integer> slotsForNewPalets = GetSlotsForRecievedItems(SiteId,skuId,no_of_palets);
			if(slotsForNewPalets.size() < no_of_palets){
				//there is no place
				return new ArrayList<Palet_Slot_CartonCount_isnew>();
			}
			for(Integer j=0; j<no_of_palets;j++){
				Integer PaletId = CreateNewIsContainingPalet(OrderId,skuId,SiteId);
				Palet_Slot_CartonCount_isnew temp = new Palet_Slot_CartonCount_isnew();
				temp.setPalet_Id(PaletId);
				temp.setNew(true);
				temp.setSlot_Id(slotsForNewPalets.get(j));
				if(no_of_palets-j == 1){
					temp.setCartonCount(noOfCartos%paletSize);
				}else{
					temp.setCartonCount(paletSize);
				}
				returnList.add(temp);
			}
			return returnList;
		}
	} 
	
	
	public static <K, V extends Comparable<? super V>> Map<K, V> 
			sortByValue( Map<K, V> map )
		{
		    List<Map.Entry<K, V>> list = new LinkedList<>( map.entrySet() );
		    Collections.sort( list, new Comparator<Map.Entry<K, V>>()
		    {
		        @Override
		        public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
		        {
		            return (o1.getValue()).compareTo( o2.getValue() );
		        }
		    } );
		
		    Map<K, V> result = new LinkedHashMap<>();
		    for (Map.Entry<K, V> entry : list)
		    {
		        result.put( entry.getKey(), entry.getValue() );
		    }
		    return result;
		}
	public void addnewSerialToBarCode(List <SerialRecored> s ) throws BusinessException{
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.addEntities(s, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}
	}
	
	public void transferPalet(Integer TransactionOrderTransferId, Integer LaborId, Integer FromSlotId, Integer ToSlotId) throws DatabaseException, BusinessException{
		Integer TOI_id = this.GetItemIdOfSlot(FromSlotId);
		CurrentBinItem CBI = this.getCBIBySlotId(FromSlotId);
		
		TransactionExecuteTransfer TET = new TransactionExecuteTransfer();
		TET.setTransactionOrderTransferId(TransactionOrderTransferId);
		TET.setSiteLaborId(LaborId);
		
		TransferItemBin TIB = new TransferItemBin();
		TIB.setTransactionExecuteTransferId(TransactionOrderTransferId);
		TIB.setTransactionOrderItemId(TOI_id);
		TIB.setFormslotId(FromSlotId);
		TIB.setToslotId(ToSlotId);
		TIB.setExecutionTime(new Date());
		
		CBI.setSlotId(ToSlotId);
				
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.addEntity(TET, session);
			dao.addEntity(TIB, session);
			dao.updateEntity(CBI, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
			throw new BusinessException("Internal Database Error");
		} finally {
			session.close();
		}	
	}
	
	public Integer GetCartonInPallet(Integer cartonId)
			throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_cartonId", cartonId);

		try{
			List<Integer> slots = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_CARTONINPALET_ID.queryName,
				parameters);
			
			return slots.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}
	}
	
	public Integer GetToPallet(Integer slotid,Integer one)
			throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_SOT_ID", slotid);
		parameters.put("P_ONE", one);

		try{
			List<Integer> slots = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_TO_PAlLET_ID.queryName,
				parameters);
			
			return slots.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}
	}
	
	public Integer get_CBI_OF_ITEMID(Integer Itemid)
			throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_itemId", Itemid);
		
		try{
			List<Integer> slots = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_CURRENT_BIN_ITEM_OF_ITEM_Id.queryName,
				parameters);
			
			return slots.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("Please check bin configration");
		}
	}
	
	
	public void transferCarton(Integer CurrentOwnerId,Integer OrderId,Integer siteId,Integer cartonId, Integer TransactionOrderTransferId, Integer LaborId, Integer FromSlotId, Integer ToSlotId) {
		
		Integer CartonInPaletid;
		CartonInPalet CIP = null;
		TransactionOrderItem fromPalet = null;
		TransactionOrderItem toPalet;
		CurrentBinItem cbi;
		CurrentBinItem CBI = null;
		
		try {
			CBI = dao.getEntityById(this.get_CBI_OF_ITEMID(cartonId),CurrentBinItem.class,"id");
		} catch (DatabaseException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		try {
			CartonInPaletid = this.GetCartonInPallet(cartonId);
			CIP = dao.getEntityById(CartonInPaletid, CartonInPalet.class, "id");
			fromPalet = dao.getEntityById(CIP.getPaletId(),TransactionOrderItem.class,"id");
		} catch (DatabaseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		try {
			Integer to = GetToPallet(ToSlotId,1);
			toPalet = dao.getEntityById(to,TransactionOrderItem.class,"id");		
		} catch (DatabaseException e1) {
			//CREATE NEW PALLET
			Integer to = this.CreateNewIsContainingPalet(OrderId, fromPalet.getStockKeepingUnitId(),siteId);
			toPalet = dao.getEntityById(to,TransactionOrderItem.class,"id");
			
			cbi = new CurrentBinItem();
			cbi.setSiteId(siteId);
			cbi.setItemId(to);
			cbi.setOwnerId(CurrentOwnerId);
			cbi.setSlotId(ToSlotId);
			CustomSession session = dao.openSession();
			try {
				session.beginTransaction();
				dao.addEntity(cbi, session);
				session.commitTransaction();
			} catch (DatabaseException e) {
				e.printStackTrace();
				session.rollbackTransaction();
			} finally {
				session.close();
			}	
		}
		
		
		TransactionExecuteTransfer TET = new TransactionExecuteTransfer();
		TET.setTransactionOrderTransferId(TransactionOrderTransferId);
		TET.setSiteLaborId(LaborId);
		
		TransferItemBin TIB = new TransferItemBin();
		TIB.setTransactionExecuteTransferId(TransactionOrderTransferId);
		TIB.setTransactionOrderItemId(cartonId);
		TIB.setFormslotId(FromSlotId);
		TIB.setToslotId(ToSlotId);
		TIB.setExecutionTime(new Date());
		
		CIP.setPaletId(toPalet.getId());
		
		CBI.setSlotId(ToSlotId);
		
		CustomSession session = dao.openSession();
		try {
			session.beginTransaction();
			dao.addEntity(TET, session);
			dao.addEntity(TIB, session);
			dao.updateEntity(CIP, session);
			dao.updateEntity(CBI, session);
			session.commitTransaction();
		} catch (DatabaseException e) {
			e.printStackTrace();
			session.rollbackTransaction();
		} finally {
			session.close();
		}	
	}
}


