package com.robovics.wims.model.service.transaction.order;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Service;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.contact.Owner;
import com.robovics.wims.model.orm.entities.contact.Supplier;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.orm.entities.transaction.orderItem.UnitType;

@Service(value = "issueService")
public class TransactionOrderIssueService {

	private DataAccess dao = DAOFactory.getInstance().getDataAccess();

	

	

	public void addTransactionOrder(TransactionOrder transactionOrder,
			ArrayList<TransactionOrderItem> items,
			HashSet<TransactionOrderIssue> tors) throws BusinessException {
		try {
			dao.addEntity(transactionOrder);
			Integer transactionOrderId = transactionOrder.getId();
			for (TransactionOrderItem item : items) {
				try {
					item.setTransactionOrderId(transactionOrderId);
					dao.addEntity(item);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}
			for (TransactionOrderIssue tor : tors) {
				try {
					tor.setTransactionOrderId(transactionOrderId);
					dao.addEntity(tor);
				} catch (DatabaseException e) {
					e.printStackTrace();
					throw new BusinessException("Internal Database Error");
				}
			}

		} catch (DatabaseException e) {
			e.printStackTrace();
			throw new BusinessException("Internal Database Error");
		}
	}
}