package com.robovics.wims.ui.sitePortal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.robovics.wims.exceptions.BusinessException;
import com.robovics.wims.exceptions.DatabaseException;
import com.robovics.wims.exceptions.OrderException;
import com.robovics.wims.model.enums.ErrorsEnum;
import com.robovics.wims.model.enums.QueryNamesEnum;
import com.robovics.wims.model.orm.CustomSession;
import com.robovics.wims.model.orm.DAOFactory;
import com.robovics.wims.model.orm.DataAccess;
import com.robovics.wims.model.orm.entities.StockKeepingUnit;
import com.robovics.wims.model.orm.entities.masterData.BinSlot;
import com.robovics.wims.model.orm.entities.masterData.BinSlotConfiguration;
import com.robovics.wims.model.orm.entities.masterData.CurrentBinItem;
import com.robovics.wims.model.orm.entities.masterData.GroupBin;
import com.robovics.wims.model.orm.entities.masterData.ItemStatus;
import com.robovics.wims.model.orm.entities.masterData.SerialRecored;
import com.robovics.wims.model.orm.entities.masterData.SiteConfigurationBinGroup;
import com.robovics.wims.model.orm.entities.staff.SiteLabor;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteIssue;
import com.robovics.wims.model.orm.entities.transaction.execute.TransactionExecuteReceive;
import com.robovics.wims.model.orm.entities.transaction.itemBin.IssueItemBin;
import com.robovics.wims.model.orm.entities.transaction.itemBin.ReceiveItemBin;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrder;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderIssue;
import com.robovics.wims.model.orm.entities.transaction.order.TransactionOrderReceive;
import com.robovics.wims.model.orm.entities.transaction.orderItem.CartonInPalet;
import com.robovics.wims.model.orm.entities.transaction.orderItem.TransactionOrderItem;
import com.robovics.wims.model.service.Palet_Slot_CartonCount_isnew;
import com.robovics.wims.model.service.ServiceFactory;
import com.robovics.wims.model.service.ServiceFactory.ServicesEnum;
import com.robovics.wims.model.service.masterData.MasterDataService;
import com.robovics.wims.model.service.transaction.order.TransactionOrderTransferService;
import com.robovics.wims.ui.GasDiaryMessages;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;

@SuppressWarnings("serial")
@Theme("wims")
 
public class ExecuteOredergui extends Window{
	
	ResourceBundle i18n ; // Automatic Localization
 	ResourceBundle i18nBundle; // Automatic localization
	
	List<ExecuteView> exeView = new ArrayList<ExecuteView>();
	private DataAccess dao = DAOFactory.getInstance().getDataAccess();
	//**************
	private MasterDataService mainService = new MasterDataService();
	final TransactionOrderTransferService transferService =(TransactionOrderTransferService) ServiceFactory.getInstance()
			.getService(ServicesEnum.TRANSACTION_ORDER_TRANSFER_SERVICE);
	private Integer SiteNo;//current site
	private Integer type;//2 for Receive , 1 for issue
	private Integer measure_type;//1 for pallet,2 for carton 
	private Integer currentSKU;
	private Integer CurrentOwnerId;
	private Integer CurrentSiteLaborId;
	private Integer CurrentCustomerId;
	private Integer CurrentOrderId;
	private Integer CurrentQualityStatusiId;
	
	private List <TransactionOrderIssue> TransactionOrderIssueIds;
	private List <TransactionOrderReceive> TransactionOrderReceiveIds;
	private List <TransactionOrderItem> items;//send from previous panal
	private List<SerialRecored> doublerecordes;
	List<String> QualityStatusType = Arrays.asList("Released", "Quantiled", "Blocked");
	ComboBox statusBox ;
	
	List<Integer> QualityStatusIds;//slot ids put in it
	List<String> QualityStatusNotes;//slot ids take from it
	List<Integer> RecieveList;//slot ids put in it
	List<Integer> IssueList;//slot ids take from it
	List<Palet_Slot_CartonCount_isnew> ReceiveCartonList;//
	Map<Integer,Integer> IssueCartonMap;//carton id,location
	Map<SerialRecored,Integer> matching = new HashMap<SerialRecored,Integer>();//carton id,location
	//gui items    
	Button DoneExecuteButton;
	Button RescanButton;
	Button maunalEnter;
	Table view = new Table();
	
	public Integer getCurrentCustomerId() {
		return CurrentCustomerId;
	}
	public void setCurrentCustomerId(Integer currentCustomerId) {
		CurrentCustomerId = currentCustomerId;
	}	
	public Integer getSiteNo() {
		return SiteNo;
	}
	public void setSiteNo(Integer siteNo) {
		SiteNo = siteNo;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public List<TransactionOrderItem> getItemIds() {
		return items;
	}
	public void setItemIds(List<TransactionOrderItem> Items) {
		this.items = Items;
	}
	public Integer getCurrentSKU() {
		return currentSKU;
	}
	public void setCurrentSKU(Integer currentSKU) {
		this.currentSKU = currentSKU;
	}
	public List<TransactionOrderIssue> getTransactionOrderIssueIds() {
		return TransactionOrderIssueIds;
	}
	public void setTransactionOrderIssueIds(
			List<TransactionOrderIssue> transactionOrderIssueIds) {
		TransactionOrderIssueIds = transactionOrderIssueIds;
	}
	public List<TransactionOrderReceive> getTransactionOrderReceiveIds() {
		return TransactionOrderReceiveIds;
	}
	public void setTransactionOrderReceiveIds(
			List<TransactionOrderReceive> transactionOrderReceiveIds) {
		TransactionOrderReceiveIds = transactionOrderReceiveIds;
	}
	public Integer getCurrentOwnerId() {
		return CurrentOwnerId;
	}
	public void setCurrentOwnerId(Integer currentOwnerId) {
		CurrentOwnerId = currentOwnerId;
	}
	public Integer getCurrentOrderId() {
		return CurrentOrderId;
	}
	public void setCurrentOrderId(Integer currentOrderId) {
		CurrentOrderId = currentOrderId;
	}
	public Integer getMeasure_type() {
		return measure_type;
	}
	public void setMeasure_type(Integer measure_type) {
		this.measure_type = measure_type;
	}
	
	public Layout FormWindow() throws DatabaseException	{
		if (((Locale)VaadinSession.getCurrent().getAttribute("Locale"))==null){
		 	i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),  new Locale("en"));
		 	i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), new Locale("en"));
		 	VaadinSession.getCurrent().setAttribute("Locale", new Locale("en"));
	 }else {
		Locale c = ((Locale)VaadinSession.getCurrent().getAttribute("Locale"));
		 i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), c );
		 i18n  = ResourceBundle.getBundle(GasDiaryMessages.class.getName(),c);
	 }
		 statusBox = new ComboBox(this.getMessage(GasDiaryMessages.QualityStatus),new IndexedContainer(QualityStatusType));
		 DoneExecuteButton = new Button(this.getMessage(GasDiaryMessages.Done));
	     RescanButton = new Button(this.getMessage(GasDiaryMessages.Scan));
	     maunalEnter = new Button(this.getMessage(GasDiaryMessages.EnterMAnually));
		final VerticalLayout Vlay = new VerticalLayout();
		final HorizontalLayout tableLayout = new HorizontalLayout();
		final HorizontalLayout MainLayout = new HorizontalLayout();
		CurrentQualityStatusiId = getdefaultQulityId(currentSKU);
		//select which table will be view
		System.out.println("item type= "+measure_type);
		if(measure_type == 1){	
			if(type == 2){//receive
				RecieveList = mainService.GetSlotsForRecievedItems(SiteNo,currentSKU,items.size());	
				
				try{
					if(RecieveList.size() < items.size()){
						throw (new OrderException(16));	
					}else{ 
						generateRecieveItemListView();
						showTable(exeView);
						tableLayout.addComponent(view);
						DoneExecuteButton.setVisible(false);
						RescanButton.setVisible(true );
						//Notification.show("ÈÑÌÇÁ ãÓÍ ÇáÈÇáÊÉ Ëã ÇáÓáæÊ ÈÇáÊÑÊíÈ æÇáÊÈÇÏá",Type.HUMANIZED_MESSAGE);
					}
				}catch(OrderException e){
					Notification.show(
							ErrorsEnum.values()[e.getType() - 1].valueAr,
							Type.ERROR_MESSAGE);
				}
			}else if(type == 1){//issue
				IssueList = mainService.GetSlotsForIssueitems(SiteNo,currentSKU,items.size(),CurrentOwnerId);
				try{
					if(IssueList.size() < items.size()){
						throw (new OrderException(8));	
					}else{
						generateIssueItemListView();
						showTable(exeView);
						tableLayout.addComponent(view);
						RescanButton.setVisible(true);
						maunalEnter.setVisible(false);
			   			DoneExecuteButton.setVisible(false);
					}
				}catch(OrderException e){
					Notification.show(
							ErrorsEnum.values()[e.getType() - 1].valueAr,
							Type.ERROR_MESSAGE);	
				}
			}
		}else if(measure_type == 2){
			if(type == 2){//receive
				ReceiveCartonList = mainService.GetIsContainingPaletForCartons(SiteNo, currentSKU, items.size(), CurrentOwnerId, CurrentOrderId);
				Integer cartonSize=0;
				for(Palet_Slot_CartonCount_isnew p :ReceiveCartonList){
					cartonSize += p.getCartonCount();
				}
				try{
					if(cartonSize < items.size()){
						throw (new OrderException(16));	
					}else{ 
						generateRecieveItemviewForCartons();
						showTable(exeView);
						tableLayout.addComponent(view);
						DoneExecuteButton.setVisible(false);
						RescanButton.setVisible(true );
						Notification.show("ÈÑÌÇÁ ãÓÍ ÌãíÚ ÇáßÑÇÊíä ÇáÎÇÕÉ ÈäÝÓ ÇáÓáæÊ Ëã ÇáÓáæÊ ÈÇáÊÑÊíÈ æÇáÊÈÇÏá",
									Type.HUMANIZED_MESSAGE);
						}
				}catch(OrderException e){
					Notification.show(
							ErrorsEnum.values()[e.getType() - 1].valueAr,
							Type.ERROR_MESSAGE);
				} 
			}else if(type == 1){//issue			
				IssueCartonMap = mainService.GetCartonsForIssueOrder(SiteNo,currentSKU,items.size(),CurrentOwnerId);
				try{
					if(IssueCartonMap.size() < items.size()){
						throw (new OrderException(8));	
					}else{
						generateIssueItemListViewForCartons();
						showTable(exeView);
						tableLayout.addComponent(view);
						RescanButton.setVisible(true);
			   			DoneExecuteButton.setVisible(false);
					}
				}catch(OrderException e){
					Notification.show(
							ErrorsEnum.values()[e.getType() - 1].valueAr,
							Type.ERROR_MESSAGE);	
				}
			}
		}
		//add listener to Done button  
		DoneExecuteButton.addClickListener(new Button.ClickListener() {
		   @Override
		   public void buttonClick(ClickEvent event) {
			   final Window subWindow = new Window("Labor login");
			   VerticalLayout subContent = new VerticalLayout();
			   subContent.setMargin(true);
			   subWindow.setContent(subContent);
			   final TextField username = new TextField();
			   username.setInputPrompt("username");
			   final TextField password = new TextField();
			   password.setInputPrompt("password");
			   Button sOk = new Button("Login");
			   subContent.addComponent(username);
			   subContent.addComponent(password);
			   subContent.addComponent(sOk);
			   sOk.addClickListener(new ClickListener() {
				   @Override
				   public void buttonClick(ClickEvent event) {
					   int sitelabor = transferService.authenticate_labor(
							   username.getValue(), password.getValue());
					   SiteLabor labor = dao.getEntityById(sitelabor, SiteLabor.class, "id");
					   if (sitelabor == -1) {
						   new Notification("Notification error",
								   "login failed' please try again").show(Page
										   .getCurrent());
					   }else if(labor.getSiteId() != SiteNo){
						   new Notification("Notification error",
								   "this a labor of another site").show(Page
										   .getCurrent());
					   }
					   else {
							CurrentSiteLaborId = sitelabor;
							if(measure_type == 1){
						   		if(type == 2){
						   			ExecuteReceiveOrder();
							   }else if(type == 1){
								   ExecuteIssueOrder();	
							   }
						   }else if(measure_type == 2){
							   if(type == 2){
								  ExecuteReceiveOrderCarton();
							   }else if(type == 1){
								   try {
									   ExecuteIssueOrderCarton();
								   } catch (DatabaseException e) {
									   e.printStackTrace();
								   }
							   }
						   }
						   subWindow.close();
						   UI.getCurrent().setEnabled(true);
						   close();
						   Page.getCurrent().getJavaScript().execute("window.location.reload();");
						}
				   }
				});
			   subWindow.center();
			   UI.getCurrent().addWindow(subWindow);
		   	}
		});
		
		RescanButton.addClickListener(new Button.ClickListener() {
			   @Override
			   public void buttonClick(ClickEvent event) {
				   
				   if(measure_type == 1){
				   		if(type == 2){//recieve
				   			try{
					   			if(ScanRecieveItemListView()){
					   				tableLayout.removeAllComponents();
					   				/*DAOFactory.getInstance().context = new AnnotationConfigApplicationContext();
					   				DAOFactory.getInstance().context.register(DataAccess.class);
					   				DAOFactory.getInstance().context.refresh();*/
					   				
					   				showTable(exeView);
					   				tableLayout.addComponent(view);
					   				
					   				RescanButton.setVisible(false);
					   				DoneExecuteButton.setVisible(true);
								}else{
									throw (new OrderException(17));
								}
				   			}catch(OrderException e){
				   				Notification.show(
										ErrorsEnum.values()[e.getType() - 1].valueAr,
										Type.ERROR_MESSAGE);	
				   				
				   				return;//error in scaning
				   			}
					    }else if(type == 1){
					    	try{
					   			if(scanMatchForIssuePalet()){
					   				tableLayout.removeAllComponents();
				   				
					   				showTable(exeView);
					   				tableLayout.addComponent(view);
					   				
					   				RescanButton.setVisible(false);
					   				DoneExecuteButton.setVisible(true);
								}else{
									throw (new OrderException(17));
								}
				   			}catch(OrderException | DatabaseException e){
				   				Notification.show(
										ErrorsEnum.values()[16].valueAr,
										Type.ERROR_MESSAGE);	
				   				
				   				return;//error in scaning
				   			}
					    }
				   }else if(measure_type == 2){
					   if(type == 2){
						   try{
					   			if(scanRecieveItemListForCartons()){
					   				tableLayout.removeAllComponents();
					   				showTable(exeView);
					   				tableLayout.addComponent(view);
					   				RescanButton.setVisible(false);
					   				DoneExecuteButton.setVisible(true);
								}else{
									throw (new OrderException(17));
								}
				   			}catch(OrderException e){
				   				Notification.show(
										ErrorsEnum.values()[e.getType() - 1].valueAr,
										Type.ERROR_MESSAGE);	
				   				
				   				return;//error in scaning
				   			}
					   }
					   else if(type == 1){
						   try{
					   			if(scanMatchForIssueCarton()){
					   				tableLayout.removeAllComponents();
				   				
					   				showTable(exeView);
					   				tableLayout.addComponent(view);
					   				
					   				RescanButton.setVisible(false);
					   				DoneExecuteButton.setVisible(true);
								}else{
									throw (new OrderException(17));
								}
				   			}catch(OrderException | DatabaseException e){
				   				Notification.show(
										ErrorsEnum.values()[16].valueAr,
										Type.ERROR_MESSAGE);	
				   				
				   				return;//error in scaning
				   			}
					   }
				   } 
				}
			});
		maunalEnter.addClickListener(new Button.ClickListener() {
			   @Override
			   public void buttonClick(ClickEvent event) {
				  System.out.println("Entered");
				   if(measure_type == 1){
					   if(type == 2){
						   SimulationbarCode n = null;
						try {
							n = new SimulationbarCode(1,SiteNo);
						} catch (DatabaseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
							  UI.getCurrent().addWindow(n);
					   }
				   }else if(measure_type == 2){
					   SimulationbarCode n = null;
					try {
						n = new SimulationbarCode(2,SiteNo);
					} catch (DatabaseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						  UI.getCurrent().addWindow(n); 
				   } 
				}
			});
		
		 view.addValueChangeListener(new ValueChangeListener() {
			 
			 @Override   
			 public void valueChange(ValueChangeEvent event) {
				 
				 if (event.getProperty().getValue() == null) {
						//formLayout.setVisible(false);
					 statusBox.setVisible(false);
						return;
					}
				 statusBox.setVisible(true);
				 
				 
			 } 
		 });
		
		view.setImmediate(true);
		tableLayout.setSizeFull();
		MainLayout.addComponent(tableLayout);
		Vlay.addComponent(MainLayout);
		statusBox.setVisible(false);
		statusBox.select(QualityStatusType.get(CurrentQualityStatusiId-1));
		statusBox.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				ExecuteView item = (ExecuteView) view.getValue();
			 	
				 item.setQualityStatus(getQulityId((String)statusBox.getValue()));
				 showTable(exeView);
				 Notification.show(
						 ""+item.getBinSymbol()+" "+ item.getSlotSymbol()+"",
							Type.HUMANIZED_MESSAGE);	
				
			}
		});
		Vlay.addComponent(statusBox);
		Vlay.addComponent(RescanButton);
		Vlay.addComponent(maunalEnter);
		Vlay.addComponent(DoneExecuteButton);
		return Vlay;		
	}
	 
	private	boolean ScanRecieveItemListView() {		
		doublerecordes = dao.getAllEntities(SerialRecored.class);		
		List<Integer> ReceiveListCopy = new ArrayList<Integer>(RecieveList.size());
		
		// if scanning of serials is wrong
		System.out.println("item size = "+items.size());
		System.out.println("recordes size = "+doublerecordes.size());
		if(doublerecordes.size() != (2*items.size())){
			DoneExecuteButton.setVisible(false);
			CustomSession session1 = dao.openSession();
			try {
				session1.beginTransaction();
				dao.deleteEntities(doublerecordes, session1);//delete all records	
				session1.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session1.rollbackTransaction();
			} finally {
				session1.close();
			}
			return false;//error in scanning				
		}
		
		for(int i=1;i<doublerecordes.size();i+=2){
			System.out.println(doublerecordes.get(i));
			SerialRecored slotRecored = doublerecordes.get(i);
			Integer slotid = Integer.parseInt(slotRecored.getSerial());
			if(RecieveList.contains(slotid)){
				if(!ReceiveListCopy.contains(slotid)){
					ReceiveListCopy.add(slotid);
				}else{
					CustomSession session1 = dao.openSession();
					try {
						session1.beginTransaction();
						dao.deleteEntities(doublerecordes, session1);//delete all records	
						session1.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session1.rollbackTransaction();
					} finally {
						session1.close();
					}

					return false;//error in scanning
				}
			}
		}
		RecieveList = ReceiveListCopy;
		exeView.removeAll(exeView);
		int counter=0;
		for	(TransactionOrderItem i :items){
			SerialRecored paletRecored = doublerecordes.get(counter);
			try {
				paletRecored.getInfoFromSerial();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Notification.show("عغوا يوجد حطأ اثناء عمليه المسج",Notification.TYPE_ERROR_MESSAGE);
			}
			
			i.setSerial(paletRecored.getSerial());// update item Serial column
			i.setProductionDate(paletRecored.getDate());// update item production date column
			i.setExpirationDate(getExpirationdate(currentSKU,paletRecored.getDate()));// update item production date column
				
			BinSlot slot = dao.getEntityById(RecieveList.get(counter/2), BinSlot.class, "id");
			BinSlotConfiguration slotConfig = dao.getEntityById(slot.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
			
			System.out.println("********************");
			System.out.println(slot.getId());
			System.out.println("********************");			
			
			/*Modified Now Easy ro roll back */
			ExecuteView temp = new ExecuteView();
			temp.setBinSymbol(groupConfig.getSymbol()+group.getBinNumber());
			temp.setSlotId(slot.getId());
			temp.setSlotSymbol(slot.getY()+"_"+slot.getZ());
			temp.setSlotSerial(slot.getSerial());
			
			/*slot finished*/
			temp.setToiId(i.getId());
			if(measure_type == 1)
				temp.setPalletCarton("Palet");
			else
				temp.setPalletCarton("Carton");
			temp.setExpireDate(i.getExpirationDate());
			temp.setProdDate(i.getProductionDate());
			temp.setItemSerial(i.getSerial());
			temp.setSkuId(currentSKU);
			temp.setSkuDesc(dao.getEntityById(currentSKU, StockKeepingUnit.class, "id").getDescription());
			temp.setQualityStatus(CurrentQualityStatusiId);
			exeView.add(temp);
			
			counter+=2;
		}
		return true;
	}

	
	private	void generateRecieveItemListView() {		
		exeView.removeAll(exeView);
		for(Integer i :RecieveList){
			BinSlot slot = dao.getEntityById(i, BinSlot.class, "id");
			BinSlotConfiguration slotConfig = dao.getEntityById(slot.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
			
			/*Modified Now Easy ro roll back */
			ExecuteView temp = new ExecuteView();
			temp.setBinSymbol(groupConfig.getSymbol()+group.getBinNumber());
			temp.setSlotId(i);
			temp.setSlotSymbol(slot.getY()+"_"+slot.getZ());
			temp.setSlotSerial(slot.getSerial());
			
			/*slot finished*/
			exeView.add(temp);
		}
	}
	
	private boolean scanRecieveItemListForCartons() {		
		doublerecordes = dao.getAllEntities(SerialRecored.class);
		List<Palet_Slot_CartonCount_isnew> ReceiveCartonListCopy = new ArrayList<Palet_Slot_CartonCount_isnew>();
		// if scanning of serials is wrong
		System.out.println("item size = "+(items.size()+ReceiveCartonList.size()));
		System.out.println("recordes size = "+doublerecordes.size());
		if(doublerecordes.size() != items.size()+ReceiveCartonList.size()){
			DoneExecuteButton.setVisible(false);
			return false;
		}
		int count = 0;
		for(int i=0;i<ReceiveCartonList.size();i++){
			System.out.println(doublerecordes.get(i+count));//first slot
			SerialRecored slotRecored = doublerecordes.get(i+count);//first slot
			Integer slotid = Integer.parseInt(slotRecored.getSerial());
			
			Palet_Slot_CartonCount_isnew temp = ReceiveCartonListContains(slotid);
			if(temp == null){
				System.out.println("lolat");
				return false;
			}else{
				ReceiveCartonListCopy.add(temp);
				count += temp.getCartonCount();
			}
		}
		ReceiveCartonList = ReceiveCartonListCopy;
		exeView.removeAll(exeView);
		int CartonIndex=0;
		for(int j=0;j<ReceiveCartonList.size();j++){
			Palet_Slot_CartonCount_isnew R = ReceiveCartonList.get(j);
			
			BinSlot slot = dao.getEntityById(R.getSlot_Id(), BinSlot.class, "id");
			BinSlotConfiguration slotConfig = dao.getEntityById(slot.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
			for(int i=CartonIndex;i<CartonIndex+R.getCartonCount();i++){
					    //for each carton		   
			   SerialRecored recored = doublerecordes.get(i+j+1);
			   try {
				recored.getInfoFromSerial();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			   items.get(i).setSerial(recored.getSerial());// update item Serial column
			   items.get(i).setProductionDate(recored.getDate());// update item production date column
			   items.get(i).setExpirationDate(getExpirationdate(recored.getSku(),recored.getDate()));// update item production date column
				   
			   //Modified Now Easy ro roll back 
			   ExecuteView temp = new ExecuteView();
			   temp.setBinSymbol(groupConfig.getSymbol()+group.getBinNumber());
			   temp.setSlotId(slot.getId());
			   temp.setSlotSymbol(slot.getY()+"_"+slot.getZ());
			   temp.setSlotSerial(slot.getSerial());
							
			   //slot finished
			   temp.setToiId(items.get(CartonIndex).getId());
			   if(measure_type == 1)
				   temp.setPalletCarton("Palet");
			   else
				   temp.setPalletCarton("Carton");
					
			   temp.setExpireDate(items.get(i).getExpirationDate());
			   temp.setProdDate(items.get(i).getProductionDate());
			   temp.setItemSerial(items.get(i).getSerial());
			   temp.setSkuId(currentSKU);
			   temp.setSkuDesc(dao.getEntityById(currentSKU, StockKeepingUnit.class, "id").getDescription());
			   temp.setQualityStatus(CurrentQualityStatusiId);
			   exeView.add(temp);					   
			}
		   CartonIndex += R.getCartonCount();
		}
		return true;
	}
	
	private	void generateIssueItemListView() {				
		exeView.removeAll(exeView);
		int counter = 0;
		for(TransactionOrderItem i :items){
			
			BinSlot slot = dao.getEntityById(IssueList.get(counter), BinSlot.class, "id");
			BinSlotConfiguration slotConfig = dao.getEntityById(slot.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
			TransactionOrderItem tempItem = dao.getEntityById(i.getId(), TransactionOrderItem.class, "id");
			
			/*Modified Now Easy ro roll back */
			ExecuteView temp = new ExecuteView();
			temp.setBinSymbol(groupConfig.getSymbol()+group.getBinNumber());
			temp.setSlotId(IssueList.get(counter));
			temp.setSlotSymbol(slot.getY()+"_"+slot.getZ());			
			/*slot finished*/
			
			temp.setToiId(tempItem.getId());
			if(measure_type == 1)
				temp.setPalletCarton("Palet");
			else
				temp.setPalletCarton("Carton");
			temp.setExpireDate(tempItem.getExpirationDate());
			temp.setProdDate(tempItem.getProductionDate());
			temp.setItemSerial(tempItem.getSerial());
			temp.setSkuId(currentSKU);
			temp.setSkuDesc(dao.getEntityById(currentSKU, StockKeepingUnit.class, "id").getDescription());
			temp.setQualityStatus(CurrentQualityStatusiId);
			exeView.add(temp);
			counter++;
		}
	}

	private	void generateIssueItemListViewForCartons() {		
		exeView.removeAll(exeView);
		for(Map.Entry<Integer,Integer> entry : IssueCartonMap.entrySet()){
			
			BinSlot slot = dao.getEntityById(entry.getValue(), BinSlot.class, "id");
			BinSlotConfiguration slotConfig = dao.getEntityById(slot.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
			GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
			SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
						
			/*Modified Now Easy  roll back */
			ExecuteView temp = new ExecuteView();
			temp.setBinSymbol(groupConfig.getSymbol()+group.getBinNumber());
			temp.setSlotId(slot.getId());
			temp.setSlotSymbol(slot.getY()+"_"+slot.getZ());
			temp.setSlotSerial(slot.getSerial());
			/*slot finished*/
			TransactionOrderItem toi = dao.getEntityById(entry.getKey(), TransactionOrderItem.class, "id");
			
			temp.setToiId(toi.getId());
			if(measure_type == 1)
				temp.setPalletCarton("Palet");
			else
				temp.setPalletCarton("Carton");
			temp.setExpireDate(toi.getExpirationDate());
			temp.setProdDate(toi.getProductionDate());
			temp.setItemSerial(toi.getSerial());
			temp.setSkuId(currentSKU);
			temp.setSkuDesc(dao.getEntityById(currentSKU, StockKeepingUnit.class, "id").getDescription());
			temp.setQualityStatus(CurrentQualityStatusiId);
			exeView.add(temp);
		}
	}

	
	private void generateRecieveItemviewForCartons() {		
		exeView.removeAll(exeView);
		for(Palet_Slot_CartonCount_isnew R: ReceiveCartonList){
				BinSlot slot = dao.getEntityById(R.getSlot_Id(), BinSlot.class, "id");
				BinSlotConfiguration slotConfig = dao.getEntityById(slot.getBinSlotConfigurationId(), BinSlotConfiguration.class, "id");
				GroupBin group = dao.getEntityById(slotConfig.getGroupBinId(), GroupBin.class, "id");
				SiteConfigurationBinGroup groupConfig = dao.getEntityById(group.getSiteConfigurationBinGroupId(), SiteConfigurationBinGroup.class, "id");
				
				ExecuteView temp = new ExecuteView();
				temp.setBinSymbol(groupConfig.getSymbol()+group.getBinNumber());
				temp.setSlotId(slot.getId());
				temp.setSlotSymbol(slot.getY()+"_"+slot.getZ());
				temp.setNoOfCartons(R.getCartonCount());
				temp.setSlotSerial(slot.getSerial());
				exeView.add(temp);					   
		}
	}
	
	@SuppressWarnings("deprecation")
	public Date getExpirationdate(Integer sku_id ,Date prodDate){
		StockKeepingUnit sku = dao.getEntityById(sku_id , StockKeepingUnit.class, "id");
		
		Date expirationDate = new Date ();
		expirationDate.setYear(prodDate.getYear()+sku.getLifeTimeYears());
		expirationDate.setMonth(prodDate.getMonth()+sku.getLifeTimeMonths());
		expirationDate.setDate(prodDate.getDate()+sku.getLifeTimeDays());
		
		return expirationDate; 
	}
	
	public Integer GetCurrentBinIdFromSlotId(Integer SlotId) throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_SOT_ID", SlotId);

		try{
			List<Integer> CBI_ID = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_CBI_BY_SLOT_ID.queryName,
				parameters);
			
			return CBI_ID.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("lol");
		}		
	}
	
	public Integer GetCurrentBinIdFromItemId(Integer ItemId) throws DatabaseException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("P_itemId", ItemId);
		
		try{
			List<Integer> CBI_ID = dao.executeNamedQuery(Integer.class,
				QueryNamesEnum.GET_CURRENT_BIN_ITEM_OF_ITEM_Id.queryName,
				parameters);
			
			return CBI_ID.get(0);
		}catch(Exception e) {
			e.printStackTrace();
			throw new DatabaseException("lol");
		}		
	}
	
	private void ExecuteReceiveOrder(){
		for(Integer i=0;i<items.size();i++){
			CustomSession session = dao.openSession();
			try {
				session.beginTransaction();
				dao.updateEntity(items.get(i), session);//update operation
				session.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session.rollbackTransaction();
			} finally {
				session.close();
			}
			
			TransactionExecuteReceive ter = new TransactionExecuteReceive();
			ter.setSiteLaborId(CurrentSiteLaborId);
			ter.setTransactionOrderReceiveId(TransactionOrderReceiveIds.get(i).getId());
			
			CustomSession session2 = dao.openSession();
			try {
				session2.beginTransaction();
				dao.addEntity(ter, session2);
				System.out.println("new TransactionExecuteReceive added with id = "+ter.getId());
				session2.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session2.rollbackTransaction();
			} finally {
				session2.close();
			}
					   
			ReceiveItemBin rib = new ReceiveItemBin();
			rib.setTransactionExecuteReceiveId(ter.getId());
			rib.setExecutionTime(new Date());
			rib.setTransactionOrderItemId(items.get(i).getId());
			rib.setSlotId(RecieveList.get(i));
			rib.setSerial(items.get(i).getSerial());
			/*
			ItemStatus itemStatus = new ItemStatus();
			itemStatus.setDate(new Date());
			itemStatus.setQualityStatusId(QualityStatusIds.get(i));
			itemStatus.setNote(QualityStatusNotes.get(i));
			itemStatus.setTransactionOrderItemId(items.get(i).getId());
			*/
			
			CurrentBinItem cbi = new CurrentBinItem();
			cbi.setSiteId(SiteNo);
			cbi.setTransactionOrderItemSerial(items.get(i).getSerial());
			cbi.setItemId(items.get(i).getId());
			cbi.setOwnerId(CurrentOwnerId);
			cbi.setSlotId(RecieveList.get(i));
										   					   
			CustomSession session3 = dao.openSession();
			try {
				session3.beginTransaction();
				dao.addEntity(rib, session3);
				System.out.println("new ReceiveItemBin added with id = "+rib.getId());
				dao.addEntity(cbi, session3);
				System.out.println("new CurrentBinItem added with id = "+cbi.getId());
				dao.updateEntity(items.get(i), session3);
				session3.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
			    session3.rollbackTransaction();
				} finally {
				session3.close();
			}
		}
		RecieveList.clear();
		TransactionOrder tr = dao.getEntityById(CurrentOrderId, TransactionOrder.class, "id");
		tr.setExecuted(1);
		CustomSession session1 = dao.openSession();
		try {
			session1.beginTransaction();
			dao.updateEntity(tr, session1);
			dao.deleteEntities(doublerecordes, session1);//delete all records
			System.out.println("update TransactionOrder with id = "+tr.getId()+" , make it executed");
			session1.commitTransaction();
		} catch (DatabaseException e) {
		    e.printStackTrace();
			session1.rollbackTransaction();
		} finally {
			session1.close();
		}
	}
	
	
	private void ExecuteReceiveOrderCarton(){	
		int CartonIndex=0;
		for(Palet_Slot_CartonCount_isnew R: ReceiveCartonList){
		   if(R.isNew()){
			   //new pallet
			   CurrentBinItem cbi = new CurrentBinItem();
			   cbi.setSiteId(SiteNo);
			   cbi.setTransactionOrderItemSerial("noSerial");
			   cbi.setItemId(R.getPalet_Id());
			   cbi.setOwnerId(CurrentOwnerId);
			   cbi.setSlotId(R.getSlot_Id());
			   CustomSession session = dao.openSession();
			   try {
					session.beginTransaction();
					dao.addEntity(cbi, session);
					session.commitTransaction();
				} catch (DatabaseException e) {
				    e.printStackTrace();
					session.rollbackTransaction();
					} finally {
					session.close();
				}
			   	for(int i=CartonIndex;i<CartonIndex+R.getCartonCount();i++){
			   		CustomSession session55 = dao.openSession();
					try {
						session55.beginTransaction();
						dao.updateEntity(items.get(i), session55);//update operation
						session55.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session55.rollbackTransaction();
					} finally {
						session55.close();
					}

			   		//for each carton		   
				    TransactionExecuteReceive ter = new TransactionExecuteReceive();
				    ter.setSiteLaborId(CurrentSiteLaborId);
				    ter.setTransactionOrderReceiveId(TransactionOrderReceiveIds.get(i).getId());
				    CustomSession session2 = dao.openSession();
				    try {
						session2.beginTransaction();
						dao.addEntity(ter, session2);
						session2.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session2.rollbackTransaction();
					} finally {
						session2.close();
					}
					ReceiveItemBin rib = new ReceiveItemBin();
					rib.setTransactionExecuteReceiveId(ter.getId());
					rib.setExecutionTime(new Date());
					rib.setTransactionOrderItemId(items.get(i).getId());
					rib.setSlotId(R.getSlot_Id());
					rib.setSerial("tobedonelater");
								
					ItemStatus itemStatus = new ItemStatus();
					itemStatus.setDate(new Date());
					itemStatus.setQualityStatusId(exeView.get(i).getQualityStatus());
					itemStatus.setTransactionOrderItemId(items.get(i).getId());
					
					CurrentBinItem cbi1 = new CurrentBinItem();
					cbi1.setSiteId(SiteNo);
					cbi1.setTransactionOrderItemSerial("tobedonelater");
					cbi1.setItemId(items.get(i).getId());
					cbi1.setOwnerId(CurrentOwnerId);
					cbi1.setSlotId(R.getSlot_Id());
					
					CartonInPalet cip = new CartonInPalet();
					cip.setPaletId(R.getPalet_Id());
					cip.setCartonId(items.get(i).getId());
					
					CustomSession session11 = dao.openSession();
					try {
						session11.beginTransaction();
						dao.addEntity(ter, session11);
						dao.addEntity(rib, session11);
						dao.addEntity(cbi1, session11);
						dao.addEntity(cip, session11);
						dao.updateEntity(items.get(i), session11);
						session11.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session11.rollbackTransaction();
						} finally {
						session11.close();
					}
				}
			   	CartonIndex += R.getCartonCount();
		   }else{
			   	for(int i=CartonIndex;i<CartonIndex+R.getCartonCount();i++){
			   		CustomSession session44 = dao.openSession();
					try {
						session44.beginTransaction();
						dao.updateEntity(items.get(i), session44);//update operation
						session44.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session44.rollbackTransaction();
					} finally {
						session44.close();
					}
			   		
			   		
			   		//for each carton		   
				    TransactionExecuteReceive ter = new TransactionExecuteReceive();
				    ter.setSiteLaborId(CurrentSiteLaborId);
				    ter.setTransactionOrderReceiveId(TransactionOrderReceiveIds.get(i).getId());
				    CustomSession session2 = dao.openSession();
				    try {
						session2.beginTransaction();
						dao.addEntity(ter, session2);
						session2.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session2.rollbackTransaction();
					} finally {
						session2.close();
					}
					ReceiveItemBin rib = new ReceiveItemBin();
					rib.setTransactionExecuteReceiveId(ter.getId());
					rib.setExecutionTime(new Date());
					rib.setTransactionOrderItemId(items.get(i).getId());
					rib.setSlotId(R.getSlot_Id());
					rib.setSerial("tobedonelater");			
					
					ItemStatus itemStatus = new ItemStatus();
					itemStatus.setDate(new Date());
					itemStatus.setQualityStatusId(exeView.get(i).getQualityStatus());
					itemStatus.setTransactionOrderItemId(items.get(i).getId());
					
					CurrentBinItem cbi1 = new CurrentBinItem();
					cbi1.setSiteId(SiteNo);
					cbi1.setTransactionOrderItemSerial("tobedonelater");
					cbi1.setItemId(items.get(i).getId());
					cbi1.setOwnerId(CurrentOwnerId);
					cbi1.setSlotId(R.getSlot_Id());
					
					CartonInPalet cip = new CartonInPalet();
					cip.setPaletId(R.getPalet_Id());
					cip.setCartonId(items.get(i).getId());
					
					CustomSession session11 = dao.openSession();
					try {
						session11.beginTransaction();
						dao.addEntity(ter, session11);
						dao.addEntity(rib, session11);
						dao.addEntity(cbi1, session11);
						dao.addEntity(cip, session11);
						dao.updateEntity(items.get(i), session11);
						session11.commitTransaction();
					} catch (DatabaseException e) {
					    e.printStackTrace();
						session11.rollbackTransaction();
						} finally {
						session11.close();
					}
				}
			   	CartonIndex += R.getCartonCount();
		   }
		}
		ReceiveCartonList.clear();
		TransactionOrder tr = dao.getEntityById(CurrentOrderId, TransactionOrder.class, "id");
		tr.setExecuted(1);
		CustomSession session1 = dao.openSession();
		try {
			session1.beginTransaction();
			dao.updateEntity(tr, session1);
			dao.deleteEntities(doublerecordes, session1);//delete all records
			session1.commitTransaction();
		} catch (DatabaseException e) {
		    e.printStackTrace();
			session1.rollbackTransaction();
		} finally {
			session1.close();
		}
	}
	
	
	private void ExecuteIssueOrder(){
		for(Integer i=0;i<items.size();i++){		   
			TransactionExecuteIssue tei = new TransactionExecuteIssue();
			tei.setSiteLaborId(CurrentSiteLaborId);
			tei.setTransactionOrderIssueId(TransactionOrderIssueIds.get(i).getId());
			   
			CustomSession session2 = dao.openSession();
			try {
				session2.beginTransaction();
				dao.addEntity(tei, session2);
				dao.deleteEntities(doublerecordes, session2);
				session2.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session2.rollbackTransaction();
			} finally {
				session2.close();
			}
			   
			Integer cbiId;
			CurrentBinItem cbi;
			TransactionOrderItem TOI;
			try {
			   cbiId = GetCurrentBinIdFromSlotId(IssueList.get(i));
			   cbi = dao.getEntityById(cbiId, CurrentBinItem.class, "id");
			   TOI = dao.getEntityById(cbi.getItemId(), TransactionOrderItem.class, "id");
			   IssueItemBin iib = new IssueItemBin();
			   iib.setTransactionExecuteIssueId(tei.getId());
			   iib.setExecutionTime(new Date());
			   iib.setTransactionOrderItemId(TOI.getId());
			   iib.setSlotId(IssueList.get(i));
			   iib.setSerial(TOI.getSerial());
			   				   					   
			   CustomSession session = dao.openSession();
			   try {
				   session.beginTransaction();
					dao.addEntity(iib, session);
					dao.deleteEntity(cbi, session);
					session.commitTransaction();
			   } catch (DatabaseException e) {
				   	e.printStackTrace(); 
					session.rollbackTransaction();
			   } finally {
					session.close();
			   }
			}catch (DatabaseException e1) {
			   e1.printStackTrace();
			}
		}
		TransactionOrder tr = dao.getEntityById(CurrentOrderId, TransactionOrder.class, "id");
		tr.setExecuted(1);
		CustomSession session1 = dao.openSession();
		try {
			session1.beginTransaction();
			dao.updateEntity(tr, session1);
			session1.commitTransaction();
		} catch (DatabaseException e) {
		    e.printStackTrace();
			session1.rollbackTransaction();
		} finally {
			session1.close();
		}   
		
		IssueList.clear();
	}
	
	private void ExecuteIssueOrderCarton() throws DatabaseException {
		Integer Index = 0;
		Map<Integer,Integer> slots =  new TreeMap<Integer, Integer>();
		for(Map.Entry<Integer,Integer> entry : IssueCartonMap.entrySet()) {
			slots.put(entry.getValue(), entry.getKey());
			TransactionExecuteIssue tei = new TransactionExecuteIssue();
			tei.setSiteLaborId(CurrentSiteLaborId);
			tei.setTransactionOrderIssueId(TransactionOrderIssueIds.get(Index).getId());
			
			CustomSession session2 = dao.openSession();
			try {
				session2.beginTransaction();
				dao.addEntity(tei, session2);
				dao.deleteEntities(doublerecordes, session2);
				session2.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session2.rollbackTransaction();
			} finally {
				session2.close();
			}
			
			Integer cbiId;
			CurrentBinItem cbi;
			TransactionOrderItem TOI;
			try {
			   cbiId = GetCurrentBinIdFromItemId(entry.getKey());
			   cbi = dao.getEntityById(cbiId, CurrentBinItem.class, "id");
			   TOI = dao.getEntityById(cbi.getItemId(), TransactionOrderItem.class, "id");
			   IssueItemBin iib = new IssueItemBin();
			   iib.setTransactionExecuteIssueId(tei.getId());
			   iib.setExecutionTime(new Date());
			   iib.setTransactionOrderItemId(TOI.getId());
			   iib.setSlotId(entry.getValue());
			   iib.setSerial(TOI.getSerial());  						   
				   					   
			   CustomSession session = dao.openSession();
			   try {
				   session.beginTransaction();
					dao.addEntity(iib, session);
					dao.deleteEntity(cbi, session);
					session.commitTransaction();
			   } catch (DatabaseException e) {
				   	e.printStackTrace(); 
					session.rollbackTransaction();
			   } finally {
					session.close();
			   }
			}catch (DatabaseException e1) {
			   e1.printStackTrace();
			}
			Index++;
		}
		
		for(Map.Entry<Integer,Integer> entry : slots.entrySet()){
			Integer cbiId2;
			CurrentBinItem cbi2;
			System.out.println(mainService.Get_No_of_Cartons_in_slot(entry.getKey())+" carton");
			if(mainService.Get_No_of_Cartons_in_slot(entry.getKey()) == 0){
				cbiId2 = GetCurrentBinIdFromSlotId(entry.getKey());
				cbi2 = dao.getEntityById(cbiId2, CurrentBinItem.class, "id");
				CustomSession session3 = dao.openSession();
				   try {
					   session3.beginTransaction();
					   dao.deleteEntity(cbi2, session3);
					   session3.commitTransaction();
				   } catch (DatabaseException e) {
					   e.printStackTrace(); 
					   session3.rollbackTransaction();
				   } finally {
					   session3.close();
				   }
			}
		}		
		TransactionOrder tr = dao.getEntityById(CurrentOrderId, TransactionOrder.class, "id");
		tr.setExecuted(1);
		CustomSession session1 = dao.openSession();
		try {
			session1.beginTransaction();
			dao.updateEntity(tr, session1);
			session1.commitTransaction();
		} catch (DatabaseException e) {
		    e.printStackTrace();
			session1.rollbackTransaction();
		} finally {
			session1.close();
		}   
		IssueCartonMap.clear();
	}
	
	private void showTable (List <ExecuteView> ex){
		//Table toBeExec = new Table("ScanResults");
		
		BeanItemContainer<ExecuteView> beans = new BeanItemContainer<ExecuteView>(ExecuteView.class);
		beans.addAll(ex);		 
		view.setImmediate(true);
		view.commit();
		view.setContainerDataSource(beans);
		view.setVisibleColumns(new Object[] { "binSymbol",
				"slotSymbol", "slotSerial", "skuDesc", "palletCarton", "noOfCartons", "itemSerial", "qualityStatus", "prodDate","expireDate" });
		view.setColumnHeaders(new String[] {  this.getMessage(GasDiaryMessages.Bins),
				this.getMessage(GasDiaryMessages.slot), this.getMessage(GasDiaryMessages.SlotSerial), this.getMessage(GasDiaryMessages.SKU),
				this.getMessage(GasDiaryMessages.Type), this.getMessage(GasDiaryMessages.CartonNo), this.getMessage(GasDiaryMessages.ItemSerial), 
				this.getMessage(GasDiaryMessages.QualityStatus), this.getMessage(GasDiaryMessages.ProductionDate) ,this.getMessage(GasDiaryMessages.ExpirationDate)});

		view.setPageLength(beans.size()+1);
		view.setSizeFull(); 
		view.setSelectable(true);
		view.setDescription(this.getMessage(GasDiaryMessages.ClickonRowtoExcutetheOrder));
		view.setImmediate(true);
		view.commit();
		
		
	}
	
	private Palet_Slot_CartonCount_isnew ReceiveCartonListContains(int slotid){
		for(Palet_Slot_CartonCount_isnew R: ReceiveCartonList){
			System.out.println("slot attr="+slotid);
			System.out.println("slot"+R.getSlot_Id());
			if(R.getSlot_Id() == slotid) 
				return R;
		}
		return null;
	}
	
	private Integer getQulityId(String symbol){
		if (symbol.equals("Released")) {
			return 1;
		} else if (symbol.equals("Quantiled")) {
			return 2;
		}
		else if (symbol.equals("Blocked")) {
			return 3;
		}
		return 1;
	}
	
	private Integer getdefaultQulityId(Integer skuId){
		return dao.getEntityById(skuId, StockKeepingUnit.class, "id").getDefaultQualityStatus();
	}
	
	private	boolean scanMatchForIssuePalet() throws DatabaseException {		
		doublerecordes = dao.getAllEntities(SerialRecored.class);
		List<String> issueSerials = new ArrayList<String>(IssueList.size());
		Integer cbiId;
		CurrentBinItem cbi;
		TransactionOrderItem TOI;
		for(Integer i=0;i<items.size();i++){
			cbiId = GetCurrentBinIdFromSlotId(IssueList.get(i));
			cbi = dao.getEntityById(cbiId, CurrentBinItem.class, "id");
			TOI = dao.getEntityById(cbi.getItemId(), TransactionOrderItem.class, "id");
			issueSerials.add(TOI.getSerial());
		}
		// if scanning of serials is wrong
		System.out.println("item size = "+IssueList.size());
		System.out.println("recordes size = "+doublerecordes.size());
		if(doublerecordes.size() != (items.size())){
			DoneExecuteButton.setVisible(false);
			CustomSession session1 = dao.openSession();
			try {
				session1.beginTransaction();
				dao.deleteEntities(doublerecordes, session1);//delete all records	
				session1.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session1.rollbackTransaction();
			} finally {
				session1.close();
			}
			return false;//error in scanning				
		}
		
		for(int i=0;i<IssueList.size();i++){
			System.out.println(doublerecordes.get(i));
			SerialRecored itemSerial = doublerecordes.get(i);
			String slotSerial = itemSerial.getSerial();
			if(issueSerials.contains(slotSerial)){
			
			}else{
				CustomSession session1 = dao.openSession();
				try {
					session1.beginTransaction();
					dao.deleteEntities(doublerecordes, session1);//delete all records	
					session1.commitTransaction();
				} catch (DatabaseException e) {
				    e.printStackTrace();
					session1.rollbackTransaction();
				} finally {
					session1.close();
				}
					return false;//error in scanning
			}
		}
		Notification.show("تم المسح بنجاح",
				Type.HUMANIZED_MESSAGE);
	
		return true;
	}
	
	private	boolean scanMatchForIssueCarton() throws DatabaseException {		
		doublerecordes = dao.getAllEntities(SerialRecored.class);
		List<String> issueSerials = new ArrayList<String>(IssueList.size());
		TransactionOrderItem TOI;
		for(Map.Entry<Integer,Integer> entry : IssueCartonMap.entrySet()){
			TOI = dao.getEntityById(entry.getKey(), TransactionOrderItem.class, "id");
			issueSerials.add(TOI.getSerial());
		}
		// if scanning of serials is wrong
		System.out.println("item size = "+IssueList.size());
		System.out.println("recordes size = "+doublerecordes.size());
		if(doublerecordes.size() != (items.size())){
			DoneExecuteButton.setVisible(false);
			CustomSession session1 = dao.openSession();
			try {
				session1.beginTransaction();
				dao.deleteEntities(doublerecordes, session1);//delete all records	
				session1.commitTransaction();
			} catch (DatabaseException e) {
			    e.printStackTrace();
				session1.rollbackTransaction();
			} finally {
				session1.close();
			}
			return false;//error in scanning				
		}
		
		for(int i=0;i<IssueList.size();i++){
			System.out.println(doublerecordes.get(i));
			SerialRecored itemSerial = doublerecordes.get(i);
			String slotSerial = itemSerial.getSerial();
			if(issueSerials.contains(slotSerial)){
			
			}else{
				CustomSession session1 = dao.openSession();
				try {
					session1.beginTransaction();
					dao.deleteEntities(doublerecordes, session1);//delete all records	
					session1.commitTransaction();
				} catch (DatabaseException e) {
				    e.printStackTrace();
					session1.rollbackTransaction();
				} finally {
					session1.close();
				}
					return false;//error in scanning
			}
		}
		Notification.show("تم المسح بنجاح",
				Type.HUMANIZED_MESSAGE);
	
		return true;
	}
	
	@Override
    public void setLocale(Locale locale) {
        super.setLocale(locale);
        i18nBundle = ResourceBundle.getBundle(GasDiaryMessages.class.getName(), getLocale());
    }
    
    /** Returns the bundle for the current locale. */
    public ResourceBundle getBundle() {
        return i18nBundle;
    }
 
    /**
     * Returns a localized message from the resource bundle
     * with the current application locale.
     **/ 
    public String getMessage(String key) {
        return i18nBundle.getString(key);
    }
} 