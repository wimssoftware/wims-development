package com.robovics.wims.ui;


import java.io.Serializable;
import java.util.ListResourceBundle;

public class GasDiaryMessages extends ListResourceBundle implements Serializable {
    private static final long serialVersionUID = -1381196948880320212L;

    public static final String switchLanguage =generateId();
    public static final String ChooseSiteName = generateId();
    public static final String ChooseOneFromthefollowing = generateId();
    public static final String WarehouseAdmin = generateId();
    public static final String SiteAdmin = generateId();
    public static final String SiteOrders = generateId();
    public static final String AllSitesNames = generateId();
    public static final String back = generateId();
    public static final String Reports = generateId();
    public static final String LogOut = generateId();
    public static final String NewCont = generateId();
    public static final String  Delete = generateId();
    public static final String NewStaff = generateId();
    public static final String CancelKey = generateId();
    public static final String Save = generateId(); 
    public static final String NewContract = generateId();
    public static final String  Update = generateId();
    public static final String NewSite = generateId();
    public static final String NewBin = generateId();
    public static final String NewSKU = generateId();
    public static final String ConfigureBin = generateId();
    
    public static final String code = generateId();
    public static final String Description = generateId();
    public static final String MaxCarton = generateId();
    public static final String DefaultQualityStatus = generateId();
    public static final String StackingNumber = generateId();
    public static final String LifeTimeDays = generateId();  
    public static final String SKUWindowCaption = generateId();
    
    public static final String firstName = generateId();
    public static final String lastName = generateId();
    public static final String mobileNumber = generateId();
    public static final String Address = generateId();
    public static final String Type = generateId();
    public static final String ContactWindowCaption = generateId();
    
    public static final String SiteName = generateId();
    public static final String StartDate = generateId();
    public static final String EndDate = generateId();
    public static final String Username = generateId();
    public static final String Password = generateId();
    public static final String Email = generateId();
    public static final String privilege = generateId();
    public static final String StaffWindowCaption = generateId();
    
    public static final String Active = generateId();
    public static final String Date = generateId();
    public static final String symbols = generateId();
    public static final String Bins = generateId();
    public static final String serial = generateId();
    public static final String slot = generateId();
    public static final String ItemName = generateId();
    public static final String ItemCode = generateId();
    public static final String width = generateId();
    public static final String height = generateId();
    public static final String lenght = generateId();
    public static final String SiteAddress = generateId();
    public static final String ActiveORinActive = generateId();
    public static final String BinWindowCaption = generateId();
    public static final String BinName = generateId();
    public static final String BinNumber = generateId();
    public static final String Sites = generateId();
    public static final String ConfigurationDate = generateId();
    public static final String SiteWindowCaption = generateId();
    public static final String UpdateSite = generateId();
    public static final String UpdateBin = generateId();
   
    public static final String OwnerName = generateId();
    public static final String SKU = generateId();
    public static final String ContractWindwoCaption = generateId();
    public static final String ContractDate = generateId();
    
    public static final String ClearAll = generateId();
    public static final String Done = generateId();
    public static final String Quantity = generateId();
    public static final String Edit = generateId();
    public static final String customer = generateId();
    public static final String OrderDate = generateId();
    public static final String Setting = generateId();
    public static final String NotExecutedOrder = generateId();
    public static final String NewOrder = generateId();
    public static final String From = generateId();
    public static final String To = generateId();
    public static final String ProductionDate = generateId();
    public static final String TransactionType = generateId();
    public static final String BatchNo = generateId();
    public static final String TransactionDate = generateId();
    public static final String UnitOfMeasurement = generateId();
    public static final String CurrentIncubatedSlots = generateId();
    public static final String CurrentIncubatedPallets = generateId();
    public static final String CreateNewPallet = generateId();
    public static final String Scan = generateId();
    public static final String EnterMAnually = generateId();
    public static final String SlotSerial = generateId();
    public static final String CartonNo = generateId();
    public static final String ItemSerial = generateId();
    public static final String ExpirationDate = generateId();
    
    
    public static final String QualityStatus = generateId();
    
    public static final String refresh = generateId();
    public static final String siteAdminName = generateId();
    public static final String ClickonRowtoExcutetheOrder = generateId();
    public static final String FromBin = generateId();
    public static final String FromSlotlength = generateId();
    public static final String FromSlotHeight = generateId();
    public static final String ToBin = generateId();
    public static final String ToSlotlength = generateId();
    public static final String ToSlotHeight = generateId();
    
    public static final String next = generateId();
    public static final String EnterSerial = generateId();
    public static final String AddCarton = generateId();
    public static final String ChooseSlot = generateId();
    
    
    public static final String OkKey = generateId(); 
    
    public static final String Reset = generateId(); 
    
    // Application
    public static final String AppTitle = generateId();
    
    // LoginScreen
   
    public static final String Login = generateId();
    public static final String LoginButton = generateId();
    public static final String RegisterNewUser = generateId();
    public static final String ForgotPassword = generateId();
    public static final String InvalidUserOrPassword = generateId();
    public static final String InvalidUserOrPasswordLong = generateId();
    public static final String DemoUsernameHint = generateId();
    
    // UserEditor
    public static final String UserNameError = generateId();
    public static final String InvalidPasswordFormat = generateId();
    public static final String PasswordAgain = generateId();
    public static final String PasswordHint = generateId();
   
    public static final String InvalidEmail = generateId();
    
    // UserView
    public static final String MyCars = generateId();
    public static final String EditCar = generateId();
    public static final String NewCar = generateId();
    public static final String DeleteCar = generateId();
    public static final String CarDetails = generateId();
    
    // CarView
    public static final String Fills = generateId();
    public static final String NewFill = generateId();
    public static final String EditFill = generateId();
    public static final String DateTimeFormat = generateId();
    public static final String DateShort = generateId();
    public static final String Station = generateId();
    public static final String AmountShort = generateId();
    public static final String PartialFillShort = generateId();
    public static final String TotalPriceShort = generateId();
    public static final String MeterShort = generateId();
    public static final String TripMeterShort = generateId();
    public static final String TripResetShort = generateId();
    
    // CarEditor 
    public static final String RegisterNumber = generateId();
    public static final String RegisterNumberError = generateId();
    public static final String MustBeGiven = generateId();
    public static final String MustBeInteger = generateId();
    public static final String Manufacturer = generateId();
    public static final String Model = generateId();
    public static final String Year = generateId();
    
    // FillEditor
    public static final String DateAndTime = generateId();
    public static final String NotADate = generateId();
    public static final String TooOldDate = generateId();
    public static final String DateMustBeGiven = generateId();
    public static final String StationMustBeGiven = generateId();
    public static final String Amount = generateId();
    public static final String MustBeAmount = generateId();
    public static final String AmountMustBeGiven = generateId();
    public static final String TotalPrice = generateId();
    public static final String MustBePrice = generateId();
    public static final String TotalMustBeGiven = generateId();
    public static final String PartialFill = generateId();
    public static final String Meter = generateId();
    public static final String MustBeKmReading = generateId();
    public static final String TripMeter = generateId();
    public static final String MustBeKmTripReading = generateId();
    public static final String TripReset = generateId();
    public static final String Comment = generateId();
    public static final String CommentPrompt = generateId();
    
    // FillEditor: station editor
    public static final String NewStation = generateId();
    public static final String StationName = generateId();
    public static final String EmptyStationName = generateId();
    
    public static String generateId() {
        return new Integer(ids++).toString();
    }
    
    static int ids = 0;

    @Override
    public Object[][] getContents() {
        return null;
    }
}